-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2012-10-12 11:03:15
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table onevisio_system_tienda.carritos
DROP TABLE IF EXISTS `carritos`;
CREATE TABLE IF NOT EXISTS `carritos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `direccion_id` int(10) DEFAULT NULL,
  `total_price` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.carritos: ~2 rows (approximately)
/*!40000 ALTER TABLE `carritos` DISABLE KEYS */;
INSERT INTO `carritos` (`id`, `user_id`, `direccion_id`, `total_price`, `created`, `status`) VALUES
	(1, 1, NULL, 407, '2012-09-24 15:21:18', 1),
	(2, 1, NULL, 164, '2012-09-24 15:25:03', 1);
/*!40000 ALTER TABLE `carritos` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.detalleproductos
DROP TABLE IF EXISTS `detalleproductos`;
CREATE TABLE IF NOT EXISTS `detalleproductos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `carrito_id` int(10) DEFAULT NULL,
  `producto_id` int(10) DEFAULT NULL,
  `direccion_id` int(10) DEFAULT NULL,
  `codigo_referencia` varchar(100) DEFAULT NULL,
  `codigo_cupon` varchar(100) DEFAULT NULL,
  `codigo_validacion` varchar(100) DEFAULT NULL,
  `is_gift` int(10) DEFAULT NULL,
  `monto` int(10) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.detalleproductos: ~5 rows (approximately)
/*!40000 ALTER TABLE `detalleproductos` DISABLE KEYS */;
INSERT INTO `detalleproductos` (`id`, `carrito_id`, `producto_id`, `direccion_id`, `codigo_referencia`, `codigo_cupon`, `codigo_validacion`, `is_gift`, `monto`, `added`, `status`) VALUES
	(1, 1, 10, NULL, '5060c0be8cd32', '5060c0be8cd89', '5060c0be8cddc', 0, 383, '2012-09-24 15:21:18', 1),
	(2, 1, 9, NULL, '5060c0cac0bf2', '5060c0cac0c0c', '5060c0cac0c21', 0, 9, '2012-09-24 15:21:30', 1),
	(3, 1, 12, NULL, '5060c0d369672', '5060c0d369688', '5060c0d36969c', 0, 15, '2012-09-24 15:21:39', 1),
	(4, 2, 2, NULL, '5060c19f4b6c9', '5060c19f4b71d', '5060c19f4b776', 0, 64, '2012-09-24 15:25:03', 1),
	(5, 2, 1, NULL, '5060c1a722137', '5060c1a72214d', '5060c1a722161', 0, 100, '2012-09-24 15:25:11', 1);
/*!40000 ALTER TABLE `detalleproductos` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.direcciones
DROP TABLE IF EXISTS `direcciones`;
CREATE TABLE IF NOT EXISTS `direcciones` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `carrito_id` int(10) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `tipo_direccion` varchar(50) DEFAULT NULL,
  `codigo_postal` varchar(50) DEFAULT NULL,
  `empresa` varchar(50) DEFAULT NULL,
  `direccion` varchar(300) DEFAULT NULL,
  `poblacion` varchar(50) DEFAULT NULL,
  `provincia` varchar(150) DEFAULT NULL,
  `pais_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.direcciones: ~15 rows (approximately)
/*!40000 ALTER TABLE `direcciones` DISABLE KEYS */;
INSERT INTO `direcciones` (`id`, `user_id`, `carrito_id`, `nombre`, `apellidos`, `telefono`, `tipo_direccion`, `codigo_postal`, `empresa`, `direccion`, `poblacion`, `provincia`, `pais_id`) VALUES
	(1, 1, NULL, 'asdfasdf', 'asdfas', 'dfasdfasdf', 'Oficina', 'asdfasdf', 'sfasdf', 'asdf', 'asdfasdf', '1', -1),
	(2, 1, NULL, 'Gerson', 'Aduviri', '235362', 'Particular', '5154', 'QuickDev', 'Calle Consuelo 116', 'Arequipa', '1', -1),
	(3, 1, 21, 'Gerson', 'Aduviri', '235362', 'oficina', '5154', 'Quickdev', 'Juan Velasco Alvarado', 'Arequipa', '3', NULL),
	(4, NULL, NULL, 'asdf', 'asdf', 'asdf', '', 'asdf', 'asdf', 'asdf', 'Asdf', '0', -1),
	(5, NULL, NULL, 'Gerson', 'Aduviri', '65984521', '', '5154', 'Quickdev', 'Juan Velasco Alvarado', 'Arequipa', '3', -1),
	(6, 1, NULL, '', '', '', '', '', '', '', '', '0', NULL),
	(7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(12, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `direcciones` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.files
DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `filetype_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` tinytext,
  `url_path` varchar(350) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`filetype_id`,`user_id`),
  CONSTRAINT `FK_files_file_types` FOREIGN KEY (`filetype_id`) REFERENCES `filetypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.files: ~41 rows (approximately)
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` (`id`, `filetype_id`, `user_id`, `name`, `description`, `url_path`, `created`, `status`) VALUES
	(1, 1, 1, 'Cabeza', '', 'user_files/2012/05/4fc63432cc48d_1_4e6b9a9fd8285.png', NULL, 1),
	(2, 1, 1, 'Cabeza2', '', 'user_files/2012/05/4fc6343aae1c4_1_4e6b987dcba48.png', NULL, 1),
	(3, 1, 1, 'Cabeza3', '', 'user_files/2012/05/4fc63444058bb_1_4e6b9886321b9.png', NULL, 1),
	(11, 1, NULL, 'Novell', NULL, 'user_files/2012/07/50047d394c892_cabecera_backoffice.jpg', '2012-07-16 03:44:41', 1),
	(12, 1, NULL, 'nuevoo', NULL, 'user_files/2012/07/50047d92a0d6c_divercity_captura.JPG', '2012-07-16 03:46:10', 1),
	(13, 1, NULL, 'nuevoo', NULL, 'user_files/2012/07/50047d9aea51e_cabecera_backoffice.jpg', '2012-07-16 03:46:18', 1),
	(14, 1, NULL, 'nuevoo', NULL, 'user_files/2012/07/50047e0b71d4e_cabecera_backoffice.jpg', '2012-07-16 03:48:11', 1),
	(15, 1, NULL, 'nuevoossss', NULL, 'user_files/2012/07/50047e11b25fd_cabecera_backoffice.jpg', '2012-07-16 03:48:17', 1),
	(16, 1, NULL, 'nuevoossss', NULL, 'user_files/2012/07/50047e2307b41_cabecera_backoffice.jpg', '2012-07-16 03:48:35', 1),
	(18, 6, NULL, 'asdfa', NULL, 'user_files/sounds/2012/07/50049426396a7_Avatar.jpg', '2012-07-16 05:22:30', 1),
	(19, 6, NULL, 'asdfa', NULL, 'user_files/sounds/2012/07/5004944891f16_Avatar.jpg', '2012-07-16 05:23:04', 1),
	(20, 6, NULL, 'adsf', NULL, 'user_files/sounds/2012/07/5004964a567da_10 - Proyecto Ser.mp3', '2012-07-16 05:31:38', 1),
	(21, 6, NULL, 'asdfa', NULL, 'user_files/sounds/2012/07/5005d799b72e4_4fff65ba31983_fondo2.jpg', '2012-07-17 04:22:33', 1),
	(22, 6, NULL, 'asdfasdfa', NULL, 'user_files/sounds/2012/07/5005d7a829c53_4fff0ba0d4826_Chrysanthemum.jpg', '2012-07-17 04:22:48', 1),
	(23, 6, NULL, 'asdfasdfaasdf', NULL, 'user_files/sounds/2012/07/5005d7d131640_4fff0ba0d4826_Chrysanthemum.jpg', '2012-07-17 04:23:29', 1),
	(24, 6, NULL, 'asdfa', NULL, 'user_files/sounds/2012/07/500d8bdfe2d2c_theSimpsons.mp3', '2012-07-23 12:37:35', 1),
	(25, 5, 1, 'Fondos', NULL, 'user_files/2012/07/500f749951f00_Avatar.jpg', '2012-07-24 11:22:49', 1),
	(26, 1, NULL, '150', NULL, 'user_files/2012/07/500f77a8613d0_Avatar.jpg', '2012-07-24 11:35:52', 1),
	(27, 1, NULL, 'asdfasdf', NULL, 'user_files/2012/08/502059d592582_boton-compartir-facebook.png', '2012-08-06 06:57:09', NULL),
	(28, 1, NULL, 'dfasdfd', NULL, 'user_files/2012/08/50205a21ac4c0_linkedin.png', '2012-08-06 06:58:25', NULL),
	(29, 1, NULL, 'Login facebook', NULL, 'user_files/2012/08/50205ba311436_fb-login.png', '2012-08-06 07:04:51', NULL),
	(30, 1, NULL, 'Fondo app', NULL, 'user_files/2012/08/50205d3859492_fondo_app2.jpg', '2012-08-06 07:11:36', NULL),
	(31, 1, NULL, 'asdfasd', NULL, 'user_files/2012/10/506a12ea40c27_cabecera_backoffice.jpg', '2012-10-01 05:02:18', NULL),
	(33, 1, NULL, 'BCP', NULL, 'user_files/2012/10/506a177113758_divercity_captura.JPG', '2012-10-01 05:21:37', NULL),
	(34, 1, NULL, 'Locofaces', NULL, 'user_files/2012/10/506a1a4b8b954_locofaces_captura.JPG', '2012-10-01 05:33:47', NULL),
	(35, 1, NULL, 'Compras', NULL, 'user_files/2012/10/506a1d1509413_HomeTheater _334_23-07.PNG', '2012-10-01 05:45:41', NULL),
	(36, 1, NULL, 'aaaa', NULL, 'user_files/2012/10/506a1d27abd63_HomeTheater _334_23-07.PNG', '2012-10-01 05:45:59', NULL),
	(37, 1, NULL, 'aaaaasdfasdfasd asd fasd', NULL, 'user_files/2012/10/506a1d396195f_20_08_2012_F 815 - 5562.tif', '2012-10-01 05:46:17', NULL),
	(38, 1, NULL, 'Florr', NULL, 'user_files/2012/10/506a1d46090b2_Flor.jpg', '2012-10-01 05:46:30', NULL),
	(39, 1, NULL, 'Documeto', NULL, 'user_files/2012/10/506a1d63ca047_HomeTheater _334_23-07.PNG', '2012-10-01 05:46:59', NULL),
	(40, 1, NULL, 'Title', NULL, 'user_files/2012/10/506a1e284b29e_hometheater-33423-07png', '2012-10-01 05:50:16', NULL),
	(41, 1, NULL, 'Doccc', NULL, 'user_files/2012/10/506a2095aa830_hometheater-33423-07pngPNG', '2012-10-01 06:00:37', NULL),
	(42, 1, NULL, 'Icono', NULL, 'user_files/2012/10/506a21ed1c0a8_address64png.png', '2012-10-01 06:06:21', NULL),
	(43, 1, NULL, 'hhhh', NULL, 'user_files/2012/10/50761117b5eae_divercitycapturajpg.JPG', '2012-10-10 07:21:43', NULL),
	(44, 1, NULL, 'asdfasdf', NULL, 'user_files/2012/10/5076edbae3523_201209201147300jpg.jpg', '2012-10-11 11:03:06', NULL),
	(45, 1, NULL, 'jpg', NULL, 'user_files/2012/10/5076eefde9e09_20120920113549jpg.jpg', '2012-10-11 11:08:29', NULL),
	(46, 1, NULL, '20120920_114730(0).jpgjpg', NULL, 'user_files/2012/10/5076ef1c71833_201209201147300jpg.jpg', '2012-10-11 11:09:00', NULL),
	(47, 1, NULL, '20120920_113624.jpg', NULL, 'user_files/2012/10/5076faade6430_20120920113624jpg.jpg', '2012-10-11 11:58:21', NULL),
	(48, 1, NULL, '20120920_113624.jpg', NULL, 'user_files/2012/10/5076fab9eb03d_20120922141055jpg.jpg', '2012-10-11 11:58:33', NULL),
	(49, 1, NULL, '20120920_121052(1).jpg', NULL, 'user_files/2012/10/5076faf008764_201209201210521jpg.jpg', '2012-10-11 11:59:28', NULL),
	(50, 1, NULL, '20120918_212338_HDR.jpg', NULL, 'user_files/2012/10/5076fb04028ae_20120918212338hdrjpg.jpg', '2012-10-11 11:59:48', NULL);
/*!40000 ALTER TABLE `files` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.filetypes
DROP TABLE IF EXISTS `filetypes`;
CREATE TABLE IF NOT EXISTS `filetypes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `description` tinytext,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.filetypes: ~6 rows (approximately)
/*!40000 ALTER TABLE `filetypes` DISABLE KEYS */;
INSERT INTO `filetypes` (`id`, `name`, `code`, `description`, `status`) VALUES
	(1, 'Imagen', 'IMAGE', 'Imagenes generadas cargadas por el usuario', 1),
	(2, 'Video', 'VIDEO', 'Videos cargados o generados por el usuario', 1),
	(3, 'Sonido', 'SOUND', 'Sonidos o música cargada por el usuario', 1),
	(4, 'Documentos', 'DOCUMENTS', 'Documentos cargados por el usuario', 1),
	(5, 'Fondos', 'IMAGE', 'Fondo/background para el juego', 1),
	(6, 'Canciones', 'SOUND', 'Canciones', 1);
/*!40000 ALTER TABLE `filetypes` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.lineas
DROP TABLE IF EXISTS `lineas`;
CREATE TABLE IF NOT EXISTS `lineas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `code` varchar(200) DEFAULT NULL,
  `url_path` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.lineas: ~5 rows (approximately)
/*!40000 ALTER TABLE `lineas` DISABLE KEYS */;
INSERT INTO `lineas` (`id`, `name`, `code`, `url_path`, `created`, `status`) VALUES
	(1, 'OVL Offers', 'offers', '/offers/', '2012-08-09 17:15:54', 1),
	(2, 'OVL Outlet', 'outlet', '/outlet/', '2012-08-09 17:16:14', 1),
	(3, 'OVL Travel', 'travel', '/travel/', '2012-08-09 17:16:36', 1),
	(4, 'OVL Coaching', 'coaching', 'coaching/', '2012-08-09 17:16:48', 1),
	(5, 'Ofertas Permanentes', 'permanentes', 'permanentes/', '2012-09-10 18:11:26', 1);
/*!40000 ALTER TABLE `lineas` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.mails
DROP TABLE IF EXISTS `mails`;
CREATE TABLE IF NOT EXISTS `mails` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `from` varchar(250) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `to` varchar(250) DEFAULT NULL,
  `body` text,
  `subject` varchar(300) DEFAULT NULL,
  `date_send` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Almacena correo a enviar, limpiar antiguos o enviados';

-- Dumping data for table onevisio_system_tienda.mails: 4 rows
/*!40000 ALTER TABLE `mails` DISABLE KEYS */;
INSERT INTO `mails` (`id`, `from`, `from_name`, `to`, `body`, `subject`, `date_send`, `status`) VALUES
	(1, 'eduardo@qd.pe', 'Gerson', 'gerson@qd.pe', 'Contenido', 'Mensaje de prueba', '2012-06-05 09:56:27', 1),
	(2, 'eduardo@qd.pe', 'Gerson', 'gerson@qd.pe', 'Contenido', 'Mensaje de prueba', '2012-06-05 09:56:27', 1),
	(3, 'eduardo@qd.pe', 'Gerson', 'gerson@qd.pe', 'Contenido', 'Mensaje de prueba', '2012-06-05 09:56:27', 1),
	(4, 'eduardo@qd.pe', 'Gerson', 'gerson@qd.pe', 'Contenido', 'Mensaje de prueba', '2012-06-05 09:56:27', 1);
/*!40000 ALTER TABLE `mails` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.person
DROP TABLE IF EXISTS `person`;
CREATE TABLE IF NOT EXISTS `person` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `lastname` varchar(200) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `phone` int(10) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.person: 0 rows
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
/*!40000 ALTER TABLE `person` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.productofiles
DROP TABLE IF EXISTS `productofiles`;
CREATE TABLE IF NOT EXISTS `productofiles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `producto_id` int(10) NOT NULL,
  `file_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_productos_files_files` (`file_id`),
  KEY `producto_id` (`producto_id`),
  CONSTRAINT `FK_productos_files_files` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`),
  CONSTRAINT `FK_productos_files_productos` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.productofiles: ~3 rows (approximately)
/*!40000 ALTER TABLE `productofiles` DISABLE KEYS */;
INSERT INTO `productofiles` (`id`, `producto_id`, `file_id`) VALUES
	(3, 18, 50),
	(4, 18, 46),
	(5, 18, 47);
/*!40000 ALTER TABLE `productofiles` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.productos
DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `tienda_id` int(10) DEFAULT NULL,
  `comercio_nombre` varchar(250) NOT NULL,
  `comercio_direccion` varchar(250) NOT NULL,
  `comercio_descripcion` varchar(250) NOT NULL,
  `comercio_logo_id` int(11) NOT NULL,
  `oferta_titulo` text NOT NULL,
  `oferta_tipo` varchar(250) NOT NULL,
  `oferta_que_incluye` text NOT NULL,
  `oferta_destacamos` text NOT NULL,
  `oferta_ubicacion_lon` varchar(600) NOT NULL,
  `oferta_ubicacion_lat` varchar(600) NOT NULL,
  `oferta_imagen_id` int(11) NOT NULL,
  `oferta_descripcion` text NOT NULL,
  `oferta_condiciones` text NOT NULL,
  `oferta_detalles` text NOT NULL,
  `oferta_comision_ovl` float NOT NULL DEFAULT '-1',
  `oferta_descuento` float NOT NULL,
  `oferta_precio_venta` float NOT NULL,
  `oferta_duracion` datetime DEFAULT NULL,
  `oferta_publicacion` datetime DEFAULT NULL,
  `file_id` int(11) NOT NULL,
  `tiempo_validez` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `status` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.productos: ~17 rows (approximately)
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` (`id`, `user_id`, `tienda_id`, `comercio_nombre`, `comercio_direccion`, `comercio_descripcion`, `comercio_logo_id`, `oferta_titulo`, `oferta_tipo`, `oferta_que_incluye`, `oferta_destacamos`, `oferta_ubicacion_lon`, `oferta_ubicacion_lat`, `oferta_imagen_id`, `oferta_descripcion`, `oferta_condiciones`, `oferta_detalles`, `oferta_comision_ovl`, `oferta_descuento`, `oferta_precio_venta`, `oferta_duracion`, `oferta_publicacion`, `file_id`, `tiempo_validez`, `created`, `status`) VALUES
	(1, 1, 1, 'D\'leos', '<p>\n	Calle Soledad #5412</p>\n', '<p>\n	Las mejores ofertas de toda Espa&ntilde;a</p>\n', 25, 'Apex 32\'\' Class 720p 60Hz LCD HDTV - Black (LD3288M)', 'cupon', '<p>\n	Screen Size: 32.0 &quot; Video Resolution: HD - 720p Maximum Resolution: 1366 x 768 Television Features: Sleep Timer, Parental Control, A/V Connects to Home Theaters, Favorite Channels, Auto Channel Programming, Closed Caption on Mute, Picture Freeze, Digital Combo Filter Mounting Features: Detachable Base Stand, VESA Mount, Wall Mountable Display Features: Closed Caption, On-Screen Display, Channel Display, High Resolution Panel, Diagonal LCD Widescreen, HD Display Electronic Functions: Digital Tuner, Built-in Speakers Screen Refresh Rate: 60 Hz Response Time: 8ms Response Time Contrast Ratio: 5000:1 Video Upconversion: 720p Comb Filter Type: 3D Y/C Digital Tuner Type: ATSC HD Compatibility: ATSC/NTSC Aspect Ratio: 16:9 Widescreen Brightness(cd/m2): 400 Audio Features: Broadcast Stereo, Stereo Speakers, Digital Noise Reduction, Selectable Sound Modes, Surround Sound, MTS, SAP Speaker Type: Down-Firing Number of Speakers: 2 Input Type: S-Video, VGA, Component Video, HDMI Port, Composite Video, RF Antenna Input Number of HDMI Inputs: 3 Number of Component Inputs: 2 Number of Composite Inputs: 2 Number of S-Video Inputs: 1 Number of PC Inputs: 1 Output Types: Digital Audio Finish: Glossy Includes: Detachable Base Stand, Power Cord, Remote Control Batteries, Quick Start Guide, User Manual Not Included: High Definition Antenna Dimensions with Stand: 21.9 &quot; H x 31.1 &quot; W x 9.0 &quot; D</p>\n', 'Desctacamos contenido"', '<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.es/?ie=UTF8&ll=40.396764,-3.713379&spn=11.45591,23.269043&t=w&z=6&output=embed"></iframe><br /><small><a href="https://maps.google.es/?ie=UTF8&ll=40.396764,-3.713379&spn=11.45591,23.269043&t=w&z=6&source=embed" style="color:#0000FF;text-align:left">Ver mapa más grande</a></small>', '', 25, '<p>\n	Update your media room with this LCD HDTV from Apex. The three HDMI inputs allow flexible connectivity options, so you can enjoy your favorite movies, games and TV shows in high-definition. It comes with a sturdy base, but it can also be mounted on a wall.</p>\n', '<p>\n	Condiciones</p>\n', 'dDetalles de oferta', -1, 50, 200, '2013-01-10 01:01:01', '2012-09-01 00:00:00', 1, '2013-01-10 01:01:01', '2012-10-11 06:55:23', '1'),
	(2, 1, 1, 'comercio 002', 'dirección comercio 002', 'comercio descripcion 002', 26, 'TItulo de la oferta 002', 'producto', 'Incluye todo', 'Desctacamos', 'http://google.com', '', 25, 'Descripcion de la oferta', 'Condiciones', 'Detalles de oferta', -1, 9, 70, '2012-06-30 00:00:00', '2012-10-02 00:00:00', 0, '2012-07-30 17:42:32', '2012-07-30 17:42:48', NULL),
	(4, 1, 5, 'Comercio de Travel', 'Dirección del comercio	', '<p>\n	Descripci&oacute;n del comercio</p>\n', 27, 'Oferta para travel', 'cupon', '<p>\n	Que incluye la oferta</p>\n', 'Destacamos', 'Ubicación', '', 30, '<p>\n	Descripci&oacute;n de la oferta</p>\n', '<p>\n	Condiciones</p>\n', '', -1, 20, 1000, '1969-12-31 01:01:01', '2012-10-02 00:00:00', 0, '1969-12-31 01:01:01', '2012-08-31 06:18:14', '1'),
	(5, 1, 5, 'Comercio de Travel', 'Dirección del comercio	', 'Descripción del comercio	', 29, 'Oferta para travel', 'cupon', 'Que incluye la oferta	', 'Destacamos', 'Ubicación', '', 30, 'Descripción de la oferta	', 'Condiciones', '', -1, 10, 0, '2012-08-29 07:22:05', '2012-10-02 00:00:00', 0, '2012-08-29 07:22:05', '2012-08-29 07:22:05', '1'),
	(6, 1, 5, 'Comercio de Travel', 'Dirección del comercio	', 'Descripción del comercio	', 29, 'Oferta para travel', 'cupon', 'Que incluye la oferta	', 'Destacamos', 'Ubicación', '', 30, 'Descripción de la oferta	', 'Condiciones', '', -1, 10, 0, '2012-08-29 07:22:15', '2012-10-02 00:00:00', 0, '2012-08-29 07:22:15', '2012-08-29 07:22:15', '1'),
	(7, 1, 5, 'Comercio de Travel', 'Dirección del comercio	', 'Descripción del comercio	', 29, 'Oferta para travel', 'cupon', 'Que incluye la oferta	', 'Destacamos', 'Ubicación', '', 30, 'Descripción de la oferta	', 'Condiciones', '', -1, 10, 0, '2012-08-29 07:22:20', '2012-09-01 00:00:00', 0, '2012-08-29 07:22:20', '2012-08-29 07:22:20', '1'),
	(8, 1, 5, 'Comercio de Travel', 'Dirección del comercio	', 'Descripción del comercio	', 29, 'Oferta para travel', 'cupon', 'Que incluye la oferta	', 'Destacamos', 'Ubicación', '', 30, 'Descripción de la oferta	', 'Condiciones', '', -1, 10, 0, '2012-08-29 07:23:12', '2012-10-02 00:00:00', 0, '2012-08-29 07:23:12', '2012-08-29 07:23:12', '1'),
	(9, 1, 1, 'Tiendas ELE', 'Direcció ele', '<p>\n	Descripci&oacute;n del comercio de ele</p>\n', 28, 'Oferta numero 03', 'producto', '<p>\n	sdfasdf</p>\n', 'Destacamos las measdf', 'asdfasdf', '', 0, '<p>\n	asdfa</p>\n', '<p>\n	sadsfasd</p>\n', '', -1, 10, 10, '2012-09-20 01:01:01', '2012-10-02 00:00:00', 0, '2012-09-13 01:01:01', '2012-09-04 05:11:16', '1'),
	(10, 1, 3, 'D\'leos', '<p>\n	Calle del comercio numero 250 la ciudad</p>\n', '<p>\n	Comerci&oacute;n con a&ntilde;os de trabajo en el rubro de las entregas</p>\n', 25, 'Oferta que se publica mañana', 'producto', '<p>\n	La oferta incluye cosas como las que tenemos aqui</p>\n<p>\n	as df asd fasd</p>\n', 'Desctacamos los mejores precios', 'as dfasd fasd fasdf ', '', 30, '<p>\n	La oferta incluye cosas como las que tenemos aqui</p>\n<p>\n	as df asd fasd</p>\n', '<p>\n	La oferta incluye cosas como las que tenemos aqui</p>\n<p>\n	De tener m&aacute;s condiciones se ver&aacute; as&iacute;.</p>\n<p>\n	Por ejemplo un listado de cosas</p>\n<ul>\n	<li>\n		Item1</li>\n	<li>\n		Item2</li>\n	<li>\n		Item3</li>\n</ul>\n<p>\n	Aqui debajo tienen m&aacute;s cosas</p>\n', '', -1, 15, 450, '2012-12-06 01:01:01', '2012-10-02 00:00:00', 0, '2012-09-10 01:01:01', '2012-10-11 07:13:18', '1'),
	(11, 1, 7, 'Comercio permanentes', '<p>\n	Ubicaci&oacute;n del comercio</p>\n<ul>\n	<li>\n		Lista1</li>\n	<li>\n		Lista2</li>\n</ul>\n', '<p>\n	Descripci&oacute;n del comercio</p>\n', 29, 'Oferta', 'producto', '<p>\n	Que incluye</p>\n', 'Desctacamos', '<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.es/?ie=UTF8&amp;ll=41.381204,2.166746&amp;spn=0.022057,0.045447&amp;t=w&amp;z=15&amp;output=embed"></iframe><br /><small><a href="https://maps.google.es/?ie=UTF8&amp;ll=41.381204,2.166746&amp;spn=0.022057,0.045447&amp;t=w&amp;z=15&amp;source=embed" style="color:#0000FF;text-align:left">Ver mapa más grande</a></small>', '', 25, '<p>\n	sdf asd fasdf asd</p>\n', '<p>\n	f asd fasdf asd&nbsp;</p>\n', '', -1, 20, 550, '2012-09-12 01:01:01', NULL, 0, '2012-09-28 01:01:01', '2012-09-10 05:32:35', '1'),
	(12, 1, 6, 'asasdf ', '<p>\n	a asd f</p>\n', '<p>\n	asd asdf</p>\n', 27, 'a sdf', 'producto', '<p>\n	sd fasd&nbsp;</p>\n', 'dfasd fa', 'aas dfasd', '', 29, '<p>\n	fasd fas</p>\n', '<p>\n	a sdf</p>\n', '', -1, 50, 30, '1969-12-31 01:01:01', '2012-10-01 18:15:52', 0, '1969-12-31 01:01:01', '2012-09-10 06:02:50', '1'),
	(13, 1, 1, 'asdf', '<p>\n	asdf</p>\n', '<p>\n	asdf</p>\n', 0, 'asdfasd', 'cupon', '<p>\n	asdfasdf</p>\n', 'asdfasdf', 'asdfas', '', 49, '<p>\n	asdf</p>\n', '<p>\n	asdfa</p>\n', '', -1, 12, 12, '1969-12-31 01:01:01', '1969-12-31 00:00:00', 0, '1969-12-31 01:01:01', '2012-10-11 06:05:08', '1'),
	(14, 1, 1, 'asdf', '<p>\n	asdf</p>\n', '<p>\n	asdf</p>\n', 0, 'asdfasd', 'cupon', '<p>\n	asdfasdf</p>\n', 'asdfasdf', 'asdfas', '', 49, '<p>\n	asdf</p>\n', '<p>\n	asdfa</p>\n', '', -1, 12, 12, '1969-12-31 01:01:01', '1969-12-31 00:00:00', 0, '1969-12-31 01:01:01', '2012-10-11 06:14:00', '1'),
	(15, 1, 1, 'asdf', '<p>\n	asdf</p>\n', '<p>\n	asdf</p>\n', 0, 'asdfasd', 'cupon', '<p>\n	asdfasdf</p>\n', 'asdfasdf', 'asdfas', '', 49, '<p>\n	asdf</p>\n', '<p>\n	asdfa</p>\n', '', -1, 12, 12, '1969-12-31 01:01:01', '1969-12-31 00:00:00', 0, '1969-12-31 01:01:01', '2012-10-11 06:14:31', '1'),
	(16, 1, 1, 'asdf', '<p>\n	asdf</p>\n', '<p>\n	asdf</p>\n', 0, 'asdfasd', 'cupon', '<p>\n	asdfasdf</p>\n', 'asdfasdf', 'asdfas', '', 49, '<p>\n	asdf</p>\n', '<p>\n	asdfa</p>\n', '', -1, 12, 12, '1969-12-31 01:01:01', '1969-12-31 00:00:00', 0, '1969-12-31 01:01:01', '2012-10-11 06:14:47', '1'),
	(17, 1, 1, 'asdf', '<p>\n	asdf</p>\n', '<p>\n	asdf</p>\n', 0, 'asdfasd', 'cupon', '<p>\n	asdfasdf</p>\n', 'asdfasdf', 'asdfas', '', 49, '<p>\n	asdf</p>\n', '<p>\n	asdfa</p>\n', '', -1, 12, 12, '1969-12-31 01:01:01', '1969-12-31 00:00:00', 0, '1969-12-31 01:01:01', '2012-10-11 06:15:54', '1'),
	(18, 1, 1, 'asdf', '<p>\n	asdf</p>\n', '<p>\n	asdf</p>\n', 0, 'asdfasd', 'cupon', '<p>\n	asdfasdf</p>\n', 'asdfasdf', 'asdfas', '', 49, '<p>\n	asdf</p>\n', '<p>\n	asdfa</p>\n', '', -1, 12, 12, '1969-12-31 01:01:01', '1969-12-31 00:00:00', 0, '1969-12-31 01:01:01', '2012-10-11 06:41:56', '1');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.rankings
DROP TABLE IF EXISTS `rankings`;
CREATE TABLE IF NOT EXISTS `rankings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `min` int(10) DEFAULT NULL,
  `max` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`file_id`),
  CONSTRAINT `FK_ranking` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table onevisio_system_tienda.rankings: ~0 rows (approximately)
/*!40000 ALTER TABLE `rankings` DISABLE KEYS */;
/*!40000 ALTER TABLE `rankings` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.roles: ~3 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `description`) VALUES
	(1, 'login', 'Login privileges, granted after account confirmation'),
	(2, 'admin', 'Administrative user, has access to everything.'),
	(3, 'api_client', 'Remote App Client');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.roles_rules
DROP TABLE IF EXISTS `roles_rules`;
CREATE TABLE IF NOT EXISTS `roles_rules` (
  `rol_id` int(10) NOT NULL AUTO_INCREMENT,
  `rule_id` int(10) NOT NULL,
  PRIMARY KEY (`rol_id`,`rule_id`),
  KEY `FK_roles_rules_rules` (`rule_id`),
  CONSTRAINT `FK_roles_rules_rules` FOREIGN KEY (`rule_id`) REFERENCES `rules` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.roles_rules: ~0 rows (approximately)
/*!40000 ALTER TABLE `roles_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_rules` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.roles_users
DROP TABLE IF EXISTS `roles_users`;
CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`),
  CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.roles_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
	(1, 1),
	(1, 2);
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.rules
DROP TABLE IF EXISTS `rules`;
CREATE TABLE IF NOT EXISTS `rules` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.rules: ~9 rows (approximately)
/*!40000 ALTER TABLE `rules` DISABLE KEYS */;
INSERT INTO `rules` (`id`, `code`, `description`) VALUES
	(1, 'ADD_USER', 'Permite agregar un usuario desde el administrador'),
	(2, 'EDIT_USER', 'Permite editar los usuario desde un aministrador'),
	(3, 'DELETE_USER', 'Permite eliminar los usuario desde un aministrador'),
	(4, 'ADD_ROLES', 'Permite agregar roles desde el administrador'),
	(5, 'EDIT_ROLES', 'Permite editar roles'),
	(6, 'DELETE_ROLES', 'Permite eliminar roles'),
	(7, 'ASIGN_ROLES_TO_USER', 'Asignar roles a usuarios'),
	(8, 'ASIGN_RULES_TO_ROLES', 'Asignar reglas a roles'),
	(9, 'ACCESS_ADMIN', 'Ingresar a admin');
/*!40000 ALTER TABLE `rules` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.sections
DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.sections: 0 rows
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.tiendas
DROP TABLE IF EXISTS `tiendas`;
CREATE TABLE IF NOT EXISTS `tiendas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `linea_id` int(10) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` text,
  `order` int(10) DEFAULT NULL,
  `comision_ovl` float DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.tiendas: ~7 rows (approximately)
/*!40000 ALTER TABLE `tiendas` DISABLE KEYS */;
INSERT INTO `tiendas` (`id`, `linea_id`, `code`, `name`, `description`, `order`, `comision_ovl`, `created`, `status`) VALUES
	(1, 1, 'oferta_dia', 'Ofertas del día', 'Ofertas el día', 1, 10, '2012-08-09 17:20:25', 1),
	(2, 1, 'planes_dia', 'Planes del día', 'Planes del día', 1, 15, '2012-08-09 17:21:09', 1),
	(3, 2, 'iten_out', 'Tienda Out', NULL, 1, 10, '2012-08-18 11:08:51', 1),
	(4, 2, 'segunda_ti', 'Segunda tienda', NULL, 2, 10, '2012-08-29 04:08:52', 1),
	(5, 3, 'viajes', 'Viajes', NULL, 1, 10, '2012-08-29 04:08:58', 1),
	(6, 4, 'cursos', 'Cursos', NULL, 1, 10, '2012-08-29 04:08:33', 1),
	(7, 5, 'permanente', 'Ofertas permanentes', NULL, 100, 10, '2012-09-10 05:09:51', 1);
/*!40000 ALTER TABLE `tiendas` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(127) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) DEFAULT '',
  `last_name` varchar(60) DEFAULT '',
  `distrito` varchar(50) DEFAULT '',
  `puntaje` int(10) DEFAULT NULL,
  `sexo` varchar(20) DEFAULT '',
  `telefono` varchar(32) DEFAULT '',
  `fecha_nacimiento` datetime DEFAULT NULL,
  `dni` int(8) DEFAULT NULL,
  `password` char(64) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  `reset_token` char(64) NOT NULL DEFAULT '',
  `status` varchar(20) NOT NULL DEFAULT '',
  `facebook_user_id` bigint(20) NOT NULL,
  `last_failed_login` datetime NOT NULL,
  `failed_login_count` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `username`, `name`, `last_name`, `distrito`, `puntaje`, `sexo`, `telefono`, `fecha_nacimiento`, `dni`, `password`, `logins`, `last_login`, `reset_token`, `status`, `facebook_user_id`, `last_failed_login`, `failed_login_count`, `created`, `modified`) VALUES
	(1, 'gerson@qd.pe', 'admin', '', '', '', NULL, '', '', NULL, NULL, 'f2a233d912774ab8aa3ff5faac501d27f40374ce74e8b127eb432015c72056d3', 53, 1349900386, '', '', 0, '2012-07-12 13:04:34', 0, '2012-05-23 12:32:55', '2012-10-10 15:19:46');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.user_identities
DROP TABLE IF EXISTS `user_identities`;
CREATE TABLE IF NOT EXISTS `user_identities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `identity` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table onevisio_system_tienda.user_identities: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_identities` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_identities` ENABLE KEYS */;


-- Dumping structure for table onevisio_system_tienda.user_tokens
DROP TABLE IF EXISTS `user_tokens`;
CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.user_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
