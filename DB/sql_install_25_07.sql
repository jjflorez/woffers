-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2012-07-25 09:36:26
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table db_modatereta.banners
DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) DEFAULT NULL,
  `name` varchar(350) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table db_modatereta.banners: ~3 rows (approximately)
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` (`id`, `code`, `name`, `created`, `status`) VALUES
	(1, 'asdfasdf', 'baner', '2012-07-24 22:48:08', 1),
	(2, 'asdfasdf', 'Fondos', '2012-07-24 11:54:57', 1),
	(3, 'asdf asdfasdfasdf', 'Fondos', '2012-07-24 11:55:00', 1);
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.canciones
DROP TABLE IF EXISTS `canciones`;
CREATE TABLE IF NOT EXISTS `canciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `name` varchar(350) DEFAULT NULL,
  `artist` varchar(250) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_modatereta.canciones: ~2 rows (approximately)
/*!40000 ALTER TABLE `canciones` DISABLE KEYS */;
INSERT INTO `canciones` (`id`, `file_id`, `name`, `artist`, `created`, `status`) VALUES
	(4, 20, 'adsf', 'asdfasdf', '2012-07-16 05:31:38', 1),
	(8, 24, 'asdfa', 'sdafasdf', '2012-07-23 12:37:35', 1);
/*!40000 ALTER TABLE `canciones` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.files
DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `filetype_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` tinytext,
  `url_path` varchar(350) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`filetype_id`,`user_id`),
  CONSTRAINT `FK_files_file_types` FOREIGN KEY (`filetype_id`) REFERENCES `filetypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.files: ~18 rows (approximately)
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` (`id`, `filetype_id`, `user_id`, `name`, `description`, `url_path`, `created`, `status`) VALUES
	(1, 1, 1, 'Cabeza', '', 'user_files/2012/05/4fc63432cc48d_1_4e6b9a9fd8285.png', NULL, 1),
	(2, 1, 1, 'Cabeza2', '', 'user_files/2012/05/4fc6343aae1c4_1_4e6b987dcba48.png', NULL, 1),
	(3, 1, 1, 'Cabeza3', '', 'user_files/2012/05/4fc63444058bb_1_4e6b9886321b9.png', NULL, 1),
	(11, 1, NULL, 'Novell', NULL, 'user_files/2012/07/50047d394c892_cabecera_backoffice.jpg', '2012-07-16 03:44:41', 1),
	(12, 1, NULL, 'nuevoo', NULL, 'user_files/2012/07/50047d92a0d6c_divercity_captura.JPG', '2012-07-16 03:46:10', 1),
	(13, 1, NULL, 'nuevoo', NULL, 'user_files/2012/07/50047d9aea51e_cabecera_backoffice.jpg', '2012-07-16 03:46:18', 1),
	(14, 1, NULL, 'nuevoo', NULL, 'user_files/2012/07/50047e0b71d4e_cabecera_backoffice.jpg', '2012-07-16 03:48:11', 1),
	(15, 1, NULL, 'nuevoossss', NULL, 'user_files/2012/07/50047e11b25fd_cabecera_backoffice.jpg', '2012-07-16 03:48:17', 1),
	(16, 1, NULL, 'nuevoossss', NULL, 'user_files/2012/07/50047e2307b41_cabecera_backoffice.jpg', '2012-07-16 03:48:35', 1),
	(18, 6, NULL, 'asdfa', NULL, 'user_files/sounds/2012/07/50049426396a7_Avatar.jpg', '2012-07-16 05:22:30', 1),
	(19, 6, NULL, 'asdfa', NULL, 'user_files/sounds/2012/07/5004944891f16_Avatar.jpg', '2012-07-16 05:23:04', 1),
	(20, 6, NULL, 'adsf', NULL, 'user_files/sounds/2012/07/5004964a567da_10 - Proyecto Ser.mp3', '2012-07-16 05:31:38', 1),
	(21, 6, NULL, 'asdfa', NULL, 'user_files/sounds/2012/07/5005d799b72e4_4fff65ba31983_fondo2.jpg', '2012-07-17 04:22:33', 1),
	(22, 6, NULL, 'asdfasdfa', NULL, 'user_files/sounds/2012/07/5005d7a829c53_4fff0ba0d4826_Chrysanthemum.jpg', '2012-07-17 04:22:48', 1),
	(23, 6, NULL, 'asdfasdfaasdf', NULL, 'user_files/sounds/2012/07/5005d7d131640_4fff0ba0d4826_Chrysanthemum.jpg', '2012-07-17 04:23:29', 1),
	(24, 6, NULL, 'asdfa', NULL, 'user_files/sounds/2012/07/500d8bdfe2d2c_theSimpsons.mp3', '2012-07-23 12:37:35', 1),
	(25, 5, 1, 'Fondos', NULL, 'user_files/2012/07/500f749951f00_Avatar.jpg', '2012-07-24 11:22:49', 1),
	(26, 1, NULL, '150', NULL, 'user_files/2012/07/500f77a8613d0_Avatar.jpg', '2012-07-24 11:35:52', 1);
/*!40000 ALTER TABLE `files` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.filetypes
DROP TABLE IF EXISTS `filetypes`;
CREATE TABLE IF NOT EXISTS `filetypes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `description` tinytext,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.filetypes: ~6 rows (approximately)
/*!40000 ALTER TABLE `filetypes` DISABLE KEYS */;
INSERT INTO `filetypes` (`id`, `name`, `code`, `description`, `status`) VALUES
	(1, 'Imagen', 'IMAGE', 'Imagenes generadas cargadas por el usuario', 1),
	(2, 'Video', 'VIDEO', 'Videos cargados o generados por el usuario', 1),
	(3, 'Sonido', 'SOUND', 'Sonidos o música cargada por el usuario', 1),
	(4, 'Documentos', 'DOCUMENTS', 'Documentos cargados por el usuario', 1),
	(5, 'Fondos', 'IMAGE', 'Fondo/background para el juego', 1),
	(6, 'Canciones', 'SOUND', 'Canciones', 1);
/*!40000 ALTER TABLE `filetypes` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.mails
DROP TABLE IF EXISTS `mails`;
CREATE TABLE IF NOT EXISTS `mails` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `from` varchar(250) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `to` varchar(250) DEFAULT NULL,
  `body` text,
  `subject` varchar(300) DEFAULT NULL,
  `date_send` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Almacena correo a enviar, limpiar antiguos o enviados';

-- Dumping data for table db_modatereta.mails: 4 rows
/*!40000 ALTER TABLE `mails` DISABLE KEYS */;
INSERT INTO `mails` (`id`, `from`, `from_name`, `to`, `body`, `subject`, `date_send`, `status`) VALUES
	(1, 'eduardo@qd.pe', 'Gerson', 'gerson@qd.pe', 'Contenido', 'Mensaje de prueba', '2012-06-05 09:56:27', 1),
	(2, 'eduardo@qd.pe', 'Gerson', 'gerson@qd.pe', 'Contenido', 'Mensaje de prueba', '2012-06-05 09:56:27', 1),
	(3, 'eduardo@qd.pe', 'Gerson', 'gerson@qd.pe', 'Contenido', 'Mensaje de prueba', '2012-06-05 09:56:27', 1),
	(4, 'eduardo@qd.pe', 'Gerson', 'gerson@qd.pe', 'Contenido', 'Mensaje de prueba', '2012-06-05 09:56:27', 1);
/*!40000 ALTER TABLE `mails` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.paises
DROP TABLE IF EXISTS `paises`;
CREATE TABLE IF NOT EXISTS `paises` (
  `id` int(10) DEFAULT NULL,
  `nombre` int(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.paises: 0 rows
/*!40000 ALTER TABLE `paises` DISABLE KEYS */;
/*!40000 ALTER TABLE `paises` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.person
DROP TABLE IF EXISTS `person`;
CREATE TABLE IF NOT EXISTS `person` (
  `id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `lastname` varchar(200) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `phone` int(10) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.person: 0 rows
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
/*!40000 ALTER TABLE `person` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.rankings
DROP TABLE IF EXISTS `rankings`;
CREATE TABLE IF NOT EXISTS `rankings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `min` int(10) DEFAULT NULL,
  `max` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`file_id`),
  CONSTRAINT `FK_ranking` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_modatereta.rankings: ~2 rows (approximately)
/*!40000 ALTER TABLE `rankings` DISABLE KEYS */;
INSERT INTO `rankings` (`id`, `file_id`, `name`, `description`, `min`, `max`, `created`, `status`) VALUES
	(5, 16, 'nuevoossss', NULL, NULL, NULL, '2012-07-16 03:48:35', 1),
	(6, 26, '150', NULL, 0, 50, '2012-07-24 11:35:52', 1);
/*!40000 ALTER TABLE `rankings` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.roles: ~3 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `description`) VALUES
	(1, 'login', 'Login privileges, granted after account confirmation'),
	(2, 'admin', 'Administrative user, has access to everything.'),
	(3, 'api_client', 'Remote App Client');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.roles_rules
DROP TABLE IF EXISTS `roles_rules`;
CREATE TABLE IF NOT EXISTS `roles_rules` (
  `rol_id` int(10) NOT NULL AUTO_INCREMENT,
  `rule_id` int(10) NOT NULL,
  PRIMARY KEY (`rol_id`,`rule_id`),
  KEY `FK_roles_rules_rules` (`rule_id`),
  CONSTRAINT `FK_roles_rules_rules` FOREIGN KEY (`rule_id`) REFERENCES `rules` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.roles_rules: ~0 rows (approximately)
/*!40000 ALTER TABLE `roles_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_rules` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.roles_users
DROP TABLE IF EXISTS `roles_users`;
CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`),
  CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.roles_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
	(1, 1),
	(1, 2);
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.rules
DROP TABLE IF EXISTS `rules`;
CREATE TABLE IF NOT EXISTS `rules` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.rules: ~9 rows (approximately)
/*!40000 ALTER TABLE `rules` DISABLE KEYS */;
INSERT INTO `rules` (`id`, `code`, `description`) VALUES
	(1, 'ADD_USER', 'Permite agregar un usuario desde el administrador'),
	(2, 'EDIT_USER', 'Permite editar los usuario desde un aministrador'),
	(3, 'DELETE_USER', 'Permite eliminar los usuario desde un aministrador'),
	(4, 'ADD_ROLES', 'Permite agregar roles desde el administrador'),
	(5, 'EDIT_ROLES', 'Permite editar roles'),
	(6, 'DELETE_ROLES', 'Permite eliminar roles'),
	(7, 'ASIGN_ROLES_TO_USER', 'Asignar roles a usuarios'),
	(8, 'ASIGN_RULES_TO_ROLES', 'Asignar reglas a roles'),
	(9, 'ACCESS_ADMIN', 'Ingresar a admin');
/*!40000 ALTER TABLE `rules` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.sections
DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.sections: 0 rows
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(127) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) DEFAULT '',
  `last_name` varchar(60) DEFAULT '',
  `distrito` varchar(50) DEFAULT '',
  `puntaje` int(10) DEFAULT NULL,
  `sexo` varchar(20) DEFAULT '',
  `telefono` varchar(32) DEFAULT '',
  `fecha_nacimiento` datetime DEFAULT NULL,
  `dni` int(8) DEFAULT NULL,
  `password` char(64) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  `reset_token` char(64) NOT NULL DEFAULT '',
  `status` varchar(20) NOT NULL DEFAULT '',
  `facebook_user_id` bigint(20) NOT NULL,
  `last_failed_login` datetime NOT NULL,
  `failed_login_count` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `username`, `name`, `last_name`, `distrito`, `puntaje`, `sexo`, `telefono`, `fecha_nacimiento`, `dni`, `password`, `logins`, `last_login`, `reset_token`, `status`, `facebook_user_id`, `last_failed_login`, `failed_login_count`, `created`, `modified`) VALUES
	(1, 'gerson@qd.pe', 'admin', '', '', '', NULL, '', '', NULL, NULL, 'f2a233d912774ab8aa3ff5faac501d27f40374ce74e8b127eb432015c72056d3', 25, 1342548725, '', '', 0, '2012-07-12 13:04:34', 0, '2012-05-23 12:32:55', '2012-07-17 13:12:05'),
	(2, 'gersonm17@gmail.com', 'gersonm17', 'Gerson', 'Aduviri', '', NULL, '', '', NULL, NULL, 'e1042644e5ca452fd0240b208f5cef72bd2f1e726fdd10bc4e315040cb26260f', 66, 1338488557, '', '', 520644593, '0000-00-00 00:00:00', 0, '2012-05-31 13:20:12', '2012-05-31 13:22:37');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.uservideos
DROP TABLE IF EXISTS `uservideos`;
CREATE TABLE IF NOT EXISTS `uservideos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL DEFAULT '0',
  `user_id` int(10) DEFAULT NULL,
  `video_id` int(10) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL,
  `url_preview` varchar(300) DEFAULT NULL,
  `pos_heads` varchar(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `video_id` (`video_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.uservideos: ~11 rows (approximately)
/*!40000 ALTER TABLE `uservideos` DISABLE KEYS */;
INSERT INTO `uservideos` (`id`, `code`, `user_id`, `video_id`, `name`, `url_preview`, `pos_heads`, `created`, `status`) VALUES
	(1, 'V3005_4fc65d94ac150', 1, 4, NULL, 'user_files/2012/05/previews/4fc65d94a6447.jpg', '3,2,1', '2012-06-05 07:08:25', NULL),
	(2, 'V3005_4fc6a79cc44b8', 1, 4, NULL, 'user_files/2012/05/previews/4fc6a79cc3d69.jpg', '3,1,2', NULL, NULL),
	(3, 'V3005_4fc6a7b7ba219', 1, 4, NULL, 'user_files/2012/05/previews/4fc6a7b7b9b02.jpg', '3,1,2', NULL, NULL),
	(4, 'V3005_4fc6bcaf13711', 1, 4, NULL, 'user_files/2012/05/previews/4fc6bcaf12fa7.jpg', '3,2,1', NULL, NULL),
	(5, 'V0406_4fcd46d0da64f', 1, 2, NULL, 'user_files/2012/06/previews/4fcd46d0d3ac6.jpg', '2,1,3', NULL, NULL),
	(6, 'V0506_4fce332d6e3ae', 1, 2, NULL, 'user_files/2012/06/previews/4fce332d6d9a3.jpg', '3,2,1', NULL, NULL),
	(7, 'V0506_4fce34f76ae4e', 1, 2, NULL, 'user_files/2012/06/previews/4fce34f76889a.jpg', '3,2,1', NULL, NULL),
	(8, 'V0506_4fce351c496cc', 1, 2, NULL, 'user_files/2012/06/previews/4fce351c42df6.jpg', '3,2,1', NULL, NULL),
	(9, 'V0506_4fce35348b1ec', 1, 2, NULL, 'user_files/2012/06/previews/4fce35348a88d.jpg', '3,2,1', NULL, NULL),
	(10, 'V0506_4fce35591d576', 1, 2, NULL, 'user_files/2012/06/previews/4fce35591cb3c.jpg', '3,2,1', NULL, NULL),
	(11, 'V0506_4fce919a9a178', 1, 2, NULL, 'user_files/2012/06/previews/4fce919a96f5b.jpg', '3,2,1', '2012-06-05 06:09:14', NULL);
/*!40000 ALTER TABLE `uservideos` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.user_identities
DROP TABLE IF EXISTS `user_identities`;
CREATE TABLE IF NOT EXISTS `user_identities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `identity` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_modatereta.user_identities: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_identities` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_identities` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.user_tokens
DROP TABLE IF EXISTS `user_tokens`;
CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.user_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;


-- Dumping structure for table db_modatereta.videos
DROP TABLE IF EXISTS `videos`;
CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `code` varchar(150) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `url_path` varchar(500) DEFAULT '0',
  `type` varchar(50) DEFAULT '0',
  `num_heads` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table db_modatereta.videos: 8 rows
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` (`id`, `name`, `code`, `description`, `url_path`, `type`, `num_heads`, `created`, `status`) VALUES
	(1, 'El baile del perrito', 'baile_perrito', 'El baile del perrito willfrido vargaz', 'public/videos/perrito', 'LF_VIDEO', 4, '2012-05-11 16:44:37', 1),
	(2, 'Hawaii', 'hawaii', 'Bailando en Hawai', 'public/videos/hawaii', 'LF_VIDEO', 3, '2012-05-11 16:44:37', 1),
	(3, 'Bad Romance', 'bad_romance', 'Lady Gaga!!', 'public/videos/bad_romance', 'LF_VIDEO', 3, '2012-05-11 16:44:37', 1),
	(4, 'Chip', 'chip', 'Baile entre amigos', 'public/videos/chip', 'LF_VIDEO', 3, '2012-05-11 16:44:37', 1),
	(5, 'Chip II', 'chip2', 'Solo para ellas', 'public/videos/chip2', 'LF_VIDEO', 4, '2012-05-11 16:44:37', 1),
	(6, 'Danza Kuduro', 'danza_kuduro', 'Don Omar', 'public/videos/danza_kuduro', 'LF_VIDEO', 3, '2012-05-11 16:44:37', 1),
	(7, 'Hips Don\'t Lie', 'hips_dont_lie', 'Shakira', 'public/videos/hips_dont_lie', 'LF_VIDEO', 3, '2012-05-11 16:44:37', 1),
	(8, 'Pitbull', 'pitbull', 'Pitbull', 'public/videos/pitbull', 'LF_VIDEO', 3, '2012-05-11 16:44:37', 1);
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
