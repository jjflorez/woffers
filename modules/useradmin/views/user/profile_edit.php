<div style="margin-top: 20px;">
	<ul class="nav nav-tabs">
		<li><a href="<?php echo URL::site('user/profile'); ?>">Perfil</a> </li>
		<li class="active"><a href="<?php echo URL::site('user/profile_edit'); ?>">Editar perfil</a> </li>
		<li><a href="<?php echo URL::site('user/unregister'); ?>">Eliminar cuenta</a> </li>
	</ul>
	<div class="tab-content">
		<!-- tab content -->


		<?php
			$form = new Appform();
			if(isset($errors)) {
			   $form->errors = $errors;
			}
			if(isset($data)) {
			   unset($data['password']);
			   $form->values = $data;
			}
			echo $form->open('user/profile_edit');
			?>
			<h2><?php echo __('edit.profile'); ?></h2>
				<?php echo View::factory('user/user_edit_form')->set(array('form' => $form)); ?>

				<h2><?php echo __('roles'); ?></h2>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th><?php echo __('role'); ?></th><th><?php echo __('description'); ?></th>
						</tr>
					</thead>
					<?php
  						$i = 0;
						foreach($user_roles as $role => $description) {
							echo '<tr';
							if($i % 2 == 0) {
							   echo ' class="odd"';
							}
							echo '>';
							echo '<td>'.ucfirst($role).'</td><td>'.$description.'</td>';
							echo '</tr>';
							$i++;
						}
					?>
				</table>
		   <br>
		<button type="submit" class="btn btn-primary"><?php echo __('save'); ?></button>
		<?php
		echo $form->close();
		?>

		<!-- end tab content -->
	</div>
</div>