
<div style="margin-top: 20px;">
	<ul class="nav nav-tabs">
		<li><a href="<?php echo URL::site('admin_user'); ?>"><?php echo __('administer.users'); ?></a> </li>
		<li><a href="<?php echo URL::site('admin_user/edit'); ?>"><?php echo __('?add.user'); ?></a> </li>
		<li class="active"><a href="<?php echo URL::site('admin_user/roles'); ?>"><?php echo __('administer.roles'); ?></a> </li>
	</ul>
	<div class="tab-content">
		<!-- tab content -->

		<div class="row">
			<div class="span3">
				<div style="border-right: 1px solid #CCC; padding-right: 20px;">
					<div class="btn-group" style="margin-bottom: 10px;">
						<a href="#" class="btn btn-small"><span class="icon icon-plus-sign"></span> Agregar</a>
						<a href="#" class="btn btn-small"><span class="icon icon-trash"></span> Eliminar Roles</a>
					</div>
					<ul id="lista_roles" class="nav nav-pills nav-stacked">
					<?php
						foreach ($roles as $rol) {
								echo '<li id="' . $rol->id . '">
								<a href="javascript:void(0);" title="' . $rol->description . '"><span class="icon-chevron-right pull-right"></span> ' . $rol->name . '</a></li>';
							}
						?>
					</ul>
				</div>
			</div>
			<div class="span9">
				<div class="pull-right">
					<a href="#" id="btn_edit_rol" class="btn btn-small"><span class="icon icon-edit"></span> Editar rol activo</a>
					<a href="#" id="btn_save_rol" class="btn btn-small" style="display: none;"><span class="icon icon-check"></span> Guardar cambios</a>
				</div>
				<h3>Permisos para <span class="nombre_rol to_edit" style="font-style: italic;"></span></h3>
				<p class="info_rol to_edit">Selecciona un rol de la izquierda para editarlo</p>
				<table id="lista_reglas" class="table table-striped">
					<thead>
						<tr class="breadcrumb">
							<th width="20"><input type="checkbox" class="inline" /></th>
							<th>Permiso
								<form action="index.php" id="form_busqueda_roles" class="pull-right form-inline" style="margin-bottom: 0px;">
									<input id="input_busqueda" type="text" style="height: 10px;" /> <button type="submit" class="btn btn-mini">Buscar</button>
								</form>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr><td colspan="2">Cargando permisos...</td></tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


	<script type="text/javascript">
		$(document).ready(initPage);

		function initPage()
		{
			$('#lista_roles a').tooltip({placement:'right'});
			$('#lista_roles a').click(CLICK_LIST_handler);
			$('#btn_edit_rol').click(BTN_CLICK_handler);
			$('#btn_save_rol').click(BTN_CLICK_handler);

			$('#lista_roles a:eq(0)').click();
			$('#form_busqueda_roles').submit(FORM_SUBMIT_handler);
			$('#input_busqueda').keyup(FORM_SUBMIT_handler);
			loadRules();
		}

		function FORM_SUBMIT_handler()
		{
			loadRules(1, 10, $('#input_busqueda').val());
			return false;
		}

		function BTN_CLICK_handler()
		{
			switch($(this).attr('id'))
			{
				case 'btn_edit_rol':
					$('.to_edit').attr('contentEditable', true).addClass('editable');
					$(this).hide();
					$('#btn_save_rol').show();
					break;
				case 'btn_save_rol':
					$('.to_edit').attr('contentEditable', false).removeClass('editable');
					$(this).hide();
					$('#btn_edit_rol').show();
					break;
			}
		}

		function CLICK_LIST_handler()
		{
			$(this).parent().parent().find('li').removeClass('active');
			$(this).parent().addClass('active');
			$(this).parent().parent().find('span').removeClass('icon-white');
			$(this).find('span').addClass('icon-white');
			$('.nombre_rol').text($(this).text());
			$('.info_rol').text($(this).attr('data-original-title'));
		}

		function loadRules(_page, _cantidad, _busqueda)
		{
			var params = new Object();
			
			if(_busqueda) params.busqueda = _busqueda;
			if(_cantidad) params.num_items = _cantidad;
			if(_page) params.page = _page;

			$('#form_busqueda_roles').ajaxStop();
			qd.core.LoadData.load(qd.URLs.API_ROLES_RULES, params, function(res)
			{
				$('#lista_reglas tbody').html('');
				for(d in res.data)
				{
					var htmlReglas = '<tr id="' + res.data[d].id + '">';
					htmlReglas += '<td><input type="checkbox" class="inline"  /></td>';
					htmlReglas += '<td><div class="label label-success pull-right">' + res.data[d].code + '</div>' + res.data[d].description + '</td>';
					htmlReglas += '</tr>';
					$('#lista_reglas tbody').append(htmlReglas);
				}
			});
		}
	</script>