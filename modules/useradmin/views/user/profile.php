<div style="margin-top: 20px;">
	<ul class="nav nav-tabs">
		<li class="active"><a href="<?php echo URL::site('user/profile'); ?>">Perfil</a> </li>
		<li><a href="<?php echo URL::site('user/profile_edit'); ?>">Editar perfil</a> </li>
		<li><a href="<?php echo URL::site('user/unregister'); ?>">Eliminar cuenta</a> </li>
	</ul>
	<div class="tab-content">
		<!-- tab content -->


			<h1><?php echo __('user.profile') ?></h1>
			<p class="intro"><?php echo __('your.user.information'); ?>, <?php echo $user->username ?>.</p>

			<h2><?php echo __('username') ?> &amp; <?php echo __('email') ?> </h2>
			<p><?php echo $user->username ?> &mdash; <?php echo $user->email ?></p>

			<h2><?php echo __('login.activity') ?> </h2>
			<p>Last login was <?php echo date('F jS, Y', $user->last_login) ?>, at <?php echo date('h:i:s a', $user->last_login) ?>.<br/><?php echo __('total.nbr.logins') ?> <?php echo $user->logins ?></p>

			<?php
				$providers = array_filter(Kohana::$config->load('useradmin.providers'));
				$identities = $user->user_identities->find_all();
				if($identities->count() > 0) {
					echo '<h2>'.__('accounts.associated.with.profile').'</h2><p>';
					foreach($identities as $identity) {
					echo '<a class="associated_account" style="background: #FFF url(/img/small/'.$identity->provider.'.png) no-repeat center center"></a>';
					unset($providers[$identity->provider]);
					}
					echo '<br style="clear: both;"></p>';
				}
				if(!empty($providers)) {
					echo '<h2>'.__('additional.accountproviders').'</h2><p>'.__('click.provider.icon.to.associate.account').'</p><p>';
					foreach($providers as $provider => $enabled) {
						echo '<a class="associated_account '.$provider.'" href="'.URL::site('/user/associate/'.$provider).'"></a>';
					}
					echo '<br style="clear: both;"></p>';
				}
			?>

		<!-- end tab content -->
	</div>
</div>