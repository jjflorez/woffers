<div style="margin-top: 20px;">
    <ul class="nav nav-tabs">
        <li><a id="login" title="Iniciar sesión" href="<?php echo URL::site('user/login'); ?>">Iniciar sesión</a></li>
        <li class="active"><a href="<?php echo URL::site('user/register'); ?>">Registro</a></li>
        <li><a href="<?php echo URL::site('user/forgot'); ?>">Recuperar contraseña</a></li>
    </ul>
    <div class="tab-content">
        <!-- tab content -->

        <h3><?php echo __('register'); ?></h3>

        <p><?php echo __('?already.have.account') . ' ' . Html::anchor('user/login', __('login.here')); ?></p>

        <p><?php echo __('enter.info.to.register'); ?></p>

        <?php
        $form = new Appform();
        if (isset($errors)) {
            $form->errors = $errors;
        }
        if (isset($defaults)) {
            $form->defaults = $defaults;
        } else {
            unset($_POST['password']);
            unset($_POST['password_confirmation']);
            $form->defaults = $_POST;
        }
        echo $form->open('user/register', array('id' => 'form_register'));
        echo View::factory('user/user_edit_form')->set(array('form' => $form));

        if (isset($captcha_enabled) && $captcha_enabled) {
            echo $recaptcha_html;
        }
        ?>

        <p style="text-align: center;">
            <button type="submit" class="btn btn-primary btn-large"><?php echo __('register.account'); ?></button>
        </p>

        <?php                echo $form->close();            ?>

        <!-- end tab content -->
    </div>
</div>

<!-- JS for this page -->

<script type="text/javascript">
    $(document).ready(initPage);

    function initPage() {
        $('#form_register input').tooltip({trigger:'focus', placement:'right'});

        $fb = <?php if (isset($_GET['fb'])) {
            echo $_GET['fb'];
        } else {
            echo 0;
        }  ?>;

        console.log("$fb");
        console.log($fb);

        if ($fb ==1) {
            console.log("$fb = 1");

            /*
            FB.init({
                appId      : '336811903060970',
                status     : true, // check login status
                cookie     : true, // enable cookies to allow the server to access the session
                xfbml      : true  // parse XFBML
            });

            console.log("FB.init done!");

            FB.login(function (response) {
                if (response.authResponse) {
                    console.log('Welcome!  Fetching your information.... ');
                    FB.api('/me', function (response) {
                        console.log("All Data");
                        console.log(response);
                        console.log('Good to see you, ' + response.name + '.');
                        $('#name').val(response.name);
                    });
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }
            }, {scope: 'about_me, user_birthday, email,'});
            */

            FBLC.login_with_perms(FBLC.perms.default_required, 'Unknown Login Source', function(){
                $user = FBLC.user_info;
                console.log("User Info:");
                console.log($user);
            });

        }
    }

</script>