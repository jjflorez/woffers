<div style="margin-top: 20px;">
	<ul class="nav nav-tabs">
		<li><a id="login" title="Iniciar sesión" href="<?php echo URL::site('user/login'); ?>" >Iniciar sesión</a> </li>
		<li><a href="<?php echo URL::site('user/register'); ?>">Registro</a> </li>
		<li class="active"><a href="<?php echo URL::site('user/forgot'); ?>">Recuperar contraseña</a> </li>
	</ul>
	<div class="tab-content">
		<!-- tab content -->
		<h3><?php echo __('?forgot.password.or.username'); ?></h3>
		<p><?php echo __('send.password.resetlink'); ?></p>

		<form id="form_forgot" class="form-inline" action="<?php echo URL::site('user/forgot'); ?>" method="post" accept-charset="utf-8">
			<label for="reset_email"><?php echo __('email.address'); ?>:</label>
			<input type="text" name="reset_email" id="reset_email" class="" />
			<button type="submit" class="btn btn-primary"><?php echo __('reset.password'); ?></php></button>
		</form>
		<!-- end tab content -->
	</div>
</div>

<!-- JS for this page -->

<script type="text/javascript">
	$(document).ready(initPage);

	function initPage()
	{
		$('#form_forgot').submit(SEND_FORM_handler);
	}

	function SEND_FORM_handler()
	{
		if($('#reset_email').val() == '')
		{
			$('#reset_email').tooltip({trigger:'manual', title:'Por favor ingresa tu dirección de e-mail', placement:'bottom'});
			$('#reset_email').tooltip('show');
			return false;
		}
	}
</script>