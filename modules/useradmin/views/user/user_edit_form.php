<div style="display: inline-block; width: 250px; float: left; background-color: #ffffff;">
    <label>Nombres</label>
    <?php echo $form->input('name', '', array('id' => 'name')); ?>
    <label>Apellidos</label>
    <?php echo $form->input('last_name', '', array('id' => 'last_name')); ?>
    <label>Distrito</label>
    <?php echo $form->input('distrito', '', array('id' => 'distrito')); ?>
    <label>Telefono</label>
    <?php echo $form->input('telefono', '', array('id' => 'telefono')); ?>

    <label><?php echo __('email.address'); ?></label>
    <?php echo $form->input('email', '', array('id' => 'email')); ?>

    <label>Sexo</label>
    <input type="radio" name="sexo" value="H" class="rbsexo" checked="checked">Hombre
    <input type="radio" name="sexo" value="F" class="rbsexo">Mujer

</div>

<div style="display: inline-block; width: 250px; background-color: #ffffff;">
    <label><?php echo __('username'); ?></label>
    <?php echo $form->input('username', null, array('title' => __('allowed.usernames'))); ?>

    <label><?php echo __('password'); ?></label>
    <?php echo $form->password('password', null, array('title' => __('allowed.passwords'))) ?>
    <label><?php echo __('retype.password'); ?></label>
    <?php echo $form->password('password_confirm') ?>

    <label>Fecha Nacimiento</label>
    <?php echo $form->input('fecha_nacimiento', '', array('id' => 'fecha_nacimiento')); ?>

    <label>DNI</label>
    <?php echo $form->input('dni', '', array('id' => 'dni')); ?>
</div>
<script type="text/javascript">
    // <![CDATA[
    $(document).ready(function () {
        console.log("dp");
        var opts = {
            // Attach input with an id of "dp-1" and give it a "d-sl-m-sl-Y" date format (e.g. 13/03/1990)
            formElements:{"fecha_nacimiento":"d-sl-m-sl-Y"},
            staticPos:false
        };
        datePickerController.createDatePicker(opts);
        // ]]>
    });
</script>