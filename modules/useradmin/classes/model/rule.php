<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Rule extends ORM
{
		protected $_rules = array(
        'id' => array(
            'min_length' => array(11),
        ),
        'code' => array(
            'not_empty'  => NULL,
            'min_length' => array(4),
            'max_length' => array(32),
            'regex'      => array('/^[-\pL\pN_.]++$/uD'),
        ),
        'description' => array(
            'not_empty'  => NULL,
            'min_length' => array(4),
            'max_length' => array(32),
            'regex'      => array('/^[-\pL\pN_.]++$/uD'),
        )
    );
}