<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Productos extends Controller_Template
{
	public $template = 'ci/view_template';

	public function before()
	{
		if(!Auth::instance()->logged_in('admin'))
			$this->request->redirect('user/login');

		return parent::before();
	}

	public function action_index()
	{
		echo $this->request->param('param1');
		$this->template->content = View::factory('admin/view_admin_home');
	}

	public function action_agregar()
	{
		$m_tiendas = new Model_Tienda();
		$m_linea = new Model_Linea();
		$m_ubicacion = new Model_Ubicacion();

		$linea = $m_linea->where('code', '=', $this->request->param('param1'))->find();
		$tiendas = $m_tiendas->where('linea_id', '=', $linea->id)->find_all();

        $provincias = $m_ubicacion->where('parent_id', '=', 1)->find_all();

        $ciudades_list = array();
        foreach($provincias as $item){
            $ciudades = $m_ubicacion->where('parent_id', '=', $item->id)->find_all();

            foreach($ciudades as $ciudad){
                $ciudad = $ciudad->as_array();
                $ciudades_list[] = $ciudad;
            }
        }

		$this->template->content = View::factory('admin/view_registro_producto')
			->set('linea', $this->request->param('param1'))
			//->set('ciudades', $m_ubicacion->where('parent_id', '=', 1)->find_all())
			->set('ciudades', $ciudades_list)
			->set('paises', $m_ubicacion->where('parent_id', '=', '-1')->find_all())
			->set('categorias', ORM::factory('categoria')->find_all())
			->set('tiendas', $tiendas);
	}

	public function action_editar()
	{
		$id_producto = $this->request->param('param1');

		if($id_producto)
		{
			$producto = new Model_Producto($id_producto);

			if($producto->loaded())
			{
				$m_tiendas = new Model_Tienda();
				$tiendas = $m_tiendas->where('linea_id', '=', $producto->tienda->linea_id)->find_all();

                $provincias = ORM::factory('ubicacion')->where('parent_id', '=', 1)->find_all();

                $ciudades_list = array();
                foreach($provincias as $item){
                    $ciudades = ORM::factory('ubicacion')->where('parent_id', '=', $item->id)->find_all();

                    foreach($ciudades as $ciudad){
                        $ciudad = $ciudad->as_array();
                        $ciudades_list[] = $ciudad;
                    }
                }

				$this->template->content = View::factory('admin/view_registro_producto')
					->set('tiendas', $tiendas)
					->set('linea', $producto->tienda->linea->code)
					//->set('ciudades', ORM::factory('ubicacion')->where('parent_id', '=', 1)->find_all())
					->set('ciudades', $ciudades_list)
					->set('paises', ORM::factory('ubicacion')->where('parent_id', '=', '-1')->find_all())
					->set('categorias', ORM::factory('categoria')->find_all())
					->set('producto', $producto);
			}else{
				$this->request->redirect($this->request->referrer());
			}
		}else{
			$this->request->redirect($this->request->referrer());
		}
	}

	public function action_eliminar()
	{
		$id_producto = $this->request->param('param1');
		if($id_producto)
		{
			$producto = new Model_Producto($id_producto);

			if($producto->loaded())
			{
				$producto->delete();
				Message::add('info', 'Producto eliminado correctamente');
				$this->request->redirect($this->request->referrer());
			}else{
				Message::add('error', 'No se pudo eliminar el producto');
				$this->request->redirect($this->request->referrer());
			}
		}else{
			$this->request->redirect($this->request->referrer());
		}
	}

	public function action_saveproducto()
	{
        $mdl_producto = new Model_Producto();

		print_r($_POST);

        $_POST['user_id'] = Auth::instance()->get_user()->id;
		$_POST['status'] = 1;
		$_POST['created'] = date('Y-m-d h:i:s');

		if($this->request->post('oferta_duracion')){
            $fd = explode('/',  $_POST['oferta_duracion'] );
            if(count($fd) > 2) $_POST['oferta_duracion'] = $fd[2] . '-'. $fd[1] . '-' . $fd[0] . ' 01:01:01';
        }else{
			$_POST['oferta_duracion'] = date('Y-m-d h:i:s');
		}

		if($this->request->post('tiempo_validez')){
			$fd = explode('/',  $_POST['tiempo_validez'] );
			if(count($fd) > 2) $_POST['tiempo_validez'] = $fd[2] . '-'. $fd[1] . '-' . $fd[0] . ' 01:01:01';
		}else{
			$_POST['tiempo_validez'] = date('Y-m-d h:i:s');
		}

		if($this->request->post('oferta_publicacion')){
			$fd = explode('/',  $_POST['oferta_publicacion'] );
			if(count($fd) > 2) $_POST['oferta_publicacion'] = $fd[2] . '-'. $fd[1] . '-' . $fd[0] . ' 00:00:00';
		}else{
			$_POST['oferta_publicacion'] = date('Y-m-d h:i:s');
		}

        //////////////////////////////////////////////////////////////////////////////
        $post = Validation::factory($_POST);
        $post->rule('comercio_nombre', 'not_empty');
        $post->rule('comercio_direccion', 'not_empty');
        $post->rule('comercio_descripcion', 'not_empty');
        $post->rule('oferta_titulo', 'not_empty');
        $post->rule('oferta_descripcion', 'not_empty');
        $post->rule('oferta_destacamos', 'not_empty');
        $post->rule('oferta_que_incluye', 'not_empty');
        $post->rule('tienda_id', 'not_empty');
        $post->rule('oferta_valor', 'numeric');

        if ($post->check())
        {
			$producto_id = -1;
			$redirect = '';
			if($this->request->post('id') != '')
			{
				$p = new Model_Producto($this->request->post('id'));
				if($p->loaded())
				{
                    $p->values($_POST);

                    if($this->request->post('oferta_nacional') == 1){
                        $p->oferta_ubicacion_ciudad = -2;
                    }

					$p->update();
					$producto_id = $this->request->post('id');
					$redirect = 'admin/productos/editar/' . $this->request->post('id');
				}
			}else{
            	$mdl_producto->values($post->data());

                if($this->request->post('oferta_nacional') == 1){
                    $mdl_producto->oferta_ubicacion_ciudad = -2;
                }

            	$mdl_producto->save();
				$producto_id = $mdl_producto->id;
            	$redirect = 'admin/lineas/';
			}

			//Actualizar imagenes externas
			Model_ProductoFile::cleanImages($producto_id);
			foreach($_POST['imagen_externa'] as $img)
			{
				if($img != ''){
					$m_pro_file = new Model_ProductoFile();
					$m_pro_file->values(array('producto_id'=>$producto_id, 'file_id'=>$img));
					$m_pro_file->save();
				}
			}

			$this->request->redirect($redirect);
        }else{
			Message::add('error', 'Por favor complete los campos del formulario');
			if($this->request->post('id') != '')
				$this->request->redirect('admin/productos/editar/' . $this->request->post('id'));
			else
            	$this->request->redirect('admin/productos/agregar/' . $_POST['linea']);
        }
	}


} // End Home Admin
