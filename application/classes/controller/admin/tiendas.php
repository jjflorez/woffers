<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Tiendas extends Controller_Template
{
	public $template = 'ci/view_template';

	public function before()
	{
		if(!Auth::instance()->logged_in('admin'))
			$this->request->redirect('user/login');

		return parent::before();
	}

	public function action_index()
	{
		echo $this->request->param('param1');
		$this->template->content = View::factory('admin/view_admin_home');
	}

	public function action_agregar()
	{
		$m_lineas = new Model_Linea();

		$linea = $m_lineas->where('code', '=', $this->request->param('param1'))->find();

		$this->template->content = View::factory('admin/view_registro_tienda')
			->set('linea', $linea);
	}

	public function action_editar()
	{
		$id_tienda = $this->request->param('param1');

		if($id_tienda)
		{
			$producto = new Model_Producto($id_tienda);

			if($producto->loaded())
			{
				$m_tiendas = new Model_Tienda();
				$tiendas = $m_tiendas->where('linea_id', '=', $producto->tienda->linea_id);
				$this->template->content = View::factory('admin/view_registro_producto')
					->set('tiendas', $tiendas)
					->set('linea', $producto->tienda->linea->code)
					->set('producto', $producto);
			}else{
				$this->request->redirect($this->request->referrer());
			}
		}else{
			$this->request->redirect($this->request->referrer());
		}
	}

	public function action_eliminar()
	{
		$id_tienda = $this->request->param('param1');
		if($id_tienda)
		{
			$producto = new Model_Tienda($id_tienda);

			if($producto->loaded())
			{
				$producto->delete();
				Message::add('info', 'Tienda eliminado correctamente');
				$this->request->redirect($this->request->referrer());
			}else{
				Message::add('error', 'No se pudo eliminar la');
				$this->request->redirect($this->request->referrer());
			}
		}else{
			$this->request->redirect($this->request->referrer());
		}
	}

	public function action_savetienda()
	{
        $m_tienda = new Model_Tienda();

		$validation = new Validation($_POST);

		$validation->rule('name', 'not_empty');
		$validation->rule('comision_ovl', 'not_empty');

		if($validation->check())
		{
			$m_tienda->values($validation->data());
			$m_tienda->save();
			Message::add('info', 'Tienda guardada correctamente');
		}else{
			Message::add('error', 'No se pudo guardar');
		}
		$this->request->redirect($this->request->referrer());
	}


} // End Home Admin
