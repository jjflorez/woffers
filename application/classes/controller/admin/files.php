<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Files extends Controller_Template
{
	public $template = 'ci/view_template';
	public $auth_required = 'admin';

	public function before()
	{
		if(!Auth::instance()->logged_in('admin')){
			$this->request->redirect('/user/login');
		}
		return parent::before();
	}

	public function action_index()
	{
		$m_files = new Model_File();
		$m_ftypes = new Model_Filetype();

		$pagination = new Pagination(array(
			'total_items' => $m_files->count_all(),
			'items_per_page' => 10,
			'auto_hide' => false,
			'view' => 'pagination/useradmin'
		));;

		$sort = isset($_GET['sort']) ? $_GET['sort'] : 'id'; // set default sorting direction here
		$dir = isset($_GET['dir']) ? 'DESC' : 'ASC';

		$results = $m_files->limit($pagination->items_per_page)
							->offset($pagination->offset)
							->order_by($sort, $dir)
							->find_all();

		$this->template->content = View::factory('admin/view_admin_file')
				->set('files', $results)
				->set('paging', $pagination)
				->set('files_types', $m_ftypes->find_all());
	}

	public function action_types()
	{
		$m_ftypes = new Model_Filetype();

		$this->template->content = View::factory('admin/view_admin_file_type')
				->set('files_types', $m_ftypes->find_all());
	}

} // End Home Admin
