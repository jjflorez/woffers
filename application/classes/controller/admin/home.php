<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Home extends Controller_Template
{
	public $template = 'ci/view_template';

	public function before()
	{
		if(!Auth::instance()->logged_in('admin'))
			$this->request->redirect('user/login');

		return parent::before();
	}

	public function action_index()
	{
		echo $this->request->param('param1');
		$this->template->content = View::factory('admin/view_admin_home');
	}

	public function action_lineas()
	{
		$current_view = '';
		if($this->request->param('param1'))
		{

		}else{
			$current_view = View::factory('admin/sp');
		}
		$this->template->content = View::factory('admin/view_admin_home')
			->set('main_view', $current_view);
	}

} // End Home Admin
