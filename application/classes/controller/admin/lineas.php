<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Lineas extends Controller_Template
{
	public $template = 'ci/view_template';

	public function before()
	{
		if(!Auth::instance()->logged_in('admin'))
			$this->request->redirect('user/login');

		return parent::before();
	}

	public function action_index()
	{
		echo $this->request->param('param1');
		$this->template->content = View::factory('admin/view_admin_home');
	}

	public function action_productos()
	{
		$m_productos = new Model_Producto();
		$m_tiendas = new Model_Tienda();
		$m_linea = new Model_Linea();

		$code_linea = $this->request->param('param1');

		$linea = $m_linea->where('code', '=', $code_linea)->find();
		$tiendas = $m_tiendas->where('linea_id', '=', $linea->id)->find_all();

		/* Start listing --------------*/
		$pagination = new Pagination(array('total_items' => $m_productos->count_all(),'items_per_page' => 10,'view' => 'pagination/useradmin'));
		$sort = isset($_GET['sort']) ? $_GET['sort'] : 'id';
		$dir = isset($_GET['dir']) ? 'DESC' : 'ASC';
		$m_productos->limit($pagination->items_per_page);
		$m_productos->offset($pagination->offset);
		$m_productos->order_by($sort, $dir);
		/* End listing --------------*/

		$m_productos->where('status','=', 1);
		if(count($tiendas)){
			$m_productos->and_where_open();
			foreach($tiendas as $t)	$m_productos->or_where('tienda_id','=', $t->id);
			$m_productos->and_where_close();
		}
		$results = $m_productos->find_all();

		$this->template->content = View::factory('admin/view_admin_productos')
			->set('productos', $results)
			->set('paging', $pagination)
			->set('linea', $this->request->param('param1'));
	}

	public function action_tiendas()
	{
		$m_tiendas = new Model_Tienda();
		$m_linea = new Model_Linea();

		$code_linea = $this->request->param('param1');

		$linea = $m_linea->where('code', '=', $code_linea)->find();

		/* Start listing --------------*/
		$pagination = new Pagination(array('total_items' => $m_tiendas->count_all(),'items_per_page' => 10,'view' => 'pagination/useradmin'));;
		$sort = isset($_GET['sort']) ? $_GET['sort'] : 'id'; // set default sorting direction here
		$dir = isset($_GET['dir']) ? 'DESC' : 'ASC';
		$results = $m_tiendas->limit($pagination->items_per_page)->offset($pagination->offset)->order_by($sort, $dir)
			->where('linea_id', '=', $linea->id)->find_all();
		/* End listing --------------*/

		$this->template->content = View::factory('admin/view_admin_tiendas')
			->set('tiendas', $results)
			->set('paging', $pagination)
			->set('linea', $this->request->param('param1'));
	}

} // End Home Admin
