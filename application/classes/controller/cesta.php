<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cesta extends Controller_Template
{
    public $template = 'ci/view_template';

    public function before()
    {
        parent::before();

		$res_provincias = ORM::factory('ubicacion')->where('parent_id', '=', 1)->find_all();

		$m_ciudades = new Model_Ubicacion();

		foreach($res_provincias as $pro)
			$m_ciudades->or_where('parent_id', '=', $pro->id);

		$m_ciudades->order_by('nombre', 'asc');
		$ciudades = $m_ciudades->find_all();

		$session = Session::instance();
		$current = 7;
        if(!$session->get('id_ciudad'))
            $session->set('id_ciudad', $current);

		if($session->get('id_ciudad'))
			$current = ORM::factory('ubicacion')->where('id', '=', $session->get('id_ciudad'))->find();

		$this->template->provincias_selected = $current;
		$this->template->provincias = $res_provincias;
		$this->template->lista_ciudades = $ciudades;
    }

    public function action_index()
    {
        $session = Session::instance();

		$carrito_id = $session->get('IDCARRITO');
		if(Auth::instance()->logged_in())
			$id_usuario = Auth::instance()->get_user()->id;
		else
			$this->request->redirect('user/login');

		if(!$carrito_id){
        	if (Auth::instance()->logged_in()){
        	    $id_usuario = Auth::instance()->get_user()->id;
				$carrito_pendiente = ORM::factory('carrito')->where('user_id','=',Auth::instance()->get_user()->id)->where('status', '=', 0)->find();
				$carrito_id = $carrito_pendiente->id;
        	}
		}

        if($carrito_id){
            $m_detalle = new Model_Detalleproducto();
            $_detalle = $m_detalle->where('carrito_id', '=', $carrito_id)
                ->group_by('producto_id')
                ->order_by('id')
                ->find_all();

            $_total_monto = $m_detalle->total_monto($carrito_id);

            $_direcciones = ORM::factory('direccion')->where('carrito_id', '=', $carrito_id)->find_all();
            $_direcciones2 = ORM::factory('direccion')->where('user_id', '=', $id_usuario)->find_all();

            //producto regalo
            $_pro_regalo = $m_detalle->where('carrito_id', '=', $carrito_id)
                ->and_where('is_gift', '=', 1)
                ->find_all();

            $_usuario = ORM::factory('user', $id_usuario);

            $this->template->content = View::factory('view_cesta')
                ->set('carrito_id', $carrito_id)
                ->set('carrito', new Model_Carrito($carrito_id))
                ->set('productos', $_detalle)
                ->set('total_monto', $_total_monto)
                ->set('direcciones', $_direcciones)
                ->set('direcciones2', $_direcciones2)
                ->set('prod_regalos', $_pro_regalo)
                ->set('usuario_id', $id_usuario)
                ->set('usuario', $_usuario);
        }
    }

    public function action_prueba()
    {
        if(Auth::instance()->logged_in()){
            $id_usuario = Auth::instance()->get_user()->id;

            $m_carrito = new Model_Carrito();
            $carritos = $m_carrito->where('user_id', '=', $id_usuario)->find_all();

            $m_detalle_pro = new Model_Detalleproducto();

            foreach($carritos as $_car){
                $_car = $_car->as_array();
                $m_detalle_pro->or_where('carrito_id', '=', $_car['id']);
            }
            $m_detalle_pro->order_by('carrito_id');

            $_productos = $m_detalle_pro->find_all();

            echo $m_detalle_pro->last_query();
            foreach($_productos as $_pro){
                echo "<pre>";
                print_r($_pro->as_array());
            }

            foreach($carritos as $_car){
                $_car = $_car->as_array();
                $carrito_id = $_car['id'];
                /*echo $carrito_id;
                echo "<pre>";
                print_r($_car);*/
            }

        }else{
            $id_usuario = NULL;
        }

        echo "hola " . $id_usuario;
    }

	public function action_felicidades()
	{
		$usuario_id = $this->request->post('id_usuario');
		$total = $this->request->post('total');
		$email = $this->request->post('email');
		$num_orden = $this->request->post('num_orden');
		$codigo = $this->request->post('codigo'); //Respuesta del TPV
		$carrito_id = $this->request->post('id_carr');

		$carrito = new Model_Carrito($carrito_id);

		if($carrito->loaded())
		{
			if($this->checkStatusTPV($codigo))
			{
				$carrito->status = Quickdev_Status::ACTIVE;
			}else{
				$carrito->status = Quickdev_Status::ERROR;
			}

			if($carrito->user_id == $usuario_id)
				$carrito->save();
		}

		$this->template->content = View::factory('view_cesta_felicidades');
	}

	private function checkStatusTPV($_codigo)
	{
		return TRUE;
	}
}