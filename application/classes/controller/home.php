<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Controller_Template
{
	public $template = 'ci/view_template';

	public function before()
	{
		parent::before();

		$res_provincias = ORM::factory('ubicacion')->where('parent_id', '=', 1)->find_all();

		$m_ciudades = new Model_Ubicacion();

		foreach($res_provincias as $pro)
			$m_ciudades->or_where('parent_id', '=', $pro->id);

		$m_ciudades->order_by('nombre', 'asc');
		$ciudades = $m_ciudades->find_all();

		$session = Session::instance();
		$current = 7;

        if(!$session->get('id_ciudad'))
            $session->set('id_ciudad', $current);

		if($session->get('id_ciudad'))
			$current = ORM::factory('ubicacion')->where('id', '=', $session->get('id_ciudad'))->find();

		$this->template->provincias_selected = $current;
		$this->template->provincias = $res_provincias;
		$this->template->lista_ciudades = $ciudades;
	}

	public function action_index()
	{
        $m_productos = new Model_Producto();
        $session = Session::instance();

        if($session->get('id_ciudad'))
            $id_ciudad = $session->get('id_ciudad');

		$t_offers = ORM::factory('tienda')->where('linea_id', '=', 1)->find();
		$t_outlet = ORM::factory('tienda')->where('linea_id', '=', 2)->find();
		$t_travel = ORM::factory('tienda')->where('linea_id', '=', 3)->find();
		$t_coaching= ORM::factory('tienda')->where('linea_id', '=', 4)->find();

        $offers = $m_productos->where('tienda_id', '=', $t_offers->id)
                            ->and_where_open()
                            ->where('oferta_ubicacion_ciudad', '=', $id_ciudad)
                            ->or_where('oferta_ubicacion_ciudad', '=', '-2')
                            ->and_where_close()
                            ->where('oferta_publicacion', '<=', date('Y-m-d h:i:s'))
                            ->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->order_by('id', 'desc')->find_all();

        $outlet = $m_productos->where('tienda_id', '=', $t_outlet->id)->and_where_open()->where('oferta_ubicacion_ciudad', '=', $id_ciudad)->or_where('oferta_ubicacion_ciudad', '=', '-2')->and_where_close()->where('oferta_publicacion', '<=', date('Y-m-d h:i:s'))->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->order_by('id', 'desc')->find_all();
        $travel = $m_productos->where('tienda_id', '=', $t_travel->id)->and_where_open()->where('oferta_ubicacion_ciudad', '=', $id_ciudad)->or_where('oferta_ubicacion_ciudad', '=', '-2')->and_where_close()->where('oferta_publicacion', '<=', date('Y-m-d h:i:s'))->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->order_by('id', 'desc')->find_all();
        $coaching = $m_productos->where('tienda_id', '=', $t_coaching->id)->and_where_open()->where('oferta_ubicacion_ciudad', '=', $id_ciudad)->or_where('oferta_ubicacion_ciudad', '=', '-2')->and_where_close()->where('oferta_publicacion', '<=', date('Y-m-d h:i:s'))->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->order_by('id', 'desc')->find_all();

		$this->template->content = View::factory('view_home')
            ->set('offers_p', $offers)
            ->set('outlet_p', $outlet)
            ->set('travel_p', $travel)
            ->set('coaching_p', $coaching);
	}

    public function action_set_ciudad()
    {
        $_id = $this->request->param('param1');
        $_controller = $this->request->param('param2');
        $session = Session::instance();
        $session->set('id_ciudad', $_id);

        $this->request->redirect('/'.$_controller);
    }
} // End Home
