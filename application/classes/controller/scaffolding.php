<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Scaffolding extends Controller_Template
{
	public $template = 'ci/view_template';

	public function action_index()
	{
		$data_base = Kohana_Database_MySQL::instance();


		$this->template->title = __('title.scaffolding');
		$this->template->t_content = View::factory('scaffolding/view_home')
			->set('tablas', $data_base->list_tables());
	}
} // End Home
