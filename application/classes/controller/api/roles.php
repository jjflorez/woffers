<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Roles extends Quickdev_Api
{
	//public $template = 'ci/view_template';

	public function action_index()
	{

	}

	public function action_get()
	{
		$role = new Model_Role();
		$response = new Quickdev_Response();

		$res_roles = $role->find_all();

		foreach($res_roles as $rules)
		{
			array_push($response->data, $rules->as_array());
		}

		$this->makeResponse($response);
	}

	public function action_edit()
	{
		$role = new Model_Role();
		$response = new Quickdev_Response();

		$res_roles = $role->find_all();

		foreach($res_roles as $rules)
		{
			array_push($response->data, $rules->as_array());
		}

		$this->makeResponse($response);
	}

	/*** Rules */

	public function action_rules()
	{
		$rule = new Model_Rule();
		$response = new Quickdev_Response();

		if($this->request->post('busqueda')){
			$rule->where('code', 'LIKE', '%' . $this->request->post('busqueda') . '%');
		}

		$res_rules = $rule->find_all();

		foreach($res_rules as $rules)
		{
			array_push($response->data, $rules->as_array());
		}

		$this->makeResponse($response);
	}
} // End Roles
