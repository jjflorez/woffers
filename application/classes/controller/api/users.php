<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Users extends Quickdev_Api
{
	//public $template = 'ci/view_template';

	public function action_index()
	{

	}

	public function action_get()
	{
		$m_user = new Model_User();
		$response = new Quickdev_Response();

		//Set Params
		if($this->request->post('username'))
			$m_user->where('username', '=', $this->request->post('username'));
		if($this->request->post('id'))
			$m_user->where('id', '=', $this->request->post('id'));
		if($this->request->post('email'))
			$m_user->where('email', '=', $this->request->post('email'));

		if($this->request->post('search'))
		{
			$m_user->or_where_open();
			$m_user->or_where('username', 'LIKE', '%' . $this->request->post('search') . '%');
			$m_user->or_where('email', 'LIKE', '%' . $this->request->post('search') . '%');
			$m_user->or_where_close();
		}

		//End of params

		$result = $m_user->find_all();

		if(Kohana::$environment == Kohana::DEVELOPMENT)
			$response->status->debug_string = $m_user->last_query();

		foreach($result as $row)
		{
			$r_row = $row->as_array();
			$r_row['roles'] = array();
			foreach($row->roles->find_all() as $rol)
			{
				$r_row['roles'][] = $rol->as_array();
			}
			array_push($response->data, $r_row);
		}

		$this->makeResponse($response);
	}

	public function action_update_puntos()
	{
		$response = new Quickdev_Response();

		$m_ranking = new Model_Ranking();

		$v_params = Validation::factory($_POST);
		$v_params->rule('puntaje', 'not_empty');
		$v_params->rule('id', 'not_empty');
		$v_params->rule('random', 'not_empty');
		$v_params->rule('hash', 'not_empty');

		if($v_params->check())
		{
			$l_hash = md5($this->request->post('random') . 'quickdev');

			if($l_hash == $this->request->post('hash'))
			{
				$id_usuario = $this->request->post('id');
				if($this->request->post('id') == -1)
					$id_usuario = Auth::instance()->get_user()->id;

				$usuario = new Model_User($id_usuario);
				$puntaje_final = $usuario->puntaje + $this->request->post('puntaje');
				$level_id= null;
				$ranking = $m_ranking->find_all();

				foreach($ranking as $level)
				{
					if($puntaje_final < $level->max && $puntaje_final > $level->min)
						$level_id = $level->id;
				}



				$usuario->values(array('puntaje'=>$puntaje_final, 'ranking_id'=>$level_id));
				$usuario->update();

				$response->data = $usuario;
			}else{
				$response->status->setStatus('SEGURIDAD');
			}
		}else{
			$response->status->setStatus('PARAMS');
			$response->status->details = $v_params->errors();
		}

		$this->makeResponse($response);
	}

	public function action_save()
	{
		//Verificar parametros
		$v_params = Validation::factory($_POST);
		$v_params->rule('username', 'not_empty');
		$v_params->rule('username', 'min_length', array(':value', '4'));
		$v_params->rule('username', 'Model_User::unique_username');
		$v_params->rule('email', 'not_empty');
		$v_params->rule('email', 'email_domain');
		$v_params->rule('password', 'not_empty');

		$response = $this->insertUpdate($v_params, 'user');

		$this->makeResponse($response);
	}

	/*-------------------------------- Custom Methods */

	public function action_list_videos()
	{
		$m_uvideo = new Model_Uservideo();
		//$m_video = new Model_File();
		$response = new Quickdev_Response();

		//Set Params
		if($this->request->post('name'))
			$m_uvideo->where('name', '=', $this->request->post('name'));
		if($this->request->post('id'))
			$m_uvideo->where('id', '=', $this->request->post('id'));

		if($this->request->post('user_id'))
			$m_uvideo->where('user_id', '=', $this->request->post('user_id'));
		else{
			if(Kohana_Auth::instance()->logged_in())
				$m_uvideo->where('user_id', '=', Kohana_Auth::instance()->get_user()->id);
		}
		
		if($this->request->post('search'))
		{
			$m_uvideo->or_where_open();
			$m_uvideo->or_where('name', 'LIKE', '%' . $this->request->post('search') . '%');
			$m_uvideo->or_where('user_id', 'LIKE', '%' . $this->request->post('search') . '%');
			$m_uvideo->or_where_close();
		}

		//End of params

		if($this->request->post('action') == 'fake'){
			$result = array($_POST);
		}else{
			$result = $m_uvideo->find_all();
		}

		if(Kohana::$environment == Kohana::DEVELOPMENT)
			$response->status->debug_string = $m_uvideo->last_query();

		foreach($result as $row)
		{
			if($this->request->post('action') == 'fake'){
				$r_row = $row;
				$video_sel = new Model_Video($row['video_id']);
				$r_row['video'] = $video_sel->as_array();
			}else{
				$r_row = $row->as_array();
				$r_row['video'] = $row->video;
			}
			$r_row['heads'] = array();
			$imagenes = explode(',', $r_row['pos_heads']);
			foreach($imagenes as $head)
			{
				$file = new Model_File($head);
				$r_row['heads'][] = $file->as_array();
			}
			array_push($response->data, $r_row);
		}

		if($this->request->post('action') == 'fake')
		{
			echo json_encode($response->data[0]);
		}else{
			$this->makeResponse($response);
		}
	}

	public function  action_create_video()
	{
		$response = new Quickdev_Response();

		if($this->request->post('user_id'))
		{
			if($_POST['user_id'] == '-1' || $_POST['user_id'] == '' || $_POST['user_id'] == '0')
			{
				if(Kohana_Auth::instance()->logged_in())
					$_POST['user_id'] = Kohana_Auth::instance()->get_user()->id;
				else
					$_POST['user_id'] = -1;
			}
		}
		//--------------- Upload file
		$path_exist = false;
		$current_path = 'user_files/' . date('Y/m') . '/previews/';
		if(!file_exists($current_path)){
			if(mkdir($current_path, 0777, true))
				$path_exist = true;
		}else{
			$path_exist = true;
		}
		
		if($path_exist){

			$nombre_archivo = uniqid() . '.jpg';
			$fp = fopen($current_path . $nombre_archivo, 'wb' );
			fwrite( $fp, base64_decode($this->request->post('preview')) );
			fclose( $fp );

			$_POST['url_preview'] = $current_path . $nombre_archivo;
			$_POST['code'] = 'V' . date('dm') . '_' . uniqid();
			$_POST['created'] = date('Y-m-d g:i:s');

			$v_params = Validation::factory($_POST);
			$v_params->rule('pos_heads', 'not_empty');
			$v_params->rule('video_id', 'not_empty');
			$v_params->rule('user_id', 'not_empty');

			$response = $this->insertUpdate($v_params, 'uservideo', $response);
		}else{

			$response->status->setStatus('ERROR');
		}

		$this->makeResponse($response);
	}

	/*----------- Session */
	public function action_logued()
	{
		$response = new Quickdev_Response();

		if(Kohana_Auth::instance()->logged_in()){
			$user_data = Kohana_Auth::instance()->get_user()->as_array();
			unset($user_data['password']);
			unset($user_data['reset_token']);
			$response->data = array($user_data);
		}

		$this->makeResponse($response);
	}

	public function action_loginfacebook()
	{
		$response = new Quickdev_Response();

            $m_user = new Model_User();

            if($this->request->post('facebook_user_id'))
            {
                $m_user->where('facebook_user_id', '=', $this->request->post('facebook_user_id'));
                $user_fb = $m_user->find_all();
                if(count($user_fb) > 0){
                    Auth::instance()->force_login($user_fb[0]);
                }else{
                    $_POST['password'] = Kohana_Auth::instance()->hash(uniqid());
                    $v_params = Validation::factory($_POST);
                    $v_params->rule('username', 'not_empty');
                    $v_params->rule('username', 'Model_User::unique_username');
                    $v_params->rule('email', 'not_empty');
                    $v_params->rule('email', 'email_domain');
                    $v_params->rule('facebook_user_id', 'not_empty');

                    if($v_params->check())
                    {
                        $m_user->where('facebook_user_id', '=', $this->request->post('facebook_user_id'));
                        $user_fb = $m_user->find_all();
                        if(count($user_fb) > 0)
                        {
                            Auth::instance()->force_login($user_fb->as_array());
                        }else{
                            $m_user->values($v_params->data());
                            $result = $m_user->save();
                            if($result)
                            {
                                Auth::instance()->force_login($result);
                            }
                        }
                    }else{
                        $response->status->setStatus('PARAMS');
                    }
                }
            }else{
                $response->status->setStatus('PARAMS');
            }

		$this->makeResponse($response);
	}
} // End Roles
