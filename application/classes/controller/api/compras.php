<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Compras extends Quickdev_Api
{
    public function action_index()
    {
    }

	public function action_get()
	{
        $response = new Quickdev_Response();
        $user_id = $this->request->param('param1');

        if($user_id)
		{
            $m_carrito = new Model_Carrito();
			$m_detalleproducto = new Model_Detalleproducto();

            $carritos = $m_carrito->where('user_id', '=', $user_id)->where('status', '=', 1)->find_all();

			foreach($carritos as $_car){
				$m_detalleproducto->or_where('carrito_id', '=', $_car->id);
			}

			$detallespro = array();
			if($carritos->count() > 0)	$detallespro = $m_detalleproducto->order_by('id', 'DESC')->find_all();

			foreach($detallespro as $_pro)
			{
				$row = $_pro->as_array();
				$row['carrito'] = $_pro->carrito->as_array();
				$row['carrito']['created'] = date('d/m/Y', strtotime($_pro->carrito->created));
				$row['producto'] = $_pro->producto->as_array();
				$row['producto']['oferta_imagen'] = $_pro->producto->oferta_imagen->as_array();
				$row['producto']['oferta_imagen']['url_path'] = URL::base('http') . $_pro->producto->oferta_imagen->url_path;
				$row['producto']['comercio_imagen'] = $_pro->producto->comercio_imagen->as_array();
				$row['producto']['comercio_imagen']['url_path'] = URL::base('http') . $_pro->producto->comercio_imagen->url_path;
				$row['producto']['tiempo_validez'] = date('d/m/Y', strtotime($_pro->producto->tiempo_validez));
				if(strtotime($_pro->producto->tiempo_validez) < time())
					$row['estado'] = 'Caducado';
				else
					$row['estado'] = 'Vigente';

				if($this->request->post('tipo'))
				{
					if($_pro->producto->oferta_tipo == $this->request->post('tipo'))
						array_push($response->data, $row);
				}else{
					array_push($response->data, $row);
				}
			}
        }else{
            $response->status->setStatus('PARAMS');
        }

        $this->makeResponse($response);


    }

    //http://localhost/tiendaovlv2/api/compras/devolver/7
    public function action_devolver()
    {
        $response = new Quickdev_Response();

        $_carrito_id = $this->request->param('param1');
        $_detalleproducto = $this->request->post('detalleproducto'); // 51;
        $_estado = $this->request->post('estado'); // 2;

        if($_carrito_id == "" || $_detalleproducto == "" || $_estado == ""){
            $response->status->setStatus('PARAMS');
        }else{
            $detalle_pro = ORM::factory('detalleproducto', $_detalleproducto);

            if ($detalle_pro->loaded()){
                $detalle_pro->status = $_estado;

                $detalle_pro->save();

                $response->data = $detalle_pro->as_array();
            }
        }
        $this->makeResponse($response);
    }

    //http://localhost/tiendaovlv2/api/compras/producto/51
    public function action_oferta()
    {
        $response = new Quickdev_Response();

        $_detalleproducto = $this->request->param('param1');

        if(!$_detalleproducto){
            $response->status->setStatus('PARAMS');
        }else{
            $producto = ORM::factory('detalleproducto', $_detalleproducto);

            if($producto->loaded()){
				$row = $producto->as_array();

				$row['carrito'] = $producto->carrito->as_array();
				$row['carrito']['user'] = $producto->carrito->user->as_array();
				$row['producto'] = $producto->producto->as_array();
				$row['producto']['oferta_imagen'] = $producto->producto->oferta_imagen->as_array();
				$row['producto']['oferta_imagen']['url_path'] = URL::base('http') . $producto->producto->oferta_imagen->url_path;
				$row['producto']['comercio_imagen'] = $producto->producto->comercio_imagen->as_array();
				$row['producto']['comercio_imagen']['url_path'] = URL::base('http') . $producto->producto->comercio_imagen->url_path;

                $response->data = $row;
                //echo "<pre>";
                //print_r($_pro);
            }
        }
        $this->makeResponse($response);
    }
}