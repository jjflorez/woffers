<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Search extends Controller_Template
{
	public $template = 'ci/view_template';

	public function action_index()
	{
		$m_videos = new Model_Video();

		if(isset($_GET['search']))
			$search = $_GET['search'];

		$m_videos->or_where_open();
		$m_videos->or_where('name', 'LIKE', '%' . $search . '%');
		$m_videos->or_where('description', 'LIKE', '%' . $search . '%');
		$m_videos->or_where_close();
		$videos = $m_videos->find_all();

		$this->template->t_content = View::factory('view_search')
					->set('videos', $videos);
	}

} // End Home
