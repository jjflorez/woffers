<?php
/**
 * Created by JetBrains PhpStorm.
 * User: GersonM
 * Date: 07/04/12
 * Time: 02:01 AM
 * To change this template use File | Settings | File Templates.
 */
 
class Controller_Error extends Controller_Template
{
	public $template = 'ci/view_template';
	
	public function before()
	{	
		parent::before();

		$this->template->page = URL::site(rawurldecode(Request::$initial->uri()));

		// Internal request only!
		if (Request::$initial !== Request::$current)
		{
			if ($message = rawurldecode($this->request->param('message')))
			{
				$this->template->message = $message;
			}
		}
		else
		{
			$this->request->action(404);
		}

		$this->response->status((int) $this->request->action());
	}

	public function action_404()
	{

		$this->template->title = '404 Not Found';
		$this->template->content = View::factory('errors/error404');

		// Here we check to see if a 404 came from our website. This allows the
		// webmaster to find broken links and update them in a shorter amount of time.
		if (isset ($_SERVER['HTTP_REFERER']) AND strstr($_SERVER['HTTP_REFERER'], $_SERVER['SERVER_NAME']) !== FALSE)
		{
			// Set a local flag so we can display different messages in our template.
			$this->template->local = TRUE;
		}

		// HTTP Status code.
		$this->response->status(404);
	}
}
