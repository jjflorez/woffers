<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Qdmedia_Home extends Controller_Template
{
	public $template = 'ci/view_template_media';

	public function action_index()
	{
		$m_files = new Model_File();
		$m_filest = new Model_Filetype();
		$types = $m_filest->find_all();


		$pagination = new Pagination(array(
			'total_items' => $m_files->count_all(),
			'items_per_page' => 6,
			'auto_hide' => false,
			'view' => 'pagination/useradmin'
		));;

		$sort = isset($_GET['sort']) ? $_GET['sort'] : 'id'; // set default sorting direction here
		$dir = isset($_GET['dir']) ? 'ASC' : 'DESC';

		$results = $m_files->limit($pagination->items_per_page)
			->offset($pagination->offset)
			->order_by($sort, $dir)
			->find_all();

		$this->template->content = View::factory('qdmedia/view_home')
				->set('files', $results)
				->set('paging', $pagination)
				->set('types', $types);
	}

	public function action_snippet()
	{
		$m_files = new Model_File();
		$m_filest = new Model_Filetype();
		$types = $m_filest->find_all();


		$pagination = new Pagination(array(
			'total_items' => $m_files->count_all(),
			'items_per_page' => 6,
			'auto_hide' => false,
			'view' => 'pagination/useradmin'
		));;

		$sort = isset($_GET['sort']) ? $_GET['sort'] : 'id'; // set default sorting direction here
		$dir = isset($_GET['dir']) ? 'ASC' : 'DESC';

		$results = $m_files->limit($pagination->items_per_page)
			->offset($pagination->offset)
			->order_by($sort, $dir)
			->find_all();

		$this->auto_render = false;
		echo View::factory('qdmedia/view_home')
			->set('paging', $pagination)
			->set('types', $types);
	}

	public function action_galeria()
	{
		$m_files = new Model_File();
		$m_filest = new Model_Filetype();
		$types = $m_filest->find_all();

		$response = new Quickdev_Response();

		$pagination = new Pagination(array(
			'total_items' => $m_files->count_all(),
			'items_per_page' => 6,
			'auto_hide' => false,
			'view' => 'pagination/useradmin'
		));;

		$sort = isset($_GET['sort']) ? $_GET['sort'] : 'id'; // set default sorting direction here
		$dir = isset($_GET['dir']) ? 'ASC' : 'DESC';

		$results = $m_files->limit($pagination->items_per_page)
			->offset($pagination->offset)
			->order_by($sort, $dir)
			->find_all();

		$this->auto_render = false;

		$response->data = array();

		foreach($results as $res)
		{
			$response->data[] = $res->as_array();
		}

		echo json_encode($response);
	}

} // End Home
