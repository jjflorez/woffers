<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Carrito extends ORM
{
	protected $_belongs_to = array(
		'user' => array(),
	);

	protected $_has_many = array(
		'detalleproducto' => array(),
		//'detalleproducto' => array('model' => 'detalleproducto', 'foreign_key' => 'carrito_id')
	);

	public static function cleanOld($user_id = NULL)
	{
		$m_carrito = new Model_Carrito();
		$_carritos = $m_carrito->where('user_id', '=', $user_id)->where('status', '=', Quickdev_Status::PENDING)->find_all();

		foreach($_carritos as $_car){
			$_query = DB::delete('detalleproductos')->where('carrito_id', '=', $_car->id);
			$_query->execute();

			$_car->delete();
		}
	}
}