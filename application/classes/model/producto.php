<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Producto extends ORM
{

	protected $_belongs_to = array(
		'user' => array(),
		'comercio_imagen' => array('model'=>'file', 'foreign_key' => 'comercio_logo_id'),
		'oferta_imagen' => array('model'=>'file', 'foreign_key' => 'oferta_imagen_id'),
		'tienda' => array(),
	);

	protected $_has_many = array(
		'productofile' => array(),
	);

	public function get_oferta_precio_final()
	{
		return round($this->oferta_precio_venta - (($this->oferta_precio_venta / 100) * $this->oferta_descuento));
	}

	public function get_oferta_ahorro()
	{
		return ($this->oferta_precio_venta - $this->get_oferta_precio_final());
	}

	public function get_tiempo_restante($_format = 'm meses d días h horas i minutos s segundos')
	{
		$time = strtotime($this->oferta_duracion) - time();
		return date($_format, $time);
	}

	public function isExpired()
	{
		$time = strtotime($this->oferta_duracion) - time();
		if($time < 0)
			return true;
		else
			return false;
	}

    public function get_destacamos($xy_){
        $numero_letras=124;
        $numero_letras_2=60;
        $items_destamos=0;
        $content_result= null;


        $saltos_destacamos= substr_count($xy_, "\n");
        if($saltos_destacamos==0){



            return "<p>".$this->get_txtmuestra($xy_,$numero_letras)."</p>";


        }
        else
        {
            $array_destadamos=explode("\n", $xy_);
            foreach ($array_destadamos as $item_dest) {

                $content_result= $content_result."<p>".$this->get_txtmuestra($item_dest,$numero_letras_2)."</p>";


                if($items_destamos==1){

                    $items_destamos=0;
                    return $content_result;
                }
                $items_destamos++;
            }
        }


    }

    public function get_txtmuestra($cadena , $size){
        $cant_destacamos=strlen($cadena);
        if($cant_destacamos<=$size)
        {
            return $cadena;
        }
        else
        {
            $pocision_espacio=strrpos(substr($cadena,0,$size ), " ");
            $resultado=substr($cadena,0,$pocision_espacio);
            if(substr($resultado,-1)==",")
            {
                $resultado= substr($cadena,0,$pocision_espacio-1)." ...";
            }

            else{
                $resultado=substr($cadena,0,$pocision_espacio). " ...";
            }
        }
        return $resultado;

    }



}