<?php
/**
 * Created by JetBrains PhpStorm.
 * User: GersonM
 * Date: 07/04/12
 * Time: 02:01 AM
 * To change this template use File | Settings | File Templates.
 */
 
class Quickdev_Status
{
	const ERROR = 'error';
	const INFO = 'info';

	const PENDING = '0'; //Carrito no pagado o no comprado
	const ACTIVE = '1'; //Carrito comprado
	const INACTIVE = '2'; //Carrito con error de pago o entrega
	const LOCKED = '3'; //Carrito entregado y finalizado

	public $code;
	public $description;
	public $debug_string;
	public $details;

	public function __construct()
	{
		$this->code = '200';
		$this->description = 'Operación realizada correctamente';
	}

	public function setStatus($_type = 'ERROR')
	{
		switch($_type)
		{
			case 'SEGURIDAD':
				$this->code = '500';
				$this->description = 'Error de seguridad, no está autorizado a usar esta acción';
				break;
			case 'PARAMS':
				$this->code = '400';
				$this->description = 'No se pudo completar la operación, faltan parametros o estos no cumplen los requerimientos necesarios';
				break;
			default:
				$this->code = '300';
				$this->description = 'No se pudo completar la operación, se ha producido un error desconocido';
		}
	}

	static public function listStatus()
	{
		return array(
			'PENDING'=>Quickdev_Status::PENDING,
			'ACTIVE'=>Quickdev_Status::ACTIVE,
			'INACTIVE'=>Quickdev_Status::INACTIVE,
			'LOCKED'=>Quickdev_Status::LOCKED
		);
	}
}
