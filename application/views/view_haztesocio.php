<div id="c_home">


<div class="container">
<div id="colum_produc_a">
    <div class="contenedor_comofunciona">
        <span class="seccion">home > <a href="#">quienes somos</a></span>

        <div class="header">
            <div class="titulo">
                <span>¿Cómo funciona?</span>
            </div>
            <p class="descripcion">
                Cuando disfrutas de un buen restaurante o una excelente película, siempre comentas tu experiencia, para que tus amigos también disfruten de ella ¿verdad? <strong>¡En One Vision Life funcionamos del mismo modo!</strong>
            </p>
        </div>

        <div class="cuerpo">
            <div class="titulo">
                En tan solo tres pasos:
            </div>
            <ul class="contenido">
                <li class="uno">
                                    <span>
                                        Disfruta con One Vision Life
                                    </span>

                    <p>
                        Podrás elegir entre miles de beneficios de todas nuestra áreas: OFFERS, OUTLET, TRAVEL Y COACHING que te permitirán disfrutar y ahorrar al mismo tiempo. Recuerda que puedes <a href="#">registrarte</a>
                        totalmente gratis.
                    </p>
                    <img src="http://img.onevisionlife.com/home_coorp/comofunciona/img_item_uno.jpg" alt=""/>
                </li>
                <li class="dos">
                                    <span>
                                        Recomienda a tus amigos
                                    </span>

                    <p>
                        Comparte con tus amigos todos los beneficios que tenemos para ti. <a href="#">Invita</a> a todas las personas que desees a través de las redes sociales, correo electrónico, en persona o, simplemente, enviando
                        el link de nuestra web con tu propio usuario.
                    </p>
                    <img src="http://img.onevisionlife.com/home_coorp/comofunciona/img_item_dos.jpg" alt="" style="margin-top: -10px;"/>
                </li>
                <li class="tres">
                                    <span>
                                        Gana con nosotros
                                    </span>

                    <p>
                        Al registrarte en nuestra web, comienzas a ganar inmediatamente por recomendar. De esta forma, si más amigos se inscriben, tú ganas mucho más. <a href="#">Inscríbete ya</a> y gana premios, puntos, viajes,
                        regalos, sorteos, descuentos e incluso <a href="#">¡Ingresos extras!</a>
                    </p>
                    <img src="http://img.onevisionlife.com/home_coorp/comofunciona/img_item_tres.jpg" alt="" style="margin-top: -5px;"/>
                </li>
            </ul>
            <div class="sabermas" style="margin-top: 15px;">
                Si quieres saber más sobre One Vision Life, haz click <a href="ovl_home/que_es_onevisionlife">AQUÍ</a>
            </div>
        </div>
    </div>


</div>
<div id="colum_produc_b">
    <?php echo View::factory('ci/view_sidebar')->set('var_sidebar','')?>
</div>
    </div>
