<?php
	$controlador = Kohana_Request::$current->controller();
	$current_line = Kohana_Request::current()->controller();
?>

<div id="c_<?php echo $controlador; ?>">
	<div id="container_lineas">
		<ul id="pestanas_lineas">
			<?php
			foreach($tiendas as $t)
			{
				if($t->code == $producto->tienda->id)	echo '<li class="selected"><a href="'. URL::site('offers/tienda/' . $t->code) .'">' . $t->name . '</a></li>';
				else			echo '<li><a href="'. URL::site($controlador.'/tienda/' . $t->code) .'">' . $t->name . '</a></li>';
			}
			?>
		</ul>
	</div>
    <div class="container">
    <!--<ul class="nav nav-tabs">
        <?php
		/*
            foreach($tiendas as $t){
                echo '<li><a href="#">' . $t->name . '</a></li>';
            }
		*/
        ?>
    </ul>-->

	<div id="colum_produc_a">
        <div id="detalles_productos_ovl">
			<!--contentido-->
            <div class="producto <?php echo $current_line; ?>">
                <span class="descripcion">
                    <span><?php echo $producto->oferta_descuento; ?>% Dto.</span>
                    <?php echo $producto->oferta_titulo; ?>
                </span>
                <ul>
					<li id="img_det_pro"><img style="width: 440px;" src="<?php echo URL::base() . $producto->oferta_imagen->url_path; ?>" alt="<?php echo $producto->oferta_titulo; ?>"></li>
					<li>
						<ul>
							<li>
								<span class="precio">
									<span class="precio2"><?php echo $producto->get_oferta_precio_final(); ?><span class="signo">€</span></span>
									<?php echo $producto->get_oferta_precio_final(); ?><span class="signo">€</span>
								</span>
								<a class="btn_comprar" id="<?php echo $producto->id; ?>" href="<?php echo URL::site('carrito/add_producto/'. $producto->id ); ?>"></a>
							</li>
							<li class="mostrar_precio">
								<span>Precio <span><?php echo $producto->oferta_precio_venta; ?>€</span></span>
								<span class="n2">Dto <span><?php echo $producto->oferta_descuento; ?>%</span></span>
								<span>Ahorro <span><?php echo $producto->get_oferta_ahorro(); ?>€</span></span>
							</li>

							<!-- Detalle de fecha -->
							<li id="tiempo_detalle">
								<?php if(!$producto->isExpired()){ ?>
									<div id="defaultCountdown"></div>
								<?php }else{ ?>
									<div class="badge badge-error">Está oferta ya no está disponible</div>
								<?php } ?>
							</li>

							<li id="regalo_content">
								<div class="opciones" style="display: none;">
								<?php
								if($current_line == "outlet"){
								?>
								<ul>
									<li>
										Color<br/>
										Talla
									</li>
									<li>
										<select>
											<option>Seleccionar</option>
											<option>Seleccionar1</option>
											<option>Seleccionar2</option>
										</select>
										<br/>
										<select>
											<option>Seleccionar</option>
											<option>Seleccionar1</option>
											<option>Seleccionar2</option>
										</select>
									</li>
								</ul>

								<?php
								}else{
								?>
									Opciones
									<select>
										<option>Seleccionar</option>
										<option>Seleccionar1</option>
										<option>Seleccionar2</option>
									</select>
									<br/>
                                    <!--
									<img src="<?php /* echo URL::base(); */ ?>public/img/flechas_regalo.jpg" /> Conoce otras ofertas de este comercio
									<br/>-->
								<?php } ?>
								</div>
								<img src="<?php echo URL::base();?>public/img/regalo_regalo.jpg">¡Regálaselo a un amigo!
								<span>
									<a class="twiter" href="javascript:void(0);"></a>
									<a class="mail" href="javascript:void(0);"></a>
									<a class="me_gusta" href="javascript:void(0);"></a>
								</span>
							</li>
						</ul>
					</li>
                </ul>
            </div>
			<!--fin contentido-->
			<ul>
				<li>

					<span>
						<span><?php echo $producto->comercio_nombre; ?></span>
						<p><?php echo $producto->comercio_direccion; ?></p>
					</span>
					<div id="img_empresa_producto">
						<img style="width: 230px;" src="<?php echo URL::base() . $producto->comercio_imagen->url_path; ?>" />
					</div>
					<div class="contenido"><?php echo $producto->comercio_descripcion; ?></div>
				</li>

				<li class="content_descripciones">

					<ul class="tabs_detalle">
						<li class="active"><a id="descripcion" href="javascript:void(0);">Descripción</a></li>
						<li><a id="condiciones" href="javascript:void(0);">Condiciones</a></li>
						<li><a id="ubicacion" href="javascript:void(0);">Ubicación</a></li>
					</ul>
					<div class="content_tabs">
						<div class="contenido descripcion active">
							<h2>Destacamos</h2>
							<?php echo nl2br($producto->oferta_destacamos); ?>
							<br/>
							<h2>¿Que incluye esta oferta?</h2>
							<?php echo $producto->oferta_que_incluye; ?>
							<br/>
							<h2>Descripcion de la oferta</h2>
							<?php echo $producto->oferta_descripcion; ?>
						</div>
						<div class="contenido condiciones"><?php echo $producto->oferta_condiciones; ?></div>
						<div class="contenido ubicacion">
							<div id="map_canvas" style="height: 380px; width: 450px; display: block;"></div>
						</div>
					</div>

				</li>

			</ul>
        </div>
    </div>
		<div id="colum_produc_b">
			<?php echo View::factory('ci/view_sidebar'); ?>
		</div>
    </div>
</div>

<?php
    echo Html::style('public/css/jquery.countdown.css');
    echo Html::script('public/js/jquery.countdown.js');

    $fecha = new DateTime($producto->oferta_duracion);
    $_format = strtotime($producto->oferta_duracion) * 1000;

?>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

	$(document).ready(init);


	$tabs_antiguo_activo = "";

	function init(){
		$('.tabs_detalle a').click(setTab);

		$('.btn_comprar').click(comprar);

        var austDay = new Date();
        austDay = new Date(<?php echo $_format ?>);
        $('#defaultCountdown').countdown({until: austDay});
	}

	function setTab()
	{
		$('.tabs_detalle li').removeClass('active');
		$(this).parent().addClass('active');

		$('.content_tabs .contenido').removeClass('active');
		$('.content_tabs .' + $(this).attr('id')).addClass('active');

		if($(this).attr('id') == 'ubicacion')
		{
			startMap();				}
	}

	function startMap()
	{
		var position = new google.maps.LatLng(<?php echo $producto->oferta_ubicacion_lat; ?>, <?php echo $producto->oferta_ubicacion_lon; ?>);
		var mapOptions = {
			zoom: 16,
			center: position,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

		var marker = new google.maps.Marker({
			position: position,
			map: map,
			title: "<?php echo $producto->comercio_nombre; ?>"
		});

		var infowindow = new google.maps.InfoWindow({
			content: "<h2><?php echo $producto->comercio_nombre; ?></h2><p>Usa el mapa para ubicar mejor la dirección del comercio</p>"
		});

		infowindow.open(map,marker);
	}

	function comprar()
	{
		$(this).effect('transfer', { to: "#abrir_carrito", className: "transfer-effect" });
		qd.app.Carrito.agregarItemCarrrito($(this).attr('id'));
		return false;
	}
</script>