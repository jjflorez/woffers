<div class="container">
	<div class="row-fluid">
		<div class="span12">

		<div id="content_cesta">

		<div class="titulo">
		Mi cesta
		</div>
		<div class="cesta">



        <h2>Mi Cesta</h2>

        <ul class="my_cesta style_content_cesta"  cellspacing="0" cellpadding="0">
            <li>
                <span class="colum1">Tipo</span>
                <span class="colum2">Detalle de tu compra</span>
                <span class="colum3">Cantidad</span>
                <span class="colum4">Regalo</span>
                <span class="colum5">Precio</span>
                <span class="colum6">Subtotal</span>
                <span class="colum7">Acciones</span>

            </li>


            <?php foreach($productos as $item): ?>
            <li class="elementos">
                <span class="colum1"><?php if($item->producto->oferta_tipo=="producto"){ echo "P";}  ?></span>
                <span class="colum2"><?php echo $item->producto->oferta_titulo ?></span>
                <span class="colum3">
                    <?php
                        $_cantidad = $item->count_producto($carrito_id, $item->producto_id);
                        /*echo $_cantidad;*/
                    ?>
                    <select id="<?php echo $item->producto_id ?>" name="cantidad" class="cantidad">
                        <?php
                            for($x=1; $x<=10; $x++):
                                $_selected = 'selected="selected"';
                        ?>
                            <option <?php echo ($_cantidad == $x) ? $_selected : ""; ?> value="<?php echo $x ?>"><?php echo $x ?></option>
                        <?php endfor; ?>
                    </select>
                </span>
                <span class="colum4"><?php echo $item->count_gift($carrito_id, $item->producto_id) ?></span>
                <span class="colum5"><?php echo $item->monto ?> €</span>
                <span class="colum6"><?php echo ($_cantidad * $item->monto) . " €" ?></span>
                <span class="colum7"><a href="<?php echo url::site() . 'carrito/delete_producto/' . $carrito_id . '/' . $item->producto_id; ?>">Eliminar</a> <?php /* echo $item->producto_id*/ ?></span>
            </li>


            <?php endforeach; ?>

            <li class="totales top">TOTAL<span><?php echo $total_monto; ?> €</span>

            </li>
            <li class="totales ">

                <img src="<?php echo URL::base();?>public/img/cesta/quees.png" >GASTOS DE ENVÍO
                <span>4.5 €</span>

            </li>
            <li class="totales">
				<div class="ovl_credits">
					TIENES <span>50 OVL CREDITS</span> DISPONIBLES
				</div>

                <img src="<?php echo URL::base();?>public/img/cesta/quees.png" >UTILIZAR OVL CREDITS
                <span>
				<img src="<?php echo URL::base();?>public/img/cesta/coins.png">
				<input type="text">
				</span>



            </li>
            <li class="totales pago">
                PRECIO TOTAL
                <span>32 €</span>

            </li>

        </ul>

		<div class="agregar_producto">
			Recuerda imprimir tu vale o descargatelo directamente a tu movil.
			<br>
			Debe presentarlo a la hora de canjearlo.

			<input type="button">
		</div>






        <h2>Mis regalos</h2>

		<ul>
			<li>

			</li>
		</ul>

		<p><strong>Detalles de tus regalos</strong></p>

            <?php foreach($prod_regalos as $_regalo): ?>
                <?php echo  $_regalo->producto->oferta_titulo . " - " . $_regalo->direccion->direccion; ?><br>
            <?php endforeach; ?>
        <p><input id="radio_regalo" type="radio" />  Quiero hacer un regalo.</p>

		
<hr>


        <div id="form-regalo">
            <form id="frm_gift_dir" action="" method="post">
            <select id="cb_regalos">
                <option value="0">Seleccione regalo</option>

                <?php foreach($productos as $item): ?>
                    <option value="<?php echo $item->producto_id ?>"><?php echo $item->producto->oferta_titulo ?></option>
                <?php endforeach; ?>
            </select>
            <br>
                Empresa: <input type="text" name="empresa" /> <br />
                Nombre: <input type="text" name="nombre" /> <br />
                Apellidos: <input type="text" name="apellidos" /> <br />
                Telefono: <input type="text" name="telefono" /> <br />
                Direccion: <input type="text" name="direccion" /> <br />
                Tipo de Direccion:
                <select name="tipo_direccion">
                    <option value="0">Tipo de direccion</option>
                    <option value="particular">Particular</option>
                </select> <br />
                Codigo Postal: <input type="text" name="codigo_postal" /> <br />
                Poblacion: <input type="text" name="poblacion" /> <br />
                Provincia:
                <select name="provincia">
                    <option value="0">provincia 001</option>
                    <option value="1">provincia 002</option>
                    <option value="2">provincia 003</option>
                    <option value="3">provincia 004</option>
                    <option value="4">provincia 005</option>
                </select> <br />
                Pais: <input type="text" name="pais" /> <br />

                <input type="submit" value="Guardar Direccion" />
            </form>
        </div>

        <h2>Mis datos de envio</h2>
        <p><strong>Detalles y configuracion de tus datos de envio</strong></p>

        <select id="direcciones">
            <option value="0">Seleccione direccion</option>
            <?php foreach($direcciones2 as $dir): ?>
                <option value="<?php echo $dir->id ?>"><?php echo $dir->direccion ?></option>
            <?php endforeach ?>
        </select>
        <a href="#">agregar direccion</a>
        <table border="1">
            <tr>
                <th>id</th>
                <th>Direccion</th>
            </tr>

            <?php foreach($direcciones as $dir): ?>
            <tr>
                <td><?php echo $dir->id ?></td>
                <td><?php echo $dir->direccion ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        <br>
        <div>
            <form id="frm_dir" action="" method="post">
                Empresa: <input type="text" name="empresa" /> <br />
                Nombre: <input type="text" name="nombre" /> <br />
                Apellidos: <input type="text" name="apellidos" /> <br />
                Telefono: <input type="text" name="telefono" /> <br />
                Direccion: <input type="text" name="direccion" /> <br />
                Tipo de Direccion:
                <select name="tipo_direccion">
                    <option value="0">Tipo de direccion</option>
                    <option value="particular">Particular</option>
                </select> <br />
                Codigo Postal: <input type="text" name="codigo_postal" /> <br />
                Poblacion: <input type="text" name="poblacion" /> <br />
                Provincia:
                <select name="provincia">
                    <option value="0">provincia 001</option>
                    <option value="1">provincia 002</option>
                    <option value="2">provincia 003</option>
                    <option value="3">provincia 004</option>
                    <option value="4">provincia 005</option>
                </select> <br />
                Pais: <input type="text" name="pais" /> <br />

                <input type="submit" value="Agregar Direccion" />
            </form>
        </div>

        <h2>Datos de Facturacion</h2>
            <input type="radio" name="_fact" value="0" checked="checked" /> No quiero factura
            <input type="radio" name="_fact" value="1" /> Si quiero factura

            <div id="form-facturacion">
                <form id="frm_fac" action="" method="post">
                    Nombre: <input type="text" name="nombre" /> <br />
                    1r Apellido: <input type="text" name="apellido1" /> <br />
                    2o Apellido: <input type="text" name="apellido2" /> <br />
                    DNI: <input type="text" name="dni" /> <br />
                    Direccion: <input type="text" name="direccion" /> <br />
                    Codigo Postal: <input type="text" name="codigo_postal" /> <br />
                    Poblacion: <input type="text" name="poblacion" /> <br />
                    Provincia: <input type="text" name="provincia" /> <br />
                    Pais: <input type="text" name="pais" /> <br />
                </form>
            </div>


        <!--<form id="form_enviar" action="http://ouanda.h3m.com/~ixl030ba/securepayment/" method="post">-->
        <form id="form_enviar" action=" http://securepayment.onevisionlife.com/" method="post">

                <input type="hidden" id="response" name="response" value="http://localhost/<?php echo url::site(); ?>cesta" />
                <input type="hidden" id="total" name="total" value="<?php echo $total_monto ?>" />
                <input type="hidden" id="idusuario" name="idusuario" value="<?php echo $usuario_id ?>" />
                <input type="hidden" id="orden" name="orden" value="" />
                <input type="hidden" id="email" name="email" value="<?php echo $usuario->email ?>" />
                <input type="hidden" id="nombre" name="nombre" value="<?php echo $usuario->name ?>" />
                <input type="hidden" id="apellidos" name="apellidos" value="<?php echo $usuario->last_name ?>" />

                <!-- Init Array de valores, no tiene limite -->
            <?php
                $_count = 0;
                foreach($productos as $key => $item):
                    $_cantidad = $item->count_producto($carrito_id, $item->producto_id);
            ?>
                <input type="hidden" name="productos[<?php echo $key ?>][nombre]" value="<?php echo $item->producto->oferta_titulo . ' (x ' . $_cantidad . ')' ?>" />
                <input type="hidden" name="productos[<?php echo $key ?>][descripcion]" value="<?php echo $item->producto->oferta_descripcion ?>" />
                <input type="hidden" name="productos[<?php echo $key ?>][monto]" value="<?php echo ($_cantidad * $item->monto) ?>" />
            <?php
                $_count ++;
                endforeach;
            ?>
                <!-- En caso de que sea de un país de la unión europea agregar IVA como un producto -->
                <input type="hidden" name="productos[<?php echo $_count ?>][nombre]" value="IVA" />
                <input type="hidden" name="productos[<?php echo $_count ?>][descripcion]" value="Impuesto unión europea" />
                <input type="hidden" name="productos[<?php echo $_count ?>][monto]" value="<?php echo ($total_monto * 0.18) ?>" />
        </form>

        <h2>Formas de pago</h2>
            <input type="radio" name="pago" checked="checked" /> Tarjeta de credito
            <input type="radio" name="pago" disabled="disabled" /> Pay Pal
            <input type="radio" name="pago" disabled="disabled" /> Cuenta OVL
            <p><input id="btn_pagar" type="submit" value="Pagar" /></p>
		</div>
		</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    var id_carrito = <?php echo $carrito_id; ?>;
    var base_url = '<?php echo url::site(); ?>';
    //var back_url = "http://dev.onevisionlife.es/backoffice/api/user/get/";
    var back_url = "http://dev.onevisionlife.es/backoffice/api/user/get/gersonm";
    var _factura;

    $(document).ready(init);
var $radio_regalo =0;
    function init(){
        console.log("hola cesta");

        $("#form-regalo").hide();
        $("#form-facturacion").hide();

        $("#radio_regalo").click(function(){

			if($radio_regalo==0){
				$("#radio_regalo").attr('checked', true);
				$radio_regalo=1;
				$("#form-regalo").show('slow');
			}
			else
			{
				$("#radio_regalo").attr('checked', false);
				$radio_regalo=0;
				$("#form-regalo").hide('slow');
			}


        });



        //ovl credits
        /*$.get(back_url, function(data){
            var _data = JSON.parse(data);
        });*/

        $(".cantidad").change(update_cantidad);
        $("#frm_gift_dir").submit(save_frm_gift);
        $("#frm_dir").submit(save_frm);
        $("#btn_pagar").click(send_pagar);

        $('input:radio[name=_fact]').click(function () {
            var id = $('input:radio[name=_fact]:checked').val();
            _factura = id;

            if(_factura == 0){
                $("#form-facturacion").hide("show");
            }else if(_factura == 1){
                $("#form-facturacion").show("show");
            }
            //alert("change " + id);
        });
    }

    function update_cantidad(){
        var id_producto = $(this).attr('id');
        var cantidad = $(this).val();

        $.get(base_url + '/carrito/update_cantidad/'+id_carrito+'/'+id_producto+'/'+cantidad+'/', function (data) {
            window.location = base_url + 'cesta/';
        });
    }

    function save_frm_gift(){
        var id_producto = $("#cb_regalos").val(); //alert("hola " + id_producto);
        var _form = $("#frm_gift_dir").serialize(); //alert(_form);

        if(id_producto > 0){
            console.log("va !!!");

            $.post(base_url + 'carrito/add_gift_direccion/'+id_carrito+'/'+id_producto+'/', _form, function (data) {
                window.location = base_url + 'cesta/';
            });
        }else{
            console.log("no va");
        }
        return false;
    }

    function save_frm(){
        console.log("form pedido");

        $.post(base_url + 'carrito/add_direccion/'+id_carrito+'/', $("#frm_dir").serialize(), function (data) {
            window.location = base_url + 'cesta/';
        });

        return false;
    }

    function send_pagar(){
        var _direccion = $("#direcciones").val();

        if(_direccion > 0){
            if(_update_direccion(_direccion)){
                $("#form_enviar").submit();
            }
        }else{
            alert("seleccione direccion");
        }
    }

    function _update_direccion(id_direccion){
        $.post(base_url + 'carrito/update_direccion/'+id_direccion+'/'+id_carrito+'/', function (data) {});

        return true;
    }

</script>