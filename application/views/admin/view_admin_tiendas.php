<div id="c_admin">
	<div class="container">
		<div class="row-fluid">
			<div class="span3">
				<?php echo View::factory('admin/view_admin_menu'); ?>
			</div>

			<div class="span9">
				<div class="content" >
					<ul class="nav nav-tabs">

						<li><a href="<?php echo URL::site('admin/lineas/productos/'.$linea); ?>">Productos</a></li>
						<li class="active"><a href="<?php echo URL::site('admin/lineas/tiendas/'.$linea); ?>">Tiendas</a></li>
					</ul>
					<div class="pull-right">
						<a href="<?php echo URL::site('admin/tiendas/agregar/'.$linea)?>" class="btn btn-mini"><span class="icon-plus-sign"></span> Agregar tienda</a>
					</div>
					<h4 style="height: ">Productos en offers</h4>

					<?php
						// format data for DataTable
						$data = array();
						foreach ($tiendas as $file) {
							$row = $file->as_array();
							$row['actions'] = Html::anchor('admin_user/edit/' . $row['id'], __('edit')) . ' | ' . Html::anchor('admin_user/delete/' . $row['id'], __('delete'));
							$data[] = $row;
						}

						$column_list = array(
							'id' => array('label' => __('id')),
							'name' => array('label' => __('name')),
							'code' => array('label' => __('description'), 'sortable' => false),
							'comision_ovl' => array('label' => 'Comisión', 'sortable' => false),
							'actions' => array('label' => __('actions'), 'sortable' => false)
						);

						$datatable = new Helper_Datatable($column_list, array('paginator' => true, 'class' => 'table table-bordered table-striped', 'sortable' => 'true', 'default_sort' => 'username'));
						$datatable->values($data);

						echo $paging->render();
						echo $datatable->render();
						echo $paging->render();
					?>
				</div>
			</div>
		</div>
	</div>
</div>