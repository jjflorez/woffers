<div style="margin-top: 20px;">
	<ul class="nav nav-tabs">
		<li><a href="<?php echo URL::site('admin/files'); ?>"><?php echo __('administer.files'); ?></a> </li>
		<li class="active"><a href="<?php echo URL::site('admin/files/types'); ?>"><?php echo __('administer.files.types'); ?></a> </li>
	</ul>
	<div class="tab-content">
		<!-- tab content -->

		<h3><?php echo __('administer.files.types'); ?></h3>

		<?php
			//echo $paging->render();
			// format data for DataTable
			$data = array();
			$merge = null;
			$providers = array_filter(Kohana::$config->load('useradmin.providers'));
			foreach ($files_types as $file) {
				$row = $file->as_array();
				$row['actions'] = Html::anchor('admin_user/edit/' . $row['id'], __('edit')) . ' | ' . Html::anchor('admin_user/delete/' . $row['id'], __('delete'));
				$data[] = $row;
			}

			$column_list = array(
						'id' => array('label' => __('id')),
						'name' => array('label' => __('name')),
						'code' => array('label' => __('code')),
						'description' => array('label' => __('description'), 'sortable' => false),
			);

			$column_list['actions'] = array('label' => __('actions'), 'sortable' => false);
			$datatable = new Helper_Datatable($column_list, array('paginator' => true, 'class' => 'table table-bordered table-striped', 'sortable' => 'true', 'default_sort' => 'username'));
			$datatable->values($data);
			echo $datatable->render();
			//echo $paging->render();
		?>

		
	</div>
</div>

<script type="text/javascript">

	$(document).ready(initPage);

	function initPage()
	{
		$('#btn_show_upload').click(CLICK_BTN_handler);
		$('#btn_hide_upload').click(CLICK_BTN_handler);

	}

	function CLICK_BTN_handler()
	{
		switch($(this).attr('id'))
		{
			case 'btn_show_upload':
				$('#block_upload_file').slideDown('fast');
				$('#btn_show_upload').hide();
				$('#btn_hide_upload').show();
				break;
			case 'btn_hide_upload':
				$('#block_upload_file').slideUp('fast');
				$('#btn_show_upload').show();
				$('#btn_hide_upload').hide();
				break;
		}
	}
</script>