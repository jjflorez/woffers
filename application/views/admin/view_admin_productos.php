<div id="c_admin">
	<div class="container">
		<div class="row-fluid">
			<div class="span3">
				<?php echo View::factory('admin/view_admin_menu'); ?>
			</div>
			<div class="span9">
				<div class="content" >
					<ul class="nav nav-tabs">
						<li class="active"><a href="<?php echo URL::site('admin/lineas/productos/'.$linea); ?>">Productos</a></li>
						<li><a href="<?php echo URL::site('admin/lineas/tiendas/'.$linea); ?>">Tiendas</a></li>
					</ul>
					<div class="pull-right">
						<a href="<?php echo URL::site('admin/productos/agregar/'.$linea)?>" class="btn btn-mini"><span class="icon-plus-sign"></span> Agregar producto</a>
					</div>
					<h4 style="height: ">Productos en offers</h4>

					<?php
						// format data for DataTable
						$data = array();
						foreach ($productos as $file) {
							$row = $file->as_array();
							$row['actions'] = Html::anchor('admin/productos/editar/' . $row['id'], __('edit')) . ' | ' . Html::anchor('admin/productos/eliminar/' . $row['id'], __('delete'));
							$row['tienda'] = $file->tienda->name;
							$row['img_externa'] = '<img src="' . URL::base() . $file->comercio_imagen->url_path . '" style="width:30px;" />';
							$data[] = $row;
						}

						$column_list = array(
							'id' => array('label' => __('id')),
							'oferta_titulo' => array('label' => __('name')),
							'oferta_descripcion' => array('label' => __('description'), 'sortable' => false),
							'tienda' => array('label' => 'Tienda', 'sortable' => false),
							'comercio_nombre' => array('label' => 'Comercio', 'sortable' => false),
							'img_externa' => array('label' => 'Imagen', 'sortable' => false, 'align'=>'center'),
							'actions' => array('label' => __('actions'), 'sortable' => false)
						);

						$datatable = new Helper_Datatable($column_list, array('paginator' => true, 'class' => 'table table-bordered table-striped', 'sortable' => 'true', 'default_sort' => 'username'));
						$datatable->values($data);

						echo $paging->render();
						echo $datatable->render();
						echo $paging->render();
					?>
				</div>
			</div>
		</div>

	</div>
</div>