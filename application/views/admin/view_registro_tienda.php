<div id="c_admin">
	<div class="container">
		<div class="row-fluid">
			<div class="span3">
				<?php echo View::factory('admin/view_admin_menu'); ?>
			</div>
			<div class="span9">
				<div class="content" style="padding-left: 0;" >

				<!-- Start Content -->
					<div class="breadcrumb">
						<a href="<?php echo URL::site('admin/lineas/tiendas/'.$linea->code); ?>">Tiendas <?php echo $linea->code; ?></a> / <strong>Agregar tienda</strong>
					</div>

					<form id="form_registro_tienda" action="<?php echo URL::site('admin/tiendas/savetienda'); ?>" method="POST" enctype='multipart/form-data' name="form_registro_producto">
						<!-- Start segunda columna -->
						<input type="hidden" name="linea_id" value="<?php echo $linea->id; ?>"/>
						<input type="hidden" name="created" value="<?php echo date('Y-m-d h:m:s'); ?>"/>
						<input type="hidden" name="status" value="<?php echo Quickdev_Status::ACTIVE; ?>"/>
						<h3 class="label_separador">Informacion de la tienda</h3>
						<table width="100%" cellpadding="5">
							<tr>
								<td align="right" valign="top" width="25%">Nombre de la tienda</td>
								<td><input type="text" name="name" /></td>
							</tr>
							<tr>
								<td align="right" valign="top" width="25%">Código</td>
								<td><input type="text" name="code" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">Orden</td>
								<td><input type="text" name="order" class="input-small"></td>
							</tr>
							<tr>
								<td align="right" valign="top">Descripción de la tienda <span class="label">opcional</span></td>
								<td><textarea rows="" cols="" name="desription" class="input-block-level"></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Comisión OVL</td>
								<td valign="top"><div class="input-append"><input type="text" name="comision_ovl" class="span1" style="margin: 0;" /><span class="add-on">%</span></div></td>
							</tr>
						</table>


						<p style="text-align: right;"><button type="submit" class="btn btn-danger">Crear tienda</button> </p>
					</form>
				<!-- End Content -->

				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(initPage);

	function initPage()
	{
		$('.open_galery').click(openGalery);

	}

	function openGalery()
	{
		var target = $(this);
		$.colorbox({href:"<?php echo URL::site('qdmedia/home/snippet'); ?>", width:740, height:700, onComplete:function(){
			qdmedia_onSelect = function(_data, _html){
                //alert(_data);
				$.colorbox.close();
                target.parent().find('input').val(_data);
				target.parent().find('.thumbnail').html('<img src="' + _html.attr('src') + '"/>');
			};
		}});
	}

</script>