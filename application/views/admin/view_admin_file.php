<div style="margin-top: 20px;">
	<ul class="nav nav-tabs">
		<li class="active"><a href="<?php echo URL::site('admin/files'); ?>"><?php echo __('administer.files'); ?></a> </li>
		<li><a href="<?php echo URL::site('admin/files/types'); ?>"><?php echo __('administer.files.types'); ?></a> </li>        
	</ul>
	<div class="tab-content">
		<!-- tab content -->

		<h3><?php echo __('administer.files'); ?></h3>
		<?php
			// format data for DataTable
			$data = array();
			$merge = null;
			$providers = array_filter(Kohana::$config->load('useradmin.providers'));
			foreach ($files as $file) {
				$row = $file->as_array();
				$row['actions'] = Html::anchor('admin_user/edit/' . $row['id'], __('edit')) . ' | ' . Html::anchor('admin_user/delete/' . $row['id'], __('delete'));
				$row['filetype'] = $file->filetype->name;
				$row['preview'] = '<img src="' . URL::base() . $row['url_path'] . '" style="height:28px; margin:-10px -8px -8px -8px;" />';
				$row['url_path'] = '<a href="' . URL::base() . $row['url_path'] . '">' . URL::base() . $row['url_path'] . '</a>';
				$data[] = $row;
			}

			$column_list = array(
						'id' => array('label' => __('id')),
						'name' => array('label' => __('name')),
						'description' => array('label' => __('description'), 'sortable' => false),
						'url_path' => array('label' => __('url_path'), 'sortable' => false),
						'preview' => array('label' => __('preview'), 'sortable' => false),
						'filetype' => array('label' => __('filetype'), 'sortable' => false),
			);

			$column_list['actions'] = array('label' => __('actions'), 'sortable' => false);
			$datatable = new Helper_Datatable($column_list, array('paginator' => true, 'class' => 'table table-bordered table-striped', 'sortable' => 'true', 'default_sort' => 'username'));
			$datatable->values($data);
		
			echo $paging->render();
			echo $datatable->render();
			echo $paging->render();
		?>

		<h3><?php echo __('upload.files'); ?>
			<a class="btn btn-mini" id="btn_show_upload"><span class="icon-chevron-down"></span></a>
			<a class="btn btn-mini" id="btn_hide_upload" style="display: none;"><span class="icon-chevron-up"></span></a>
		</h3>
		<form action="<?php echo URL::site('api/files/save'); ?>" method="post" enctype="multipart/form-data">
			<input type="hidden" name="doredirect" value="true">
			<div class="well" id="block_upload_file" style="display: none;">
				<div class="row">
					<div class="span4">
						<label>Nombre: <input type="text" class="input-xlarge" name="name" /></label>
						<label>Tipo: <select class="input-xlarge" name="filetype_id">
							<?php
								foreach($files_types as $ftype)
								{
									echo '<option value="' . $ftype->id . '">' . $ftype->name . '</option>';
								}
							?>
						</select></label>
						<label>Estado: <select class="input-xlarge" name="status">
							<?php
								foreach(Quickdev_Status::listStatus() as $name => $val)
								{
									echo '<option value="'.$val.'">' . __(Inflector::decamelize($name)) . '</option>';
								}
							?>
						</select></label>
					</div>
					<div class="span7">
						<label>Archivo: <input type="file" name="file_upload" /></label>
						<label>Propietario: <input type="text" class="input-xxlarge" name="user_id" /></label>
						<label>Descripción: <textarea class="input input-xxlarge" name="description" ></textarea></label>
					</div>
				</div>
				<p style="text-align: center;"><button class="btn btn-primary">Subir imagen</button></p>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(initPage);

	function initPage()
	{
		$('#btn_show_upload').click(CLICK_BTN_handler);
		$('#btn_hide_upload').click(CLICK_BTN_handler);

	}

	function CLICK_BTN_handler()
	{
		switch($(this).attr('id'))
		{
			case 'btn_show_upload':
				$('#block_upload_file').slideDown('fast');
				$('#btn_show_upload').hide();
				$('#btn_hide_upload').show();
				break;
			case 'btn_hide_upload':
				$('#block_upload_file').slideUp('fast');
				$('#btn_show_upload').show();
				$('#btn_hide_upload').hide();
				break;
		}
	}
</script>