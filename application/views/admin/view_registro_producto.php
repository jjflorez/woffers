<?php
	if(!isset($producto))	$producto = new Model_Producto();
?>
<div id="c_admin">
	<div class="container">
		<div class="row-fluid">
			<div class="span3">
				<?php echo View::factory('admin/view_admin_menu'); ?>
			</div>
			<div class="span9">
				<div class="content" style="padding-left: 0;" >

				<!-- Start Content -->
					<div class="breadcrumb"><a href="<?php echo URL::site('admin/lineas/productos/'.$linea); ?>">Productos <?php echo $linea; ?></a> / agregar producto / <strong><?php echo $linea; ?></strong></div>

					<form id="form_registro_producto" action="<?php echo URL::site('admin/productos/saveproducto'); ?>" method="POST" name="form_registro_producto">
						<!-- Start segunda columna -->
						<input type="hidden" value="<?php echo $linea; ?>" name="linea">
						<input type="hidden" value="<?php echo $producto->id; ?>" name="id">

						<?php if($linea == 'permanentes'){ ?>
                        	<h3 class="label_separador">Información de la ciudad y categoría</h3>

							<!-- Campos ubicacion -->
                        	<table width="100%" cellpadding="3" border="0">
								<tr>
									<td align="right" valign="top" width="25%">Pais</td>
									<td>
										<select class="ubicacion" name="oferta_ubicacion_pais" id="oferta_ubicacion_pais" target="oferta_ubicacion_provincia">
											<option>Seleccione uno</option>
											<?php foreach($paises as $item){
												$selected = '';
												if($producto->oferta_ubicacion_pais == $item->id) $selected = 'selected="selected"';
												echo '<option '.$selected.'value="' . $item->id .'">' . $item->nombre . '</option>';
											} ?>
										</select>
									</td>
									<td align="right">C.P.</td>
									<td><select><option>Seleccione código postal</option></select></td>
								</tr>
								<tr>
									<td align="right" valign="top" width="25%">Provincia</td>
									<td><select class="ubicacion" name="oferta_ubicacion_provincia" id="oferta_ubicacion_provincia" target="oferta_ubicacion_ciudad" ><option value="<?php echo $producto->oferta_ubicacion_provincia; ?>">Seleccione uno</option></select></td>
                                    <td align="right">Categoría</td>
                                    <td><select name="oferta_categoria_id">
										<option>Seleccione una categoría</option>
										<?php
											foreach($categorias as $cat)
											{
												$selected_cat = '';
												if($producto->oferta_categoria_id == $cat->id) $selected_cat = 'selected="selected"';
												echo '<option '.$selected_cat.' value="' . $cat->id . '">' . $cat->nombre . '</option>';
											}
										?>
                                    </select></td>
								</tr>
								<tr>
									<td align="right" valign="top" width="25%">Ciudad</td>
									<td><select class="ubicacion" name="oferta_ubicacion_ciudad"  id="oferta_ubicacion_ciudad" target="oferta_ubicacion_distrito"><option value="<?php echo $producto->oferta_ubicacion_ciudad; ?>">Seleccione uno</option></select></td>
                                    <td align="right">Precio</td>
                                    <td><select><option >Seleccione un rango</option></select></td>
								</tr>
								<tr>
									<td align="right" valign="top" width="25%">Distrito</td>
									<td><select class="ubicacion" name="oferta_ubicacion_distrito"  id="oferta_ubicacion_distrito" target="oferta_ubicacion_barrio"><option value="<?php echo $producto->oferta_ubicacion_distrito; ?>">Seleccione uno</option></select></td>
								</tr>
								<tr>
									<td align="right" valign="top" width="25%">Barrio</td>
									<td><select class="ubicacion" name="oferta_ubicacion_barrio"  id="oferta_ubicacion_barrio" target=""><option value="<?php echo $producto->oferta_ubicacion_barrio; ?>">Seleccione uno</option></select></td>
								</tr>
							</table>

						<?php }else{ ?>

                        <h3 class="label_separador">Información de la ciudad</h3>
                        <table width="100%" cellpadding="5" border="0">
                            <tr>
                                <td align="right" valign="top" width="25%">Pais</td>
                                <td>
                                    <select class="ubicacion" name="oferta_ubicacion_pais" id="oferta_pais" target="oferta_ciudad">
                                        <option>Seleccione uno</option>
										<?php foreach($paises as $item): ?>
										<option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option>
										<?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top" width="25%">Oferta Nacional</td>
                                <td>
									<label class="radio inline"><input type="radio" name="oferta_nacional" value="1" checked="checked" class="oferta_nacional"/> Si</label>
                                    <label class="radio inline"><input type="radio" name="oferta_nacional" value="0" class="oferta_ciudad"/> No</label>

                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ciudad: <select class="ubicacion" name="oferta_ubicacion_ciudad" id="oferta_ciudad" target="" disabled="disabled" style="margin-bottom: 0;">
                                            <option value="-1">Seleccione la ciudad</option>
                                            <?php foreach($ciudades as $item): ?>
                                                <option value="<?php echo $item['id'] ?>"><?php echo $item['nombre'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                </td>
                            </tr>
                        </table>

						<?php } ?>

						<h3 class="label_separador">Información del comercio</h3>
						<table width="100%" cellpadding="5">
							<tr>
								<td align="right" valign="top" width="25%">Nombre del comercio</td>
								<td><input type="text" name="comercio_nombre" value="<?php echo $producto->comercio_nombre; ?>"></p></td>
							</tr>
							<tr>
								<td align="right" valign="top">Dirección del comercio</td>
								<td><textarea rows="" cols="" name="comercio_direccion" class="input-block-level"><?php echo $producto->comercio_direccion; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Descripción del comercio</td>
								<td><textarea rows="" cols="" name="comercio_descripcion" id="comercio_descripcion" class="input-block-level"><?php echo $producto->comercio_descripcion; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Cargar Logo</td>
								<td>
									<input type="hidden" name="comercio_logo_id" value="<?php echo $producto->comercio_logo_id; ?>">
									<?php if($producto->comercio_logo_id != ''){ ?>
									<div class="thumbnail span4"><img src="<?php echo URL::base() . $producto->comercio_imagen->url_path ?>" /></div>
									<?php }else{ ?>
									<div class="thumbnail span4"></div>
									<?php } ?>
									<a href="javascript:void(0);" class="btn btn-mini open_galery"><span class="icon-camera"></span> Seleccionar imagen</a> </td>
							</tr>
						</table>

						<!-- Separador -->

						<br/>
						<h3 class="label_separador">Información de la oferta</h3>
						<table width="100%" cellpadding="5">
							<tr>
								<td align="right" valign="top" width="25%">Tipo de tienda</td>
								<td>
									<select name="tienda_id" class="inline">
										<!--<option value="">Seleccione una tienda</option>-->
										<?php
											foreach($tiendas as $tienda){
												if($producto->tienda_id == $tienda->id)
													echo '<option selected="selected" value="'.$tienda->id.'">'.$tienda->name.'</option>';
												else
													echo '<option value="'.$tienda->id.'">'.$tienda->name.'</option>';
											}
										?>
									</select>
									&nbsp;&nbsp;&nbsp;
									Tipo de oferta <select name="oferta_tipo" class="inline span3" readonly="readonly">
										<?php

											switch($linea)
											{
												case 'offers':
												case 'travel':
												case 'coaching':
												case 'permanentes':
													echo '<option value="cupon" selected="selected">Cupón</option>';
													break;
												case 'outlet':
													echo '<option value="producto" selected="selected">Cupón de producto</option>';
													break;
											}

										?>
										<option value="">Seleccione uno</option>
										<option value="producto" <?php echo ($linea==''); ?>>Producto</option>
										<option value="cupon">Cupón</option>
										<option value="cupon_producto">Cupón de producto</option>
									</select>
								</td>
							</tr>

							<tr>
								<td align="right" valign="top">Titulo de la oferta</td>
								<td><input type="text" maxlength="95" name="oferta_titulo" value="<?php echo $producto->oferta_titulo; ?>" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">Destacamos</td>
								<td><textarea rows="" cols="" name="oferta_destacamos" class="input-block-level"><?php echo $producto->oferta_destacamos; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Que incluye la oferta</td>
								<td><textarea rows="" cols="" name="oferta_que_incluye" class="input-block-level"><?php echo $producto->oferta_que_incluye; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Descripción de la oferta</td>
								<td><textarea rows="" cols="" name="oferta_descripcion" class="input-block-level"><?php echo $producto->oferta_descripcion; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Condiciones</td>
								<td><textarea rows="" cols="" name="oferta_condiciones" class="input-block-level"><?php echo $producto->oferta_condiciones; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Ubicación</td>
								<td valign="middle">
									Latitud: <input type="text" name="oferta_ubicacion_lat" class="input-small" value="<?php echo $producto->oferta_ubicacion_lat; ?>" />&nbsp;&nbsp;&nbsp;
									Longitud: <input type="text" name="oferta_ubicacion_lon" class="input-small" value="<?php echo $producto->oferta_ubicacion_lon; ?>" />
								</td>
							</tr>

							<!-- Inicio despleagables -->
							<!--
							<tr>
								<td align="right" valign="top">&nbsp;</td>
								<td><h4>Desplegable 1</h4></td>
							</tr>
							<tr>
								<td align="right" valign="top">Titulo desplegable</td>
								<td><input type="text" name="" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">Opción 1</td>
								<td><input type="text" name="" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">Opción 2</td>
								<td><input type="text" name="" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">&nbsp;</td>
								<td><h4>Desplegable 2</h4></td>
							</tr>
							<tr>
								<td align="right" valign="top">Titulo desplegable</td>
								<td><input type="text" name="" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">Opción 1</td>
								<td><input type="text" name="" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">Opción 2</td>
								<td><input type="text" name="" /></td>
							</tr>
							-->
							<!-- End  despleagables ############################# -->


							<tr>
								<td align="right" valign="top">Imagen externa</td>
								<td>
									<input type="hidden" name="oferta_imagen_id" value="<?php echo $producto->oferta_imagen_id; ?>">
									<div class="thumbnail span4"></div>
									<a href="javascript:void(0);" class="btn btn-mini open_galery"><span class="icon-camera"></span> Seleccionar imagen</a>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">Imagenes internas</td>
								<td>
									<div id="lista_externas">
										<?php
											foreach($producto->productofile->find_all() as $file)
											{
												echo '<div>
														<input type="hidden" name="imagen_externa[]" value="' . $file->file_id . '">
														<div class="thumbnail span4"><img src="' . URL::base() . $file->file->url_path . '" /></div>
														<a href="javascript:void(0);" class="btn btn-mini open_galery"><span class="icon-camera"></span> Seleccionar imagen</a> <a href="#" class="">&times;</a>
													</div>';
											}
										?>
										<div>
											<input type="hidden" name="imagen_externa[]" value="">
											<div class="thumbnail span4"></div>
											<a href="javascript:void(0);" class="btn btn-mini open_galery"><span class="icon-camera"></span> Seleccionar imagen</a> <a href="#" class="">&times;</a>
										</div>
									</div>
									<a href="javascript:void(0);" class="btn btn-mini btn-success add_image"><span class="icon-plus-sign icon-white"></span> Agregar imagen</a>
								</td>
							</tr>

						</table>
						<!-- Tercera tabla -->
						<br/>
						<h3 class="label_separador">Información de descuento</h3>
						<table width="100%" cellpadding="5">
							<tr>
								<td align="right" valign="top" width="25%">Comisión OVL:</td>
								<td valign="top">
									<?php
										$valor_comision = $producto->oferta_comision_ovl;
										$input_attr = '';
										if($producto->oferta_comision_ovl == -1 || $producto->oferta_comision_ovl == ''){
											$valor_comision = $producto->tienda->comision_ovl;
											$input_attr = 'disabled="disabled"';
										}
									?>
									<input rel="tooltip" title="Para cambiar click en editar, caso contrario se usará el valor de la tienda" id="oferta_comision_ovl" type="text" <?php echo $input_attr; ?> name="oferta_comision_ovl" class="input-small" value="<?php echo $valor_comision; ?>" /> <button class="btn btn-mini activar_descuento" type="button"><span class="icon-edit"></span> Editar</button>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">Precio VP:</td><td><input type="text" name="oferta_precio_venta" id="oferta_precio_venta" class="input-small" value="<?php echo $producto->oferta_precio_venta; ?>" /> €</td>
							</tr>
							<tr>
								<td align="right" valign="top">Descuento OVL:</td><td><input type="text" name="oferta_descuento" id="oferta_descuento" class="input-small" value="<?php echo $producto->oferta_descuento; ?>" /> %</td>
							</tr>
							<tr>
								<td align="right" valign="top">Precio OVL:</td><td><span class="badge badge-warning oferta_precio_ovl">0€</span></td>
							</tr>
							<tr>
								<td align="right" valign="top">Ahorro:</td><td><span class="badge badge-warning oferta_ahorro">0€</span></td>
							</tr>
						</table>


						<h3 class="label_separador">Información del tiempo y periodo de validez</h3>
						<table width="100%" cellpadding="5">
							<tr>
								<td align="right" valign="middle" width="25%">Tiempo de la oferta:</td>
								<td>
									<div class="row-fluid">
										<div class="span6">
											Fecha Inicio<br/>
											<input type="text" class="t" name="oferta_publicacion" id="oferta_publicacion" value="<?php echo ($producto->oferta_publicacion) ? date('d/m/Y', strtotime($producto->oferta_publicacion)) : date('d/m/Y');  ?>" />
										</div>
										<div class="span6">
											Fecha Finalización<br/>
											<input type="text" class="" name="oferta_duracion" id="oferta_duracion" value="<?php echo ($producto->oferta_duracion) ? date('d/m/Y', strtotime($producto->oferta_duracion)) : date('d/m/Y'); ?>" />
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">Periodo de validez:</td>
								<td>
									<div class="row-fluid">
										<div class="span6">
											<?php if($linea == 'permanentes') {?> <label class="checkbox"><input type="checkbox" class="inline" /> Fecha limite consumo</label> <?php } ?>
											<input type="text" class="t" name="tiempo_validez" id="tiempo_validez" value="<?php echo ($producto->tiempo_validez) ? date('d/m/Y', strtotime($producto->tiempo_validez)) : date('d/m/Y'); ?>" />
										</div>
										<?php if($linea == 'permanentes') {?>
										<div class="span6">
											<label class="checkbox"><input type="checkbox" class="checkbox inline" /> Tiempo para consumir</label>
											<input type="text" class="t" name="tiempo_consumir" id="tiempo_consumir" value="<?php echo date('d/m/Y', strtotime($producto->tiempo_validez)); ?>" />
										</div>
										<?php } ?>
									</div>
								</td>
							</tr>

						</table>
						<?php if($producto->id == ''){?>
						<p style="text-align: right;"><button type="submit" class="btn btn-danger">Crear oferta</button> </p>
						<?php }else{ ?>
						<p style="text-align: right;"><button type="submit" class="btn btn-danger">Guardar cambios</button> </p>
						<?php } ?>
					</form>
				<!-- End Content -->

				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

    $(document).ready(initPage);
    var _param = '<?php echo Kohana_Request::$current->param('param1') ?>';

	var rules = {
		'comercio_nombre' : {required:true, minlength:2},
		'comercio_direccion' : {required:true, minlength:2},
		'comercio_descripcion' : {required:true, minlength:2},
		'tienda_id': 'required',
		'oferta_titulo': {required:true, minlength:2},
		'oferta_destacamos': {required:true, minlength:2},
		'oferta_que_incluye': {required:true, minlength:2},
		'oferta_descripcion': {required:true, minlength:2},
		'oferta_condiciones': {required:true, minlength:2},
		'oferta_ubicacion': 'required',
		'oferta_imagen_id': 'required',
		'oferta_comision_ovl': {number:true, max:100},
		'oferta_precio_venta': {number:true, required:true},
		'oferta_descuento': {number:true, required:true, max:100},
		'oferta_duracion': 'required',
		'tiempo_validez': 'required'
	};

	function initPage()
	{
		$('.open_galery').live('click', openGalery);
		$('.add_image').click(addImage);

		var meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

		$('#tiempo_validez').datepicker({dateFormat:'dd/mm/yy', minDate:0, monthNames:meses});
		$('#oferta_duracion').datepicker({dateFormat:'dd/mm/yy', minDate:0, monthNames:meses});
		$('#oferta_publicacion').datepicker({dateFormat:'dd/mm/yy',  monthNames:meses});
		$('.activar_descuento').click(activarComisionOVL);

		$('#form_registro_producto').validate({rules: rules,invalidHandler:null,submitHandler: onSubmitForm	});

		CKEDITOR.replace( 'comercio_descripcion');
		CKEDITOR.replace( 'oferta_que_incluye');
		CKEDITOR.replace( 'oferta_descripcion');
		CKEDITOR.replace( 'oferta_condiciones');
		CKEDITOR.replace( 'comercio_direccion');

		$('#oferta_descuento').keyup(updateMontos);
		$('#oferta_precio_venta').keyup(updateMontos);

        $('.radio_oferta').click(checkOferta);
        $('#ciudad').change(changeCiudad);

		$('.oferta_nacional').click(CLICK_UBICACION_handler);
		$('.oferta_ciudad').click(CLICK_UBICACION_handler);

		$('.ubicacion').change(CHANGE_SELECT_handler);
	}

	function CHANGE_SELECT_handler()
	{
		updateListUbicacion($(this).val(), '#' + $(this).attr('target'));
	}

	function addImage()
	{
		var html = '<div>';
		html += '<div class="thumbnail span4"></div>';
		html += '<input type="hidden" name="imagen_externa[]" value="">';
		html += '<a href="javascript:void(0);" class="btn btn-mini open_galery"><span class="icon-camera"></span> Seleccionar imagen</a> <a href="#" class="">&times;</a>';
		html += '</div>';

		$('#lista_externas').append(html);
	}

	function CLICK_UBICACION_handler()
	{
		var target = $(this);

		switch (target.attr('class'))
		{
			case 'oferta_nacional':
				$('#oferta_ciudad').attr('disabled', 'disabled');
				break;
			case 'oferta_ciudad':
				$('#oferta_ciudad').removeAttr('disabled');
				break;
		}
	}

	function updateMontos()
	{
		if($('#oferta_descuento').val() != '' && $('#oferta_precio_venta').val() != '')
		{
			var ahorroOVL = ($('#oferta_precio_venta').val() / 100 ) * $('#oferta_descuento').val();
			var precioFinal = Math.round($('#oferta_precio_venta').val() - ahorroOVL);
			$('.oferta_ahorro').text(($('#oferta_precio_venta').val() - precioFinal) + '€');
			$('.oferta_precio_ovl').text(precioFinal.toString() + '€');
		}
	}

	function onSubmitForm(form)
	{
		form.submit();
	}

	function activarComisionOVL()
	{
		if($('#oferta_comision_ovl').attr('disabled') == 'disabled'){
			$('#oferta_comision_ovl').removeAttr('disabled');
			$('#oferta_comision_ovl').focus();
			$(this).html('<span class="icon-minus"></span> Desactivar');
		}else{
			$('#oferta_comision_ovl').attr('disabled', 'disabled');
			$(this).html('<span class="icon-edit"></span> Editar');
		}
	}

	function openGalery()
	{
		var target = $(this);
		$.colorbox({href:"<?php echo URL::site('qdmedia/home/snippet'); ?>", width:740, height:590, onComplete:function(){
			qdmedia_onSelect = function(_data, _html){
                //alert(_data);
				$.colorbox.close();
                target.parent().find('input').val(_data);
				target.parent().find('.thumbnail').html('<img src="' + _html.attr('src') + '"/>');
			};
		}});
		return false;
	}

	function toogleContent(_id)
	{
		if($(_id).attr('display') == 'none')
			$(_id).slideDown();
		else
			$(_id).slideUp();
	}

    function checkOferta(){

        if($(this).val() == 0){
            $('#sel_ciudad').show();
        }else{
            $('#sel_ciudad').hide();
        }
    }

	function updateListUbicacion($_id, $_target)
	{
        if(_param == 'permanentes'){
            _url = 'api/ubicaciones/get/';
        }else{
            _url = 'api/ubicaciones/getciudades/';
        }
		qd.core.LoadData.load(_url + $_id, null, function(_data){
			if(_data.data.length > 0)	$($_target).html('<option value="-1">Seleccionar una opción</option>');
			else						$($_target).html('<option value="-1">No hay resultados</option>');
			for(i in _data.data)
			{
				var it = _data.data[i];
				$($_target).append('<option value="' +  it.id + '">' + it.nombre + '</option>');
			}
		})
	}

    function changeCiudad(){
        //alert($(this).val());
    }

</script>