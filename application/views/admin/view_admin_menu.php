		<?php
			$param = Kohana_Request::$current->param('param1');
		?>
		<h3 style="text-align: center;">Lineas</h3>
		<ul class="nav nav-list nav-stacked">
			<li class="<?php echo $param=='offers'?'active':''; ?>"><a href="<?php echo URL::site('admin/lineas/productos/offers'); ?>">OVL Offers</a></li>
			<li class="<?php echo $param=='outlet'?'active':''; ?>"><a href="<?php echo URL::site('admin/lineas/productos/outlet'); ?>">OVL Outlet</a></li>
			<li class="<?php echo $param=='travel'?'active':''; ?>"><a href="<?php echo URL::site('admin/lineas/productos/travel'); ?>">OVL Travel</a></li>
			<li class="<?php echo $param=='coaching'?'active':''; ?>"><a href="<?php echo URL::site('admin/lineas/productos/coaching'); ?>">OVL Coaching</a></li>
			<li class="divider"></li>
			<li class="<?php echo $param=='permanentes'?'active':''; ?>"><a href="<?php echo URL::site('admin/lineas/productos/permanentes'); ?>">Ofertas permanentes</a></li>
			<li class="divider"></li>
			<li class="<?php echo $param==''?'active':''; ?>"><a href="<?php echo URL::site('admin/lineas/'); ?>">Gestión de nuestras lineas</a></li>
			<li class="<?php echo $param==''?'active':''; ?>"><a href="<?php echo URL::site('admin/pedidos'); ?>">Gestión de pedidos</a></li>
		</ul>