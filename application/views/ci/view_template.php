<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset=utf-8 />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>
		<?php
			if(isset($title))
				echo $title . ' :: ' . 'OVL';
			else
				echo 'OVL :. Las mejores ofertas en tu ciudad';
		?>
	</title>
	<?php
		echo "\t" . Html::style('public/css/bootstrap.css') . "\n";
		//echo "\t" . Html::style('public/css/bootstrap-responsive.css') . "\n";
		echo "\t" . Html::style('public/css/admin.css') . "\n";
		echo "\t" . Html::style('public/css/style.css') . "\n";
		echo "\t" . Html::style('public/css/dp/datepicker.css') . "\n";
		echo "\t" . Html::style('public/css/humanity/jquery-ui-1.8.23.custom.css') . "\n";

		echo "\t" . Html::script('public/js/jquery-1.8.0.min.js') . "\n";
		echo "\t" . Html::script('public/js/jquery-ui-1.8.23.custom.min.js') . "\n";
		echo "\t" . Html::script('public/plugins/qd.core.js') . "\n";
		echo "\t" . Html::script('public/js/bootstrap.js') . "\n";


        echo "\t" . Html::script('public/js/list_appearance.js') . "\n";

		echo "\t" . Html::script('public/js/bootstrap.js') . "\n";
		echo "\t" . Html::script('public/js/swfobject.js') . "\n";
		echo "\t" . Html::script('public/plugins/jquery.validate.js') . "\n";
        echo "\t" . Html::script('public/plugins/jquery.zrssfeed.js') . "\n";

		//echo "\t" . Html::script('public/plugins/fb_lf.js') . "\n";
		//echo "\t" . Html::script('public/js/dp/datepicker.js') . "\n";
		echo "\t" . Html::script('public/plugins/uploader/fileuploader.js') . "\n";
		echo "\t" . Html::script('public/plugins/ckeditor/ckeditor.js') . "\n";

		echo "\t" . Html::script('public/plugins/qd.core.js') . "\n";
		echo "\t" . Html::script('public/plugins/qd.app.js') . "\n";

		echo "\t" . Html::script('public/plugins/colorbox/jquery.colorbox-min.js') . "\n";
		echo "\t" . Html::style('public/plugins/colorbox/colorbox.css') . "\n";

		if(isset($header))	echo $header;

	//<script src="//connect.facebook.net/en_US/all.js"></script>

	?>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>	<![endif]-->
	<!--[if lt IE 9]>		<?php echo "\t" . Html::style('public/css/appFaces_IE.css') . "\n"; ?>	<![endif]-->

	<link rel="shortcut icon" href="<?php echo URL::base(); ?>public/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo URL::base(); ?>ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo URL::base(); ?>ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="<?php echo URL::base(); ?>ico/apple-touch-icon-57-precomposed.png">
	<script type="text/javascript">
		var  Config = new Object();
		Config.Path = "<?php echo URL::base(); ?>";
		Config.Site = "<?php echo URL::site(); ?>";
	</script>
</head>
<body>
	<div id="fb-root"></div>

	<!-- Header web site -->
	<div id="header">
		<!-- Start Session top bar -->
		<div class="block_top_bar">
			<div class="container">
				<div class="block_info_usuario pull-left">
					<?php if(Auth::instance()->logged_in()){ ?>
						<span class="pull-right"><a href="#">¿No eres <?php echo Kohana_Auth::instance()->get_user()->name; ?>?</a> </span>
						Bienvenido <strong><?php echo Kohana_Auth::instance()->get_user()->name . ' ' . Kohana_Auth::instance()->get_user()->last_name; ?></strong> Socio Oro
					<?php }else{ ?>
						Bienvenido, inicia sesión usando el boton de la derecha
					<?php } ?>
				</div>
				<div class="pull-right">
					<a href="#">Oficina Virtual</a>
					<?php if(Auth::instance()->logged_in()){ ?>
						<a href="<?php echo URL::site('user/logout'); ?>">Logout</a>
					<?php }else{ ?>
						<a href="<?php echo URL::site('user/login'); ?>">Login</a>
					<?php } ?>

					<a href="#">Contactos</a> |	<a href="#">País</a>
				</div>
			</div>
		</div>
		<!-- End Session top bar -->

		<?php $current_line = Kohana_Request::current()->controller(); ?>

		<div class="block_header">
			<div class="container">

                <div class="selecciona_pais">

                    <h3 class="select-pais-<?php echo $current_line; ?>"><?php if(isset($provincias_selected)) echo $provincias_selected->nombre; ?></h3>

					<div id="list_paises">
						<a id="a_list_paises" href="javascript:void(0);"><img src="<?php echo URL::base();?>public/img/flecha_ciudades.jpg ">Selecciona tu ciudad</a>

						<div id="cont_desp_cuid">
							<div id="desp_ciud_sup">
								<ul>
									<?php
										if(isset($provincias_selected))
										{
											foreach($lista_ciudades as $item)
											{
												echo '<li><a href="' . URL::site('home/set_ciudad/' . $item->id) . '">' . $item->nombre . '</a></li>';
											}
										}
									?>
								</ul>
							</div>
						</div>
					</div>
                </div>
				<!--<span class="abajo select-pais-<?php echo $current_line; ?>">Barcelona</span>-->

				<a href="<?php echo URL::site($current_line); ?>"><h1 class="logo logo-<?php echo $current_line; ?> pull-left ">OneVisionLife.com</h1></a>



				<div class="navegacion-line">
						<a class="n1 <?php echo $current_line=='offers'?'active':''; ?>" href="<?php echo URL::site('offers'); ?>"></a>
						<a class="n3 <?php echo $current_line=='travel'?'active':''; ?>" href="<?php echo URL::site('travel'); ?>"></a>
						<a class="n2 <?php echo $current_line=='outlet'?'active':''; ?>" href="<?php echo URL::site('outlet'); ?>"></a>
						<a class="n4 <?php echo $current_line=='coaching'?'active':''; ?>" href="<?php echo URL::site('coaching'); ?>"></a>
						<a class="n5 <?php echo $current_line=='village'?'active':''; ?>" href="<?php echo URL::site('village'); ?>"></a>
						<a class="n6 <?php echo $current_line=='home'?'active':''; ?>" href="<?php echo URL::site('home'); ?>"></a>

				</div>
			</div>
		</div>
	</div>


	<!-- End web site -->


	<div id="content">

		<!-- Start carrito-->
		<div class="container">
			<div id="content_carrito">
			<ul>
				<li class="boton01">
					<a id="abrir_carrito" href="javascript:void(0);"></a>
					<div class="content_mini_cesta"></div>
				</li>

                <?php if($current_line<>'home'){; ?>
				<li class="boton02">
					<a href="javascript:void(0);"></a>
				</li>
				<li class="boton03">
					<a href="javascript:void(0);"></a>
				</li>
                <?php }?>

			</ul>
		</div>
		</div>
		<!--End carrito-->

		<!-- content -->
		<?php	if(isset($content)){echo '<div class="container">' . View::factory('useradmin/messages') . '</div>' . $content;}	?>
		<!-- end content -->
	</div>

	<!-- Header web site -->
	<div id="footer">
		<ul>
			<li >
				<ul>

					<li id="noticias_destacadas">
						<h2>Noticias destacadas</h2>
						<span>
							<img class="img_rss1">
							<a target="_blank">
                                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of lettersh.
							</a>

						</span>
						<span>
							<img>
							<a target="_blank">
                                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of lettersh.
							</a>

						</span>
						<span>
							<img>
							<a target="_blank">
                                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of lettersh.
							</a>

						</span>

						<a id="btn_visita_blog" href="javascript:void(0);"></a>


					</li>


					<li class="ultimo">
						<h2>¿tienes alguna duda?</h2>
						<span>
							Escribemos para resolver tus preguntas y nuestro equipo te contactará a la brevedad.
						</span>
						<a id="btn_contactanos" href="javascript:void(0);"></a>
						<h2>Conexión social</h2>
						<span>
							Siguenos en toda la red y encuentra todas nuestras novedades, noticias, promociones, ofertas y participa en nuestros concursos.
						</span>

						<div class="btn_footer_redes">
						<a  href="https://www.facebook.com/pages/One-Vision-Life/162556580460156">
							<img src="http://img.onevisionlife.com/home_coorp/footer_face.jpg">
						</a>
						<a  href="http://twitter.com/#!/OneVisionLife">
							<img src="http://img.onevisionlife.com/home_coorp/footer_youtube.jpg">
						</a>
						<a  href="http://www.youtube.com/user/onevisionlife">
							<img src="http://img.onevisionlife.com/home_coorp/footer_twiter.jpg">
						</a>
						<a  href="http://ovlnews.com/">
							<img src="http://img.onevisionlife.com/home_coorp/footer_rss.jpg">
						</a>
						</div>

							<script>(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) {return;}
								js = d.createElement(s); js.id = id;
								js.src = "//connect.facebook.net/en_US/all.js#appId=160691790683765&xfbml=1";
								fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>

							<div id="face_ggg" class="fb-like" data-href="https://www.facebook.com/pages/One-Vision-Life/162556580460156" data-send="false" data-layout="button_count" data-width="80" data-show-faces="false">
							</div>

						<div id="btn_siguenos_twiter">
								<a href="https://twitter.com/OneVisionLife" class="twitter-follow-button" data-show-count="false">Follow @OneVisionLife</a>
						</div>
						<script src="//platform.twitter.com/widgets.js" type="text/javascript"></script>
					</li>
				</ul>
			</li>

			<li class="row02">
				<ul>
					<li>
						<h2>Nosotros</h2>
						<a href="javascript:void(0);">> Empresa</a>
						<a href="javascript:void(0);">> Qué es One Vision Life</a>
						<a href="javascript:void(0);">> Cómo funciona One Vision Life</a>
					</li>
					<li>
						<h2>Descubre</h2>
						<a href="javascript:void(0);">> Tipos de socios</a>
						<a href="javascript:void(0);">> Gana con nosotros</a>
						<a href="javascript:void(0);">> Area empresas</a>
					</li>
					<li>
						<h2>Visítanos</h2>
						<a href="javascript:void(0);">> www.ovlnews.com</a>
						<a href="javascript:void(0);">> www.disfrutadelcoaching.com</a>
						<a href="javascript:void(0);">> www.disfrutadelocio.com</a>
					</li>
					<li>
						<h2>Recomendamos</h2>
						<a href="javascript:void(0);">> Agencia virtual de viajes</a>
						<a href="javascript:void(0);">> ​Resorts</a>
						<a href="javascript:void(0);">> Promocion del mes</a>
					</li>
				</ul>
				<p>
					<a><img src="http://img.onevisionlife.com/ovl_new/mc.jpg"></a>
					<a><img src="http://img.onevisionlife.com/ovl_new/visa.jpg"></a>
					<a><img src="http://img.onevisionlife.com/ovl_new/cirrus.jpg"></a>
					<a><img src="http://img.onevisionlife.com/ovl_new/azu.jpg"></a>
					<a><img src="http://img.onevisionlife.com/ovl_new/paypal.jpg"></a>
				</p>


			</li>
			<li class="row03">
				Copyright © One Vision Life 2012. Todos los derechos reservados. <a href="javascript:void(0);">Condiciones de uso</a> y <a href="javascript:void(0);">política de privacidad</a>
			</li>

		</ul>

	</div>
	<!-- End web site -->
    <div style="display:none;" id="ticker1"></div>

<script type="text/javascript">

    var _controller = '<?php echo Kohana_Request::$current->controller(); ?>';

	$(document).ready(initMain);

	var cestaIsOpen = false;

    function initMain(){

        $('#content_carrito .boton01 #abrir_carrito').click(qd.app.MiniCesta.toggleCesta);
        $('#content_carrito .boton01 #cierre_carrito').live('click', qd.app.MiniCesta.toggleCesta);

		qd.app.MiniCesta.updateCesta(false);
        noticias_destacadas();

        $('#ciudad').change(changeCiudad);
		$('#a_list_paises').click(function(){
			$('#cont_desp_cuid').toggle('fast');
		});
    }

    function changeCiudad(){
        var _url = Config.Site + "home/set_ciudad/" + $(this).val() + "/" + _controller;
        window.location = _url;

        //alert(Config.Site + _controller + "/index/" + $(this).val());
        //alert(_url);
    }


    function noticias_destacadas() {
        var count_img = 1;
        var count_p = 0;
        var count_txt_rss = "";
        var url_txt_rss;
        $('#ticker1').rssfeed('http://ovlnews.com/?feed=atom', {
            snippet:false,
            limit:3
        }, function (e) {
            $('.rssBody ul li').each(function () {

                if ($(this).find('img').attr('src') == undefined) {
                    $('.img_rss' + count_img).attr('src', 'http://img.onevisionlife.com/ovl_new/default_rss.jpg');
                    count_img++;
                }
                else {
                    $('.img_rss' + count_img).attr('src', $(this).find('img').attr('src'));
                    count_img++;
                }
                $(this).find('p').each(function () {
                    count_p++;
                    if (count_p < 4) {
                        count_txt_rss += $(this).text();
                    }
                });
                url_txt_rss = $(this).find('h4').find('a').attr('href');
                $('.txt_rss' + (count_img - 1)).html('<a target="_blank" href="' + url_txt_rss + '">' + count_txt_rss.substr(0, 250) + '</a>');
                count_p = 0;
                count_txt_rss = "";
                url_txt_rss = 0;
            });
        });
    }

</script>
<?php //echo Kohana_Request::$current->controller(); ?>
</body>
</html>
