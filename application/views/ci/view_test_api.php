<div class="container-fluid">
	<form action="index.php" id="form_test" class="form-inline">
		<p style="text-align: center;" class="pull-right"><button type="submit" class="btn btn-danger">Ejecutar!</button></p>
		<h2>Probar API</h2>
		<div class="well">

				<p><label>URL: <input type="text" id="url" name="url" class="input-xxlarge" /></label>
				<label>Token: <input type="text" id="token" name="token" class="input-xlarge" /></label></p>
				<p style="font-weight: bold;">Parametros <a href="javascript:void(0);" class="btn btn-mini" id="btn_add_param"><span class="icon-plus-sign"></span> Agregar parametro</a></p>
				<div class="lista_parametros">
				</div>

			<hr/>
			<p style="font-weight: bold;">Parametros enviados:</p>
			<div id="params_panel">
				<pre style="background: #FFF !important;"></pre>
			</div>
			<p style="font-weight: bold;">Respuesta:</p>
			<div id="response_panel">
				<pre style="background: #FFF !important;"></pre>
			</div>
		</div>
	</form>
</div> <!-- /container -->


<script type="text/javascript">

	$(document).ready(initPage);

	function initPage()
	{
		$('#form_test').submit(SUBMIT_FORM_handler);
		$('#btn_add_param').click(CLICK_handler);
		$('.btn_del_param').live('click', CLICK_DEL_handler);
		$('#response_panel').ajaxError(onAPILoadError);
	}

	function CLICK_DEL_handler()
	{
		$(this).parent().remove();
		return false;
	}

	function SUBMIT_FORM_handler()
	{
		var params = new Object();
		params.token = $('#token').val();

		$('.lista_parametros p').each(function(){
			params[$(this).find('.param_name').val()] = $(this).find('.param_value').val();
		});

		qd.core.LoadData.load($('#url').val(), params, function(_data, _params)
		{
			$('#response_panel pre').html(qd.app.output(_data));
			$('#params_panel pre').html(qd.app.output(_params));
		}, onAPILoadError);
		return false;
	}

	function CLICK_handler()
	{
		switch($(this).attr('id'))
		{
			case 'btn_add_param':
				$('.lista_parametros').append('<p><a href="javascript:void(0);" class="btn_del_param"><span class="icon-minus-sign"></a> <input type="text" class="param_name input" /> : <input type="text" class="param_value input" /></label></p>');
				break;
		}

		return false;
	}

	function onAPILoadError(_data, _params)
	{
		alert('Error con la solicitud');
		/*$('#response_panel pre').html(qd.app.output(_data));
		$('#params_panel pre').html(qd.app.output(_params));*/
	}

</script>