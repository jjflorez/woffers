
<?php
$controlador = Kohana_Request::$current->controller();
?>
<div id="c_<?php echo $controlador; ?>" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
    <div id="container_lineas">
        <ul id="pestanas_lineas">
            <?php
            foreach($tiendas as $t)
            {
                if($t->code == Kohana_Request::$current->param('param1'))	echo '<li class="selected"><a href="'. URL::site($controlador.'/tienda/' . $t->code) .'">' . $t->name . '</a></li>';
                else			echo '<li><a href="'. URL::site($controlador.'/tienda/' . $t->code) .'">' . $t->name . '</a></li>';
            }
            ?>
        </ul>
    </div>

	<!-- Inicio contenedor -->

    <div class="container">
        <div id="colum_produc_a">

			<!-- Inicio filtros -->

			<?php if($controlador !="outlet"){ ?>

			<div class="filtros_ofertas">
				<form id="form_filtros" style="margin: 0;" method="get" action="<?php echo URL::site('offers/tienda/permanente') ?>">
				<table width="100%" border="0">
					<tr>
						<td colspan="2" class="titulo">Filtrar ofertas</td>
						<td colspan="2"><input class="buscador_ofertas" name="texto_buscar" type="text"></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
					</tr>
					<tr>
						<td>Provincia</td>
						<td><div class="select_list" id="oferta_ubicacion_provincia"></div></td>
                        <td>Distrito</td>
                        <td><div class="select_list" id="oferta_ubicacion_distrito"></div></td>
                        <td>Categoría</td>
                        <td><div class="select_list" id="oferta_categoria_id"></div></td>
					</tr>
                    <tr>
                        <td>Ciudad</td>
                        <td><div class="select_list" id="oferta_ubicacion_ciudad"></div></td>
                        <td width="57">Barrio</td>
                        <td><div class="select_list" id="oferta_ubicacion_barrio"></div></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
				</table>
				</form>
			</div>

            <!-- Fin filtros -->

            <?php }else{ ?>
				<li>
					<h2>Filtra las ofertas:</h2>
					<a href="javascript:void(0);" class="list_op_busqueda">
						<input id="categoria" type="hidden" >
					</a>
					Categoria

				</li>
				<li class="second">
					<a href="javascript:void(0);" class="list_op_busqueda">
						<input id="precio" type="hidden" >
					</a>
					Precio

				</li>
				<li>
					<div class="buscador_op">
						<input id="txt_busc" type="text">
						<a href="javascript:void(0);"></a>
					</div>
					<a href="javascript:void(0);" class="list_op_busqueda">
						<input id="ordenar" type="hidden" >
					</a>
					Ordenar

				</li>
			<?php }?>

			<!-- Listado de productos -->

			<?php
				if(count($productos) == 0) echo '<p class="alert alert-danger" style="margin:0 10px;"><span class="icon-comment"></span> No se encontraron productos con estos filtros, prueba otra combinación</p>';
			?>
            <ul class="lista_productos_general cont_<?php echo $controlador; ?> permanentes_general">
                <?php
                foreach($productos as $p)
                {
                    ?>
                    <li>
                        <ul>
                            <li>
                                <span class="dto"><?php echo $p->oferta_descuento; ?>% Dto.</span>
                                <span class="descripcion"><?php
                                    $text_cortado=90;
                                    if($controlador=="outlet"){
                                        $text_cortado=49;
                                    }
                                    echo $p->get_txtmuestra($p->oferta_titulo,$text_cortado);?></span>


                            </li>
                            <li class="img">
                                <span></span>
                                <div>
                                    <img border="0" alt="" src="<?php echo URL::base() . $p->oferta_imagen->url_path; ?>">
                                </div>
                            </li>
                            <li class="caracteristicas">
                                <ul>
                                    <li>
										<span class="ver_plan">
											<span class="prec">
												<span class="prec_2"><?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
												</span>
                                                <?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
											</span>
											<a href="<?php echo URL::site($controlador.'/producto/' . $p->id ); ?>"></a>
										</span>
                                        <span>Precio:</span>
                                        <br/>
                                        <span class="precio"><?php echo $p->oferta_precio_venta; ?>€</span>
                                    </li>

                                    <li>
										<span class="redes">
                                                <?php if($controlador!="outlet"){?>
                                                <span id="first">Restaurante La Lluna</span>
                                                <?php }?>
												<a class="twiter"></a>
												<a class="mail"></a>
												<a class="megusta"></a>
                                            <span id="alerta_producto_op">
                                                ¡ÚLTIMOS 10<br/> CUPONES!
                                            </span>
										</span>

                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>


                    <?php
                }
                ?>
            </ul>
        </div>
        <div id="colum_produc_b">
            <?php echo View::factory('ci/view_sidebar')->set('var_sidebar','')?>
        </div>
    </div>
</div>

<?php
	$catJSON = array();
	foreach($categorias as $cat) array_push($catJSON, $cat->as_array());

	$provJSON = array();
	foreach($provincias as $pr)	array_push($provJSON, $pr->as_array());

	$ciudadJSON = array();
	foreach($ciudades as $ci)	array_push($ciudadJSON, $ci->as_array());

	$distritoJSON = array();
	foreach($distritos as $di)	array_push($distritoJSON, $di->as_array());

	$barrioJSON = array();
	foreach($barrios as $ba)	array_push($barrioJSON, $ba->as_array());
?>

<script type="text/javascript">
    $(document).ready(init);

	var filterValues = new Array();
    filterValues['oferta_categoria_id'] = '<?php echo isset($_GET['oferta_categoria_id'])?$_GET['oferta_categoria_id']:''; ?>';
    filterValues['oferta_ubicacion_provincia'] = '<?php echo isset($_GET['oferta_ubicacion_provincia'])?$_GET['oferta_ubicacion_provincia']:''; ?>';
    filterValues['oferta_ubicacion_ciudad'] = '<?php echo isset($_GET['oferta_ubicacion_ciudad'])?$_GET['oferta_ubicacion_ciudad']:''; ?>';
    filterValues['oferta_ubicacion_distrito'] = '<?php echo isset($_GET['oferta_ubicacion_distrito'])?$_GET['oferta_ubicacion_distrito']:''; ?>';
    filterValues['oferta_ubicacion_barrio'] = '<?php echo isset($_GET['oferta_ubicacion_barrio'])?$_GET['oferta_ubicacion_barrio']:''; ?>';


	var categoriasJSON = '<?php echo str_replace("'", "\\'", json_encode($catJSON)); ?>';
	var provinciaJSON = '<?php echo str_replace("'", "\\'", json_encode($provJSON)); ?>';
	var ciudadJSON = '<?php echo str_replace("'", "\\'", json_encode($ciudadJSON)); ?>';
	var distritoJSON = '<?php echo str_replace("'", "\\'", json_encode($distritoJSON)); ?>';
	var barrioJSON = '<?php echo str_replace("'", "\\'", json_encode($barrioJSON)); ?>';

    function init(){
        //list_op_ovl();
        initSelectListFiltros();

		setDataList('oferta_categoria_id', JSON.parse(categoriasJSON));
		setDataList('oferta_ubicacion_provincia', JSON.parse(provinciaJSON));
		setDataList('oferta_ubicacion_ciudad', JSON.parse(ciudadJSON));
		setDataList('oferta_ubicacion_distrito', JSON.parse(distritoJSON));
		setDataList('oferta_ubicacion_barrio', JSON.parse(barrioJSON));
    }

	function setDataList(_id, _data)
	{
		var target = $('#' + _id);
        target.find('.select_options').html('');
        target.find('.select_options').html('<li><a href="javascript:void(0);" id="-1">Todos</a></li>');
		for(i in _data)
		{
            target.find('.select_options').append('<li><a href="javascript:void(0);" id="' + _data[i].id + '">' + _data[i].nombre + '</a></li>');
            if(filterValues[_id] == _data[i].id)
            {
                target.find('.select_label').text(_data[i].nombre);
                target.find('input').val(_data[i].id);
            }
        }
	}

	function initSelectListFiltros()
	{

        $('.select_list').each(function(){
			var target = $(this);
            target.html('');
			target.append('<a href="javascript:void(0);" class="select_label">Selecciona</a>');
			target.append('<input type="hidden" name="' + target.attr('id') + '" value="" />');
			target.append('<ul class="select_options"><li><a href="javascript:void(0);" id="">No hay opciones</a></li></ul>');

			target.find('.select_label').click(function()
			{
                $("body").unbind('click');
                target.find('.select_options').slideDown('fast', function()
				{
                	$("body").click(eventBodySelect);

                    target.find('.select_options a').click(function()
					{
                        target.find('.select_options').slideUp('fast');
                        $("body").unbind('click');
						if($(this).attr('id') != '')
						{
                            target.find('.select_label').text($(this).text());
                            target.find('input').val($(this).attr('id'));
							if(target.attr('id') == 'oferta_ubicacion_provincia')
							{
								$('#oferta_ubicacion_ciudad input').val('');
								$('#oferta_ubicacion_distrito input').val('');
								$('#oferta_ubicacion_barrio input').val('');
							}
                            if(target.attr('id') == 'oferta_ubicacion_ciudad')
                            {
                                $('#oferta_ubicacion_distrito input').val('');
                                $('#oferta_ubicacion_barrio input').val('');
                            }
                            SUBMIT_form();
						}
					});

				});

			});

		});
    /*<div class="select_list" id="oferta_categoria_id">
            <a href="#" class="select_label">dfgsdfg</a>
            <ul class="select_options">
            <li><a href="">asdfas</a> </li>
            <li><a href="">asdfas</a> </li>
            </ul>
    </div>*/
	}

	function SUBMIT_form()
	{
		$('#form_filtros').submit();
	}

	function eventBodySelect()
	{
        $('.select_options').slideUp('fast');
        $("body").unbind('click');
	}

</script>