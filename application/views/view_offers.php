<?php
$controlador = Kohana_Request::$current->controller();
?>

<div id="c_offers">
	<div id="container_lineas">
		<ul id="pestanas_lineas">
			<?php
			foreach($tiendas as $t)
			{
				if($t->code == Kohana_Request::$current->param('param1'))	echo '<li class="selected"><a href="'. URL::site($controlador.'/tienda/' . $t->code) .'">' . $t->name . '</a></li>';
				else			echo '<li><a href="'. URL::site($controlador.'/tienda/' . $t->code) .'">' . $t->name . '</a></li>';
			}
			?>
		</ul>
	</div>
    <div class="container">
	<div id="colum_produc_a">
			<ul class="lista_productos_general cont_offers">
				<?php
                $_ids = array();
                $_count = 0;
				foreach($productos as $p){
                    $_duracion = strtotime($p->oferta_duracion) * 1000;

                    $_ids[$_count]["p_id"] = $p->id;
                    $_ids[$_count]["p_duracion"] = $_duracion;
				?>
					<li >
						<ul>
							<li>
								<span class="dto"><?php echo $p->oferta_descuento; ?>% Dto.</span>
								<span class="descripcion"><?php  echo $p->get_txtmuestra($p->oferta_titulo,100);?></span>
							</li>
							<li class="img">
								<span></span>
                                <div>
								<img border="0" alt="" src="<?php echo URL::base() . $p->oferta_imagen->url_path; ?>">
                                </div>
							</li>
							<li class="caracteristicas">
								<ul>
									<li>
										<span class="ver_plan">
											<span class="prec">
												<span class="prec_2"><?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
												</span>
												<?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
											</span>
											<a href="<?php echo URL::site('offers/producto/' . $p->id ); ?>"></a>
										</span>
										<span>Precio:</span>
										<span class="precio"><?php echo $p->oferta_precio_venta; ?>€</span>
									</li>
									<li>
										<span class="redes">
												<a class="twiter"></a>
												<a class="mail"></a>
												<a class="megusta"></a>
                                        <!--
												<span class="tiempo_restante">
													<?php
													if(!$p->isExpired()){
														?>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('d'); ?><span style="font-size:14px;">dias</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('H'); ?><span style="font-size:14px;">h</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('i'); ?><span style="font-size:14px;">m</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('s'); ?><span style="font-size:14px;">s</span></span>
														<?php
													}else{
														?>
														<div class="badge badge-error">Está oferta ya no está disponible</div>
														<?php
													}
													?>
												</span>
                                        -->
                                                <span class="tiempo_restante">
                                                    <?php if(!$p->isExpired()){ ?>
                                                        <div id="defaultCountdown<?php echo $p->id ?>">count</div>
                                                    <?php }else{ ?>
                                                        <div class="badge badge-error">Está oferta ya no está disponible</div>
                                                    <?php } ?>
                                                </span>
										</span>
										<span class="destacamos">
											<span>Destacamos:</span>
											<p>
                                                <?php
                                                echo $p->get_destacamos($p->oferta_destacamos)
                                                ?>
                                            </p>
										</span>
									</li>
								</ul>
							</li>
						</ul>
					</li>
					<?php
                    $_count ++;
				}

				?>
			</ul>
		</div>
		<div id="colum_produc_b">
			<?php echo View::factory('ci/view_sidebar')->set('var_sidebar','')?>
		</div>
    </div>
</div>
<?php
    echo Html::script('public/js/jquery.countdown.js');
    //echo json_encode($_ids); //die();
?>

<script type="text/javascript">
    var obj = jQuery.parseJSON('<?php echo json_encode($_ids) ?>');

    $(document).ready(init);

    function init(){

        jQuery.each(obj, function(i, val) {

            var austDay = new Date();
            austDay = new Date(val.p_duracion);
            //$('#defaultCountdown'+val.p_id).countdown({until: austDay});
            $('#defaultCountdown'+val.p_id).countdown({until: austDay, format: 'dHM'});

            //alert(val.p_id);
        });
    }
</script>