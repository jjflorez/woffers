
<div id="c_home">
<div class="container container_home">
	<div class="row-fluid">
		<div class="span9">
			<ul>
				<li class="head_offers">
					<img class="img_head" src="http://img.onevisionlife.com/home_coorp/head_offers.jpg">
					<a>
						<img src="http://img.onevisionlife.com/home_coorp/btn_offers.jpg">
					</a>
				</li>
				<li>
					<ul class="lista_productos_general cont_offers">

						<?php
						$cant_product=0;
						foreach($offers_p as $p)
						{
							if($cant_product==2)	break;
							$cant_product++;
						?>

						<li>
							<ul>
								<li>
									<span class="dto"><?php echo $p->oferta_descuento; ?>% Dto.</span>
									<span class="descripcion"><?php echo $p->oferta_titulo; ?></span>
								</li>
								<li class="img">
									<span></span>

                                    <div>
									<img border="0" alt="" src="<?php echo URL::base() . $p->oferta_imagen->url_path; ?>">
                                    </div>
								</li>
								<li class="caracteristicas">
									<ul>
										<li>
											<span class="ver_plan">
                                                <span class="prec">
                                                    <span class="prec_2"><?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span></span>
													<?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
                                                </span>
                                                <a href="<?php echo URL::site('offers/producto/' . $p->id ); ?>"></a>
                                            </span>

											<span>Precio:</span>
											<span class="precio">
                        					<?php echo $p->oferta_precio_venta; ?>€</span>
										</li>
										<li>

											<span class="redes">
													<a class="twiter"></a>
													<a class="mail"></a>
													<a class="megusta"></a>
													<span class="tiempo_restante">
													<?php
													if(!$p->isExpired()){
														?>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('d'); ?><span style="font-size:14px;">dias</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('H'); ?><span style="font-size:14px;">h</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('i'); ?><span style="font-size:14px;">m</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('s'); ?><span style="font-size:14px;">s</span></span>
														<?php
													}else{
														?>
														<div class="badge badge-error">Está oferta ya no está disponible</div>
														<?php
													}
													?>
													</span>
											</span>
											<span class="destacamos">
												<span>Destacamos:</span>


                                                <p>
                                                    <?php
                                                    destacamos($p->oferta_destacamos);
                                                    ?>
                                                </p>

											</span>
										</li>
									</ul>
								</li>
							</ul>
						</li>
						<?php
							}
						?>
					</ul>
				</li>
			</ul>



			<!-- Lista travel -->

			<ul>
				<li class="head_travel">
					<img class="img_head" src="http://img.onevisionlife.com/home_coorp/head_travel.jpg">
					<a>
						<img src="http://img.onevisionlife.com/home_coorp/btn_travel.jpg">
					</a>
				</li>
				<li>
					<ul class="lista_productos_general cont_travel">

						<?php
						$cant_product = 0;
						foreach($travel_p as $p)
						{
							if($cant_product == 2)	break;
							$cant_product++;
						?>

							<li>
								<ul>
									<li>
										<span class="dto"><?php echo $p->oferta_descuento; ?>% Dto.</span>
										<span class="descripcion"><?php echo $p->oferta_titulo; ?></span>
									</li>
									<li class="img">
										<span></span>
                                        <div>
										<img border="0" alt="" src="<?php echo URL::base() . $p->oferta_imagen->url_path; ?>">
                                        <div>
									</li>
									<li class="caracteristicas">
										<ul>
											<li>
											<span class="ver_plan">
                                                <span class="prec">
                                                    <span class="prec_2"><?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
                                                    </span>
													<?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
                                                </span>
                                                <a href="<?php echo URL::site('travel/producto/' . $p->id ); ?>"></a>
                                            </span>

												<span>Precio:</span>
											<span class="precio">
                        					<?php echo $p->oferta_precio_venta; ?>€</span>
											</li>
											<li>
											<span class="redes">
												<a class="twiter"></a>
												<a class="mail"></a>
												<a class="megusta"></a>

												<span class="tiempo_restante">
													<?php
													if(!$p->isExpired()){
														?>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('d'); ?><span style="font-size:14px;">dias</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('H'); ?><span style="font-size:14px;">h</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('i'); ?><span style="font-size:14px;">m</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('s'); ?><span style="font-size:14px;">s</span></span>
														<?php
													}else{
														?>
														<div class="badge badge-error">Está oferta ya no está disponible</div>
														<?php
													}
													?>
												</span>
											</span>
											<span class="destacamos">
												<span>Destacamos:</span>
												<p>
                                                    <?php
                                                    destacamos($p->oferta_destacamos);
                                                    ?>
                                                </p>
											</span>
											</li>
										</ul>
									</li>
								</ul>
							</li>
						<?php
						}
						?>
					</ul>
				</li>

			</ul>

			<!-- Lista coaching -->

			<ul>
				<li class="head_coaching">
					<img src="http://img.onevisionlife.com/home_coorp/head_coaching.png">
					<a>
						<img src="http://img.onevisionlife.com/home_coorp/btn_coaching.jpg">
					</a>
				</li>
				<li>
					<ul class="lista_productos_general cont_coaching">

						<?php
						foreach($coaching_p as $p)
						{
							if($cant_product==2){
								$cant_product=0;
								break;
							}
							$cant_product++;

							?>

							<li>
								<ul>
									<li>
										<span class="dto"><?php echo $p->oferta_descuento; ?>% Dto.</span>
										<span class="descripcion"><?php echo $p->oferta_titulo; ?></span>
									</li>
									<li class="img">
										<span></span>
                                        <div>
										<img border="0" alt="" src="<?php echo URL::base() . $p->oferta_imagen->url_path; ?>">
                                        <div>
									</li>
									<li class="caracteristicas">
										<ul>
											<li>
											<span class="ver_plan">
                                                <span class="prec">
                                                    <span class="prec_2"><?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
                                                    </span>
													<?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
                                                </span>
                                                <a href="<?php echo URL::site('coaching/producto/' . $p->id ); ?>"></a>
                                            </span>

												<span>Precio:</span>
											<span class="precio">
                        					<?php echo $p->oferta_precio_venta; ?>€</span>
											</li>
											<li>

											<span class="redes">
												<a class="twiter"></a>
												<a class="mail"></a>
												<a class="megusta"></a>

												<span class="tiempo_restante">
													<?php
													if(!$p->isExpired()){
														?>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('d'); ?><span style="font-size:14px;">dias</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('H'); ?><span style="font-size:14px;">h</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('i'); ?><span style="font-size:14px;">m</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('s'); ?><span style="font-size:14px;">s</span></span>
														<?php
													}else{
														?>
														<div class="badge badge-error">Está oferta ya no está disponible</div>
														<?php
													}
													?>
												</span>
											</span>
											<span class="destacamos">
												<span>Destacamos:</span>
												<p>
                                                    <?php
                                                    destacamos($p->oferta_destacamos);
                                                    ?>
                                                </p>
											</span>
											</li>
										</ul>
									</li>
								</ul>
							</li>
						<?php
						}
						?>
					</ul>
				</li>
			</ul>

			<!-- Lista Outlet -->

			<ul>
				<li class="head_outlet">
					<img class="img_head" src="http://img.onevisionlife.com/home_coorp/head_outlet.jpg">
					<a>
						<img src="http://img.onevisionlife.com/home_coorp/btn_outlet.jpg">
					</a>
				</li>
				<li>
					<ul class="lista_productos_general cont_outlet">

						<?php
						$cant_product=0;
						foreach($outlet_p as $p)
						{
							if($cant_product==2)	break;
							$cant_product++;
							?>
							<li>
								<ul>
									<li>
										<span class="dto"><?php echo $p->oferta_descuento; ?>% Dto.</span>
										<span class="descripcion"><?php echo $p->oferta_titulo; ?></span>
									</li>
									<li class="img">
										<span></span>
                                        <div>
										<img border="0" alt="" src="<?php echo URL::base() . $p->oferta_imagen->url_path; ?>">
                                        <div>
									</li>
									<li class="caracteristicas">
										<ul>
											<li>
											<span class="ver_plan">
                                                <span class="prec">
                                                    <span class="prec_2"><?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
                                                    </span>
													<?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
                                                </span>
                                                <a href="<?php echo URL::site('outlet/producto/' . $p->id ); ?>"></a>
                                            </span>

												<span>Precio:</span>
											<span class="precio">
                        					<?php echo $p->oferta_precio_venta; ?>€</span>
											</li>
											<li>

											<span class="redes">
												<a class="twiter"></a>
												<a class="mail"></a>
												<a class="megusta"></a>

												<span class="tiempo_restante">
												<?php
													if(!$p->isExpired()){
														?>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('d'); ?><span style="font-size:14px;">dias</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('H'); ?><span style="font-size:14px;">h</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('i'); ?><span style="font-size:14px;">m</span></span>
														<span class="tvc1_span"><?php echo $p->get_tiempo_restante('s'); ?><span style="font-size:14px;">s</span></span>
														<?php
													}else{
														?>
														<div class="badge badge-error">Está oferta ya no está disponible</div>
														<?php
													}
													?>
												</span>
											</span>
											</li>
										</ul>
									</li>
								</ul>
							</li>
							<?php
						}
						?>
					</ul>
				</li>
			</ul>

		</div>

		<div class="span3">
			<?php echo View::factory('ci/view_sidebar')->set('var_sidebar','')?>
		</div>
	</div>
</div>
</div>

