<img src="<?php echo URL::site(); ?>public/img/carrito/background_cuello_carrito.png" alt="">
<ul id="lista_minicesta">
	<?php
	if(isset($carrito))
	{
		$lista_carrito = array();
		foreach($carrito->detalleproducto->order_by('id', 'desc')->find_all() as $detalle_a)
		{
			if(!isset($lista_carrito['c_' . $detalle_a->producto->id])){
				$lista_carrito['c_' . $detalle_a->producto->id] = array();
				$lista_carrito['c_' . $detalle_a->producto->id]['cantidad'] = 0;
			}

			$lista_carrito['c_' . $detalle_a->producto->id]['producto'] = $detalle_a->producto;
			$lista_carrito['c_' . $detalle_a->producto->id]['cantidad']++;
		}
		if(count($lista_carrito) == 0) echo '<li><p class="badge badge-error">Tu carrito está vacío.</p></li>';
		foreach($lista_carrito as $detalle){
			echo '<li>
				<img width="67" height="50" src="' . URL::site() . $detalle['producto']->oferta_imagen->url_path . '" alt="">
				<span class="descripcion">' . $detalle['producto']->oferta_titulo . '</span>
				<a href="javascript:void(0);" idCarrito="' . $carrito->id . '" idProducto="' . $detalle['producto']->id . '" class="eliminar_item"></a>
				<span class="costo">' . ($detalle['producto']->get_oferta_precio_final() * $detalle['cantidad']) . '€</span>
				<span class="cantidad">' . $detalle['cantidad'] . '</span>
			</li>';
		}
	}else{
		echo '<li><p class="badge badge-error">Tu carrito está vacío.</p></li>';
	}

	?>
</ul>
<?php if(isset($carrito)){ ?>
<span id="total">Total <span><?php echo $carrito->total_price; ?>€</span></span>
<?php }?>

<a id="first" href="<?php echo URL::site('cesta'); ?>"><img src="<?php echo URL::base(); ?>public/img/carrito/btn_carrito_editar.jpg" alt=""></a>
<a href="javascript:void(0);"><img src="<?php echo URL::base(); ?>public/img/carrito/btn_carrito_seguir.jpg" alt=""></a>
<a href="<?php echo URL::site('cesta'); ?>"><img src="<?php echo URL::base(''); ?>public/img/carrito/btn_carrito_realizar.jpg" alt=""></a>
<span id="tiempo">
	<img src="<?php echo URL::site(); ?>public/img/carrito/carrito_reloj.jpg">Tiempo de caducidad de esta cesta: 24m 13s
</span>
<a href="javascript:void(0);" id="cierre_carrito"></a>

	<script type="text/javascript">
		$(document).ready(initSnippet);
		function initSnippet()
		{
			$('#lista_minicesta .eliminar_item').click(function(){
				qd.app.Carrito.borrarItemCarrrito($(this).attr('idCarrito'), $(this).attr('idProducto'), function(){
					qd.app.MiniCesta.updateCesta(false);
				});
			});
		}
	</script>