<ul class="nav nav-tabs" id="">
	<li class="active"><a href="#share_facebook" data-toggle="tab"><span class="icon-share-alt"></span> <?php echo __('share.facebook'); ?></a></li>
	<li><a href="#share_email" data-toggle="tab"><span class="icon-envelope"></span> <?php echo __('share.email'); ?></a></li>
</ul>
<div class="tab-content">
	<div class="tab-pane active" id="share_facebook">
		<div class="well well-small" style="text-align: center; margin-bottom: 10px;">
			<img src="" id="fb_picture" height="20"> <a href="javascript:void(0);" id="btn_publicar_own"><img src="<?php echo URL::base(); ?>public/img/boton-compartir-facebook.png"></a>
		</div>
		<hr/>
		<h4 style="margin-bottom: 15px;">
			<a href="javascript:void(0);" id="btn_publicar_friend" class="btn btn-primary pull-right"><?php echo __('post.friend.wall'); ?></a>
			<?php echo __('post.friend.wall'); ?>
		</h4>
		<div id="content_facebook" style="clear: both;"></div>
	</div>

	<!-- Email strat -->

	<div class="tab-pane" id="share_email">
		<form action="index.php" class="form-inline" id="form_send_mail" style="margin-bottom: 0;">
			<input type="hidden" name="video_url" value="<?php echo URL::site('play/video?video=' . $uservideo->code, 'http'); ?>" />
			<input type="hidden" name="video_preview" value="<?php echo URL::base('http') . $uservideo->url_preview; ?>" />
		<table width="100%">
			<tr>
				<td valign="top">
					<p><?php echo __('from'); ?>:<br/>
						<?php
							if(Kohana_Auth::instance()->logged_in()){
						?>
							<input type="text" id="from" name="from" value="<?php echo Kohana_Auth::instance()->get_user()->username; ?>">
						<?php }else{ ?>
							<input type="text" id="from" name="from">
						<?php } ?>

					</p>
					<p><?php echo __('to'); ?>: <br/>
						<span class="label"><?php echo __('info.insert.separated.coma'); ?></span><br/>
						<textarea rows="" cols="" name="to" id="to"></textarea>
					</p>
				</td>
				<td rowspan="2" valign="top">
					<?php echo __('message'); ?>:<br/>
					<textarea name="message" style="width: 345px; height: 110px !important;"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="well well-small" style="margin-bottom: 0;">
						<p class="pull-right" style="margin-top: 20px;">
							<button class="btn btn-primary" type="submit"><?php echo __('send.email'); ?></button>
						</p>

						<p><label class="radio"><input type="radio" name="send" value="now" class="radio" checked="checked"> <?php echo __('send.now'); ?></label></p>
						<p>
							<label class="radio"><input type="radio" name="send" value="on" class="radio send_on"> <?php echo __('send.on'); ?></label>
							<span class="input-append"><input type="text" class="input-small apen" name="date" id="send_on_picker"><span class="add-on"><span class="icon-calendar"></span></span></span>
						</p>
					</div>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(initSnippet);

	function initSnippet() {

		$('#content_facebook').jfmfs({max_selected:1,max_selected_message:'XX',
			label:{	filter_title:"Find",all:false}
		});

		$('#btn_publicar_own').click(function(){postOnFB();});
		$('#btn_publicar_friend').click(function()
		{
			var fselector = $("#content_facebook").data('jfmfs');
			var selectedFriends = fselector.getSelectedIds();
			postOnFB(selectedFriends);
		});

		$('#send_on_picker').datepicker({minDate: 0,maxDate: "+12M +10D",dateFormat:'dd/mm/yy'});
		$('#send_on_picker').click(function(){
			$('.send_on').attr('checked', 'checked');
		});
		$('#fb_picture').attr('src', 'https://graph.facebook.com/' + FBLC.user_info.username + '/picture');
		$('#form_send_mail').submit(SUBMIT_handler);
	}

	function SUBMIT_handler()
	{
		var params = $(this).serializeArray();
		if(!$('#from').val() || !$('#to').val())
		{
			alert('Por favor completa ingresa al menos un destino');
			return false;
		}

		if(!$('#data').val() && $('#data').val())
		{
			alert('Por favor ingresa una fecha de envío');
			return false;
		}

		qd.core.LoadData.load(qd.URLs.API_SEND_MAIL, params, function(_data){
			if(_data.status.code == '200')
			{
				$('#share_video').modal('hide');
				alert('El e-mail fue enviado correctamente');
			}else{
				alert('No se pudo enviar, por favor intentalo nuevamente');
			}
		});
		return false;
	}

	function postOnFB(_friendid)
	{
		if(!_friendid) _friendid = null;
		else _friendid = Number(_friendid);
		FB.ui(
			{
				method: 'feed',
				name: "Locofaces - <?php echo $uservideo->video->name; ?>",
				link: '<?php echo URL::site('play/video?video=' . $uservideo->code, 'http'); ?>',
				picture: '<?php echo URL::base('http') . $uservideo->url_preview; ?>',
				caption: 'http://locofaces.com',
				description: '<?php echo $uservideo->video->description; ?>',
				to:_friendid
			},
			// Si quieres que salga una alerta
			function(response) {
				if (response && response.post_id) {
					alert('Tu video fue compartido!');
					$('#share_video').modal('hide');
				} else {
					//alert('No he publicado');
				}
			}
		);
	}
</script>