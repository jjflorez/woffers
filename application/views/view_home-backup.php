<div id="c_home">
<div class="container">
	<div class="row-fluid">
		<div class="span9">
			<div class="head_offers">
				<h2 style="display: none;">Offers las mejores ofertas</h2>
				<a href="<?php echo URL::site('offers'); ?>"><img src="http://img.onevisionlife.com/home_coorp/btn_offers.jpg"></a>
			</div>

			<div class="row-fluid">
				<?php
				foreach($offers_p as $p)
				{
				?>
					<div class="span6 producto_medium">
						<a href="#"><span><?php echo $p->oferta_descuento; ?>% Dto. </span><?php echo $p->oferta_titulo; ?></a>
						<img src="<?php echo URL::base() . $p->oferta_imagen->url_path; ?>" />
						<div class="precios">
							<?php echo $p->oferta_precio_venta; ?>
							<span class="precio_venta">
								<?php echo $p->oferta_precio_venta - $p->oferta_descuento; ?>
								<a href="#" class="link_detalle">Ver el plan</a>
							</span>
						</div>
						<div>
							<div class="social"></div>
							<div class="tiempo"></div>
							<p style="font-weight: bold;">Destacamos</p>
							<?php echo $p->oferta_destacamos; ?>
						</div>
					</div>
				<?php
				}
				?>
			</div>

			<h2>Outlet Productos con increibles descuentos</h2>
			<ul>
				<?php
				foreach($outlet_p as $p)
				{
					echo '<li><a href="#">' . $p->oferta_titulo . '</a> </li>';
				}
				?>
			</ul>

			<h2>Travel - Los mejores viajes, hoteles y resorts</h2>
			<ul>
				<?php
				foreach($travel_p as $p)
				{
					echo '<li><a href="#">' . $p->oferta_titulo . '</a></li>';
				}
				?>
			</ul>

			<h2>Coaching - Formación de calidad</h2>
			<ul>
				<?php
				foreach($coaching_p as $p)
				{
					echo '<li><a href="#">' . $p->oferta_titulo . '</a></li>';
				}
				?>
			</ul>

		</div>

		<!-- Start Sidebar  -->

		<div class="span3">
			<?php echo View::factory('ci/view_sidebar')->set('var_sidebar','Data')?>
		</div>
	</div>
</div>
</div>