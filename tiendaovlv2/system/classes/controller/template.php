<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Template extends Kohana_Controller_Template
{
	function before()
	{
		parent::before();
		if(!Auth::instance()->logged_in())
		{
			if(Kohana_Cookie::get('TOVLID'))
			{
				$usuario = new Model_User(Kohana_Cookie::get('TOVLID'));
				Auth::instance()->force_login($usuario);
			}
		}


	}
}
