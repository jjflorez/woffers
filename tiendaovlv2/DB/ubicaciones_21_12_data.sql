-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2012-12-21 12:51:44
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for onevisio_system_tienda
CREATE DATABASE IF NOT EXISTS `onevisio_system_tienda` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `onevisio_system_tienda`;


-- Dumping structure for table onevisio_system_tienda.ubicaciones
DROP TABLE IF EXISTS `ubicaciones`;
CREATE TABLE IF NOT EXISTS `ubicaciones` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- Dumping data for table onevisio_system_tienda.ubicaciones: ~60 rows (approximately)
/*!40000 ALTER TABLE `ubicaciones` DISABLE KEYS */;
INSERT INTO `ubicaciones` (`id`, `parent_id`, `nombre`, `status`) VALUES
	(1, -1, 'España', NULL),
	(2, 1, 'Alicante', NULL),
	(3, 1, 'Barcelona', 1),
	(4, 1, 'Madrid', NULL),
	(5, 1, 'Tarragona', NULL),
	(6, 1, 'Valencia', 1),
	(7, 3, 'Barcelona', 1),
	(8, 3, 'Hospitalet de LLobregat', 0),
	(9, 3, 'Sabadell', 0),
	(10, 3, 'Terrassa', 0),
	(11, 3, 'Esplugues de LLobregat', 0),
	(12, 3, 'Badalona', 0),
	(13, 3, 'Santa Coloma de Gramenet', 0),
	(14, 5, 'Tarragona', NULL),
	(15, 6, 'Valencia', 1),
	(16, 4, 'Madrid', NULL),
	(17, 2, 'Alicante', NULL),
	(18, 7, 'Ciutat Vella', NULL),
	(19, 7, 'L\'Eixample', 1),
	(20, 7, 'Sants-Montuic', 1),
	(21, 7, 'Les Corts', 1),
	(22, 7, 'Sarrià-Sant Gervasi', NULL),
	(23, 7, 'Gràcia', NULL),
	(24, 7, 'Horta-Guinardó', NULL),
	(25, 7, 'Nou Barris', NULL),
	(26, 7, 'Sant Andreu', NULL),
	(27, 7, 'Sant Martí', NULL),
	(28, 8, 'Distrito 5', NULL),
	(29, 9, 'Distrito 1', NULL),
	(30, 10, 'Distrito 6', NULL),
	(31, 11, 'Todos', NULL),
	(32, 12, 'Distrito 4', NULL),
	(33, 13, 'Distrito 1', NULL),
	(34, 14, 'Distrito 4', NULL),
	(35, 15, 'Benimaclet', 1),
	(36, 15, 'Camins al Grau', 1),
	(37, 17, 'Distrito 1', NULL),
	(38, 17, 'Distrito 2', NULL),
	(39, 16, 'Chamartín', NULL),
	(40, 16, 'Salamanca', NULL),
	(41, 18, 'El Raval', NULL),
	(42, 18, 'El Gótic', NULL),
	(43, 19, 'Sant Antoni', 1),
	(44, 20, 'Sants', 1),
	(45, 21, 'Les Corts', 1),
	(46, 22, 'Sarrià', NULL),
	(47, 23, 'Vila de Gràcia', NULL),
	(48, 24, 'Horta', NULL),
	(49, 25, 'Les Roquetes', NULL),
	(50, 26, 'Sant Andreu', NULL),
	(51, 26, 'La Sagrera', NULL),
	(52, 27, 'El Camp de l\'Arpa del Clot', NULL),
	(53, 27, 'El Clot', NULL),
	(54, 6, 'Paterna', 1),
	(55, 54, 'La Cañada', 1),
	(56, 55, 'La Cañada', 1),
	(57, 15, 'Poblados Marítimos', 1),
	(58, 35, 'Benimaclet', 1),
	(59, 36, 'Camí Fondo', 1),
	(60, 57, 'El Grao', 1);
/*!40000 ALTER TABLE `ubicaciones` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
