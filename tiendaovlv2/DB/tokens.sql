CREATE TABLE IF NOT EXISTS `tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `token_publico` varchar(200) NOT NULL,
  `token_privado` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token_privado`),
  KEY `fk_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `tokens`
--

INSERT INTO `tokens` (`id`, `user_id`, `token_publico`, `token_privado`) VALUES
(1, 1, '837ec5754f503cfaaee0929fd48974e75877b2a7c02f75c4199ff9bac175c29b59eec8b55c87fdf6f94db4c5af56460a', 'admin');