-- --------------------------------------------------------
-- Host:                         216.224.174.59
-- Server version:               5.1.65-cll - MySQL Community Server (GPL)
-- Server OS:                    unknown-linux-gnu
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2012-12-13 09:17:36
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table redovl_tiendaovlv2.carritos
DROP TABLE IF EXISTS `carritos`;
CREATE TABLE IF NOT EXISTS `carritos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `direccion_id` int(10) DEFAULT NULL,
  `factura` int(1) NOT NULL,
  `total_price` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `pago` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.carritos: ~138 rows (approximately)
/*!40000 ALTER TABLE `carritos` DISABLE KEYS */;
INSERT INTO `carritos` (`id`, `user_id`, `direccion_id`, `factura`, `total_price`, `created`, `pago`, `status`) VALUES
	(1, NULL, NULL, 0, 43, '2012-09-11 18:14:35', NULL, 0),
	(3, NULL, NULL, 0, NULL, '2012-09-08 09:40:18', NULL, 0),
	(5, NULL, NULL, 0, 100, NULL, NULL, NULL),
	(6, NULL, NULL, 0, 200, NULL, NULL, NULL),
	(7, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(11, NULL, NULL, 0, 19, '2012-09-13 15:44:27', NULL, 0),
	(12, NULL, NULL, 0, 100, '2012-09-13 15:50:19', NULL, 0),
	(13, NULL, NULL, 0, 95, '2012-09-13 15:53:12', NULL, 0),
	(14, NULL, NULL, 0, 100, '2012-09-13 15:56:31', NULL, 0),
	(15, NULL, NULL, 0, 51, '2012-09-13 16:02:29', NULL, 0),
	(17, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(20, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(23, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(24, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(25, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(26, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(28, NULL, NULL, 0, 6, '2012-09-17 17:57:05', NULL, 0),
	(29, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(30, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(32, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(33, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(34, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(35, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(36, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(39, NULL, NULL, 0, -48, NULL, NULL, NULL),
	(40, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(45, NULL, NULL, 0, 72, '2012-09-22 19:18:23', NULL, 0),
	(46, 1, NULL, 0, 720, '2012-09-22 19:21:12', NULL, 1),
	(47, 1, NULL, 0, 450, '2012-09-24 17:55:40', NULL, 1),
	(48, 1, NULL, 0, 90, '2012-09-24 20:00:49', NULL, 1),
	(49, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(50, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(51, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(53, NULL, NULL, 0, 100, '2012-09-26 04:14:41', NULL, 0),
	(54, NULL, NULL, 0, 90, '2012-09-26 06:21:00', NULL, 0),
	(55, NULL, NULL, 0, 100, '2012-09-26 07:38:48', NULL, 0),
	(56, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(57, NULL, NULL, 0, 90, '2012-09-27 11:49:33', NULL, 0),
	(58, NULL, NULL, 0, 0, '2012-09-27 12:06:00', NULL, 0),
	(59, NULL, NULL, 0, 9, '2012-09-27 12:09:00', NULL, 0),
	(60, NULL, NULL, 0, 64, '2012-09-27 12:09:33', NULL, 0),
	(61, NULL, NULL, 0, 160, '2012-09-27 12:10:05', NULL, 0),
	(62, NULL, NULL, 0, 100, '2012-09-27 12:10:37', NULL, 0),
	(63, NULL, NULL, 0, 0, '2012-09-27 12:11:10', NULL, 0),
	(64, NULL, NULL, 0, 0, '2012-09-27 12:11:43', NULL, 0),
	(65, NULL, NULL, 0, 0, '2012-09-27 12:12:15', NULL, 0),
	(66, NULL, NULL, 0, 0, '2012-09-27 12:21:41', NULL, 0),
	(67, NULL, NULL, 0, 100, '2012-09-27 13:21:48', NULL, 0),
	(68, NULL, NULL, 0, 95, '2012-09-27 13:44:41', NULL, 0),
	(69, NULL, NULL, 0, 100, '2012-09-27 14:13:28', NULL, 0),
	(70, NULL, NULL, 0, 90, '2012-09-27 14:40:00', NULL, 0),
	(71, NULL, NULL, 0, 450, '2012-09-27 14:42:32', NULL, 0),
	(72, NULL, NULL, 0, 43, '2012-09-27 15:11:51', NULL, 0),
	(73, NULL, NULL, 0, 51, '2012-09-27 15:40:34', NULL, 0),
	(74, NULL, NULL, 0, 19, '2012-09-27 15:55:07', NULL, 0),
	(75, NULL, NULL, 0, 100, '2012-09-27 15:57:43', NULL, 0),
	(76, NULL, NULL, 0, 72, '2012-09-27 16:00:30', NULL, 0),
	(77, NULL, NULL, 0, 39, '2012-09-27 16:03:37', NULL, 0),
	(78, NULL, NULL, 0, 100, '2012-09-27 16:19:29', NULL, 0),
	(79, NULL, NULL, 0, 6, '2012-09-27 17:21:18', NULL, 0),
	(80, NULL, NULL, 0, 100, '2012-10-02 05:24:52', NULL, 0),
	(81, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(82, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(83, NULL, NULL, 0, 100, '2012-10-04 07:06:53', NULL, 0),
	(84, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(85, NULL, NULL, 0, 86, '2012-10-13 17:44:36', NULL, 0),
	(86, NULL, NULL, 0, 9, '2012-10-14 11:26:49', NULL, 0),
	(87, NULL, NULL, 0, 86, '2012-10-14 14:02:50', NULL, 0),
	(88, NULL, NULL, 0, 100, '2012-10-17 04:07:18', NULL, 0),
	(89, NULL, NULL, 0, 100, '2012-10-19 11:10:15', NULL, 0),
	(90, NULL, NULL, 0, 100, '2012-10-19 11:25:36', NULL, 0),
	(91, NULL, NULL, 0, 160, '2012-10-19 11:29:53', NULL, 0),
	(92, NULL, NULL, 0, 180, '2012-10-19 11:32:49', NULL, 0),
	(93, NULL, NULL, 0, 100, '2012-10-19 11:41:52', NULL, 0),
	(94, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(97, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(98, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(99, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(100, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(101, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(102, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(103, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(106, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(109, NULL, NULL, 0, 300, NULL, NULL, NULL),
	(114, NULL, NULL, 0, 108, '2012-11-10 07:39:35', NULL, 0),
	(115, NULL, NULL, 0, 50, '2012-11-10 07:39:51', NULL, 0),
	(116, NULL, NULL, 0, 50, '2012-11-10 07:39:52', NULL, 0),
	(117, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(119, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(121, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(122, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(123, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(124, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(127, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(128, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(129, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(130, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(131, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(132, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(133, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(134, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(135, 1, NULL, 0, 1, '2012-11-23 15:33:16', NULL, 1),
	(136, NULL, NULL, 0, 100, '2012-11-23 15:36:29', NULL, 0),
	(137, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(139, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(141, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(142, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(145, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(146, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(148, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(149, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(151, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(152, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(153, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(155, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(157, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(158, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(159, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(160, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(162, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(163, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(165, 1, NULL, 0, 50, '2012-11-30 10:14:09', '2012-11-30 10:14:09', 1),
	(166, 1, NULL, 0, 828, '2012-11-30 13:13:30', '2012-11-30 13:13:30', 1),
	(168, NULL, NULL, 0, 50, '2012-12-02 00:09:36', NULL, 0),
	(169, NULL, NULL, 0, 138, '2012-12-02 00:10:08', NULL, 0),
	(170, NULL, NULL, 0, 180, '2012-12-02 00:10:25', NULL, 0),
	(171, NULL, NULL, 0, 70, '2012-12-02 00:10:25', NULL, 0),
	(172, 1, NULL, 0, 108, '2012-12-03 07:53:05', '2012-12-03 07:53:05', 1),
	(174, NULL, NULL, 0, 75, '2012-12-04 13:09:28', NULL, 0),
	(175, NULL, NULL, 0, 41, '2012-12-06 16:02:26', NULL, 0),
	(176, 1, NULL, 0, 123, '2012-12-07 11:32:27', NULL, 1),
	(178, 1, NULL, 0, 288, '2012-12-11 04:00:34', '2012-12-13 04:00:34', 1),
	(179, 1, NULL, 1, 138, '2012-12-11 17:47:10', '2012-12-12 00:15:26', 1),
	(180, 1, NULL, 0, 720, '2012-12-12 00:18:22', '2012-12-12 00:17:08', 1),
	(182, 1, NULL, 0, 158, '2012-12-12 21:29:06', '2012-12-12 23:17:46', 1),
	(183, 1, NULL, 0, 41, '2012-12-12 23:20:50', '2012-12-12 23:21:35', 1),
	(185, 1, NULL, 0, 180, '2012-12-13 12:23:02', '2012-12-13 12:39:05', 1),
	(186, 1, NULL, 0, 207, '2012-12-13 12:42:48', NULL, 0);
/*!40000 ALTER TABLE `carritos` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.categorias
DROP TABLE IF EXISTS `categorias`;
CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.categorias: ~4 rows (approximately)
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` (`id`, `parent_id`, `nombre`) VALUES
	(1, NULL, 'Restauración'),
	(2, NULL, 'Belleza'),
	(3, NULL, 'Servicios'),
	(4, NULL, 'Formación');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.detalleproductos
DROP TABLE IF EXISTS `detalleproductos`;
CREATE TABLE IF NOT EXISTS `detalleproductos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `carrito_id` int(10) DEFAULT NULL,
  `producto_id` int(10) DEFAULT NULL,
  `direccion_id` int(10) DEFAULT NULL,
  `codigo_referencia` varchar(100) DEFAULT NULL,
  `codigo_cupon` varchar(100) DEFAULT NULL,
  `codigo_validacion` varchar(100) DEFAULT NULL,
  `is_gift` int(10) DEFAULT NULL,
  `monto` int(10) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `regalo_nombre` varchar(300) DEFAULT NULL,
  `regalo_email` varchar(200) DEFAULT NULL,
  `regalo_mensaje` text,
  `regalo_send` tinyint(1) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.detalleproductos: ~44 rows (approximately)
/*!40000 ALTER TABLE `detalleproductos` DISABLE KEYS */;
INSERT INTO `detalleproductos` (`id`, `carrito_id`, `producto_id`, `direccion_id`, `codigo_referencia`, `codigo_cupon`, `codigo_validacion`, `is_gift`, `monto`, `added`, `regalo_nombre`, `regalo_email`, `regalo_mensaje`, `regalo_send`, `status`) VALUES
	(1, 1, 10, NULL, 'ES-12111012', 'CC50C6789665', '5060c0be8cddc', 0, 383, '2012-09-24 15:21:18', NULL, NULL, NULL, NULL, 1),
	(2, 1, 9, NULL, 'ES-1211912', 'CC50C6789666', '5060c0cac0c21', 0, 9, '2012-09-24 15:21:30', NULL, NULL, NULL, NULL, 1),
	(3, 1, 12, NULL, 'ES-12111212', 'CC50C6789666', '5060c0d36969c', 0, 15, '2012-09-24 15:21:39', NULL, NULL, NULL, NULL, 1),
	(4, 2, 2, NULL, 'ES-1211212', 'CC50C6789667', '5060c19f4b776', 0, 64, '2012-09-24 15:25:03', NULL, NULL, NULL, NULL, 1),
	(5, 2, 1, NULL, 'ES-1211112', 'CC50C6789667', '5060c1a722161', 0, 100, '2012-09-24 15:25:11', NULL, NULL, NULL, NULL, 1),
	(8, 4, 1, 16, 'ES-1211112', 'CC50C6789668', '5091ad5bb580c', 1, 100, '2012-10-31 17:59:39', NULL, NULL, NULL, NULL, 1),
	(9, 4, 1, NULL, 'ES-1211112', 'CC50C6789668', '5093f27d647fc', 0, 100, '2012-11-02 11:19:09', NULL, NULL, NULL, NULL, 1),
	(10, 4, 10, NULL, 'ES-12111012', 'CC50C6789669', '50943e26980f4', 0, 383, '2012-11-02 16:41:58', NULL, NULL, NULL, NULL, 1),
	(24, 114, 29, NULL, 'ES-12112912', 'CC50C6789669', '509e5917dd238', 0, 108, '2012-11-10 07:39:35', NULL, NULL, NULL, NULL, 1),
	(25, 115, 30, NULL, 'ES-12113012', 'CC50C678966A', '509e5927edba7', 0, 50, '2012-11-10 07:39:51', NULL, NULL, NULL, NULL, 1),
	(26, 116, 26, NULL, 'ES-12112612', 'CC50C678966A', '509e5928707f3', 0, 50, '2012-11-10 07:39:52', NULL, NULL, NULL, NULL, 1),
	(62, 136, 20, NULL, 'ES-12112012', 'CC50C678966A', '50afde4d5fcc5', 0, 50, '2012-11-23 15:36:29', NULL, NULL, NULL, NULL, 1),
	(63, 136, 30, NULL, 'ES-12113012', 'CC50C678966B', '50afde60bde4f', 0, 50, '2012-11-23 15:36:48', NULL, NULL, NULL, NULL, 1),
	(65, 135, 30, NULL, 'ES-12113012', 'CC50C678966B', '50aff1470449f', 0, 1, '2012-11-23 16:57:27', NULL, NULL, NULL, NULL, 1),
	(120, 165, 20, NULL, 'ES-12112012', 'CC50C678966C', '50b8cd416430d', 0, 50, '2012-11-30 10:14:09', NULL, NULL, NULL, NULL, 1),
	(121, 166, 34, NULL, 'ES-12113412', 'CC50C678966C', '50b8f74a74fcb', 0, 138, '2012-11-30 13:13:30', NULL, NULL, NULL, NULL, 1),
	(122, 166, 34, NULL, 'ES-12113412', 'CC50C678966D', '50b94068813b4', 0, 138, '2012-11-30 18:25:28', NULL, NULL, NULL, NULL, 1),
	(123, 166, 34, NULL, 'ES-12113412', 'CC50C678966D', '50b9407569ae9', 0, 138, '2012-11-30 18:25:41', NULL, NULL, NULL, NULL, 1),
	(124, 166, 34, NULL, 'ES-12113412', 'CC50C678966E', '50b940756b200', 0, 138, '2012-11-30 18:25:41', NULL, NULL, NULL, NULL, 1),
	(125, 166, 34, NULL, 'ES-12113412', 'CC50C678966E', '50b940756ba3e', 0, 138, '2012-11-30 18:25:41', NULL, NULL, NULL, NULL, 1),
	(126, 166, 34, NULL, 'ES-12113412', 'CC50C678966F', '50b940756c236', 0, 138, '2012-11-30 18:25:41', NULL, NULL, NULL, NULL, 1),
	(132, 168, 32, NULL, 'ES-12113212', 'CC50C678966F', '50bae2909b580', 0, 50, '2012-12-02 00:09:36', NULL, NULL, NULL, NULL, 1),
	(133, 169, 34, NULL, 'ES-12113412', 'CC50C6789670', '50bae2b07b8b0', 0, 138, '2012-12-02 00:10:08', NULL, NULL, NULL, NULL, 1),
	(134, 170, 36, NULL, 'ES-12113612', 'CC50C6789670', '50bae2c169a14', 0, 180, '2012-12-02 00:10:25', NULL, NULL, NULL, NULL, 1),
	(135, 171, 35, NULL, 'ES-12113512', 'CC50C6789671', '50bae2c1f265c', 0, 70, '2012-12-02 00:10:25', NULL, NULL, NULL, NULL, 1),
	(137, 172, 29, NULL, 'ES-12112912', 'CC50C6789671', '50bca0b195da0', 0, 108, '2012-12-03 07:53:05', NULL, NULL, NULL, NULL, 1),
	(141, 174, 33, NULL, 'ES-12113312', 'CC50C6789672', '50be3c5863b72', 0, 75, '2012-12-04 13:09:28', NULL, NULL, NULL, NULL, 1),
	(142, 175, 37, NULL, 'ES-12113712', 'CC50C6789672', '50c107e29585a', 0, 41, '2012-12-06 16:02:26', NULL, NULL, NULL, NULL, 1),
	(143, 176, 37, NULL, 'ES-12113712', 'CC50C6789673', '50c219f645166', 0, 41, '2012-12-07 11:31:50', NULL, NULL, NULL, NULL, 1),
	(144, 176, 37, NULL, 'ES-12113712', 'CC50C6789673', '50c21a1ba89c1', 0, 41, '2012-12-07 11:32:27', NULL, NULL, NULL, NULL, 1),
	(145, 176, 37, NULL, 'ES-12113712', 'CC50C6789674', '50c22af99adcf', 0, 41, '2012-12-07 12:44:25', NULL, NULL, NULL, NULL, 1),
	(152, 178, 29, NULL, 'ES-12112912', 'CC50C6A1A3B6', 'CV1355194787', 0, 108, '2012-12-11 03:59:47', NULL, NULL, NULL, NULL, 1),
	(153, 178, 36, NULL, 'ES-12113612', 'CC50C6A1D2A2', 'CV1355194834', 0, 180, '2012-12-11 04:00:34', NULL, NULL, NULL, NULL, 1),
	(156, 179, 34, NULL, 'ES-12113412', 'CC50C7638ED4', 'CV1355244430', 0, 138, '2012-12-11 17:47:10', NULL, NULL, NULL, NULL, 1),
	(160, 180, 36, NULL, 'ES-12113612', 'CC50C7BED2D3', 'CV1355267794', 0, 180, '2012-12-12 00:16:34', NULL, NULL, NULL, NULL, 1),
	(161, 180, 36, NULL, 'ES-12113612', 'CC50C7BF2F08', 'CV1355267887', 0, 180, '2012-12-12 00:18:07', NULL, NULL, NULL, NULL, 1),
	(162, 180, 36, NULL, 'ES-12113612', 'CC50C7BF35F1', 'CV1355267893', 0, 180, '2012-12-12 00:18:13', NULL, NULL, NULL, NULL, 1),
	(163, 180, 36, NULL, 'ES-12113612', 'CC50C7BF3EBE', 'CV1355267902', 0, 180, '2012-12-12 00:18:22', NULL, NULL, NULL, NULL, 1),
	(165, 182, 29, NULL, 'ES-12112912', 'CC50C8E9122F', 'CV1355344146', 0, 108, '2012-12-12 21:29:06', NULL, NULL, NULL, NULL, 1),
	(166, 182, 20, NULL, 'ES-12112012', 'CC50C90253D6', 'CV1355350611', 0, 50, '2012-12-12 23:16:51', NULL, NULL, NULL, NULL, 1),
	(167, 183, 37, NULL, 'ES-12113712', 'CC50C9034240', 'CV1355350850', 1, 41, '2012-12-12 23:20:50', '', 'asdfasdf', 'asdfasd', 1, 1),
	(171, 185, 36, NULL, 'ES-12113612', 'CC50C9BE2CCB', 'CV1355398700', 1, 180, '2012-12-13 12:38:20', '', 'david.kundalini@gmail.com', 'd', 1, 1),
	(172, 186, 36, NULL, 'ES-12113612', 'CC50C9BEDE18', 'CV1355398878', 1, 180, '2012-12-13 12:41:18', '', 'david.kundalini@gmail.com', 'd', 1, 1),
	(174, 186, 39, NULL, 'ES-12133912', 'CC50C9C62596', 'CV1355400741', 0, 27, '2012-12-13 13:12:21', NULL, NULL, NULL, NULL, 1);
/*!40000 ALTER TABLE `detalleproductos` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.direcciones
DROP TABLE IF EXISTS `direcciones`;
CREATE TABLE IF NOT EXISTS `direcciones` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `carrito_id` int(10) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `tipo_direccion` varchar(50) DEFAULT NULL,
  `codigo_postal` varchar(50) DEFAULT NULL,
  `empresa` varchar(50) DEFAULT NULL,
  `direccion` varchar(300) DEFAULT NULL,
  `poblacion` varchar(50) DEFAULT NULL,
  `provincia` varchar(150) DEFAULT NULL,
  `pais_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.direcciones: ~28 rows (approximately)
/*!40000 ALTER TABLE `direcciones` DISABLE KEYS */;
INSERT INTO `direcciones` (`id`, `user_id`, `carrito_id`, `nombre`, `apellidos`, `telefono`, `tipo_direccion`, `codigo_postal`, `empresa`, `direccion`, `poblacion`, `provincia`, `pais_id`) VALUES
	(1, 1, NULL, 'Gerson', 'Aduviri', '235362', 'Particular', '5154', 'OVL', 'Juan Velasco Alvarado', 'Arequipa', '2', NULL),
	(2, NULL, NULL, 'ww', 'w', 'w', 'Particular', 'w', 'w', 'w', 'w', '0', -1),
	(3, 1, NULL, 'e', 'e', 'e', '', 'e', 'e', 'e', 'e', '0', NULL),
	(4, 1, NULL, 'e', 'e', 'e', '', 'e', 'e', 'e', 'e', '0', NULL),
	(5, 1, 25, 'w', 'w', 'ww', '', 'w', 'w', 'w', 'w', '1', NULL),
	(6, NULL, NULL, '', '', '', '', 'kkmo', '', '', 'kmkmk', '0', -1),
	(7, 1, NULL, 'lkmkm', 'mpokop', '89089080\'', 'Particular', '0989809', 'kmpmk', 'ljnklmkm', 'jnkj', '1', NULL),
	(8, NULL, NULL, '4', '4', '4', '', '4', '4', '4', '4', '0', -1),
	(9, NULL, 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, NULL, 38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, NULL, NULL, '3', '3', '3', '', '3', '3', '3', '3', '0', -1),
	(12, NULL, 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, NULL, 42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, NULL, 43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, NULL, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, NULL, 46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, NULL, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, NULL, 48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(19, NULL, NULL, 'ryy', 'ertert', '63454', '', '45525', 'ty', 'twwtw', 'sdgsgsd', '2', -1),
	(20, NULL, 105, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(21, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(22, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(23, NULL, 113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, NULL, 118, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(25, 1, NULL, '', '', '', '', '', '', '', '', '0', NULL),
	(26, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(27, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(28, NULL, 126, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `direcciones` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.files
DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `filetype_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` tinytext,
  `url_path` varchar(350) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`filetype_id`,`user_id`),
  CONSTRAINT `FK_files_file_types` FOREIGN KEY (`filetype_id`) REFERENCES `filetypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.files: ~54 rows (approximately)
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` (`id`, `filetype_id`, `user_id`, `name`, `description`, `url_path`, `created`, `status`) VALUES
	(1, 1, 1, 'Cabeza', '', 'user_files/2012/05/4fc63432cc48d_1_4e6b9a9fd8285.png', NULL, 1),
	(2, 1, 1, 'Cabeza2', '', 'user_files/2012/05/4fc6343aae1c4_1_4e6b987dcba48.png', NULL, 1),
	(3, 1, 1, 'Cabeza3', '', 'user_files/2012/05/4fc63444058bb_1_4e6b9886321b9.png', NULL, 1),
	(11, 1, NULL, 'Novell', NULL, 'user_files/2012/07/50047d394c892_cabecera_backoffice.jpg', '2012-07-16 03:44:41', 1),
	(12, 1, NULL, 'nuevoo', NULL, 'user_files/2012/07/50047d92a0d6c_divercity_captura.JPG', '2012-07-16 03:46:10', 1),
	(13, 1, NULL, 'nuevoo', NULL, 'user_files/2012/07/50047d9aea51e_cabecera_backoffice.jpg', '2012-07-16 03:46:18', 1),
	(14, 1, NULL, 'nuevoo', NULL, 'user_files/2012/07/50047e0b71d4e_cabecera_backoffice.jpg', '2012-07-16 03:48:11', 1),
	(15, 1, NULL, 'nuevoossss', NULL, 'user_files/2012/07/50047e11b25fd_cabecera_backoffice.jpg', '2012-07-16 03:48:17', 1),
	(16, 1, NULL, 'nuevoossss', NULL, 'user_files/2012/07/50047e2307b41_cabecera_backoffice.jpg', '2012-07-16 03:48:35', 1),
	(18, 6, NULL, 'asdfa', NULL, 'user_files/sounds/2012/07/50049426396a7_Avatar.jpg', '2012-07-16 05:22:30', 1),
	(19, 6, NULL, 'asdfa', NULL, 'user_files/sounds/2012/07/5004944891f16_Avatar.jpg', '2012-07-16 05:23:04', 1),
	(20, 6, NULL, 'adsf', NULL, 'user_files/sounds/2012/07/5004964a567da_10 - Proyecto Ser.mp3', '2012-07-16 05:31:38', 1),
	(21, 6, NULL, 'asdfa', NULL, 'user_files/sounds/2012/07/5005d799b72e4_4fff65ba31983_fondo2.jpg', '2012-07-17 04:22:33', 1),
	(22, 6, NULL, 'asdfasdfa', NULL, 'user_files/sounds/2012/07/5005d7a829c53_4fff0ba0d4826_Chrysanthemum.jpg', '2012-07-17 04:22:48', 1),
	(23, 6, NULL, 'asdfasdfaasdf', NULL, 'user_files/sounds/2012/07/5005d7d131640_4fff0ba0d4826_Chrysanthemum.jpg', '2012-07-17 04:23:29', 1),
	(24, 6, NULL, 'asdfa', NULL, 'user_files/sounds/2012/07/500d8bdfe2d2c_theSimpsons.mp3', '2012-07-23 12:37:35', 1),
	(25, 5, 1, 'Fondos', NULL, 'user_files/2012/07/500f749951f00_Avatar.jpg', '2012-07-24 11:22:49', 1),
	(26, 1, NULL, '150', NULL, 'user_files/2012/07/500f77a8613d0_Avatar.jpg', '2012-07-24 11:35:52', 1),
	(27, 1, NULL, 'asdfasdf', NULL, 'user_files/2012/08/502059d592582_boton-compartir-facebook.png', '2012-08-06 06:57:09', NULL),
	(28, 1, NULL, 'dfasdfd', NULL, 'user_files/2012/08/50205a21ac4c0_linkedin.png', '2012-08-06 06:58:25', NULL),
	(29, 1, NULL, 'Login facebook', NULL, 'user_files/2012/08/50205ba311436_fb-login.png', '2012-08-06 07:04:51', NULL),
	(30, 1, NULL, 'Fondo app', NULL, 'user_files/2012/08/50205d3859492_fondo_app2.jpg', '2012-08-06 07:11:36', NULL),
	(31, 1, NULL, 'Chart', NULL, 'user_files/2012/08/5040ecca8a01f_256.png', '2012-08-31 11:56:42', NULL),
	(32, 1, NULL, 'Cargo', NULL, 'user_files/2012/08/5040edea0e2f1_cargo1.png', '2012-08-31 12:01:30', NULL),
	(33, 1, NULL, 'sewerwerw', NULL, 'user_files/2012/10/5076136f7ca4a_divercitycapturajpg.JPG', '2012-10-10 07:31:43', NULL),
	(34, 1, NULL, 'hola', NULL, 'user_files/2012/10/507615a2f2276_penguinsjpg.jpg', '2012-10-10 07:41:06', NULL),
	(35, 1, NULL, 'hola', NULL, 'user_files/2012/10/507615c74bce9_tulipsjpg.jpg', '2012-10-10 07:41:43', NULL),
	(36, 1, NULL, 'koala', NULL, 'user_files/2012/10/507615f937e6c_koalajpg.jpg', '2012-10-10 07:42:33', NULL),
	(37, 1, NULL, 'caribe', NULL, 'user_files/2012/10/50775c1234338_playas-caribejpg.jpg', '2012-10-11 06:53:54', NULL),
	(38, 1, NULL, 'prueba', NULL, 'user_files/2012/10/5078ad7b30c9b_pruebapng.png', '2012-10-12 06:53:31', NULL),
	(39, 1, NULL, 'bicicleta', NULL, 'user_files/2012/11/50b9351a9dd14_190612ovloutletprodunicosjpg.jpg', '2012-11-30 05:37:14', NULL),
	(40, 1, NULL, 'cu', NULL, 'user_files/2012/11/50b948580e78b_abcdela-bancajpg.JPG', '2012-11-30 06:59:20', NULL),
	(41, 1, NULL, 'Clínica Médica Body Care', NULL, 'user_files/2012/12/50bc9c0b80b47_logobodycarejpg.jpg', '2012-12-03 07:33:15', NULL),
	(42, 1, NULL, 'Mesoterapia - Clínica Médico Estética Body Care', NULL, 'user_files/2012/12/50bc9cd4c5504_mesoterapiabodycarejpg.jpg', '2012-12-03 07:36:36', NULL),
	(43, 1, NULL, 'Mesoterapia - Clínica Médico Estética Body Care', NULL, 'user_files/2012/12/50bcb77e45a7d_mesoterapiabodycareextjpg.jpg', '2012-12-03 09:30:22', NULL),
	(44, 1, NULL, 'faro', NULL, 'user_files/2012/12/50bd306bcc9ca_lighthousejpg.jpg', '2012-12-03 06:06:19', NULL),
	(45, 1, NULL, 'koala', NULL, 'user_files/2012/12/50c2a95d2c137_koalajpg.jpg', '2012-12-08 03:43:41', NULL),
	(46, 1, NULL, 'Clinica Body Care', NULL, 'user_files/2012/12/50c9ab0c80900_logobodycarejpg.jpg', '2012-12-13 11:16:44', NULL),
	(47, 1, NULL, 'Clinica Body Care', NULL, 'user_files/2012/12/50c9ab55e1dfd_logobodycarejpg.jpg', '2012-12-13 11:17:57', NULL),
	(48, 1, NULL, 'Collados Peluqueros', NULL, 'user_files/2012/12/50c9adaaf2dcb_logocolladospeluquerosjpg.jpg', '2012-12-13 11:27:54', NULL),
	(49, 1, NULL, 'Collados Peluqueros', NULL, 'user_files/2012/12/50c9af09ea27d_colladospeluquerosexteriorjpg.jpg', '2012-12-13 11:33:45', NULL),
	(50, 1, NULL, 'Collados Peluqueros', NULL, 'user_files/2012/12/50c9af259820b_colladospeluquerosinteriorjpg.jpg', '2012-12-13 11:34:13', NULL),
	(51, 1, NULL, 'BCN Fisio', NULL, 'user_files/2012/12/50c9ba8baeb26_logobcnfisiojpg.jpg', '2012-12-13 12:22:51', NULL),
	(52, 1, NULL, 'BCN Fisio', NULL, 'user_files/2012/12/50c9bc3f7199b_bcnfisioexteriorjpg.jpg', '2012-12-13 12:30:07', NULL),
	(53, 1, NULL, 'BCN Fisio', NULL, 'user_files/2012/12/50c9bc4bd3ccd_bcnfisiointeriorjpg.jpg', '2012-12-13 12:30:19', NULL),
	(54, 1, NULL, 'Cocktail Bar Casablanca', NULL, 'user_files/2012/12/50c9bfd40c2a1_logocasablancajpg.jpg', '2012-12-13 12:45:24', NULL),
	(55, 1, NULL, 'Cocktail Bar Casablanca', NULL, 'user_files/2012/12/50c9c823f3903_casablancaexterior1jpg.jpg', '2012-12-13 01:20:52', NULL),
	(56, 1, NULL, 'Cocktail Bar Casablanca', NULL, 'user_files/2012/12/50c9c82f1fc76_casablancainterior1ajpg.jpg', '2012-12-13 01:21:03', NULL),
	(57, 1, NULL, 'Cocktail Bar Casablanca', NULL, 'user_files/2012/12/50c9c83b5bef9_casablancainterior1bjpg.jpg', '2012-12-13 01:21:15', NULL),
	(58, 1, NULL, 'Cocktail Bar Casablanca', NULL, 'user_files/2012/12/50c9c842d8231_casablancainterior1cjpg.jpg', '2012-12-13 01:21:22', NULL),
	(59, 1, NULL, 'PFK Entrenadores', NULL, 'user_files/2012/12/50c9d1d98395d_logopfkentrenadoresjpg.jpg', '2012-12-13 02:02:17', NULL),
	(60, 1, NULL, 'PFK Entrenadores', NULL, 'user_files/2012/12/50c9d37f5388d_pkentrenadores1ajpg.jpg', '2012-12-13 02:09:19', NULL),
	(61, 1, NULL, 'PFK Entrenadores', NULL, 'user_files/2012/12/50c9d428b4408_pkentrenadores1ajpg.jpg', '2012-12-13 02:12:08', NULL),
	(62, 1, NULL, 'PFK Entrenadores', NULL, 'user_files/2012/12/50c9d55ac701b_pkentrenadoresexterior1jpg.jpg', '2012-12-13 02:17:14', NULL);
/*!40000 ALTER TABLE `files` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.filetypes
DROP TABLE IF EXISTS `filetypes`;
CREATE TABLE IF NOT EXISTS `filetypes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `description` tinytext,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.filetypes: ~6 rows (approximately)
/*!40000 ALTER TABLE `filetypes` DISABLE KEYS */;
INSERT INTO `filetypes` (`id`, `name`, `code`, `description`, `status`) VALUES
	(1, 'Imagen', 'IMAGE', 'Imagenes generadas cargadas por el usuario', 1),
	(2, 'Video', 'VIDEO', 'Videos cargados o generados por el usuario', 1),
	(3, 'Sonido', 'SOUND', 'Sonidos o música cargada por el usuario', 1),
	(4, 'Documentos', 'DOCUMENTS', 'Documentos cargados por el usuario', 1),
	(5, 'Fondos', 'IMAGE', 'Fondo/background para el juego', 1),
	(6, 'Canciones', 'SOUND', 'Canciones', 1);
/*!40000 ALTER TABLE `filetypes` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.lineas
DROP TABLE IF EXISTS `lineas`;
CREATE TABLE IF NOT EXISTS `lineas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `code` varchar(200) DEFAULT NULL,
  `url_path` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.lineas: ~5 rows (approximately)
/*!40000 ALTER TABLE `lineas` DISABLE KEYS */;
INSERT INTO `lineas` (`id`, `name`, `code`, `url_path`, `created`, `status`) VALUES
	(1, 'OVL Offers', 'offers', '/offers/', '2012-08-09 17:15:54', 1),
	(2, 'OVL Outlet', 'outlet', '/outlet/', '2012-08-09 17:16:14', 1),
	(3, 'OVL Travel', 'travel', '/travel/', '2012-08-09 17:16:36', 1),
	(4, 'OVL Coaching', 'coaching', 'coaching/', '2012-08-09 17:16:48', 1),
	(5, 'Ofertas Permanentes', 'permanentes', 'permanentes/', '2012-09-10 18:11:26', 1);
/*!40000 ALTER TABLE `lineas` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.mails
DROP TABLE IF EXISTS `mails`;
CREATE TABLE IF NOT EXISTS `mails` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `from` varchar(250) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `to` varchar(250) DEFAULT NULL,
  `body` text,
  `subject` varchar(300) DEFAULT NULL,
  `date_send` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Almacena correo a enviar, limpiar antiguos o enviados';

-- Dumping data for table redovl_tiendaovlv2.mails: 4 rows
/*!40000 ALTER TABLE `mails` DISABLE KEYS */;
INSERT INTO `mails` (`id`, `from`, `from_name`, `to`, `body`, `subject`, `date_send`, `status`) VALUES
	(1, 'eduardo@qd.pe', 'Gerson', 'gerson@qd.pe', 'Contenido', 'Mensaje de prueba', '2012-06-05 09:56:27', 1),
	(2, 'eduardo@qd.pe', 'Gerson', 'gerson@qd.pe', 'Contenido', 'Mensaje de prueba', '2012-06-05 09:56:27', 1),
	(3, 'eduardo@qd.pe', 'Gerson', 'gerson@qd.pe', 'Contenido', 'Mensaje de prueba', '2012-06-05 09:56:27', 1),
	(4, 'eduardo@qd.pe', 'Gerson', 'gerson@qd.pe', 'Contenido', 'Mensaje de prueba', '2012-06-05 09:56:27', 1);
/*!40000 ALTER TABLE `mails` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.person
DROP TABLE IF EXISTS `person`;
CREATE TABLE IF NOT EXISTS `person` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `lastname` varchar(200) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `phone` int(10) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.person: 0 rows
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
/*!40000 ALTER TABLE `person` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.productofiles
DROP TABLE IF EXISTS `productofiles`;
CREATE TABLE IF NOT EXISTS `productofiles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `producto_id` int(10) NOT NULL,
  `file_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_productos_files_productos` (`producto_id`),
  KEY `FK_productos_files_files` (`file_id`),
  CONSTRAINT `FK_productos_files_files` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`),
  CONSTRAINT `FK_productos_files_productos` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=212 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.productofiles: ~18 rows (approximately)
/*!40000 ALTER TABLE `productofiles` DISABLE KEYS */;
INSERT INTO `productofiles` (`id`, `producto_id`, `file_id`) VALUES
	(23, 3, 34),
	(29, 21, 38),
	(30, 1, 36),
	(31, 1, 35),
	(37, 32, 34),
	(39, 33, 35),
	(51, 35, 37),
	(78, 20, 38),
	(79, 20, 36),
	(113, 37, 44),
	(125, 36, 44),
	(126, 36, 36),
	(194, 39, 53),
	(199, 34, 42),
	(200, 34, 42),
	(201, 38, 50),
	(210, 40, 57),
	(211, 40, 58);
/*!40000 ALTER TABLE `productofiles` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.productos
DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `tienda_id` int(10) DEFAULT NULL,
  `comercio_nombre` varchar(250) NOT NULL,
  `comercio_direccion` varchar(250) NOT NULL,
  `comercio_descripcion` text NOT NULL,
  `comercio_logo_id` int(11) NOT NULL,
  `oferta_titulo` text NOT NULL,
  `oferta_tipo` varchar(250) NOT NULL,
  `oferta_que_incluye` text NOT NULL,
  `oferta_destacamos` text NOT NULL,
  `oferta_ubicacion_lon` varchar(600) NOT NULL,
  `oferta_ubicacion_lat` varchar(600) NOT NULL,
  `oferta_nacional` int(3) NOT NULL,
  `oferta_ubicacion_pais` int(3) DEFAULT NULL,
  `oferta_ubicacion_provincia` int(3) DEFAULT NULL,
  `oferta_ubicacion_ciudad` int(3) DEFAULT NULL,
  `oferta_ubicacion_distrito` int(3) DEFAULT NULL,
  `oferta_ubicacion_barrio` int(3) DEFAULT NULL,
  `oferta_categoria_id` varchar(50) DEFAULT NULL,
  `oferta_imagen_id` int(11) NOT NULL,
  `oferta_descripcion` text NOT NULL,
  `oferta_condiciones` text NOT NULL,
  `oferta_detalles` text NOT NULL,
  `oferta_comision_ovl` float NOT NULL DEFAULT '-1',
  `oferta_descuento` float NOT NULL,
  `oferta_precio_venta` float NOT NULL,
  `oferta_duracion` datetime DEFAULT NULL,
  `oferta_publicacion` datetime DEFAULT NULL,
  `file_id` int(11) NOT NULL,
  `tiempo_validez` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `status` text,
  `num_referencia` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.productos: ~13 rows (approximately)
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` (`id`, `user_id`, `tienda_id`, `comercio_nombre`, `comercio_direccion`, `comercio_descripcion`, `comercio_logo_id`, `oferta_titulo`, `oferta_tipo`, `oferta_que_incluye`, `oferta_destacamos`, `oferta_ubicacion_lon`, `oferta_ubicacion_lat`, `oferta_nacional`, `oferta_ubicacion_pais`, `oferta_ubicacion_provincia`, `oferta_ubicacion_ciudad`, `oferta_ubicacion_distrito`, `oferta_ubicacion_barrio`, `oferta_categoria_id`, `oferta_imagen_id`, `oferta_descripcion`, `oferta_condiciones`, `oferta_detalles`, `oferta_comision_ovl`, `oferta_descuento`, `oferta_precio_venta`, `oferta_duracion`, `oferta_publicacion`, `file_id`, `tiempo_validez`, `created`, `status`, `num_referencia`) VALUES
	(1, 1, 1, 'D\'leos', '<p>\n	Calle Soledad #5412</p>\n', '<p>Las mejores ofertas de toda Espa&ntilde;a</p>\n', 25, 'Apex 32\'\' Class 720p 60Hz LCD HDTV - Black (LD3288M)', 'cupon', '<p>\n	Screen Size: 32.0 &quot; Video Resolution: HD - 720p Maximum Resolution: 1366 x 768 Television Features: Sleep Timer, Parental Control, A/V Connects to Home Theaters, Favorite Channels, Auto Channel Programming, Closed Caption on Mute, Picture Freeze, Digital Combo Filter Mounting Features: Detachable Base Stand, VESA Mount, Wall Mountable Display Features: Closed Caption, On-Screen Display, Channel Display, High Resolution Panel, Diagonal LCD Widescreen, HD Display Electronic Functions: Digital Tuner, Built-in Speakers Screen Refresh Rate: 60 Hz Response Time: 8ms Response Time Contrast Ratio: 5000:1 Video Upconversion: 720p Comb Filter Type: 3D Y/C Digital Tuner Type: ATSC HD Compatibility: ATSC/NTSC Aspect Ratio: 16:9 Widescreen Brightness(cd/m2): 400 Audio Features: Broadcast Stereo, Stereo Speakers, Digital Noise Reduction, Selectable Sound Modes, Surround Sound, MTS, SAP Speaker Type: Down-Firing Number of Speakers: 2 Input Type: S-Video, VGA, Component Video, HDMI Port, Composite Video, RF Antenna Input Number of HDMI Inputs: 3 Number of Component Inputs: 2 Number of Composite Inputs: 2 Number of S-Video Inputs: 1 Number of PC Inputs: 1 Output Types: Digital Audio Finish: Glossy Includes: Detachable Base Stand, Power Cord, Remote Control Batteries, Quick Start Guide, User Manual Not Included: High Definition Antenna Dimensions with Stand: 21.9 &quot; H x 31.1 &quot; W x 9.0 &quot; D</p>\n', 'Desctacamos contenido"', '-3.6793', '40.4580', 0, 1, 2, 17, 0, 0, '', 25, '<p>\n	Update your media room with this LCD HDTV from Apex. The three HDMI inputs allow flexible connectivity options, so you can enjoy your favorite movies, games and TV shows in high-definition. It comes with a sturdy base, but it can also be mounted on a wall.</p>\n', '<p>\n	Condiciones</p>\n', 'dDetalles de oferta', -1, 50, 200, '2013-01-10 01:01:01', '2012-09-01 00:00:00', 1, '2013-01-10 01:01:01', '2012-11-07 06:18:08', '1', 'ES-1211112'),
	(2, 1, 1, 'comercio 002', 'dirección comercio 002', 'comercio descripcion 002', 26, 'TItulo de la oferta 002', 'producto', 'Incluye todo', 'Desctacamos', 'http://google.com', '', 0, 0, 0, 0, 0, 0, '', 25, 'Descripcion de la oferta', 'Condiciones', 'Detalles de oferta', -1, 9, 70, '2012-06-30 00:00:00', '2012-10-02 00:00:00', 0, '2012-07-30 17:42:32', '2012-07-30 17:42:48', NULL, 'ES-1211212'),
	(4, 1, 5, 'Comercio de Travel', '<p>\n	Direcci&oacute;n del comercio</p>\n', '<p>\n	Descripci&oacute;n del comercio</p>\n', 27, 'Oferta para travel', 'cupon', '<p>\n	Que incluye la oferta</p>\n', '<p>\n	Destacamos</p>\n', 'Ubicación', '', 0, 0, 0, -1, 0, 0, '', 30, '<p>\n	Descripci&oacute;n de la oferta</p>\n', '<p>\n	Condiciones</p>\n', '', -1, 20, 1000, '1969-12-31 01:01:01', '2012-10-02 00:00:00', 0, '1969-12-31 01:01:01', '2012-11-30 06:07:42', '1', 'ES-1211412'),
	(5, 1, 5, 'Comercio de Travel', 'Dirección del comercio	', 'Descripción del comercio	', 29, 'Oferta para travel', 'cupon', 'Que incluye la oferta	', 'Destacamos', 'Ubicación', '', 0, 0, 0, 0, 0, 0, '', 30, 'Descripción de la oferta	', 'Condiciones', '', -1, 10, 0, '2012-08-29 07:22:05', '2012-10-02 00:00:00', 0, '2012-08-29 07:22:05', '2012-08-29 07:22:05', '1', 'ES-1211512'),
	(6, 1, 5, 'Comercio de Travel', 'Dirección del comercio	', 'Descripción del comercio	', 29, 'Oferta para travel', 'cupon', 'Que incluye la oferta	', 'Destacamos', 'Ubicación', '', 0, 0, 0, 0, 0, 0, '', 30, 'Descripción de la oferta	', 'Condiciones', '', -1, 10, 0, '2012-08-29 07:22:15', '2012-10-02 00:00:00', 0, '2012-08-29 07:22:15', '2012-08-29 07:22:15', '1', 'ES-1211612'),
	(7, 1, 5, 'Comercio de Travel', 'Dirección del comercio	', 'Descripción del comercio	', 29, 'Oferta para travel', 'cupon', 'Que incluye la oferta	', 'Destacamos', 'Ubicación', '', 0, 0, 0, 0, 0, 0, '', 30, 'Descripción de la oferta	', 'Condiciones', '', -1, 10, 0, '2012-08-29 07:22:20', '2012-09-01 00:00:00', 0, '2012-08-29 07:22:20', '2012-08-29 07:22:20', '1', 'ES-1211712'),
	(8, 1, 5, 'Comercio de Travel', 'Dirección del comercio	', 'Descripción del comercio	', 29, 'Oferta para travel', 'cupon', 'Que incluye la oferta	', 'Destacamos', 'Ubicación', '', 0, 0, 0, 0, 0, 0, '', 30, 'Descripción de la oferta	', 'Condiciones', '', -1, 10, 0, '2012-08-29 07:23:12', '2012-10-02 00:00:00', 0, '2012-08-29 07:23:12', '2012-08-29 07:23:12', '1', 'ES-1211812'),
	(10, 1, 3, 'D\'leos', '<p>\n	Calle del comercio numero 250 la ciudad</p>\n', '<p>\n	Comerci&oacute;n con a&ntilde;os de trabajo en el rubro de las entregas</p>\n', 25, 'Oferta que se publica mañana', 'producto', '<p>\n	La oferta incluye cosas como las que tenemos aqui</p>\n<p>\n	as df asd fasd</p>\n', 'Desctacamos los mejores precios', 'as dfasd fasd fasdf ', '', 0, 0, 0, 0, 0, 0, '', 30, '<p>\n	La oferta incluye cosas como las que tenemos aqui</p>\n<p>\n	as df asd fasd</p>\n', '<p>\n	La oferta incluye cosas como las que tenemos aqui</p>\n<p>\n	De tener m&aacute;s condiciones se ver&aacute; as&iacute;.</p>\n<p>\n	Por ejemplo un listado de cosas</p>\n<ul>\n	<li>\n		Item1</li>\n	<li>\n		Item2</li>\n	<li>\n		Item3</li>\n</ul>\n<p>\n	Aqui debajo tienen m&aacute;s cosas</p>\n', '', -1, 15, 450, '2012-12-06 01:01:01', '2012-10-02 00:00:00', 0, '2012-09-10 01:01:01', '2012-10-11 07:13:18', '1', 'ES-12111012'),
	(11, 1, 7, 'Comercio permanentes', '<p>\n	Ubicaci&oacute;n del comercio</p>\n<ul>\n	<li>\n		Lista1</li>\n	<li>\n		Lista2</li>\n</ul>\n', '<p>\n	Descripci&oacute;n del comercio</p>\n', 29, 'Oferta', 'cupon', '<p>\n	Que incluye</p>\n', 'Desctacamos', '<iframe width=', '', 0, 1, 3, 7, 18, 41, '1', 25, '<p>\n	sdf asd fasdf asd</p>\n', '<p>\n	f asd fasdf asd&nbsp;</p>\n', '', -1, 20, 550, '2013-02-13 01:01:01', '1970-07-08 00:00:00', 0, '2012-09-28 01:01:01', '2012-10-26 05:32:01', '1', 'ES-12111112'),
	(12, 1, 6, 'asasdf ', '<p>\n	a asd f</p>\n', '<p>\n	asd asdf</p>\n', 27, 'a sdf', 'producto', '<p>\n	sd fasd&nbsp;</p>\n', 'dfasd fa', 'aas dfasd', '', 0, 0, 0, 0, 0, 0, '', 29, '<p>\n	fasd fas</p>\n', '<p>\n	a sdf</p>\n', '', -1, 50, 30, '1969-12-31 01:01:01', '2012-10-01 18:15:52', 0, '1969-12-31 01:01:01', '2012-09-10 06:02:50', '1', 'ES-12111212'),
	(20, 1, 1, 'prueba', '<p>\n	prueba</p>\n', '<p>\n	prueba</p>\n', 37, 'Oferta nacional 15', 'cupon', '<p>\n	prueba</p>\n', '<p>\n	prueba</p>\n', '0', '0', 1, 0, NULL, -2, NULL, NULL, NULL, 35, '<p>\n	prueba</p>\n', '<p>\n	prueba</p>\n', '', 20, 50, 100, '2013-04-18 01:01:01', '2012-09-12 00:00:00', 0, '2013-03-14 01:01:01', '2012-11-30 07:14:23', '1', 'ES-12112012'),
	(21, 1, 7, 'prueba', '<p>\n	prueba</p>\n', '<p>\n	prueba</p>\n', 36, 'prueba', 'cupon', '<p>\n	prueba</p>\n', 'prueba', '', '', 0, 1, 2, 17, 37, -1, '1', 37, '<p>\n	prueba</p>\n', '<p>\n	prueba</p>\n', '', 20, 50, 100, '2013-03-21 01:01:01', '2012-10-17 00:00:00', 0, '2013-04-24 01:01:01', '2012-10-26 05:31:10', '1', 'ES-12112112'),
	(22, 1, 7, 'df', '<p>\n	df</p>\n', '<p>\n	df</p>\n', 37, 'df', 'cupon', '<p>\n	df</p>\n', 'df', '', '', 0, 1, 7, 45, -1, 0, 'Seleccione una categoría', 36, '<p>\n	df</p>\n', '<p>\n	df</p>\n', '', 20, 50, 100, '2013-02-01 01:01:01', '2012-11-08 00:00:00', 0, '2013-03-01 01:01:01', '2012-11-09 12:04:41', '1', 'ES-12112212'),
	(26, 1, 5, 'qwe', '<p>\n	qwe</p>\n', '<p>\n	qwe</p>\n', 31, 'buena 2', 'cupon', '<p>\n	ads</p>\n', 'ads', '', '', 1, 0, NULL, -2, NULL, NULL, NULL, 0, '<p>\n	asd</p>\n', '<p>\n	ad</p>\n', '', 0, 0, 0.85, '2012-11-23 01:01:01', '2012-11-08 00:00:00', 0, '2012-12-01 01:01:01', '2012-11-22 06:53:58', '1', 'ES-12112612'),
	(27, 1, 5, '232332', '<p>\n	weqe</p>\n', '<p>\n	asdads</p>\n', 36, 'buena 3', 'cupon', '<p>\n	asdf</p>\n', 'asdf', '', '', 0, 1, NULL, 15, NULL, NULL, NULL, 33, '<p>\n	asf</p>\n', '<p>\n	asf</p>\n', '', 20, 50, 100, '2012-11-23 01:01:01', '2012-11-08 00:00:00', 0, '2013-01-01 01:01:01', '2012-11-09 12:28:08', '1', 'ES-12112712'),
	(30, 1, 5, 'koko', '<p>\n	erere</p>\n', '<p>\n	rerer</p>\n', 35, 'kokitos', 'cupon', '<p>\n	sf</p>\n', 'sfsdf', '', '', 1, 0, NULL, -2, NULL, NULL, NULL, 33, '<p>\n	sfsf</p>\n', '<p>\n	asfasf</p>\n', '', 0, 0, 1, '2012-11-30 01:01:01', '2012-11-09 00:00:00', 0, '2013-02-01 01:01:01', '2012-11-23 04:57:12', '1', 'ES-12113012'),
	(31, 1, 5, 'qwe', '<p>\n	qwe</p>\n', '<p>\n	qwe</p>\n', 31, 'buena 2', 'cupon', '<p>\n	ads</p>\n', 'ads', '', '', 1, 0, NULL, -2, NULL, NULL, NULL, 0, '<p>\n	asd</p>\n', '<p>\n	ad</p>\n', '', 20, 50, 100, '2012-11-23 01:01:01', '2012-11-08 00:00:00', 0, '2012-12-01 01:01:01', '2012-11-09 12:26:10', '1', 'ES-12113112'),
	(32, 1, 1, 'comercio 10', '<p>\n	xcxc</p>\n', '<p>\n	xcxc</p>\n', 37, 'prueba 10', 'cupon', '<p>\n	asfd</p>\n', 'df\ndsf', '', '', 0, 0, NULL, 9, NULL, NULL, NULL, 37, '<p>\n	afas</p>\n', '<p>\n	asfa</p>\n', '', 20, 50, 100, '2012-12-31 01:01:01', '2012-11-26 00:00:00', 0, '2013-01-10 01:01:01', '2012-11-26 06:19:31', '1', 'ES-12113212'),
	(33, 1, 7, 'comercio 20', '<p>\n	sd</p>\n', '<p>\n	sd</p>\n', 35, 'prueba 20', 'cupon', '<p>\n	asdf</p>\n', 'safd\nasdf', '', '', 0, 1, 6, 15, 35, -1, '3', 34, '<p>\n	asdf</p>\n', '<p>\n	asf</p>\n', '', 20, 23, 98, '2012-12-12 01:01:01', '2012-11-26 00:00:00', 0, '2012-12-31 01:01:01', '2012-11-27 07:06:33', '1', 'ES-12113312'),
	(34, 1, 1, 'Centro Médico Estético Body Care', '<p>\n	Avenida Emilio Bar&oacute; 33, 2&ordm;- 4&ordf;</p>\n<p>\n	46020, Valencia</p>\n<p>\n	Tel. 96 389 22 11</p>\n<div>\n	<strong><a href="http://www.clinicabodycare.com"><span style="color:#4e4d4d;">www.clinicabodycare.com</span></a></strong></div>\n', '<p>\n	El <strong>Centro M&eacute;dico Est&eacute;tico Body Care</strong> cuenta con una amplia experiencia en el campo de la medicina &eacute;stetica, y con sus profesionales podr&aacute;s optar a los tratamientos m&aacute;s exclusivos e innovadores.</p>\n<div>\n	&nbsp;</div>\n<div>\n	&iexcl;Llega a la belleza a trav&eacute;s de la salud!</div>\n', 46, 'Mejora la salud de tu piel con una innovadora sesión de mesoterapia en la Clínica Body Care', 'cupon', '<ul>\n	<li style="text-align: justify;">\n		- <strong>1 sesi&oacute;n de 45&rsquo; de duraci&oacute;n, de Mesoterapia con Factor de Crecimiento Plaquetario </strong>individual.</li>\n	<li style="text-align: justify;">\n		- Opci&oacute;n de a&ntilde;adir <strong>vitaminas al Factor de Crecimiento Plaquetario</strong> (con un 15% de descuento sobre el precio habitual de estas vitaminas).</li>\n	<li style="text-align: justify;">\n		- Opci&oacute;n de adquirir <strong>Serum para casa con incorporaci&oacute;n del Factor de Crecimiento Plaquetario</strong> (con un 30% de descuento sobre el precio habitual de este serum).</li>\n</ul>\n', '<ul>\n	<li>\n		- El mejor tratamiento para la salud de tu belleza.</li>\n	<li>\n		- Valoraci&oacute;n y diagn&oacute;stico para un tratamiento individualizado.</li>\n</ul>\n', '2.14540', '41.39355', 0, 1, NULL, 15, NULL, NULL, NULL, 43, '<p style="text-align: justify;">\n	En el <strong>Centro M&eacute;dico Est&eacute;tico Boy Care</strong> recibir&aacute;s el <strong>mejor tratamiento individual y personalizado de Mesoterapia</strong>, con los <strong>mejores profesionales</strong> de Valencia.</p>\n<div style="text-align: justify;">\n	<strong>Realza</strong> tu <strong>belleza</strong> a trav&eacute;s de la<strong> salud</strong>, con este tratamiento que<strong> rejuvenecer&aacute; tu piel</strong> y le aportar&aacute; luminosidad y elasticidad, perdidas con el paso del tiempo sin importar tu edad.</div>\n', '<ul>\n	<li style="text-align: justify;">\n		- Un cup&oacute;n v&aacute;lido por persona.</li>\n	<li style="text-align: justify;">\n		- Sin l&iacute;mite de compra por persona.</li>\n	<li style="text-align: justify;">\n		- Horario: de lunes a viernes, de 09:30h a 13:30h y de 16:00h a 20:00h.</li>\n	<li style="text-align: justify;">\n		- Imprescindible presentar el cup&oacute;n impreso.</li>\n</ul>\n<p style="text-align: justify;">\n	<strong><span style="color:#680034;">Validez</span></strong></p>\n<div>\n	<ul>\n		<li style="text-align: justify;">\n			- Puedes consumir tu cup&oacute;n en un plazo de 3 meses desde la compra del mismo.</li>\n	</ul>\n</div>\n', '', 16, 54, 299, '2012-12-20 01:01:01', '2012-12-03 00:00:00', 0, '2013-03-01 01:01:01', '2012-12-13 02:24:12', '1', 'ES-12113412'),
	(35, 1, 1, 'sadfasf', '<p>\n	asfasf</p>\n', '<p>\n	asdfa</p>\n', 35, 'pastillas de freno', 'cupon', '<p>\n	Revisi&oacute;n de moto de m&aacute;s de 400 c.c.: revisi&oacute;n del aceite, filtro de aceite, filtro de aire y revisi&oacute;n de puntos clave (revisi&oacute;n de discos, de pastillas, de liquido refrigerante, y de neum&aacute;ticos)</p>\n', 'xc\nxc\n', '', '', 0, 1, NULL, 14, NULL, NULL, NULL, 37, '<p>\n	Precio muy bueno en los servicios, si la moto tiene alg&uacute;n defecto se lo dice al cliente para su tranquilidad, con total sinceridad y profesionalidad. Aceptaci&oacute;n muy buena de tus clientes.&nbsp;</p>\n', '<p>\n	N&ordm; de personas que pueden disfrutar con un cup&oacute;n: 1</p>\n<div>\n	M&aacute;ximo de cupones que se puede comprar por persona: 5</div>\n<div>\n	Excepci&oacute;n: Es un cup&oacute;n por matr&iacute;cula lo que sirve</div>\n<div>\n	Horario y/o fechas: 9 y 30 a 13 y 30 de 16 a 20 de lunes a viernes</div>\n<div>\n	Reserva previa (tel&eacute;fono): 963311369&nbsp;</div>\n<div>\n	Cancelaciones (si hay un tiempo m&aacute;ximo):&nbsp;</div>\n<div>\n	Duraci&oacute;n del servicio (si procede): 1 hora y 30&nbsp;</div>\n', '', 20, 30, 100, '2012-12-20 01:01:01', '2012-11-29 00:00:00', 0, '2012-12-11 01:01:01', '2012-11-30 08:22:43', '1', 'ES-12113512'),
	(36, 1, 1, 'Nombre del comercio', '<p>\n	Direcci&oacute;n del comercio</p>\n<p>\n	<a href="http://yahoo.com" target="_blank">link</a></p>\n', '<p>\n	Descripci&oacute;n del comercio</p>\n', 37, 'Prueba del mapa', 'cupon', '<p>\n	mapa</p>\n', '<p>\n	mapa</p>\n<p>\n	segunda linea</p>\n', '-71.5469', '-16.4038', 1, 1, NULL, -2, NULL, NULL, NULL, 43, '<p>\n	mapa</p>\n', '<p>\n	mapa</p>\n', '', -1, 10, 200, '2013-01-17 01:01:01', '2012-11-21 00:00:00', 0, '2013-01-18 01:01:01', '2012-12-08 01:07:56', '1', 'ES-12113612'),
	(37, 1, 5, 'Hotel Balneario Lanjarón 4*', '<p>\n	Hotel Balneario Lanjar&oacute;n 4*</p>\n<div>\n	Avenida Madrid, 2</div>\n<div>\n	(18420) Lanjar&oacute;n<span class="Apple-tab-span" style="white-space:pre"> </span> (Granada)&nbsp;</div>\n<div>\n	&nbsp;</div>\n', '<p>\n	El prestigio de Lanjar&oacute;n como estaci&oacute;n balnearia reside en la variedad y calidad de sus aguas. La falla de Lanjar&oacute;n, frontera entre 2 parajes &uacute;nicos, Sierra Nevada y la Alpujarra, permite que en los entornos pr&oacute;ximos a la poblaci&oacute;n haya emergido una gran profusi&oacute;n de manantiales.</p>\n<p>\n	&nbsp;</p>\n', 44, 'Desconecta 1 noche en Sierra Nevada y disfruta de las aguas termales en Lanjarón por 40,50€', 'cupon', '<ul style="margin: 0px 0px 15px 10px; padding-right: 0px; padding-left: 0px; list-style: none; color: rgb(33, 33, 33); font-family: Arial, sans-serif; font-size: 13px;">\n	<li style="background-image: url(http://www.offerum.com/assets2/img/list.gif); padding: 0px 0px 6px 13px; text-align: justify; background-position: 0px 5px; background-repeat: no-repeat no-repeat;">\n		Alojamiento de 1 noches.</li>\n	<li style="background-image: url(http://www.offerum.com/assets2/img/list.gif); padding: 0px 0px 6px 13px; text-align: justify; background-position: 0px 5px; background-repeat: no-repeat no-repeat;">\n		Desayuno.</li>\n	<li style="background-image: url(http://www.offerum.com/assets2/img/list.gif); padding: 0px 0px 6px 13px; text-align: justify; background-position: 0px 5px; background-repeat: no-repeat no-repeat;">\n		Termas Al-Lanchar: Espacio termal con ba&ntilde;os de contrate caliente, frio y templado y Hamman.</li>\n	<li style="background-image: url(http://www.offerum.com/assets2/img/list.gif); padding: 0px 0px 6px 13px; text-align: justify; background-position: 0px 5px; background-repeat: no-repeat no-repeat;">\n		Hidrataci&oacute;n de manantiales San Vicente, Salud y Capilla.</li>\n	<li style="background-image: url(http://www.offerum.com/assets2/img/list.gif); padding: 0px 0px 6px 13px; text-align: justify; background-position: 0px 5px; background-repeat: no-repeat no-repeat;">\n		Cuestionario de salud.</li>\n	<li style="background-image: url(http://www.offerum.com/assets2/img/list.gif); padding: 0px 0px 6px 13px; text-align: justify; background-position: 0px 5px; background-repeat: no-repeat no-repeat;">\n		Servicio de albornoz y toalla.</li>\n	<li style="background-image: url(http://www.offerum.com/assets2/img/list.gif); padding: 0px 0px 6px 13px; text-align: justify; background-position: 0px 5px; background-repeat: no-repeat no-repeat;">\n		15% de dto. en tratamientos.</li>\n	<li style="background-image: url(http://www.offerum.com/assets2/img/list.gif); padding: 0px 0px 6px 13px; text-align: justify; background-position: 0px 5px; background-repeat: no-repeat no-repeat;">\n		Parking.</li>\n	<li style="background-image: url(http://www.offerum.com/assets2/img/list.gif); padding: 0px 0px 6px 13px; text-align: justify; background-position: 0px 5px; background-repeat: no-repeat no-repeat;">\n		Late check-out hasta las 14h.</li>\n</ul>\n', '<p>\n	- Relax en las terma Al-Lanchar</p>\n<p>\n	- Hidr&aacute;tate en los manantiales</p>\n', '2.2188', '41.4117', 1, 1, NULL, -2, NULL, NULL, NULL, 44, '<div>\n	Uno de los elementos naturales, el agua, bien podr&iacute;a cubrir todo el escudo de la bella poblaci&oacute;n de Lanjar&oacute;n, a los pies de Sierra Nevada, por el significado y la importancia que tiene este l&iacute;quido con poderes en esta zona de Andaluc&iacute;a.</div>\n<div>\n	&nbsp;</div>\n<div>\n	Un lugar donde gozar de la naturaleza en todos sus sentidos y pasar una estancia de relax absoluto con la inestimable ayuda de las aguas termales.</div>\n<div>\n	&nbsp;</div>\n<div>\n	Qu&eacute;date 1 noche en el hotel balneario Lanjar&oacute;n 4* y encuentra el bienestar con sus tratamientos de aguas por s&oacute;lo 40,50&euro; (precio real: 68,64&euro;).</div>\n', '<div>\n	Reserva no v&aacute;lida para habitaci&oacute;n individual, alojamiento m&iacute;nimo de 2 personas.</div>\n<div>\n	Una vez realizada la compra, dir&iacute;gete a &quot;Mis Compras&quot; y selecciona la opci&oacute;n: &quot;Solicitar reserva&quot;.</div>\n<div>\n	En caso de que est&eacute; disponible la fecha solicitada, recibir&aacute;s la confirmaci&oacute;n de la reserva y tu bono.</div>\n<div>\n	Recibir&aacute;s el bono de tu reserva que podr&aacute;s canjear en el establecimiento.</div>\n<div>\n	Si la fecha solicitada no est&aacute; disponible, recibir&aacute;s un e-mail de notificaci&oacute;n y tendr&aacute;s que solicitar otra fecha.</div>\n<div>\n	Hay que llamar para reservar una vez tengas tu cup&oacute;n.</div>\n<div>\n	No se admiten cancelaciones.</div>\n<div>\n	Oferta sujeta a disponibilidad.</div>\n<div>\n	Oferta no v&aacute;lida para las fechas: del 16/12 al 17/01, ni para el puente de diciembre.</div>\n<div>\n	Cup&oacute;n v&aacute;lido hasta el: 09/02/2013.</div>\n', '', 16, 40, 68.64, '2012-12-20 01:01:01', '2012-12-04 00:00:00', 0, '2013-02-01 01:01:01', '2012-12-04 11:38:16', '1', 'ES-12113712'),
	(38, 1, 1, 'Collados Peluqueros', '<p>\n	Calle 29, n&ordm; 53</p>\n<p>\n	46182, La Ca&ntilde;ada - Paterna, Valencia</p>\n<p>\n	Tel. 96 132 22 95</p>\n<p>\n	<strong><a href="http://colladospeluqueros.com" target="_blank"><span style="color:#4e4d4d;">www.colladospeluqueros.com</span></a></str', '<p style="text-align: justify;">\n	<strong>Collados Peluqueros</strong>, es un sal&oacute;n de<strong> peluquer&iacute;a de vanguardia</strong> en el que podr&aacute;s encontrar un trato excelente, con los servicios m&aacute;s novedosos, coloraci&oacute;n sin amon&iacute;aco, extensiones, tratamientos de keratina y botox.</p>\n<p style="text-align: justify;">\n	Especialistas en novias.</p>\n', 48, '¡Melenas al aire! Alisados como nunca has visto con el mejor tratamiento de Keratina o Botox', 'cupon', '<ul>\n	<li>\n		<strong>- 1 tratamiento capilar de Keratina o Botox.</strong></li>\n</ul>\n', '<ul>\n	<li>\n		- Tratamientos ultraefectivos como jam&aacute;s has so&ntilde;ado.</li>\n	<li>\n		- Trato excelente con los servicios m&aacute;s novedosos.</li>\n</ul>\n', '-0.4819019000000253', '39.5300194131047', 0, 1, NULL, 15, NULL, NULL, NULL, 49, '<p style="text-align: justify;">\n	<strong>&iquest;Cansada de no lograr que tu pelo deslumbre?</strong> Los cambios que sufre el pelo por el estr&eacute;s o por los cambios de temperatura, lo castigan e impiden que brille con luz propia.</p>\n<p style="text-align: justify;">\n	Con este<strong> tratamiento de Keratina puedes conseguir </strong>que tu <strong>pelo luzca impresionante durante mucho m&aacute;s tiempo, evitando </strong>el<strong> encrespamiento y la sequedad</strong> tan<strong> antiest&eacute;ticos.</strong></p>\n<p style="text-align: justify;">\n	<strong>En Collados Peluqueros </strong>puedes<strong> elegir</strong> entre <strong>dos tratamientos, </strong>bien <strong>alisar tu pelo como nunca hab&iacute;as imaginado</strong> hasta ahora,<strong> o</strong> si lo prefieres<strong> recuperar la vida de tu cabello y presumir de melena super cuidada, &iexcl;por tan solo 90&euro;!</strong></p>\n', '<ul>\n	<li>\n		- Un cup&oacute;n v&aacute;lido por persona.</li>\n	<li>\n		- Sin l&iacute;mite de compra por persona.</li>\n	<li>\n		- Necesaria reserva previa: 96 132 22 95.</li>\n	<li>\n		- Horario:</li>\n	<li>\n		&nbsp;&nbsp;&nbsp;&nbsp; Lunes a viernes de 9:30 h. a 14:00 h. y de 15:30 h. a 20:00 h.</li>\n	<li>\n		&nbsp;&nbsp;&nbsp;&nbsp; Martes tarde cerrado.</li>\n	<li>\n		&nbsp;&nbsp;&nbsp;&nbsp; S&aacute;bados de 9:00 h. a 14:00 h.</li>\n	<li>\n		- Imprescindible presentar el cup&oacute;n impreso.</li>\n</ul>\n<p>\n	&nbsp;</p>\n<p>\n	<span style="color:#680034;"><strong>Validez</strong></span></p>\n<ul>\n	<li>\n		- Puedes comprar tu cup&oacute;n hasta el 10 de enero de 2013.</li>\n	<li>\n		- Puedes consumir tu cup&oacute;n hasta el 10 de marzo de 2013.</li>\n	<li>\n		- No podr&aacute;s consumir tu cup&oacute;n entre el 21 de diciembre de 2012 y el 31 de diciembre de 2012, ambos inclusive.</li>\n</ul>\n', '', 16, 40, 150, '2013-01-10 01:01:01', '2012-12-13 00:00:00', 0, '2013-03-10 01:01:01', '2012-12-13 02:24:51', '1', 'ES-12133812'),
	(39, 1, 1, 'BCN Fisio', '<p>\n	Tel. 617 093 396</p>\n<p>\n	<a href="http://www.bcnfisio.com" target="_blank"><span style="color:#4e4d4d;"><strong>www.bcnfisio.com</strong></span></a></p>\n', '<p>\n	En <strong>BCN Fisio</strong> estamos especializados en <strong>lesiones deportivas</strong>, tanto de deportistas de alto rendimiento como de personas que buscan<strong> Fitness</strong>.</p>\n<p>\n	Prevenimos tratamientos y eliminamos<strong> lesiones de espalda</strong> (lumbares, cervicales),<strong> rodilla</strong>, tobillo, <strong>hombro</strong>, etc. abordando la lesi&oacute;n de manera global, tanto desde la fisioterapia (para eliminar el dolor), como desde el entrenamiento para prevenir reca&iacute;das.</p>\n<p>\n	Nuestro equipo, con amplia experiencia, te ofrece el mejor servicio y una atenci&oacute;n personalizada aplicando las t&eacute;cnicas m&aacute;s modernas, para conservar tu bienestar y para aumentar tu capacidad de rendimiento.</p>\n', 51, '¡Espaldas rectas! Termina con los dolores con una sesión de readaptación y reeducación postural', 'cupon', '<ul>\n	<li>\n		- <strong>Sesi&oacute;n de diagn&oacute;stico y reequilibrio postural</strong> (90&rsquo; aprox.).</li>\n</ul>\n', '<ul>\n	<li>\n		- Activa tu musculatura en tiempo r&eacute;cord.</li>\n	<li>\n		- Termina con las molestias y los dolores musculares.</li>\n</ul>\n', '', '', 0, 1, NULL, 7, NULL, NULL, NULL, 52, '<p>\n	Mejora tus h&aacute;bitos posturales, que consciente o inconscientemente te crean molestias y que se agravan con los a&ntilde;os, con una soluci&oacute;n tan sencilla como volver a adoptar las posturas anat&oacute;micas o fisiol&oacute;gicas adecuadas, tanto de pie como sentado, en tu puesto de trabajo, caminando o incluso estirado.</p>\n<p>\n	<strong>Elimina de forma definitiva contracturas cr&oacute;nicas, molestias de espalda</strong>, dolores articulares, etc. <strong>mediante </strong>un trabajo de <strong>reequilibrio muscular</strong>, <strong>reeducaci&oacute;n </strong>de la <strong>postura y activaci&oacute;n muscular.</strong> Conoce, adem&aacute;s, que <strong>posturas o ejercicios</strong> debes <strong>realizar para no recaer </strong>en las mismas lesiones.</p>\n<p>\n	Los <strong>Fisioterapeutas y Entrenadores Personales de BCN Fisio</strong> te ofrecen:</p>\n<p>\n	- <strong>Una sesi&oacute;n de diagn&oacute;stico y reequilibrio de 90&rsquo;</strong> de duraci&oacute;n, donde te gu&iacute;amos, explicamos y te ayudamos en la <strong>readaptaci&oacute;n y reeducaci&oacute;n postural</strong>. <strong>Realizaremos ejercicios adaptados a t&iacute; para activar la musculatura</strong> que debido al desuso est&aacute; d&eacute;bil e inhibida y evitar actitudes posturales inadecuadas.</p>\n', '<ul>\n	<li>\n		- Un cup&oacute;n v&aacute;lido por persona.</li>\n	<li>\n		- Compra m&aacute;xima de 1 cup&oacute;n por persona.</li>\n	<li>\n		- Horario: de lunes a viernes, de 09:30h a 21:30h.</li>\n	<li>\n		- Necesaria reserva previa: 617 093 396.</li>\n	<li>\n		- Cancelaciones: m&aacute;ximo 24 horas antes de la sesi&oacute;n.</li>\n	<li>\n		- 1 sesi&oacute;n de 90&rsquo; de duraci&oacute;n.</li>\n	<li>\n		- Imprescindible presentar el cup&oacute;n impreso.</li>\n</ul>\n<p>\n	&nbsp;</p>\n<p>\n	<span style="color:#680034;"><strong>&iquest;D&oacute;nde recibes las sesiones?</strong></span></p>\n<p>\n	A escoger entre:</p>\n<ul>\n	<li>\n		&nbsp;&nbsp; - Gimnasio Oddisey (zona Sants),</li>\n	<li>\n		&nbsp;&nbsp; - Estudio de entrenadores Fisiomind (zona Les Corts).</li>\n	<li>\n		&nbsp;&nbsp; - O bien a domicilio.</li>\n</ul>\n<p>\n	&nbsp;</p>\n<p>\n	<span style="color:#680034;"><strong>Validez</strong></span></p>\n<ul>\n	<li>\n		- Puedes consumir tu cup&oacute;n en un plazo de 3 meses desde la compra del mismo.</li>\n</ul>\n', '', 16, 55, 60, '2012-12-31 01:01:01', '2012-12-13 00:00:00', 0, '2013-03-13 01:01:01', '2012-12-13 01:19:21', '1', 'ES-12133912'),
	(40, 1, 1, 'Cocktail Bar Casablanca', '<p>\n	Calle Dos de Maig 277</p>\n<p>\n	08025, Barcelona</p>\n<p>\n	Tel. 93 455 29 24</p>\n<p>\n	<a href="http://www.casablancacocktailbar.es" target="_blank"><span style="color:#4e4d4d;"><strong>www.casablancacocktailbar.es</strong></span></a></p>\n', '<p>\n	<strong>&iexcl;Cocktail Bar Casablanca</strong> es un lugar diferente en Barcelona!</p>\n<p>\n	<strong>Cenas y cocktails</strong> de todo tipo unidos en <strong>el mejor ambiente</strong> posible para disfrutar con tu pareja o amigos.</p>\n<p>\n	<strong>&iexcl;29 a&ntilde;os de experiencia nos avalan!</strong></p>\n', 54, 'Disfruta de 1 cena para 2 y elige 2 cocktails exquisitos en el Cocktail Bar Casablanca', 'cupon', '<ul>\n	<li>\n		- <strong>Ensalada para 2</strong> (a escoger entre ensalada americana, cocktail, tropical, suiza y especial).</li>\n	<li>\n		- <strong>2 &ldquo;torradas&rdquo;</strong> (a escoger entre bolo&ntilde;esa, Casablanca, at&uacute;n, lomo con queso, roquefort, salm&oacute;n, criolla, tejana, campera y pollo).</li>\n	<li>\n		- <strong>Bebida para 2</strong> (a escoger entre agua, refresco, cerveza o vino de la casa).</li>\n	<li>\n		- <strong>2 cocktails</strong> (a escoger entre una variedad de 33 combinados: cocktails sin alcohol, cocktails Casablanca o cocktails con cava).</li>\n</ul>\n', '<ul>\n	<li>\n		- Los mejores cocktails de Barcelona.</li>\n	<li>\n		- Ambiente ideal para empezar la noche.</li>\n</ul>\n', '2.17935829999999', '41.40895241400479', 0, 1, NULL, -1, NULL, NULL, NULL, 55, '<p>\n	Pasa una <strong>velada agradable</strong> con quien t&uacute; quieras en unos de los Cocktail Bar con mejor ambiente de Barcelona.</p>\n<p>\n	Disfruta de una <strong>cena</strong> compuesta por ensalada a compartir, &ldquo;torrada&rdquo; y bebida. Y remata la noche con un <strong>cocktail a elegir</strong> de entre una gran variedad. El <strong>ambiente acogedor y desenfadado</strong> que se respira en el Cocktail Bar Casablanca har&aacute; que no os quer&aacute;is levantar de sus c&oacute;modos sof&aacute;s.</p>\n', '<ul>\n	<li>\n		- M&aacute;ximo 2 cupones por persona.</li>\n	<li>\n		- Un cup&oacute;n v&aacute;lido para 2 personas.</li>\n	<li>\n		- Horario: martes, mi&eacute;rcoles, jueves y domingo de 20:00 h. a 02:30 h.</li>\n	<li>\n		- No es necesario llamar al comercio para reservar.</li>\n	<li>\n		- Imprescindible presentar el cup&oacute;n impreso.</li>\n</ul>\n<p>\n	<span style="color:#680034;"><strong>Validez</strong></span></p>\n<ul>\n	<li>\n		- Puedes consumir tu cup&oacute;n hasta el 1 de marzo de 2013.</li>\n	<li>\n		- No podr&aacute;s consumir tu cup&oacute;n entre el 21 de diciembre de 2012 y el 7 de enero de 2013, ambos inclusive.</li>\n</ul>\n', '', 16, 40, 34, '2012-12-31 01:01:01', '2012-12-13 00:00:00', 0, '2013-03-01 01:01:01', '2012-12-13 02:30:20', '1', 'ES-12134012'),
	(41, 1, 1, 'PFK Entrenadores', '<p>\n	Tel. 617 093 396</p>\n<p>\n	<a href="http://www.pfkentrenadores.com" target="_blank"><span style="color:#4e4d4d;"><strong>www.pfkentrenadores.com</strong></span></a></p>\n', '<p>\n	En <strong>PFK Entrenadores Personales</strong> nos dedicamos a la <strong>salud</strong> y al <strong>fitness</strong>.</p>\n<p>\n	A trav&eacute;s del <strong>Entrenamiento Personal (Personal Trainer)</strong>, te solucionamos todo lo relacionado con el Fitness &amp; Wellnes, la <strong>prevenci&oacute;n y recuperaci&oacute;n de lesiones </strong>(readaptaci&oacute;n), <strong>problemas de espalda</strong>, la<strong> preparaci&oacute;n f&iacute;sica</strong> de <strong>deportistas de rendimiento</strong>, p&eacute;rdida de peso o hipertrofia muscular.</p>\n<p>\n	Ofrecemos <strong>desplazamientos a domicilio</strong> y la <strong>posibilidad</strong> de <strong>acudir</strong> a <strong>diversas instalaciones deportivas</strong> repartidas por toda <strong>Barcelona.</strong></p>\n<p>\n	Ponemos a tu disposici&oacute;n a los mejores <strong>Entrenadores Personales titulados</strong>, y todo con un <strong>tratamiento</strong> siempre <strong>personalizado y con flexibilidad de horarios.</strong></p>\n', 59, '¡Sé el próximo IronMan! Mejora tu marca con un entrenamiento de fondo y ultrafondo', 'cupon', '<p>\n	-<strong> Entrenamiento de fondo y ultrafondo</strong>:</p>\n<ul>\n	<li>\n		&nbsp;&nbsp; &nbsp;2 sesiones de 60&rsquo;</li>\n	<li>\n		&nbsp;&nbsp; &nbsp;Test de nivel</li>\n	<li>\n		&nbsp;&nbsp; &nbsp;Correcci&oacute;n t&eacute;cnica</li>\n	<li>\n		&nbsp;&nbsp; &nbsp;Planificaci&oacute;n y programaci&oacute;n de los entrenamientos del primer mes<br />\n		&nbsp;</li>\n</ul>\n', '<ul>\n	<li>\n		- Optimiza tu cuerpo y &iexcl;sup&eacute;rate!</li>\n	<li>\n		- Mejora tus marca y evita lesiones.</li>\n</ul>\n', '', '', 0, 0, NULL, 7, NULL, NULL, NULL, 62, '<p>\n	<strong>Mejora tu marca en pruebas de resistencia:</strong> marathon, media marathon, triatl&oacute;n, ironman, carreras de monta&ntilde;a a pie o en bici.</p>\n<p>\n	<strong>Obt&eacute;n</strong> los <strong>mejores resultados</strong> con un <strong>entrenamiento supervisado</strong> por un <strong>profesional.</strong> Adem&aacute;s <strong>optimiza</strong> el<strong> tiempo</strong> de tus <strong>entrenamientos y evita lesiones.</strong></p>\n<p>\n	<strong>Con estas 2 sesiones de 60&lsquo; de entrenamiento obtienes:</strong></p>\n<ul>\n	<li>\n		- <strong>Test de nivel</strong>, de tus cualidades f&iacute;sicas b&aacute;sicas para mejorar tus marcas.</li>\n	<li>\n		- <strong>Correcci&oacute;n t&eacute;cnica</strong>, para mejorar la ergonom&iacute;a y eficacia de tu gesto deportivo.</li>\n	<li>\n		- <strong>Planificaci&oacute;n y programaci&oacute;n de los entrenamientos del primer mes</strong>, para alargar tu vida deportiva y prevenir lesiones.</li>\n</ul>\n', '<ul>\n	<li>\n		- Un cup&oacute;n v&aacute;lido por persona.</li>\n	<li>\n		- Compra m&aacute;xima de 1 cup&oacute;n por persona.</li>\n	<li>\n		- Horario: de lunes a viernes, de 09:30 h. a 21:30 h.</li>\n	<li>\n		- Necesaria reserva previa: 617 093 396.</li>\n	<li>\n		- Cancelaciones: m&aacute;ximo 24 horas antes de la sesi&oacute;n.</li>\n	<li>\n		- 2 sesiones de 60&rsquo; de duraci&oacute;n, cada una.</li>\n	<li>\n		- Imprescindible presentar el cup&oacute;n impreso.</li>\n</ul>\n<p>\n	<br />\n	<span style="color:#680034;"><strong>&iquest;D&oacute;nde recibes las sesiones?</strong></span></p>\n<p>\n	A escoger entre:</p>\n<ul>\n	<li>\n		&nbsp;&nbsp; &nbsp;Gimnasio Oddisey (zona Sants),</li>\n	<li>\n		&nbsp;&nbsp; &nbsp;Estudio de entrenadores Fisiomind (zona Les Corts).</li>\n	<li>\n		&nbsp;&nbsp; &nbsp;O bien a domicilio.</li>\n</ul>\n<p>\n	<span style="color:#680034;"><strong>Validez</strong></span></p>\n<ul>\n	<li>\n		- Puedes consumir tu cup&oacute;n en un plazo de 3 meses desde la compra del mismo.</li>\n</ul>\n', '', 16, 55, 100, '2012-12-31 01:01:01', '2012-12-13 00:00:00', 0, '2013-03-13 01:01:01', '2012-12-13 02:18:39', '1', 'ES-12134112');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.rankings
DROP TABLE IF EXISTS `rankings`;
CREATE TABLE IF NOT EXISTS `rankings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `min` int(10) DEFAULT NULL,
  `max` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`file_id`),
  CONSTRAINT `FK_ranking` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table redovl_tiendaovlv2.rankings: ~0 rows (approximately)
/*!40000 ALTER TABLE `rankings` DISABLE KEYS */;
/*!40000 ALTER TABLE `rankings` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.roles: ~3 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `description`) VALUES
	(1, 'login', 'Login privileges, granted after account confirmation'),
	(2, 'admin', 'Administrative user, has access to everything.'),
	(3, 'api_client', 'Remote App Client');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.roles_rules
DROP TABLE IF EXISTS `roles_rules`;
CREATE TABLE IF NOT EXISTS `roles_rules` (
  `rol_id` int(10) NOT NULL AUTO_INCREMENT,
  `rule_id` int(10) NOT NULL,
  PRIMARY KEY (`rol_id`,`rule_id`),
  KEY `FK_roles_rules_rules` (`rule_id`),
  CONSTRAINT `FK_roles_rules_rules` FOREIGN KEY (`rule_id`) REFERENCES `rules` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.roles_rules: ~0 rows (approximately)
/*!40000 ALTER TABLE `roles_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_rules` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.roles_users
DROP TABLE IF EXISTS `roles_users`;
CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`),
  CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.roles_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
	(1, 1),
	(1, 2);
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.rules
DROP TABLE IF EXISTS `rules`;
CREATE TABLE IF NOT EXISTS `rules` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.rules: ~9 rows (approximately)
/*!40000 ALTER TABLE `rules` DISABLE KEYS */;
INSERT INTO `rules` (`id`, `code`, `description`) VALUES
	(1, 'ADD_USER', 'Permite agregar un usuario desde el administrador'),
	(2, 'EDIT_USER', 'Permite editar los usuario desde un aministrador'),
	(3, 'DELETE_USER', 'Permite eliminar los usuario desde un aministrador'),
	(4, 'ADD_ROLES', 'Permite agregar roles desde el administrador'),
	(5, 'EDIT_ROLES', 'Permite editar roles'),
	(6, 'DELETE_ROLES', 'Permite eliminar roles'),
	(7, 'ASIGN_ROLES_TO_USER', 'Asignar roles a usuarios'),
	(8, 'ASIGN_RULES_TO_ROLES', 'Asignar reglas a roles'),
	(9, 'ACCESS_ADMIN', 'Ingresar a admin');
/*!40000 ALTER TABLE `rules` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.sections
DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.sections: 0 rows
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.tiendas
DROP TABLE IF EXISTS `tiendas`;
CREATE TABLE IF NOT EXISTS `tiendas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `linea_id` int(10) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` text,
  `order` int(10) DEFAULT NULL,
  `comision_ovl` float DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.tiendas: ~7 rows (approximately)
/*!40000 ALTER TABLE `tiendas` DISABLE KEYS */;
INSERT INTO `tiendas` (`id`, `linea_id`, `code`, `name`, `description`, `order`, `comision_ovl`, `created`, `status`) VALUES
	(1, 1, 'oferta_dia', 'Ofertas del día', 'Ofertas el día', 1, 10, '2012-08-09 17:20:25', 1),
	(2, 1, 'planes_dia', 'Planes del día', 'Planes del día', 1, 15, '2012-08-09 17:21:09', 1),
	(3, 2, 'iten_out', 'Tienda Out', NULL, 1, 10, '2012-08-18 11:08:51', 1),
	(4, 2, 'segunda_ti', 'Segunda tienda', NULL, 2, 10, '2012-08-29 04:08:52', 1),
	(5, 3, 'viajes', 'Viajes', NULL, 1, 10, '2012-08-29 04:08:58', 1),
	(6, 4, 'cursos', 'Cursos', NULL, 1, 10, '2012-08-29 04:08:33', 1),
	(7, 5, 'permanente', 'Ofertas permanentes', NULL, 100, 15, '2012-09-10 05:09:10', 1);
/*!40000 ALTER TABLE `tiendas` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.ubicaciones
DROP TABLE IF EXISTS `ubicaciones`;
CREATE TABLE IF NOT EXISTS `ubicaciones` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.ubicaciones: ~53 rows (approximately)
/*!40000 ALTER TABLE `ubicaciones` DISABLE KEYS */;
INSERT INTO `ubicaciones` (`id`, `parent_id`, `nombre`) VALUES
	(1, -1, 'España'),
	(2, 1, 'Alicante'),
	(3, 1, 'Barcelona'),
	(4, 1, 'Madrid'),
	(5, 1, 'Tarragona'),
	(6, 1, 'Valencia'),
	(7, 3, 'Barcelona'),
	(8, 3, 'Hospitalet de LLobregat'),
	(9, 3, 'Sabadell'),
	(10, 3, 'Terrassa'),
	(11, 3, 'Esplugues de LLobregat'),
	(12, 3, 'Badalona'),
	(13, 3, 'Santa Coloma de Gramenet'),
	(14, 5, 'Tarragona'),
	(15, 6, 'Valencia'),
	(16, 4, 'Madrid'),
	(17, 2, 'Alicante'),
	(18, 7, 'Ciutat Vella'),
	(19, 7, 'L\'Eixample'),
	(20, 7, 'Sants-Montuic'),
	(21, 7, 'Les Corts'),
	(22, 7, 'Sarrià-Sant Gervasi'),
	(23, 7, 'Gràcia'),
	(24, 7, 'Horta-Guinardó'),
	(25, 7, 'Nou Barris'),
	(26, 7, 'Sant Andreu'),
	(27, 7, 'Sant Martí'),
	(28, 8, 'Distrito 5'),
	(29, 9, 'Distrito 1'),
	(30, 10, 'Distrito 6'),
	(31, 11, 'Todos'),
	(32, 12, 'Distrito 4'),
	(33, 13, 'Distrito 1'),
	(34, 14, 'Distrito 4'),
	(35, 15, 'Ciutat Vella'),
	(36, 15, 'Eixample'),
	(37, 17, 'Distrito 1'),
	(38, 17, 'Distrito 2'),
	(39, 16, 'Chamartín'),
	(40, 16, 'Salamanca'),
	(41, 18, 'El Raval'),
	(42, 18, 'El Gótic'),
	(43, 19, 'Sant Antoni'),
	(44, 20, 'Sants'),
	(45, 21, 'Les Corts'),
	(46, 22, 'Sarrià'),
	(47, 23, 'Vila de Gràcia'),
	(48, 24, 'Horta'),
	(49, 25, 'Les Roquetes'),
	(50, 26, 'Sant Andreu'),
	(51, 26, 'La Sagrera'),
	(52, 27, 'El Camp de l\'Arpa del Clot'),
	(53, 27, 'El Clot');
/*!40000 ALTER TABLE `ubicaciones` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(127) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) DEFAULT '',
  `last_name` varchar(60) DEFAULT '',
  `distrito` varchar(50) DEFAULT '',
  `puntaje` int(10) DEFAULT NULL,
  `sexo` varchar(20) DEFAULT '',
  `telefono` varchar(32) DEFAULT '',
  `fecha_nacimiento` datetime DEFAULT NULL,
  `dni` int(8) DEFAULT NULL,
  `password` char(64) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  `reset_token` char(64) NOT NULL DEFAULT '',
  `status` varchar(20) NOT NULL DEFAULT '',
  `facebook_user_id` bigint(20) NOT NULL,
  `last_failed_login` datetime NOT NULL,
  `failed_login_count` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `username`, `name`, `last_name`, `distrito`, `puntaje`, `sexo`, `telefono`, `fecha_nacimiento`, `dni`, `password`, `logins`, `last_login`, `reset_token`, `status`, `facebook_user_id`, `last_failed_login`, `failed_login_count`, `created`, `modified`) VALUES
	(1, 'gerson@qd.pe', 'admin', '', '', '', NULL, '', '', NULL, NULL, 'f2a233d912774ab8aa3ff5faac501d27f40374ce74e8b127eb432015c72056d3', 219, 1355397790, '', '', 0, '2012-11-30 10:14:14', 0, '2012-05-23 12:32:55', '2012-12-13 12:23:10');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.user_identities
DROP TABLE IF EXISTS `user_identities`;
CREATE TABLE IF NOT EXISTS `user_identities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `identity` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table redovl_tiendaovlv2.user_identities: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_identities` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_identities` ENABLE KEYS */;


-- Dumping structure for table redovl_tiendaovlv2.user_tokens
DROP TABLE IF EXISTS `user_tokens`;
CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table redovl_tiendaovlv2.user_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
