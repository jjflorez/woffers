<div style="margin-top: 20px;">
	<ul class="nav nav-tabs">
		<li><a href="<?php echo URL::site('user/profile'); ?>">Perfil</a> </li>
		<li><a href="<?php echo URL::site('user/profile_edit'); ?>">Editar perfil</a> </li>
		<li class="active"><a href="<?php echo URL::site('user/unregister'); ?>">Eliminar cuenta</a> </li>
	</ul>
	<div class="tab-content">
		<!-- tab content -->

		   <h2><?php echo __('confirm.remove.user.account'); ?></h2>
			<?php
				echo Form::open('user/unregister/'.$id, array('style' => 'display: inline;'));

				echo Form::hidden('id', $id);

				echo '<p>'.__('?sure.to.remove.user.account').'</p>';

				echo '<p>'.Form::radio('confirmation', 'Y').' '.__('yes').'<br/>';
				echo Form::radio('confirmation', 'N', true).' '.__('no').'<br/></p>';

				echo '<button class="btn btn-primary" type="submit">' . __('confirm') . '</button>';
				echo Form::close();

				echo Form::open('user/profile', array('style' => 'display: inline; padding-left: 10px;'));
				echo '<button class="btn" type="submit">' . __('cancel') . '</button>';
				echo Form::close();
			?>

		<!-- end tab content -->
	</div>
</div>