<?php
	$form = new Appform();
	if (isset($errors)) {
		$form->errors = $errors;
	}
	if (isset($data)) {
		unset($data['password']);
		unset($data['password_confirm']);
		$form->values = $data;
	}
	echo $form->open('admin_user/edit/' . $id);
	echo $form->hidden('id', $id);
?>

<div style="margin-top: 20px;">
	<ul class="nav nav-tabs">
		<li><a href="<?php echo URL::site('admin_user'); ?>"><?php echo __('administer.users'); ?></a> </li>
		<li class="active"><a href="<?php echo URL::site('admin_user/edit'); ?>"><?php echo __('?add.user'); ?></a> </li>
		<li><a href="<?php echo URL::site('admin_user/roles'); ?>"><?php echo __('administer.roles'); ?></a> </li>
	</ul>

	<div class="tab-content">
		<h3><?php echo __('?edit.or.add.user') ?></h3>
			<?php echo View::factory('user/user_edit_form')->set(array('form' => $form)); ?>

			<h3><?php echo __('roles'); ?></h3>
			<table class="table table-bordered table-striped">
				<thead>
					<tr class="heading">
						<th></th>
						<th><?php echo __('role'); ?></th>
						<th><?php echo __('description'); ?></th>
					</tr>
				</thead>
				<?php
					$i = 0;
					foreach ($all_roles as $role => $description) {
						echo '<tr';
						if ($i % 2 == 0) {
							echo ' class="odd"';
						}
						echo '>';
						echo '<td>' . Form::checkbox('roles[' . $role . ']', $role, (in_array($role, $user_roles) ? true
									: false)) . '</td>';
						echo '<td>' . ucfirst($role) . '</td><td>' . $description . '</td>';
						echo '</tr>';
						$i++;
					}
				?>
			</table>
			<p style="text-align: center;"><button type="submit" class="btn btn-primary"><?php echo __('save'); ?></button></p>
			<?php		echo $form->close();		?>
	</div>
</div>
