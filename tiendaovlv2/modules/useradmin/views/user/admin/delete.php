<div style="margin-top: 20px;">
	<ul class="nav nav-tabs">
		<li class="active"><a href="<?php echo URL::site('admin_user'); ?>"><?php echo __('administer.users'); ?></a>
		</li>
		<li><a href="<?php echo URL::site('admin_user/edit'); ?>"><?php echo __('?add.user'); ?></a></li>
	</ul>
	<div class="tab-content">
		<!-- tab content -->

		<h2><?php echo __('?delete.user'); ?></h2>

		<?php
			echo Form::open('admin_user/delete/' . $id, array('style' => 'display: inline;'));

			echo Form::hidden('id', $id);

			echo '<p>' . __('?sure.to.delete.user', array(':user' => $data['username'])) . '</p>';

			echo '<p>' . Form::radio('confirmation', 'Y', false, array('id' => 'conf_y')) . ' <label for="conf_y" style="display: inline;">' . __('yes') . '</label><br/>';
			echo Form::radio('confirmation', 'N', true, array('id' => 'conf_n')) . ' <label for="conf_n" style="display: inline;">' . __('no') . '</label><br/></p>';
			echo Form::submit(NULL, __('delete'), array('class'=>'btn btn-primary'));
			echo Form::close();

			echo Form::open('admin_user/index', array('style' => 'display: inline; padding-left: 10px;'));
			echo Form::submit(NULL, __('cancel'), array('class'=>'btn'));
			echo Form::close();
		?>

	</div>
</div>

