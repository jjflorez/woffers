<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset=utf-8 />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>
		<?php
			if(isset($title))
				echo $title . ' :: ' . 'OVL Media';
			else
				echo 'OVL :. Administración multimedia';
		?>
	</title>
	<?php
		echo "\t" . Html::style('public/css/bootstrap.css') . "\n";
		//echo "\t" . Html::style('public/css/bootstrap-responsive.css') . "\n";
		echo "\t" . Html::style('public/css/admin.css') . "\n";
		echo "\t" . Html::style('public/css/style.css') . "\n";
        echo "\t" . Html::style('public/css/dp/datepicker.css') . "\n";

		echo "\t" . Html::script('public/js/jquery1_7_2.js') . "\n";
		echo "\t" . Html::script('public/plugins/qd.core.js') . "\n";
		echo "\t" . Html::script('public/js/bootstrap.js') . "\n";
		echo "\t" . Html::script('public/js/bootstrap.js') . "\n";
		echo "\t" . Html::script('public/js/swfobject.js') . "\n";
		echo "\t" . Html::script('public/plugins/fb_lf.js') . "\n";
        echo "\t" . Html::script('public/js/dp/datepicker.js') . "\n";
        echo "\t" . Html::script('public/plugins/uploader/fileuploader.js') . "\n";

		if(isset($header))	echo $header;

    //<script src="//connect.facebook.net/en_US/all.js"></script>
    
	?>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>	<![endif]-->
	<!--[if lt IE 9]>		<?php echo "\t" . Html::style('public/css/appFaces_IE.css') . "\n"; ?>	<![endif]-->

	<link rel="shortcut icon" href="<?php echo URL::base(); ?>public/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo URL::base(); ?>ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo URL::base(); ?>ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="<?php echo URL::base(); ?>ico/apple-touch-icon-57-precomposed.png">
	<script type="text/javascript">
		var  Config = new Object();
		Config.Path = "<?php echo URL::base(); ?>";
		Config.Site = "<?php echo URL::site(); ?>";
	</script>
</head>
<body>
	<div id="fb-root"></div>

	<!-- Header web site -->
	<div id="header_media">

		<!-- Start Session top bar -->

		<!-- End Session top bar -->

	</div>
	<!-- End web site -->

	<div id="content">
		<!-- content -->
		<?php	if(isset($content)){echo View::factory('useradmin/messages') . $content;}	?>
		<!-- end content -->
	</div>

</body>
</html>
