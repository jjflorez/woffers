<!DOCTYPE html>
<html lang="es">
<head>

	<style>
		.lista_productos_general.permanentes_general > li{
			height: 285px !important;
		}
		.titulo_c{
			font-family: Arial;
			font-weight: bold;
			color: #757373;
			font-size: 12px;
			margin: 8px;
		}
		#filtro{}
		#filtro td{
			padding: 6px 0;
		}
		#filtro .t_border{
			border-left: solid 1px #aeaeae;
			padding-left: 14px;
		}

	</style>
	<meta charset=utf-8 />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>
		<?php
		if(isset($title))
			echo $title . ' :: ' . 'OVL';
		else
			echo 'OVL :. Las mejores ofertas en tu ciudad';
		?>
	</title>
	<?php
	echo "\t" . Html::style('public/css/bootstrap.css') . "\n";
	//echo "\t" . Html::style('public/css/bootstrap-responsive.css') . "\n";
	echo "\t" . Html::style('public/css/admin.css') . "\n";
	echo "\t" . Html::style('public/css/style.css') . "\n";
	echo "\t" . Html::style('public/css/eddy.css') . "\n";
	echo "\t" . Html::style('public/css/dp/datepicker.css') . "\n";
	echo "\t" . Html::style('public/css/humanity/jquery-ui-1.8.23.custom.css') . "\n";

	echo "\t" . Html::script('public/js/jquery-1.8.0.min.js') . "\n";
	echo "\t" . Html::script('public/js/jquery-ui-1.8.23.custom.min.js') . "\n";
	echo "\t" . Html::script('public/plugins/qd.core.js') . "\n";
	echo "\t" . Html::script('public/js/bootstrap.js') . "\n";


	echo "\t" . Html::script('public/js/list_appearance.js') . "\n";

	echo "\t" . Html::script('public/js/bootstrap.js') . "\n";
	echo "\t" . Html::script('public/js/swfobject.js') . "\n";
	echo "\t" . Html::script('public/plugins/jquery.validate.js') . "\n";
	echo "\t" . Html::script('public/plugins/jquery.zrssfeed.js') . "\n";

	//echo "\t" . Html::script('public/plugins/fb_lf.js') . "\n";
	//echo "\t" . Html::script('public/js/dp/datepicker.js') . "\n";
	echo "\t" . Html::script('public/plugins/uploader/fileuploader.js') . "\n";
	echo "\t" . Html::script('public/plugins/ckeditor/ckeditor.js') . "\n";

	echo "\t" . Html::script('public/plugins/qd.core.js') . "\n";
	echo "\t" . Html::script('public/plugins/qd.app.js') . "\n";

	echo "\t" . Html::script('public/plugins/colorbox/jquery.colorbox-min.js') . "\n";
	echo "\t" . Html::style('public/plugins/colorbox/colorbox.css') . "\n";

	echo "\t" . Html::script('public/plugins/chosen/chosen.jquery.js') . "\n";
	echo "\t" . Html::script('public/plugins/chosen/docsupport/prism.js') . "\n";
	echo "\t" . Html::style('public/plugins/chosen/chosen.css') . "\n";
	echo "\t" . Html::style('public/plugins/chosen/docsupport/prism.css') . "\n";

	if(isset($header))	echo $header;

	//<script src="//connect.facebook.net/en_US/all.js"></script>

	?>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>	<![endif]-->
	<!--[if lt IE 9]>		<?php echo "\t" . Html::style('public/css/appFaces_IE.css') . "\n"; ?>	<![endif]-->

	<link rel="shortcut icon" href="<?php echo URL::base(); ?>public/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo URL::base(); ?>ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo URL::base(); ?>ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="<?php echo URL::base(); ?>ico/apple-touch-icon-57-precomposed.png">
	<script type="text/javascript">
		var  Config = new Object();
		Config.Path = "<?php echo URL::base(); ?>";
		Config.Site = "<?php echo URL::site(); ?>";
	</script>
</head>
<body>
<div id="fb-root"></div>

<!-- Header web site -->
<div id="header">
	<!-- Start Session top bar -->
	<div class="bar_user">
		<div class="eddy-aux">
			<div class="eddy-cols">
				<div class="block_info_usuario eddy-col-0">
					<?php if(Auth::instance()->logged_in()){ ?>
						<span class="pull-right"><a href="#">¿No eres <?php echo Kohana_Auth::instance()->get_user()->name; ?>?</a> </span>
						Bienvenido <strong><?php echo Kohana_Auth::instance()->get_user()->name . ' ' . Kohana_Auth::instance()->get_user()->last_name; ?></strong> Socio Oro
					<?php }elseif(Cookie::get('user_datos')){ ?>
						<!--<strong><?php //echo Cookie::get('user_name') . ' | ' . Cookie::get('user_email'); ?></strong> Socio Oro-->
						<strong><?php echo Cookie::get('user_datos') ?></strong>
					<?php }else{ ?>
						Bienvenido, inicia sesión usando el boton de la derecha
					<?php } ?>
				</div>
				<div class="login_cont eddy-col-0">

					<?php if(Auth::instance()->logged_in()){ ?>
						<a href="http://dev.onevisionlife.es/backoffice/" class="oficinavirtual_link">Oficina Virtual</a>
						<a href="<?php echo URL::site('user/logout'); ?>" class="login_link">Logout</a>
					<?php }else{ ?>
					<a href="http://dev.onevisionlife.es/backoffice/" class="oficinavirtual_link oficinavirtual_link2">Hazte socio</a>
					<!--<a href="<?php echo URL::site('home/login'); ?>" class="login_link">Login</a>-->

					<a id="a_login_frm" href="javascript:void(0);" class="login_link">Login</a>
					<div id="f_login">
						<ul>
							<form action="http://dev.onevisionlife.es/backoffice/cuentas/sesion/login/?redirect_url=http://dev.onevisionlife.es/tiendaovlv2/session/generate/" method="post" name="form1" id="form1">
								<a class="cerrarForm" href="javascript:void(0);">x</a>
								<li style="width:70px !important; text-align:right; line-height: 21px; padding-right: 2px;">Usuario: Contraseña:
								</li>
								<li style="width:205px !important; text-align:left;">
									<input type="text" id="textfield2" name="l_user">
									<input type="password" id="textfield" name="l_pass">
									<!--<input type="checkbox" name="radio" id="radio" value="radio" />-->
									<label for="check_01_02" class="etiqueta"></label>
									<input type="checkbox" class="check_02" value="1" id="check_01_02" name="check_01_02">
									<span class="span_form_check">Mantener tu Sessión activa</span>
								</li>
								<li style="width:67px !important;">
									<input type="submit" value="" id="button_top" name="button">
								</li>
								<li style="width:342px; height:21px !important; margin:2px 0 0 0 !important;">
									<p align="right"><a href="http://backoffice.onevisionlife.com/cuentas/recupera_password" target="_blank">¿Has olvidado tu contraseña?</a></p>
								</li>
							</form>
						</ul>
					</div>

				</div>
				<?php } ?>

				<a href="#">Contactos</a> |	<a href="#">País</a>

				<!--
				<div class="pull-right">
					<a href="http://dev.onevisionlife.es/backoffice/">Oficina Virtual</a>
					<?php if(Auth::instance()->logged_in()){ ?>
						<a href="<?php echo URL::site('user/logout'); ?>">Logout</a>
					<?php }else{ ?>
						<a href="<?php echo URL::site('home/login'); ?>">Login</a>
					<?php } ?>
					<a href="#">Contactos</a> |	<a href="#">País</a>
				</div>
				-->
			</div>
		</div>
	</div>
	<!-- End Session top bar -->
	<?php
	$current_line = Kohana_Request::current()->controller();
	$comercio = Kohana_Request::current()->directory();
	if($current_line == "content") $current_line = '/';
	if($current_line=="static") $current_line="home";
	if($comercio=="comercios") $current_line="comercio";
	?>
	<div class="block_header">
		<div class="eddy-aux">
			<div class="eddy-cols">
				<a href="<?php echo URL::site(''); ?>" class="eddy-col-0 logo logo-<?php echo $current_line; ?>"></a>
				<div class="eddy-col-0 selecciona_pais">
					<h3 class="select-pais-<?php echo $current_line; ?>"><?php if(isset($provincias_selected)) echo $provincias_selected->nombre; ?></h3>
					<select name="select_pais" id="select_pais">
						<?php
						if(isset($provincias_selected))
						{
							foreach($lista_ciudades as $item)
							{
								echo '<option value="'.$item->id.'"><a href="' . URL::site('home/set_ciudad/' . $item->id) . '/'. Kohana_Request::$current->controller().'/">' . $item->nombre . '</a></option>';
							}
						}
						?>
					</select>
				</div>
				<!--<span class="abajo select-pais-<?php echo $current_line; ?>">Barcelona</span>-->
				<div class="navegacion-line">
					<a class="n5 <?php echo $current_line=='village'?'active':''; ?>" href="<?php echo URL::site('village'); ?>"></a>
					<a class="n6 <?php echo $current_line=='home'?'active':''; ?>" href="<?php echo URL::site('home'); ?>"></a>
				</div>
			</div>
			<?php
			if($comercio!="comercios"){ ?>
				<div id="content_carrito">
					<ul>
						<li class="boton01">
							<a id="abrir_carrito" href="javascript:void(0);"></a>
							<div class="content_mini_cesta"></div>
						</li>
						<?php if($current_line<>'home' && $current_line<>'bussiness'){; ?>
							<li class="boton02"><a href="javascript:void(0);"></a></li>
							<li class="boton03"><a href="javascript:void(0);"></a></li>
						<?php }?>
					</ul>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<!-- End web site -->

<div id="content">
	<!-- Start carrito-->

	<!--End carrito-->
	<!-- content -->


	<?php

	if(isset($content)){echo $content;}

	?>

	<!-- end content -->
</div>

<!-- Header web site -->
<div id="footer">
	<div style="height: 40px; background: #f4f4f4; border-top: solid 3px #849410; border-bottom: solid 3px #c0c0c0;">
		<div style="width: 1006px; margin: 4px auto 0;">
			<a href="<?php echo URL::site('village'); ?>"><img src="<?php echo URL::base();?>public/img/village2_btn.jpg" ></a>
			<a href="<?php echo URL::site('home'); ?>"><img src="<?php echo URL::base();?>public/img/home2_btn.jpg" ></a>
		</div>
	</div>
	<div>
		<div>

		</div>
	</div>
	<ul>
		<li>
			<ul>
				<li id="noticias_destacadas">
					<h2>Destacados</h2>

					<div class="cont-noticia" style="width: 280px; float: left; margin-right: 20px;">
						<div class="noticia">
							<img style="float: left; margin-right: 6px;" src="<?php echo URL::base();?>public/img/blank.jpg" >
							<h4 style="padding-top: 34px; color: #575755;">Nuevo curso online de coaching</h4>
							<div style="clear: both;"></div>
						</div>
						<p><br>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
						</p>
						<a href="#"><img style="float: right; margin-right: 6px;" src="<?php echo URL::base();?>public/img/leermas_btn.png" ></a>
					</div>
					<div style="width: 280px; float: left; padding-left: 26px; border-left: solid 1px #c9c7c8;">
						<div class="noticia">
							<img style="float: left; margin-right: 6px;" src="<?php echo URL::base();?>public/img/blank.jpg" >
							<h4 style="padding-top: 34px; color: #575755;">Nuevo curso online de coaching</h4>
							<div style="clear: both;"></div>
						</div>
						<p><br>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
						</p>
						<a href="#"><img style="float: right; margin-right: 6px;" src="<?php echo URL::base();?>public/img/leermas_btn.png" ></a>
					</div>
					<div style="clear: both;"></div>
				</li>

				<li class="ultimo">
					<h2>¿Tienes alguna duda?</h2>
						<span>
							Contacta con nosotros para cualquier consulta que tengas y te responderemos con la mayor brevedad.
						</span>
					<p style="text-align: right; padding: 12px 0;">
						<a href="#"><img src="<?php echo URL::base();?>public/img/contacta_btn.png" ></a><br>
					</p>
					<table>
						<tr>
							<td width="120">
								<h2>Siguenos</h2>
							</td>
							<td>
								<div class="btn_footer_redes">
									<a  href="https://www.facebook.com/pages/One-Vision-Life/162556580460156">
										<img src="<?php echo URL::base();?>public/img/footer_face2.jpg">
									</a>
									<a  href="http://twitter.com/#!/OneVisionLife">
										<img src="<?php echo URL::base();?>public/img/footer_youtube2.jpg">
									</a>
									<a  href="http://www.youtube.com/user/onevisionlife">
										<img src="<?php echo URL::base();?>public/img/footer_twiter2.jpg">
									</a>
									<a  href="http://ovlnews.com/">
										<img src="<?php echo URL::base();?>public/img/footer_rss2.jpg">
									</a>
								</div>
							</td>
						</tr>
					</table>
					<!--
                        <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) {return;}
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/en_US/all.js#appId=160691790683765&xfbml=1";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>

                        <div id="face_ggg" class="fb-like" data-href="https://www.facebook.com/pages/One-Vision-Life/162556580460156" data-send="false" data-layout="button_count" data-width="80" data-show-faces="false">
                        </div>

                    <div id="btn_siguenos_twiter">
                            <a href="https://twitter.com/OneVisionLife" class="twitter-follow-button" data-show-count="false">Follow @OneVisionLife</a>
                    </div>
                    <script src="//platform.twitter.com/widgets.js" type="text/javascript"></script>
                    -->

				</li>
			</ul>
		</li>

		<li class="row02">
			<ul>
				<li>
					<h2>Sobre One Vision Life</h2>
					<a href="<?php echo URL::site('sobre/que-es'); ?>">¿Qué es One Vision Life?</a>
					<a href="<?php echo URL::site('sobre/como-funciona'); ?>">¿Cómo funciona?</a>
					<a href="<?php echo URL::site('sobre/vision-solidaria'); ?>">Visión solidaria</a>
				</li>
				<li>
					<h2>Oportunidad de Negocio</h2>
					<a href="<?php echo URL::site('oportunidad/gana-con-nosotros'); ?>">Gana con nosotros</a>
					<a href="<?php echo URL::site('oportunidad/tipos-socios'); ?>">Tipos de socios</a>
					<a href="<?php echo URL::site('oportunidad/conferencias'); ?>">Conferencias</a>
				</li>
				<li>
					<h2>One Vision Life Business</h2>
					<a href="<?php echo URL::site();?>bussiness/promocion_comercio">Promociona tu comercio</a>
					<a href="<?php echo URL::site();?>bussiness/promocion_comercio">Contacto para comercios</a>
				</li>
				<li>
					<h2>Nuestros Blogs</h2>
					<a href="http://ovlnews.com/" target="_blank">www.ovlnews.com</a>
				</li>
			</ul>
		</li>
		<li class="row03">
			<p>
				<img src="<?php echo URL::base() ?>public/img/creditcards.jpg">
			</p>
			One Vision Life | El Secreto del Fénix S.L | B - 65393837 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <a href="http://backoffice.onevisionlife.com/documentos/condiciones_uso" target="_blank">Condiciones de uso</a> y <a href="http://backoffice.onevisionlife.com/documentos/condiciones_uso" target="_blank">política de privacidad</a>
		</li>
	</ul>

</div>
<!-- End web site -->
<div style="display:none;" id="ticker1"></div>

<script type="text/javascript">

	var _controller = '<?php echo Kohana_Request::$current->controller(); ?>';
	$(document).ready(initMain);
	var cestaIsOpen = false;


	function initMain(){

		$('#select_pais').change(function(){
			var option=($(this).val());
			var categoria_id=$("#categoria option:selected").val();
			var subcategoria_id=$("#subcategoria option:selected").val();
			var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/producto/paises/');?>';
			$.ajax({
				url:url+option,
				type: 'GET',
				data:{categoria_id:categoria_id,subcategoria_id:subcategoria_id},
				dataType: 'json',
				success: function(data) {
					$('#select_provincia').html('');
					var count=0;
					if((data.data.provincias.length)==0)
					{
						$('#list_provincias').html('');
						$('#list_provincias').append('<select style="width: 150px;" name="select_provincia" id="select_provincia"><option disabled selected>No se encontraron resultados</option></select>');

						$('#list_ciudad').html('');
						$('#list_ciudad').append('<select style="width: 150px;" name="select_ciudad" id="select_ciudad"><option value="todos" selected>No se encontraron resultados</option></select>');

					}
					data.data.provincias.forEach(function( i ) {
						if(count==0){
							$('#list_provincias').html('');
							$('#list_provincias').append('<select style="width: 150px;" name="select_provincia" id="select_provincia"><option value="todos" selected>Todos</option></select>');

							$('#list_ciudad').html('');
							$('#list_ciudad').append('<select style="width: 150px;" name="select_ciudad" id="select_ciudad"><option value="todos" selected>Todos</option></select>');

						}
						$('#select_provincia').append('<option value="'+i.id+'">'+ i.nombre+'</option>');
						count=count+1;
					});
					$('#lista_productos_general').html('');

					if((data.data.productos.length)==0)
					{
						$('#lista_productos_general').html('');
						$('#lista_productos_general').append('<div style="margin: 8px;" class="alert alert-info">No se encontraron ofertas para este país.</div>');
					}
					data.data.productos.forEach(function(i){

						$('#lista_productos_general').append('<li><ul><li><span class="dto">'+ i.oferta_descuento+'% Dto.</span><span class="descripcion">'+ i.oferta_titulo+'</span></li><li class="img"><span></span><div><img border="0" alt="" src="'+i.url_imagen+'"></div></li><li class="caracteristicas"><ul><li><span class="ver_plan"><span class="prec">15<span style="font-size:24px;">€</span></span><a href="/tienda/village/producto/140"></a></span><span>Precio:</span><br><span class="precio">22€</span></li><li><p class="titulo_c"></p></li></ul></li></ul></li>');
					});
					selectprovincia();


				},
				error: function(e) {
				}
			});
		});
		selectprovincia();
		function selectprovincia()
		{
			$('#select_provincia').change(function(){
				var option=($(this).val());
				var categoria_id=$("#categoria option:selected").val();
				var subcategoria_id=$("#subcategoria option:selected").val();
				if(option=='todos'){
					$('#list_ciudad').html('');
					$('#list_ciudad').append('<select style="width: 150px;" name="select_ciudad" id="select_ciudad"><option value="todos" selected>Todos</option></select>');
				}
				var pais_id=$("#select_pais option:selected").val();
				var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/producto/provincias/');?>';
				$.ajax({
					url:url+option,
					type: 'POST',
					data:{categoria_id:categoria_id,subcategoria_id:subcategoria_id,pais_id:pais_id},
					dataType: 'json',
					success: function(data) {
						var count=0;
						data.data.provincias.forEach(function( i ) {
							if(count==0){
								$('#list_ciudad').html('');
								$('#list_ciudad').append('<select style="width: 150px;" name="select_ciudad" id="select_ciudad"><option value="todos" selected>Todos</option></select>');
							}
							$('#select_ciudad').append('<option value="'+i.id+'">'+ i.nombre+'</option>');
							count=count+1;
						});
						selectciudad();
						$('#lista_productos_general').html('');
						/*if((data.data.productos.length)==0)
						 {
						 $('#lista_productos_general').html('');
						 $('#lista_productos_general').append('<div style="margin: 8px;" class="alert alert-info">No se encontraron ofertas para esta provincia.</div>');
						 }*/

						if(data.data.mensaje!=null){
							$('#lista_productos_general').html('');
							$('#lista_productos_general').append('<div style="margin: 8px;" class="alert alert-info">'+data.data.mensaje+'</div>');
						}
						data.data.productos.forEach(function(i){
							$('#lista_productos_general').append('<li><ul><li><span class="dto">'+ i.oferta_descuento+'% Dto.</span><span class="descripcion">'+ i.oferta_titulo+'</span></li><li class="img"><span></span><div><img border="0" alt="" src="'+i.url_imagen+'"></div></li><li class="caracteristicas"><ul><li><span class="ver_plan"><span class="prec">15<span style="font-size:24px;">€</span></span><a href="/tienda/village/producto/140"></a></span><span>Precio:</span><br><span class="precio">22€</span></li><li><p class="titulo_c"></p></li></ul></li></ul></li>');
						});
					},
					error: function(e) {
					}
				});
				selectciudad();
			});
		}

		function selectciudad()
		{
			$('#select_ciudad').change(function(){
				var option=($(this).val());
				var pais_id=$("#select_pais option:selected").val();
				var provincia_id=$("#select_provincia option:selected").val();
				var categoria_id=$("#categoria option:selected").val();
				var subcategoria_id=$("#subcategoria option:selected").val();
				var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/producto/ciudades/');?>';
				$.ajax({
					url:url+option,
					type: 'POST',
					data:{categoria_id:categoria_id,subcategoria_id:subcategoria_id,pais_id:pais_id,provincia_id:provincia_id},
					dataType: 'json',
					success: function(data) {
						$('#lista_productos_general').html('');
						if(data.data.mensaje!=null){
							$('#lista_productos_general').html('');
							$('#lista_productos_general').append('<div style="margin: 8px;" class="alert alert-info">'+data.data.mensaje+'</div>');
						}
						data.data.productos.forEach(function(i){
							$('#lista_productos_general').append('<li><ul><li><span class="dto">'+ i.oferta_descuento+'% Dto.</span><span class="descripcion">'+ i.oferta_titulo+'</span></li><li class="img"><span></span><div><img border="0" alt="" src="'+i.url_imagen+'"></div></li><li class="caracteristicas"><ul><li><span class="ver_plan"><span class="prec">15<span style="font-size:24px;">€</span></span><a href="/tienda/village/producto/140"></a></span><span>Precio:</span><br><span class="precio">22€</span></li><li><p class="titulo_c"></p></li></ul></li></ul></li>');
						});
					},
					error: function(e) {
					}
				});
			});
		}

		$('#categoria').change(function(){
			var option=($(this).val());
			var pais_id=$("#select_pais option:selected").val();
			var provincia_id=$("#select_provincia option:selected").val();
			var ciudad_id=$("#select_ciudad option:selected").val();

			var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/producto/categorias/');?>';
			$.ajax({
				url:url+option,
				type: 'POST',
				data:{pais_id:pais_id,provincia_id:provincia_id,ciudad_id:ciudad_id},
				dataType: 'json',
				success: function(data) {
					var count=0;
					data.data.subcategorias.forEach(function( i ) {
						if(count==0){
							$('#subcategoria').html('');
							$('#subcategoria').append('<select style="width: 150px;" name="subcategorias" id="subcategorias"><option value="todos" selected>Todos</option></select>');
						}
						$('#subcategorias').append('<option value="'+i.id+'">'+ i.nombre+'</option>');
						count=count+1;
					});

					/*if((data.data.subcategorias.length)==0) {
					 $('#subcategoria').html('');
					 $('#subcategoria').append('<select style="width: 150px;" name="subcategoria" id="subcategoria"><option value="todos" selected>No se encontraron resultados</option></select>');
					 }*/

					if(data.data.mensaje!=null){
						$('#lista_productos_general').html('');
						$('#lista_productos_general').append('<div style="margin: 8px;" class="alert alert-info">'+data.data.mensaje+'</div>');
					}

					$('#lista_productos_general').html('');
					if((data.data.productos.length)==0)
					{
						$('#lista_productos_general').html('');
						$('#lista_productos_general').append('<div style="margin: 8px;" class="alert alert-info">No se encontraron ofertas para esta categoria.</div>');
					}
					data.data.productos.forEach(function(i){
						$('#lista_productos_general').append('<li><ul><li><span class="dto">'+ i.oferta_descuento+'% Dto.</span><span class="descripcion">'+ i.oferta_titulo+'</span></li><li class="img"><span></span><div><img border="0" alt="" src="'+i.url_imagen+'"></div></li><li class="caracteristicas"><ul><li><span class="ver_plan"><span class="prec">15<span style="font-size:24px;">€</span></span><a href="/tienda/village/producto/140"></a></span><span>Precio:</span><br><span class="precio">22€</span></li><li><p class="titulo_c"></p></li></ul></li></ul></li>');
					});
				},
				error: function(e) {
				}

			});
			subcategorias();
		});

		function subcategorias()
		{
			$('#subcategoria').change(function(){
				var option=$("#subcategoria option:selected").val();
				var pais_id=$("#select_pais option:selected").val();
				var provincia_id=$("#select_provincia option:selected").val();
				var ciudad_id=$("#select_ciudad option:selected").val();
				var categoria_id=$("#categoria option:selected").val();

				var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/producto/subcategorias/');?>';
				$.ajax({
					url:url+option,
					type: 'POST',
					data:{pais_id:pais_id,provincia_id:provincia_id,ciudad_id:ciudad_id,categoria_id:categoria_id},
					dataType: 'json',
					success: function(data) {
						$('#lista_productos_general').html('');
						/* if((data.data.productos.length)==0)
						 {
						 $('#lista_productos_general').html('');
						 $('#lista_productos_general').append('<div style="margin: 8px;" class="alert alert-info">No se encontraron ofertas para esta subcategoria.</div>');
						 }*/
						if(data.data.mensaje!=null){
							$('#lista_productos_general').html('');
							$('#lista_productos_general').append('<h4 style="margin: 8px;">'+data.data.busqueda+'</h4>');
							$('#lista_productos_general').append('<div style="margin: 8px;" class="alert alert-info">'+data.data.mensaje+'</div>');
						}
						data.data.productos.forEach(function(i){
							$('#lista_productos_general').append('<li><ul><li><span class="dto">'+ i.oferta_descuento+'% Dto.</span><span class="descripcion">'+ i.oferta_titulo+'</span></li><li class="img"><span></span><div><img border="0" alt="" src="'+i.url_imagen+'"></div></li><li class="caracteristicas"><ul><li><span class="ver_plan"><span class="prec">15<span style="font-size:24px;">€</span></span><a href="/tienda/village/producto/140"></a></span><span>Precio:</span><br><span class="precio">22€</span></li><li><p class="titulo_c"></p></li></ul></li></ul></li>');
						});
					},
					error: function(e) {
					}
				});
			});
		}

		$('#filtrobusqueda').click(function(){
			var pais_id=$("#select_pais option:selected").val();
			var provincia_id=$("#select_provincia option:selected").val();
			var ciudad_id=$("#select_ciudad option:selected").val();
			var categoria_id=$("#categoria option:selected").val();
			var subcategoria_id=$("#subcategoria option:selected").val();
			var texto=$("#buscadortxt").val();

			var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/producto/filtrobusqueda/');?>';
			$.ajax({
				url:url,
				type: 'POST',
				data:{texto:texto,subcategoria_id:subcategoria_id,pais_id:pais_id,provincia_id:provincia_id,ciudad_id:ciudad_id,categoria_id:categoria_id},
				dataType: 'json',
				success: function(data) {
					$('#lista_productos_general').html('');

					if(data.data.mensaje!=null){
						$('#lista_productos_general').html('');
						$('#lista_productos_general').append('<h4 style="margin: 8px;">'+data.data.busqueda+'</h4>');
						$('#lista_productos_general').append('<div style="margin: 8px;" class="alert alert-info">'+data.data.mensaje+'</div>');
					}
					if(data.data.productos!=null)
					{
						data.data.productos.forEach(function(i){
							$('#lista_productos_general').append('<li><ul><li><span class="dto">'+ i.oferta_descuento+'% Dto.</span><span class="descripcion">'+ i.oferta_titulo+'</span></li><li class="img"><span></span><div><img border="0" alt="" src="'+i.url_imagen+'"></div></li><li class="caracteristicas"><ul><li><span class="ver_plan"><span class="prec">15<span style="font-size:24px;">€</span></span><a href="/tienda/village/producto/140"></a></span><span>Precio:</span><br><span class="precio">22€</span></li><li><p class="titulo_c"></p></li></ul></li></ul></li>');
						});
					}
				},
				error: function(e) {
				}
			});
		});
		selectciudad();
		subcategorias();
		selectciudad();


		$('#content_carrito .boton01 #abrir_carrito').click(qd.app.MiniCesta.toggleCesta);
		$('#content_carrito .boton01 #cierre_carrito').live('click', qd.app.MiniCesta.toggleCesta);
		$('#content_carrito .boton01 #seguir_carrito').live('click', qd.app.MiniCesta.toggleCesta);

		qd.app.MiniCesta.updateCesta(false);
		noticias_destacadas();



		$('#ciudad').change(changeCiudad);
		$('#a_list_paises').click(function(){
			$('#cont_desp_cuid').toggle('fast');
		});

		$('#a_login_frm, .cerrarForm').click(function(){
			$('#f_login').slideToggle('fast');
		});
	}

	function changeCiudad(){

		var _url = Config.Site + "home/set_ciudad/" + $(this).val() + "/" + _controller;
		window.location = _url;

		//alert(Config.Site + _controller + "/index/" + $(this).val());
		//alert(_url);
	}


	function noticias_destacadas() {
		var count_img = 1;
		var count_p = 0;
		var count_txt_rss = "";
		var url_txt_rss;
		$('#ticker1').rssfeed('http://ovlnews.com/?feed=atom', {
			snippet:false,
			limit:3
		}, function (e) {
			$('.rssBody ul li').each(function () {
				if ($(this).find('img').attr('src') == undefined) {
					$('.img_rss' + count_img).attr('src', 'http://img.onevisionlife.com/ovl_new/default_rss.jpg');
					count_img++;
				}
				else {
					$('.img_rss' + count_img).attr('src', $(this).find('img').attr('src'));
					count_img++;
				}
				$(this).find('p').each(function () {
					count_p++;
					if (count_p < 4) {
						count_txt_rss += $(this).text();
					}
				});
				url_txt_rss = $(this).find('h4').find('a').attr('href');
				$('.txt_rss' + (count_img - 1)).html('<a target="_blank" href="' + url_txt_rss + '">' + count_txt_rss.substr(0, 250) + '</a>');
				count_p = 0;
				count_txt_rss = "";
				url_txt_rss = 0;
			});
		});
	}

</script>
<?php //echo Kohana_Request::$current->controller(); ?>
</body>
</html>
