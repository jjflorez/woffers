<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset=utf-8 />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>
        <?php
        if(isset($title))
            echo $title . ' :: ' . 'OVL';
        else
            echo 'OVL :. Las mejores ofertas en tu ciudad';
        ?>
    </title>
    <?php
    echo "\t" . Html::style('public/css/bootstrap.css') . "\n";
    //echo "\t" . Html::style('public/css/bootstrap-responsive.css') . "\n";
    echo "\t" . Html::style('public/css/admin.css') . "\n";
    echo "\t" . Html::style('public/css/style.css') . "\n";
    echo "\t" . Html::style('public/css/eddy.css') . "\n";
    echo "\t" . Html::style('public/css/dp/datepicker.css') . "\n";
    echo "\t" . Html::style('public/css/humanity/jquery-ui-1.8.23.custom.css') . "\n";

    echo "\t" . Html::script('public/js/jquery-1.8.0.min.js') . "\n";
    echo "\t" . Html::script('public/js/jquery-ui-1.8.23.custom.min.js') . "\n";
    echo "\t" . Html::script('public/plugins/qd.core.js') . "\n";
    echo "\t" . Html::script('public/js/bootstrap.js') . "\n";


    echo "\t" . Html::script('public/js/list_appearance.js') . "\n";

    echo "\t" . Html::script('public/js/bootstrap.js') . "\n";
    echo "\t" . Html::script('public/js/swfobject.js') . "\n";
    echo "\t" . Html::script('public/plugins/jquery.validate.js') . "\n";
    echo "\t" . Html::script('public/plugins/jquery.zrssfeed.js') . "\n";

    //echo "\t" . Html::script('public/plugins/fb_lf.js') . "\n";
    //echo "\t" . Html::script('public/js/dp/datepicker.js') . "\n";
    echo "\t" . Html::script('public/plugins/uploader/fileuploader.js') . "\n";
    echo "\t" . Html::script('public/plugins/ckeditor/ckeditor.js') . "\n";

    echo "\t" . Html::script('public/plugins/qd.core.js') . "\n";
    echo "\t" . Html::script('public/plugins/qd.app.js') . "\n";

    echo "\t" . Html::script('public/plugins/colorbox/jquery.colorbox-min.js') . "\n";
    echo "\t" . Html::style('public/plugins/colorbox/colorbox.css') . "\n";

    echo "\t" . Html::script('public/plugins/chosen/chosen.jquery.js') . "\n";
    echo "\t" . Html::script('public/plugins/chosen/docsupport/prism.js') . "\n";
    echo "\t" . Html::style('public/plugins/chosen/chosen.css') . "\n";
    echo "\t" . Html::style('public/plugins/chosen/docsupport/prism.css') . "\n";

    if(isset($header))	echo $header;

    //<script src="//connect.facebook.net/en_US/all.js"></script>

    ?>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>	<![endif]-->
    <!--[if lt IE 9]>		<?php echo "\t" . Html::style('public/css/appFaces_IE.css') . "\n"; ?>	<![endif]-->

    <link rel="shortcut icon" href="<?php echo URL::base(); ?>public/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo URL::base(); ?>ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo URL::base(); ?>ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo URL::base(); ?>ico/apple-touch-icon-57-precomposed.png">
    <script type="text/javascript">
        var  Config = new Object();
        Config.Path = "<?php echo URL::base(); ?>";
        Config.Site = "<?php echo URL::site(); ?>";
    </script>
</head>
<body>
<div id="fb-root"></div>

<!-- Header web site -->
<div id="header">
    <!-- Start Session top bar -->
    <div class="bar_user">
        <div class="eddy-aux">
            <div class="eddy-cols">
                <div class="block_info_usuario eddy-col-0">
                    <?php if(Auth::instance()->logged_in()){ ?>
                        <span class="pull-right"><a href="#">¿No eres <?php echo Kohana_Auth::instance()->get_user()->name; ?>?</a> </span>
                        Bienvenido <strong><?php echo Kohana_Auth::instance()->get_user()->name . ' ' . Kohana_Auth::instance()->get_user()->last_name; ?></strong> Socio Oro
                    <?php }elseif(Cookie::get('user_datos')){ ?>
                        <!--<strong><?php //echo Cookie::get('user_name') . ' | ' . Cookie::get('user_email'); ?></strong> Socio Oro-->
                        <strong><?php echo Cookie::get('user_datos') ?></strong>
                    <?php }else{ ?>
                        Bienvenido, inicia sesión usando el boton de la derecha
                    <?php } ?>
                </div>
                <div class="login_cont eddy-col-0">

                    <?php if(Auth::instance()->logged_in()){ ?>
                        <a href="http://dev.onevisionlife.es/backoffice/" class="oficinavirtual_link">Oficina Virtual</a>
                        <a href="<?php echo URL::site('user/logout'); ?>" class="login_link">Logout</a>
                    <?php }else{ ?>
                    <a href="http://dev.onevisionlife.es/backoffice/" class="oficinavirtual_link oficinavirtual_link2">Hazte socio</a>
                    <!--<a href="<?php echo URL::site('home/login'); ?>" class="login_link">Login</a>-->

                    <a id="a_login_frm" href="javascript:void(0);" class="login_link">Login</a>
                    <div id="f_login">
                        <ul>
                            <form action="http://dev.onevisionlife.es/backoffice/cuentas/sesion/login/?redirect_url=http://dev.onevisionlife.es/tiendaovlv2/session/generate/" method="post" name="form1" id="form1">
                                <a class="cerrarForm" href="javascript:void(0);">x</a>
                                <li style="width:70px !important; text-align:right; line-height: 21px; padding-right: 2px;">Usuario: Contraseña:
                                </li>
                                <li style="width:205px !important; text-align:left;">
                                    <input type="text" id="textfield2" name="l_user">
                                    <input type="password" id="textfield" name="l_pass">
                                    <!--<input type="checkbox" name="radio" id="radio" value="radio" />-->
                                    <label for="check_01_02" class="etiqueta"></label>
                                    <input type="checkbox" class="check_02" value="1" id="check_01_02" name="check_01_02">
                                    <span class="span_form_check">Mantener tu Sessión activa</span>
                                </li>
                                <li style="width:67px !important;">
                                    <input type="submit" value="" id="button_top" name="button">
                                </li>
                                <li style="width:342px; height:21px !important; margin:2px 0 0 0 !important;">
                                    <p align="right"><a href="http://backoffice.onevisionlife.com/cuentas/recupera_password" target="_blank">¿Has olvidado tu contraseña?</a></p>
                                </li>
                            </form>
                        </ul>
                    </div>

                </div>
                <?php } ?>

                <a href="#">Contactos</a> |	<a href="#">País</a>

                <!--
				<div class="pull-right">
					<a href="http://dev.onevisionlife.es/backoffice/">Oficina Virtual</a>
					<?php if(Auth::instance()->logged_in()){ ?>
						<a href="<?php echo URL::site('user/logout'); ?>">Logout</a>
					<?php }else{ ?>
						<a href="<?php echo URL::site('home/login'); ?>">Login</a>
					<?php } ?>
					<a href="#">Contactos</a> |	<a href="#">País</a>
				</div>
				-->
            </div>
        </div>
    </div>
    <!-- End Session top bar -->
    <?php
    $current_line = Kohana_Request::current()->controller();
    $comercio = Kohana_Request::current()->directory();
    if($current_line == "content") $current_line = '/';
    if($current_line=="static") $current_line="home";
    if($comercio=="comercios") $current_line="comercio";
    ?>
    <div class="block_header">
        <div class="eddy-aux">
            <div class="eddy-cols">
                <a href="<?php echo URL::site(''); ?>" class="eddy-col-0 logo logo-<?php echo $current_line; ?>"></a>
                <div class="eddy-col-0 selecciona_pais">
                    <h3 class="select-pais-<?php echo $current_line; ?>"><?php if(isset($provincias_selected)) echo $provincias_selected->nombre; ?></h3>
                    <div id="list_paises">
                        <a id="a_list_paises" href="javascript:void(0);"><img src="<?php echo URL::base();?>public/img/flecha_ciudades.jpg ">Selecciona tu ciudad</a>
                        <div id="cont_desp_cuid">
                            <div id="desp_ciud_sup">
                                <ul>
                                    <?php
                                    if(isset($provincias_selected))
                                    {
                                        foreach($lista_ciudades as $item)
                                        {
                                            echo '<li><a href="' . URL::site('home/set_ciudad/' . $item->id) . '/'. Kohana_Request::$current->controller().'/">' . $item->nombre . '</a></li>';
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<span class="abajo select-pais-<?php echo $current_line; ?>">Barcelona</span>-->
                <div class="navegacion-line">
                    <a class="n5 <?php echo $current_line=='village'?'active':''; ?>" href="<?php echo URL::site('village'); ?>"></a>
                    <a class="n6 <?php echo $current_line=='home'?'active':''; ?>" href="<?php echo URL::site('home'); ?>"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End web site -->
<div id="content">
    <!-- Start carrito-->
    <div class="container" style="    width: 100%;margin: auto"></div>
    <!--End carrito-->
    <!-- content -->

    <?php
    /*echo '<div id="c_'.$current_line.'">'.$menu.'<div class="eddy-aux">';*/
    if(isset($content)){echo '<div class="container" style="margin: auto">'. View::factory('useradmin/messages') . '</div><div id="c_'.$current_line.'"><div class="eddy-aux" style="width: inherit;margin-left: 50px; margin-right: 50px; margin-bottom: 100px;">'.View::factory('ci/view_menu') . $content;}
    echo '</div></div>';
    ?>
    <!-- end content -->
</div>

<!-- Header web site -->
<div id="footer">
    <div style="height: 40px; background: #f4f4f4; border-top: solid 3px #849410; border-bottom: solid 3px #c0c0c0;">
        <div style="width: 1006px; margin: 4px auto 0;">
            <a href="<?php echo URL::site('village'); ?>"><img src="<?php echo URL::base();?>public/img/village2_btn.jpg" ></a>
            <a href="<?php echo URL::site('home'); ?>"><img src="<?php echo URL::base();?>public/img/home2_btn.jpg" ></a>
        </div>
    </div>
    <div>
        <div>

        </div>
    </div>
    <ul>
        <li>
            <ul>
                <li id="noticias_destacadas">
                    <h2>Destacados</h2>

                    <div class="cont-noticia" style="width: 280px; float: left; margin-right: 20px;">
                        <div class="noticia">
                            <img style="float: left; margin-right: 6px;" src="<?php echo URL::base();?>public/img/blank.jpg" >
                            <h4 style="padding-top: 34px; color: #575755;">Nuevo curso online de coaching</h4>
                            <div style="clear: both;"></div>
                        </div>
                        <p><br>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
                        </p>
                        <a href="#"><img style="float: right; margin-right: 6px;" src="<?php echo URL::base();?>public/img/leermas_btn.png" ></a>
                    </div>
                    <div style="width: 280px; float: left; padding-left: 26px; border-left: solid 1px #c9c7c8;">
                        <div class="noticia">
                            <img style="float: left; margin-right: 6px;" src="<?php echo URL::base();?>public/img/blank.jpg" >
                            <h4 style="padding-top: 34px; color: #575755;">Nuevo curso online de coaching</h4>
                            <div style="clear: both;"></div>
                        </div>
                        <p><br>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
                        </p>
                        <a href="#"><img style="float: right; margin-right: 6px;" src="<?php echo URL::base();?>public/img/leermas_btn.png" ></a>
                    </div>
                    <div style="clear: both;"></div>
                </li>

                <li class="ultimo">
                    <h2>¿Tienes alguna duda?</h2>
						<span>
							Contacta con nosotros para cualquier consulta que tengas y te responderemos con la mayor brevedad.
						</span>
                    <p style="text-align: right; padding: 12px 0;">
                        <a href="#"><img src="<?php echo URL::base();?>public/img/contacta_btn.png" ></a><br>
                    </p>
                    <table>
                        <tr>
                            <td width="120">
                                <h2>Siguenos</h2>
                            </td>
                            <td>
                                <div class="btn_footer_redes">
                                    <a  href="https://www.facebook.com/pages/One-Vision-Life/162556580460156">
                                        <img src="<?php echo URL::base();?>public/img/footer_face2.jpg">
                                    </a>
                                    <a  href="http://twitter.com/#!/OneVisionLife">
                                        <img src="<?php echo URL::base();?>public/img/footer_youtube2.jpg">
                                    </a>
                                    <a  href="http://www.youtube.com/user/onevisionlife">
                                        <img src="<?php echo URL::base();?>public/img/footer_twiter2.jpg">
                                    </a>
                                    <a  href="http://ovlnews.com/">
                                        <img src="<?php echo URL::base();?>public/img/footer_rss2.jpg">
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!--
                        <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) {return;}
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/en_US/all.js#appId=160691790683765&xfbml=1";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>

                        <div id="face_ggg" class="fb-like" data-href="https://www.facebook.com/pages/One-Vision-Life/162556580460156" data-send="false" data-layout="button_count" data-width="80" data-show-faces="false">
                        </div>

                    <div id="btn_siguenos_twiter">
                            <a href="https://twitter.com/OneVisionLife" class="twitter-follow-button" data-show-count="false">Follow @OneVisionLife</a>
                    </div>
                    <script src="//platform.twitter.com/widgets.js" type="text/javascript"></script>
                    -->

                </li>
            </ul>
        </li>

        <li class="row02">
            <ul>
                <li>
                    <h2>Sobre One Vision Life</h2>
                    <a href="<?php echo URL::site('sobre/que-es'); ?>">¿Qué es One Vision Life?</a>
                    <a href="<?php echo URL::site('sobre/como-funciona'); ?>">¿Cómo funciona?</a>
                    <a href="<?php echo URL::site('sobre/vision-solidaria'); ?>">Visión solidaria</a>
                </li>
                <li>
                    <h2>Oportunidad de Negocio</h2>
                    <a href="<?php echo URL::site('oportunidad/gana-con-nosotros'); ?>">Gana con nosotros</a>
                    <a href="<?php echo URL::site('oportunidad/tipos-socios'); ?>">Tipos de socios</a>
                    <a href="<?php echo URL::site('oportunidad/conferencias'); ?>">Conferencias</a>
                </li>
                <li>
                    <h2>One Vision Life Business</h2>
                    <a href="<?php echo URL::site();?>bussiness/promocion_comercio">Promociona tu comercio</a>
                    <a href="<?php echo URL::site();?>bussiness/promocion_comercio">Contacto para comercios</a>
                </li>
                <li>
                    <h2>Nuestros Blogs</h2>
                    <a href="http://ovlnews.com/" target="_blank">www.ovlnews.com</a>
                </li>
            </ul>
        </li>
        <li class="row03">
            <p>
                <img src="<?php echo URL::base() ?>public/img/creditcards.jpg">
            </p>
            One Vision Life | El Secreto del Fénix S.L | B - 65393837 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <a href="http://backoffice.onevisionlife.com/documentos/condiciones_uso" target="_blank">Condiciones de uso</a> y <a href="http://backoffice.onevisionlife.com/documentos/condiciones_uso" target="_blank">política de privacidad</a>
        </li>
    </ul>

</div>
<!-- End web site -->
<div style="display:none;" id="ticker1"></div>

<script type="text/javascript">

    var _controller = '<?php echo Kohana_Request::$current->controller(); ?>';
    $(document).ready(initMain);
    var cestaIsOpen = false;

    function initMain(){
        $('#content_carrito .boton01 #abrir_carrito').click(qd.app.MiniCesta.toggleCesta);
        $('#content_carrito .boton01 #cierre_carrito').live('click', qd.app.MiniCesta.toggleCesta);
        $('#content_carrito .boton01 #seguir_carrito').live('click', qd.app.MiniCesta.toggleCesta);

        qd.app.MiniCesta.updateCesta(false);
        noticias_destacadas();

        $('#ciudad').change(changeCiudad);
        $('#a_list_paises').click(function(){
            $('#cont_desp_cuid').toggle('fast');
        });

        $('#a_login_frm, .cerrarForm').click(function(){
            $('#f_login').slideToggle('fast');
        });
    }

    function changeCiudad(){
        var _url = Config.Site + "home/set_ciudad/" + $(this).val() + "/" + _controller;
        window.location = _url;
        //alert(Config.Site + _controller + "/index/" + $(this).val());
        //alert(_url);
    }

    function noticias_destacadas() {
        var count_img = 1;
        var count_p = 0;
        var count_txt_rss = "";
        var url_txt_rss;
        $('#ticker1').rssfeed('http://ovlnews.com/?feed=atom', {
            snippet:false,
            limit:3
        }, function (e) {
            $('.rssBody ul li').each(function () {

                if ($(this).find('img').attr('src') == undefined) {
                    $('.img_rss' + count_img).attr('src', 'http://img.onevisionlife.com/ovl_new/default_rss.jpg');
                    count_img++;
                }
                else {
                    $('.img_rss' + count_img).attr('src', $(this).find('img').attr('src'));
                    count_img++;
                }
                $(this).find('p').each(function () {
                    count_p++;
                    if (count_p < 4) {
                        count_txt_rss += $(this).text();
                    }
                });
                url_txt_rss = $(this).find('h4').find('a').attr('href');
                $('.txt_rss' + (count_img - 1)).html('<a target="_blank" href="' + url_txt_rss + '">' + count_txt_rss.substr(0, 250) + '</a>');
                count_p = 0;
                count_txt_rss = "";
                url_txt_rss = 0;
            });
        });
    }

</script>
<?php //echo Kohana_Request::$current->controller(); ?>
</body>
</html>