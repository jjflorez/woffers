<div class="container">
	<h2><?php echo __('search.results'); ?></h2>
	<hr/>
	<div class="row-fluid home-lista-videos">
		<?php
			$count = 1;
			if(count($videos) == 0)
			{
				echo '<div class="alert alert-info">' . __('no.results') . '</div>';
			}
			foreach($videos as $video)
			{
				echo '<div class="span4">
						<a href="' . URL::site('make?video=' . $video->code) . '">
						<img src="' . URL::base() . $video->url_path . '/preview.jpg" alt="" /></a><br/>
						<a href="' . URL::site('make?video=' . $video->code) . '" class="pull-right btn btn-danger" style="margin:5px 7px 0 0	;">' . __('view.now') . '</a>
						<a href="' . URL::site('make?video=' . $video->code) . '" class="video-title">' . $video->name . '</a>
						<p class="video-description">' . $video->description . '</p>
					</div>';
				if(($count%3) == 0)
				{
					echo '</div><div class="row-fluid home-lista-videos">';
				}
				$count++;
			}
		?>
	</div>
</div> <!-- /container -->