<?php
$controlador = Kohana_Request::$current->controller();
?>
<style>
    .lista_productos_general.permanentes_general > li{
        height: 285px !important;
    }
    .titulo_c{
        font-family: Arial;
        font-weight: bold;
        color: #757373;
        font-size: 12px;
        margin: 8px;
    }
    #filtro{}
    #filtro td{
        padding: 6px 0;
    }
    #filtro .t_border{
        border-left: solid 1px #aeaeae;
        padding-left: 14px;
    }

</style>
<!--<div id="c_<?php echo $controlador; ?>">-->
<!--
    <div id="container_lineas">
        <ul id="pestanas_lineas">
            <?php
foreach($tiendas as $key => $t){
    if($t->code == Kohana_Request::$current->param('param1')){
        echo '<li class="selected"><a href="'. URL::site($controlador.'/tienda/' . $t->code) .'">' . $t->name . '</a></li>';
    }else{
        if(Kohana_Request::$current->param('param1') == "" && $key == 0)
            echo '<li class="selected"><a href="'. URL::site($controlador.'/tienda/' . $t->code) .'">' . $t->name . '</a></li>';
        else
            echo '<li><a href="'. URL::site($controlador.'/tienda/' . $t->code) .'">' . $t->name . '</a></li>';
    }
}
?>
        </ul>
    </div>
-->
<!-- Inicio contenedor -->
<div class="container" style="padding-bottom: 146px;">
    <div id="colum_produc_a">
        <!-- Inicio filtros -->

        <?php if($controlador !="outlet"){ ?>

            <div class="filtros_ofertas">

                    <table id="filtro" width="100%" border="0">
                        <tr>
                            <td colspan="4" class="titulo">Filtrar ofertas &nbsp;&nbsp;&nbsp;&nbsp;
                                <img src="<?php echo URL::base() ?>public/img/icons_village.jpg" alt="">
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <!--
                                <input class="buscador_ofertas" name="texto_buscar" type="text">
                                -->
                            </td>
                        </tr>
                        <tr>
                            <td>Provincia</td>

                            <td>
                                <div id="list_provincias">
                                    <select style="width: 150px;" name="select_provincia" id="select_provincia">
                                        <option value="todos" selected>Todos</option>
                                        <?php
                                        foreach($provincias as $item):
                                            $selec0 = '';
                                            if($producto->oferta_categoria_id == $item->id)
                                                $selec0 = 'selected="selected"';
                                            ?>
                                            <option <?php echo $selec0 ?> value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </td>
                            <td>Categorias</td>
                            <td>
                                <select style="width: 150px;" id="categoria" >
                                    <option value="todos" selected>Todos</option>
                                    <?php
                                    foreach($categorias as $item):
                                        $selec0 = '';
                                        if($producto->oferta_categoria_id == $item->id)
                                            $selec0 = 'selected="selected"';
                                        ?>
                                        <option <?php echo $selec0 ?> value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>

                            <td>Buscar</td>
                            <td>
                                <div id="buscador">
                                    <input id="buscadortxt" style="width: 150px;" placeholder="c.postal o Distrito">
                                    <a class="btn" id="filtrobusqueda" style="margin-bottom: 9px; margin-left: -4px;">Buscar</a>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>Ciudad</td>
                            <td>
                                <div id="list_ciudad">
                                    <select style="width: 150px;" name="select_ciudad" id="select_ciudad">
                                        <option value="todos" selected>Todos</option>
                                    </select>
                                </div>
                            </td>

                            <td>Subcategoría</td>
                            <td>
                                <div id="subcategoria">
                                    <select style="width: 150px;" id="subcategorias">
                                        <option value="todos" selected>Todos</option>
                                        <?php
                                        foreach($subcategorias as $item):
                                            $selec0 = '';
                                            if($producto->oferta_categoria_id == $item->id)
                                                $selec0 = 'selected="selected"';
                                            ?>
                                            <option <?php echo $selec0 ?> value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </td>


                        </tr>
                        <tr>

                        </tr>
                    </table>

            </div>

            <!-- Fin filtros -->

        <?php }else{ ?>
            <li>
                <h2>Filtra las ofertas:</h2>
                <a href="javascript:void(0);" class="list_op_busqueda">
                    <input id="categoria" type="hidden" >
                </a>
                Categoria

            </li>
            <li class="second">
                <a href="javascript:void(0);" class="list_op_busqueda">
                    <input id="precio" type="hidden" >
                </a>
                Precio

            </li>
            <li>
                <div class="buscador_op">
                    <input id="txt_busc" type="text">
                    <a href="javascript:void(0);"></a>
                </div>
                <a href="javascript:void(0);" class="list_op_busqueda">
                    <input id="ordenar" type="hidden" >
                </a>
                Ordenar

            </li>
        <?php }?>
        <!-- Listado de productos -->
        <?php
        if(count($productos) == 0)

        ?>
        <ul id="lista_productos_general" class="lista_productos_general cont_<?php echo $controlador; ?> permanentes_general">
            <?php
            foreach($productos as $p)
            {
                ?>
                <li>
                    <ul>
                        <li>
                            <span class="dto"><?php echo $p->oferta_descuento; ?>% Dto.</span>
                                <span class="descripcion"><?php
                                    $text_cortado=90;
                                    echo $p->get_txtmuestra($p->oferta_titulo,$text_cortado);?></span>
                        </li>
                        <li class="img">
                            <span></span>
                            <div>
                                <img border="0" alt="" src="<?php echo URL::base() . $p->oferta_imagen->url_path; ?>">
                            </div>
                        </li>
                        <li class="caracteristicas">
                            <ul>
                                <li>
										<span class="ver_plan">
											<span class="prec">
                                                <?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
											</span>
											<a href="<?php echo URL::site($controlador.'/producto/' . $p->id ); ?>"></a>
										</span>
                                    <span>Precio:</span>
                                    <br/>
                                    <span class="precio"><?php echo $p->oferta_precio_venta; ?>€</span>
                                </li>

                                <li>
                                    <p class="titulo_c"><?php echo $p->comercio_nombre ?></p>
                                    <!--
										<span class="redes">
                                                <?php if($controlador!="outlet"){?>
                                                <?php }?>
                                            <a class="twiter"></a>
												<a class="mail"></a>
												<a class="megusta"></a>
                                            <span id="alerta_producto_op">
                                            </span>
										</span>
										-->
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
    <div id="colum_produc_b">
        <?php echo View::factory('ci/view_sidebar')->set('var_sidebar','')?>
    </div>
</div>


<script>

</script>



