<div class="content">
	<div class="well well-small">
		<h4>Agregar imagen</h4>
		<div class="row-fluid">
			<div class="span6">
				Nombre de la imagen:<br/>
				<input type="text" id="file_name" name="name" class="input-block-level" value="">
			</div>

			<div class="span6">
				Elige imagen<br/>
				<button id="file-uploader" class="btn">Seleccionar una imagen</button>
				<button class="btn btn-primary" id="submit_upload_file">Cargar</button>
			</div>
		</div>
	</div>

	<div id="qdmedia_lista_imagenes">
		<div class="btn-group pull-right">
			<a class="previous btn btn-mini" href="javascript:void(0);"><span class="icon-chevron-left"></span></a>
			<a class="next btn btn-mini" href="javascript:void(0);"><span class="icon-chevron-right"></span></a>
		</div>
		<h4>Galería de imagenes</h4>
		<div class="thumbnails">

		<div id="lista_qdmedia">
		</div>
		</div>

	</div>
</div>

	<script type="text/javascript">

		$(document).ready(initSnippet);

		var qdmedia_page = 1;
		var qdmedia_onSelect = null;
		function initSnippet()
		{
			$('#qdmedia_lista_imagenes .btn-group .previous').click(function(){qdmedia_page--; load_qdmedia(); return false;});
			$('#qdmedia_lista_imagenes .btn-group .next').click(function(){qdmedia_page++; load_qdmedia(); return false;});

			init_uploader();
			load_qdmedia();
		}
		var uploader;

		function init_uploader()
		{
			uploader = new AjaxUpload($('#file-uploader'), {
				name:'file_upload',
				onSubmit:submit_uploader,
				onComplete:complete_uploader,
				action: '<?php echo URL::site('api/files/save'); ?>',
				autoSubmit:false
			});

			//uploader.se

			$('#submit_upload_file').click(function(){
				uploader.submit();
			});
		}

		function submit_uploader(_name, _extention)
		{
			if($('#file_name').val() == ''){
				$('#file_name').val(_name);
			}
			uploader.setData({name:$('#file_name').val(), filetype_id:1});
			$('#submit_upload_file').attr('disabled', 'disabled');
			$('#submit_upload_file').text('Cargando...');
		}

		function complete_uploader()
		{
			$('#file_name').val('');
			$('#submit_upload_file').removeAttr('disabled');
			$('#submit_upload_file').text('Cargar imagen');
			load_qdmedia();
		}

		function load_qdmedia()
		{
			if(qdmedia_page < 1 ){
				qdmedia_page = 1;
				return;
			}
			qd.core.LoadData.load('qdmedia/home/galeria?page=' + qdmedia_page, {page:qdmedia_page}, function(_data){
				$('#lista_qdmedia').html('');
				for(i in _data.data)
				{
					$('#lista_qdmedia').append('<div class="span3"><a href="javascript:void(0);" class="thumbnail" style="margin-bottom: 10px; height: 170px;"><span class="label label-info" style="position:absolute; margin: 0 0 -15px 0;">' + _data.data[i].name + '</span><img src="' + Config.Path + _data.data[i].url_path + '" alt="Imagen no disponible" id="' + _data.data[i].id + '"></a></div>');
				}
				$('#lista_qdmedia a.thumbnail').click(selectImagen);
			});
		}

		function selectImagen()
		{
			var target = $(this).find('img');
			if(qdmedia_onSelect)
			{
				qdmedia_onSelect(target.attr('id'), target);
                document.form_registro_producto.hdn_id_logo_comercio.value=target.attr('id');
			}
			return false;
		}

	</script>