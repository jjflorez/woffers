<div id="c_offers">
	<div id="content_cesta">
		<div class="titulo">Gracias</div>
		<div class="cesta">
            <div id="cesta_frm">
                <br>
                <h3>Gracias por comprar en One Vision Life, tu compra se ha realizado con éxito.</h3>
                <p>
                    <a href="<?php echo URL::site('/'); ?>">Seguir comprando</a><br>
                    <a href="http://dev.onevisionlife.es/backoffice_user/">Ver tus compras</a>
                </p>

                <br><br><br>
            </div>
		</div>
    </div>
</div>