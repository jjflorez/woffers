<?php
$form = new Appform();
if (isset($errors)) {
    $form->errors = $errors;
}

// set custom classes to get labels moved to bottom:
$form->error_class = 'error block';
$form->info_class = 'info block';
?>

<div id="c_offers">
    <div id="content_cesta">
        <div class="titulo">
            ¿Ya estás registrado en One Vision Life?
        </div>

        <div class="login_cesta">

            <div class="estas_registrado_ovl style_content_cesta">

                <table width="100%"style="padding-top: 28px;padding-bottom: 35px;">
                    <tr>
                        <td style="vertical-align:top; padding-left:101px; padding-right:102px;">

                            <?php
                            $tienda='/app_server/';
                            //$tienda='/tiendaovlv2/';
                            echo $form->open('http://'.$_SERVER['SERVER_NAME'].
                            			'/backoffice/logon/access/?url=http://'.
                            			$_SERVER['SERVER_NAME'].
                            			$tienda.
                            			'&redirect_url=http://'.
                            			$_SERVER['SERVER_NAME'].
										$tienda);
                            echo '<p>' . $form->label('username', __('email.or.username')) . '</p>';
                            echo $form->input('usuario', null);
                            echo '<p>' . $form->label('password', __('password'), array('style' => 'display: inline; margin-right:10px;')) .
                                '<small> ' . Html::anchor('user/forgot', __('?forgot.password')) . '<br></small>' .
                                '</p>';
                            echo $form->password('clave', null);

                            $authClass = new ReflectionClass(get_class(Auth::instance()));

                            //set a valid salt in useradmin config or your bootstrap.php
                            if ($authClass->hasMethod('auto_login') AND Cookie::$salt) {
                                echo '<p>' . Kohana_Form::checkbox('remember', 'remember', false, array('style' => 'margin-right: 10px', 'id' => 'remember')) .
                                    $form->label('remember', __('remember.me'), array('style' => 'display: inline')) .
                                    $form->submit(NULL, __('login'), array('style' => 'float: right;')) .
                                    '</p>';
                            }
                            else
                            {
                                echo '<p><button type="submit" class="btn btn-primary">' . __('login') . '</button></p>';
                            }

                            echo $form->close();

                            echo '</td><td width="5" style="border-right:1px solid #DDD;">&nbsp;</td>
                            <td><td width="426" style="padding-left: 2px; vertical-align: top;padding-left: 94px;">';

                            echo '<h4>' . __('?dont.have.account') . '</h4><p>' .
                                Html::anchor('user/register', '<span class="icon-check"></span> ' . __('register.new.account'), array('class' => 'btn')) . '</p>';


                            echo '<br/><p></p>';
                            echo '<p>';
                            echo '';
                            echo '<br style="clear: both;">
					        </p>';
                            echo '</td></tr></table>';
                            ?>
            </div>
        </div>
    </div>
</div>