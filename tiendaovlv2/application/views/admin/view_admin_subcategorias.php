<div id="c_admin">
    <div class="container" style="width: 100%">
        <div class="row-fluid">
            <div class="span12">
                <div class="content" >
                    <div class="breadcrumb">
                        <a href="<?php echo URL::site('admin/categorias/'); ?>">
                            Categorias
                        </a>
                        /
                        <strong>
                            Subcategorias
                        </strong>
                    </div>
                    <div class="pull-right">

                        <a href="<?php echo URL::site('admin/categorias/agregarsubcategoria/'.$id_categoria)?>" class="btn btn-mini">
							<span class="icon-plus-sign">
							</span>
                            Agregar Subcategoria
                        </a>
                    </div>
                    <h4 style="height: ">
                        Subcategorias
                    </h4>
                        <?php $temp_categoria=$categorias->as_array();
                            if(count($temp_categoria)==0)
                                echo '<div class="alert alert-info">No se encontraron subcategorias.</div>';
                        ?>

                        <?php if(count($temp_categoria)!=0) {?>
                        <?php foreach ($categorias as $key=>$item) {
                            $row = $item->as_array();
                            $row['actions'] = Html::anchor('admin/subcategorias/editar/' . $row['id'], __('edit')) . ' | ' .
                                Html::anchor('admin/categorias/eliminar/' . $row['id'], 'Eliminar', array('class' => 'delete_item'));
                            $row['nombre'] = $item->nombre;
                            $row['descripcion'] = $item->descripcion;
                            $row['key']=$key+1;
                            $data[] = $row;
                        }
                        $column_list = array(
                            'key' => array('label' => __('id')),
                            'nombre' => array('label' => __('Nombre')),
                            'descripcion' => array('label' => __('Description')),
                            //'logo' => array('label' => 'Logo', 'sortable' => false),
                            //'status' => array('label' => 'Estado', 'sortable' => true),
                            'actions' => array('label' => __('actions'), 'sortable' => false)
                        );

                        $datatable = new Helper_Datatable(
                            $column_list,
                            array('paginator' => true, 'class' => 'table table-bordered table-striped', 'sortable' => 'true', 'default_sort' => 'username')
                        );
                        $datatable->values($data);
                        try {
                            if ($paging != null) {
                                echo $paging->render();
                            }
                        } catch (Exception $ex) {
                        }

                        echo $datatable->render();

                        try {
                            if ($paging != null) {
                                echo $paging->render();
                            }
                        } catch (Exception $ex) {
                        }
                    ?>
                    <?php }?>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(init);

    function init(){
        $('.delete_item').click(function(){
            var r = confirm('¿Esta seguro que desea eliminar esta subcategoria?');
            if(!r){
                return false;
            }
        });
    }
</script>