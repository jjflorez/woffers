
<div id="c_admin">
	<div class="container" style="width: 100%">
		<div class="row-fluid">
			<div class="span12">
				<div class="content" style="padding-left: 0;margin-left: 30px; margin-right: 30px;" >

				<!-- Start Content -->
					<div class="breadcrumb">
						<a href="<?php echo URL::site('admin/comercios/'); ?>">
							Comercios <?php echo $linea->id; ?>
						</a>
							 /
						<strong>
							Agregar Comercio
						</strong>
					</div>

					<form id="form_registro_comercio" action="<?php echo URL::site('admin/comercios/savecomercio'); ?>"
						method="POST" enctype='multipart/form-data' name="form_registro_comercio">
						<!-- Start segunda columna -->
							<input type="hidden" name="comercio_id" value="<?php echo $comercio->id; ?>"/>
							<input type="hidden" name="created" value="<?php echo date('Y-m-d h:m:s'); ?>"/>
							<?php /*  		?>
							   <input type="hidden" name="status" value="<?php echo Quickdev_Status::ACTIVE; ?>"/>
							   <?php /**/  	?>
						<h3 class="label_separador">Informacion de la comercio</h3>
						<table width="100%" cellpadding="5">
							<tr>
								<td align="right" valign="top" width="25%">Nombre de comercio</td>
								<td><input type="text" name="name" value="<?php echo $comercio->name; ?>" /></td>
							</tr>
							<tr>
								<td align="right" valign="top" width="25%">
									Usuario
								</td>
								<td>
										<select class="" name="user_id" id="user_combo" target="">
											<option>Seleccione uno</option>
											<?php
												foreach($usuarios as $item){
													$selected='';
													if($item->id==$comercio->user_id){
														$selected="selected";
													}
													echo '<option '.$selected.' value="' . $item->id .'">' . $item->name . '</option>';
												}
											?>
										</select>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">Direccion de comercio <span class="label">*</span></td>
								<td><textarea rows="" cols="" name="address" id="comercio_direccion" class="input-block-level"><?php echo $comercio->address; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Cargar Logo</td>
								<td>
									<input type="hidden" name="logo_id" value="<?php echo $logo->id; ?>">
									<?php if($logo->id != ''){ ?>
										<div class="thumbnail span4"><img src="<?php echo URL::base() . $logo->url_path ?>" /></div>
									<?php }else{ ?>
										<div class="thumbnail span4"></div>
									<?php } ?>
									<a href="javascript:void(0);" class="btn btn-mini open_galery">
										<span class="icon-camera">
										</span>
										Seleccionar imagen
									</a>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">Descripción de comercio <span class="label">opcional</span></td>
								<td><textarea rows="" cols="" name="description" id="comercio_descripcion" class="input-block-level"><?php echo $comercio->description; ?></textarea></td>
							</tr>

							<tr>
								<td align="right" valign="top">Estado</td>
								<td>
										<select class="" name="status" id="status_combo" target="">
											<option>Seleccione uno</option>
											<option <?php if($comercio->status==0){ echo "selected"; }?> value="0">Borrador</option>
											<option <?php if($comercio->status==1){ echo "selected"; }?> value="1">Publicado</option>
										</select>
								</td>
							</tr>
						</table>


						<p style="text-align: right;">
							<button type="submit" class="btn btn-danger">
								Guardar Cambios
							</button>
						</p>
					</form>
				<!-- End Content -->

				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(initPage);

	function initPage()
	{
		try{
			$('.open_galery').live('click', openGalery);
		}catch(ex){}
		try{
			CKEDITOR.replace( 'comercio_direccion');
		}catch(ex){}
		try{
			CKEDITOR.replace( 'comercio_descripcion');
		}catch(ex){}
	}

	function openGalery()
	{
		var target = $(this);
		$.colorbox({href:"<?php echo URL::site('qdmedia/home/snippet'); ?>", width:740, height:700, onComplete:function(){
			qdmedia_onSelect = function(_data, _html){
                //alert(_data);
				$.colorbox.close();
                target.parent().find('input').val(_data);
				target.parent().find('.thumbnail').html('<img src="' + _html.attr('src') + '"/>');
			};
		}});
	}

</script>