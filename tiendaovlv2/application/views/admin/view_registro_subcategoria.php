<div id="c_admin">
    <div class="container" style="width: 100%">
        <div class="row-fluid">
            <div class="span12">
                <div class="content" style="padding-left: 0;margin-left: 30px; margin-right: 30px;" >

                    <!-- Start Content -->
                    <div class="breadcrumb">
                        <a href="<?php echo URL::site('admin/categorias/'); ?>">
                            Categorias
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/subcategorias/categoria/'.$categoria->id); ?>">
                            Subcategoria
                        </a>
                        /
                        <strong>
                            Agregar Subcategoria
                        </strong>
                    </div>

                    <form id="form_registro_comercio" action="<?php echo URL::site('admin/subcategorias/save'); ?>"
                          method="POST" enctype='multipart/form-data' name="form_registro_comercio">
                        <!-- Start segunda columna -->
                        <input type="hidden" name="categoria_id" value="<?php echo $categoria->id; ?>"/>
                        <input type="hidden" name="created" value="<?php echo date('Y-m-d h:m:s'); ?>"/>

                        <h3 class="label_separador">Información de la Subcategoria</h3>
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td align="right" valign="top" width="25%">Nombre de la Subcategoria</td>
                                <td><input type="text" name="name" /></td>
                            </tr>
                            <tr>
                                <td align="right" valign="top">Descripción <span class="label">opcional</span></td>
                                <td><textarea rows="" cols="" name="description" id="comercio_descripcion" class="input-block-level"></textarea></td>
                            </tr>

                        </table>

                        <p style="text-align: right;">
                            <button id="save_categoria" type="submit" class="btn btn-danger">
                                Crear Subcategoria
                            </button>
                        </p>
                    </form>
                    <!-- End Content -->

                </div>
            </div>
        </div>
    </div>
</div>
