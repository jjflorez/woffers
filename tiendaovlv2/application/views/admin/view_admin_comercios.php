<div id="c_admin">
	<div class="container" style="width: 100%;">
		<div class="row-fluid">
			<div class="span12">
				<div class="content" >
					<ul class="nav nav-tabs">
						<li class="active"><a href="<?php echo URL::site('admin/comercios/'); ?>">Comercios</a></li>
					</ul>
					<div class="pull-right">
						<a href="<?php echo URL::site('admin/comercios/agregar/'.$linea)?>" class="btn btn-mini">
							<span class="icon-plus-sign">
							</span>
							Agregar Comercio
						</a>
					</div>
					<h4 style="height: ">
						Comercios
					</h4>

					<?php
						// format data for DataTable
						$data = array();
						foreach ($comercios as $item) {
							$row = $item->as_array();
							$row['actions'] = Html::anchor('admin/comercios/editar/'.$row['id'], __('edit')) . ' | '.
								Html::anchor('admin/comercios/eliminar/' . $row['id'], 'Eliminar', array('class' => 'delete_item')). ' | '.
								Html::anchor('admin/lineas/productos/all/' . $row['id'], 'Ver productos'). ' | '.
								Html::anchor('admin/carritos/compras/' . $row['id']."/2", 'Ver Compras');
							$row['comercio'] = $item->name;
							$row['address'] = $item->address;

							if(trim($logos[ $row['id'] ]['url_path'])==''){
								$row['logo'] = 'Sin logo';
							}else{
								$row['logo'] = '<img src="' . URL::base() . $logos[ $row['id'] ]['url_path'] . '" style="width:30px;" />';
							}

							$data[] = $row;
						}

						$column_list = array(
							'id' => array('label' => __('id')),
							'name' => array('label' => __('name')),
							'address' => array('label' => 'Direccion', 'sortable' => false),
							'description' => array('label' => __('description'), 'sortable' => false),
							'logo' => array('label' => 'Logo', 'sortable' => false),

							'actions' => array('label' => __('actions'), 'sortable' => false)
						);

						$datatable = new Helper_Datatable(
								$column_list,
								array('paginator' => true, 'class' => 'table table-bordered table-striped', 'sortable' => 'true', 'default_sort' => 'username')
							);
						$datatable->values($data);
						try{
							if($paging!=null){
								echo $paging->render();
							}
						}catch(Exception $ex){}

						echo $datatable->render();

						try{
							if($paging!=null){
								echo $paging->render();
							}
						}catch(Exception $ex){}
					?>
				</div>
			</div>
		</div>

	</div>
</div>
<script type="text/javascript">
    $(document).ready(init);

    function init(){
        $('.delete_item').click(function(){
            var r = confirm('Desea eliminar el registro?');
            if(!r){
                return false;
            }
        });
    }
</script>