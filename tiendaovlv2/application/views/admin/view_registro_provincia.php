<div id="c_admin">
    <div class="container" style="width: 100%">
        <div class="row-fluid">
            <div class="span12">
                <div class="content" style="padding-left: 0;margin-left: 30px; margin-right: 30px;" >

                    <!-- Start Content -->
                    <div class="breadcrumb">
                        <a href="<?php echo URL::site('admin/ubicaciones/'); ?>">
                            Paises
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/provincias/'.$ubicacion_id); ?>">
                            Provincias
                        </a>
                        /
                        <strong>
                            Agregar Provincia
                        </strong>
                    </div>

                    <form id="form_registro_comercio" action="<?php echo URL::site('admin/ubicaciones/saveprovincia/'.$ubicacion_id); ?>"
                          method="POST" enctype='multipart/form-data' name="form_registro_comercio">
                        <!-- Start segunda columna -->

                        <input type="hidden" name="created" value="<?php echo date('Y-m-d h:m:s'); ?>"/>
                        <h3 class="label_separador">Información la provincia</h3>
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td align="right" valign="top" width="25%">Nombre la provincia</td>
                                <td><input type="text" name="name" /></td>
                            </tr>
                        </table>

                        <p style="text-align: right;">
                            <button type="submit" class="btn btn-danger">
                                Crear Provincia
                            </button>
                        </p>
                    </form>
                    <!-- End Content -->

                </div>
            </div>
        </div>
    </div>
</div>