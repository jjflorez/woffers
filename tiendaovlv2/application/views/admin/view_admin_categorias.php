<div id="c_admin">
    <div class="container" style="width: 100%;">
        <div class="row-fluid">
            <div class="span12">
                <div class="content" >
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="<?php echo URL::site('admin/categorias/'); ?>">Categorias</a></li>

                    </ul>
                    <div class="pull-right">
                        <a href="<?php echo URL::site('admin/categorias/agregar/')?>" class="btn btn-mini">
							<span class="icon-plus-sign">
							</span>
                            Agregar Categoria
                        </a>
                    </div>
                    <h4 style="height: ">
                        Categorias
                    </h4>

                    <?php foreach ($categorias as $key=> $item) {
                        $row = $item->as_array();

                        $row['actions'] = Html::anchor('admin/subcategorias/categoria/'.$row['id'],'Ver Subcategorias') . ' | '.
                                          Html::anchor('admin/categorias/editar/'.$row['id'], __('edit')) . ' | '.
                                          Html::anchor('admin/categorias/eliminar/'.$row['id'], 'Eliminar', array('class' => 'delete_item')
                                          );
                        $row['nombre'] = $item->nombre;
                        $row['key']=$key+1;
                        $row['descripcion'] = $item->descripcion;

                        $data[] = $row;
                        }
                        $column_list = array(
                            'key' => array('label' => __('id')),
                            'nombre' => array('label' => __('Nombre')),
                            'descripcion' => array('label' => __('Description')),
                            //'logo' => array('label' => 'Logo', 'sortable' => false),
                            //'status' => array('label' => 'Estado', 'sortable' => true),
                            'actions' => array('label' => __('actions'), 'sortable' => false)
                        );

                        $datatable = new Helper_Datatable(
                            $column_list,
                            array('paginator' => true, 'class' => 'table table-bordered table-striped', 'sortable' => 'true', 'default_sort' => 'username')
                        );
                        $datatable->values($data);
                        try{
                            if($paging!=null){
                                echo $paging->render();
                            }
                        }catch(Exception $ex){}

                        echo $datatable->render();

                        try{
                            if($paging!=null){
                                echo $paging->render();
                            }
                        }catch(Exception $ex){}
                     ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $(document).ready(init);

    function init(){
        $('.delete_item').click(function(){
            var r = confirm('Al eliminar esta categoria tambien se elimaran las subcategorias correspondientes a esta ¿Desea continuar?');
            if(!r){
                return false;
            }
        });
    }
</script>