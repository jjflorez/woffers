<div id="c_admin">
    <div class="container" style="width: 100%">
        <div class="row-fluid">
            <div class="span12">
                <div class="content" style="padding-left: 0;margin-left: 30px; margin-right: 30px;" >

                    <!-- Start Content -->
                    <div class="breadcrumb">
                        <a href="<?php echo URL::site('admin/ubicaciones/'); ?>">
                            Paises
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/provincias/'.$provincia_id); ?>">
                            Provincias
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/ciudades/'.$ciudad_id); ?>">
                            Ciudades
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/codigos/'.$distrito_id); ?>">
                            Codigos postales
                        </a>
                        /
                        <strong>
                            Agregar códigos postales
                        </strong>
                    </div>

                    <form id="form_registro_comercio" action="<?php echo URL::site('admin/ubicaciones/savecodigo/'.$distrito_id); ?>"
                          method="POST" enctype='multipart/form-data' name="form_registro_comercio">
                        <!-- Start segunda columna -->
                        <input type="hidden" name="created" value="<?php echo date('Y-m-d h:m:s'); ?>"/>
                        <h3 class="label_separador">Información del codigo</h3>
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td align="right" valign="top" width="25%">Código postal</td>
                                <td><input id="codigo" type="text" name="codigo" /></td>
                            </tr>

                        </table>
                        <p style="text-align: right;">
                            <button type="submit" class="btn btn-danger">
                                Crear código
                            </button>
                        </p>
                    </form>
                    <!-- End Content -->
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#default').click(function()
        {
            if( $('#default').is(':checked')){
                $('#name').prop('disabled',true);
            }
            else{
                $('#name').prop('disabled',false);
            }
        });
    });
</script>