<div id="c_admin">
	<div class="container" style=" width: 100%;">
		<div class="row-fluid">
			<!--<div class="span2">
				<?php echo View::factory('admin/view_admin_menu'); ?>
			</div>-->
			<div class="span12">
				<div class="content" >
					<ul class="nav nav-tabs">

					</ul>
					<div class="pull-left">
						<a id="aprobar" class="btn btn-mini">
							<span class="icon-check">
							</span>
							Aprobar oferta
						</a>

						<a id="desaprobar" class="btn btn-mini">
							<span class="icon-remove">
							</span>
							Cancelar oferta
						</a>
					</div>
					<div class="pull-right">
						<a id="publicar" class="btn btn-mini">
							<span class="icon-ok">
							</span>
							Publicar
						</a>
						<a id="despublicar"class="btn btn-mini">
							<span class="icon-remove-circle">
							</span>
							Despublicar
						</a>

						<a id="editar" class="btn btn-mini">
							<span class="icon-edit">
							</span>
							Editar
						</a>
						<a id="borrar" class="btn btn-mini">
							<span class="icon-trash">
							</span>
							Borrar
						</a>

						<a href="<?php echo URL::site('admin/productos/agregar/'.$linea)?>" class="btn btn-mini">
							<span class="icon-plus-sign">
							</span>
							Agregar oferta
						</a>
					</div>
					<div class="pull-right">
						<br>
						<form action="<?php echo URL::base(); ?>admin/lineas/productos/" method="get" id="form_productos_view">
							<!--<select class="select_list" name="comercio" id="comercio_combo_view">
								<?php
									if($comercio=='all' || $comercio==''){
										$select="selected";
									}
									$option='<option value="all" '.$select.'>'.
												'Seleccione un comercio '.
											'</option>';
									echo $option;
									foreach($comercios as $item){
										$select='';
										if($item->id==$comercio){
											$select="selected";
										}
										$option='<option '.$select.' value="'.$item->id.'" >'.
													$item->name.' '.
												'</option>';
										echo $option;

									}
								?>
							</select>-->
						</form>
					</div>
					<br>
					<br>
					<div class="span12">
						Estado
						<select id="estado">
							<option value="" selected>Todos</option>
							<option <?php if($_GET['estado']=='0')echo('selected')?> value="0" >Pendiente de confirmar</option>
							<option <?php if($_GET['estado']=='1')echo('selected')?> value="1">Confimada</option>
						</select>

						Publicada
						<select id="publicada">
							<option value="" selected >Todos</option>
							<option value="1" <?php if($_GET['publicada']=='1')echo('selected')?> >Si</option>
							<option value="0" <?php if($_GET['publicada']=='0')echo('selected')?> >No</option>
						</select>
					</div>
					<br>
					<table id="ofertas" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th></th>
								<th>Código de la oferta</th>
								<th>Nombre</th>
								<th>Estado</th>
								<th>Publicada</th>
								<th>Comienza</th>
								<th>Finaliza</th>
								<th>Total vendido</th>
								<th>Empresa</th>
								<th>Actualizado</th>
								<th>ID</th>
								<th>Ver</th>
							</tr>
						</thead>
						<tbody  id="ofertas_body">

							<?php foreach ($productos as $k=>$p) {
								$k++;
								$td1='<td>'.$k.'</td>';
								$td2='<td><input class="list_values" value="'.$p->id.'" type="checkbox"></td>';
								$td3='<td>'.$p->num_referencia.'</td>';
								$td4='<td>'.$p->oferta_titulo.'</td>';
								$td5='<td>'.$p->oferta_estado.'</td>';

								if($p->oferta_publicada=='1'){
									$td6='<td style="text-align: center"><img src="'.URL::site('public/img/check_circle.png').'"></td>';
								}

								if($p->oferta_publicada=='0'){
									$td6='<td style="text-align: center"><img src="'.URL::site('public/img/cross_circle.png').'"></td>';
								}

								$td7='<td>'.$p->oferta_publicacion.'</td>';
								$td8='<td>'.$p->oferta_duracion.'</td>';
								$td9='<td>'.$p->total_vendido.'</td>';
								$td10='<td>'.$p->comercio_id.'</td>';
								$td11='<td>'.$p->updated.'</td>';
								$td12='<td>'.$p->id.'</td>';
								$td13='<td><a class="ver_oferta" value="'.URL::base('http').'preview/index/'.$p->id.'">Ver oferta</a></td>';
								$td='<tr>'.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$td10.$td11.$td12.$td13.'</tr>';
								echo($td);
							}?>

						</tbody>
					</table>


				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var selected;
<?php
	//url de redireccion de form
	$url=URL::base().'admin/lineas/productos/';
	if($linea!='')	{
		$url.=$linea.'/';
	}else{
		$url.='all/';
	}
?>
	/**
	 * Click handler for button of form comercio
	 * @access public
	 * @return void
	 **/
	function comercio_combo_view_change(event){
		event.preventDefault();
		var comercio=$('#comercio_combo_view').val();
		if(comercio==''){
			alert('Seleccione un comercio');
			return;
		}
		window.location='<?php echo $url; ?>'+comercio;
		return;
	}

	$(
		function(){
			//adding hanlder for comercio form
			$('#comercio_combo_view').change(comercio_combo_view_change);
			return;
		}
	);

    $(document).ready(init);

	//array de checks ids
	function list_values(){
		$(".list_values").click(function(){
			var chkArray = [];
			$(".list_values:checked").each(function() {
				chkArray.push($(this).val());
			});
			selected = chkArray.join(',');
		});
	}
	list_values();


	$('#publicar').click(function(){
		if(selected!='')
		{
			var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/producto/publicar');?>';
			$.ajax({
				url:url,
				type:'POST',
				data:{list_id:selected},
				dataType:'json',
				success:function(data){
					alert(data.data.mensaje);
					location.reload();
				},
				error:function(e){}
			});
		}
	});

	$('#despublicar').click(function(){
		if(selected!='')
		{
			var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/producto/despublicar');?>';
			$.ajax({
				url:url,
				type:'POST',
				data:{list_id:selected},
				dataType:'json',
				success:function(data){
					alert('Las ofertas se despublicar con éxito');
					location.reload();
				},
				error:function(e){}
			});
		}
	});

	$('#aprobar').click(function(){
		if(selected!='')
		{
			var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/producto/aprobar');?>';
			$.ajax({
				url:url,
				type:'POST',
				data:{list_id:selected},
				dataType:'json',
				success:function(data){
					alert('Las ofertas se aprobar con éxito');
					location.reload();
				},
				error:function(e){}
			});
		}
	});

	$('#desaprobar').click(function(){
		if(selected!='')
		{
			var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/producto/desaprobar');?>';
			$.ajax({
				url:url,
				type:'POST',
				data:{list_id:selected},
				dataType:'json',
				success:function(data){
					alert('Las ofertas se canelaron con éxito');
					location.reload();
				},
				error:function(e){}
			});
		}
	});

	$('#borrar').click(function(){
		if(confirm('¿Esta seguro que desea eliminar las ofertas seleccionadas?'))
		{
			if(selected!='')
			{
				var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/producto/borrar');?>';
				$.ajax({
					url:url,
					type:'POST',
					data:{list_id:selected},
					dataType:'json',
					success:function(data){
						alert('Las ofertas se despublicar con éxito');
						location.reload();
					},
					error:function(e){}
				});
			}
		}
	});

	$('#editar').click(function(){
		var id=selected.split(',');

		if(id.length==1)
		{
			var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/admin/productos/editar/');?>';
			var win = window.open(url+selected, '_blank');
			win.focus();
		}
		else{
			alert('Para editar una oferta selecciones solo una.');
		}
	});

	$("#estado").change(function()
	{
		var var_get='';
		/*var option=$("#estado option:selected").val();
		var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/producto/estado/');?>';
		$.ajax({
			url:url,
			type: 'POST',
			data:{estado:option},
			dataType: 'json',
			success: function(data) {
				//$('#ofertas').html('');
				//$('#ofertas_body').html('');
				$('#ofertas_body').html('');
				var count=1;
				data.data.productos.forEach(function(i){
					$('#ofertas_body').append('<tr><td>'+count+'</td><td><input class="list_values" value="'+ i.id+'" type="checkbox"></td><td>'+ i.num_referencia+'</td><td>'+ i.oferta_titulo+'</td><td>'+ i.oferta_estado+'</td><td style="text-align: center"><img src="'+i.oferta_publicada+'"></td><td>'+ i.oferta_publicacion+'</td><td>'+ i.oferta_duracion+'</td><td>'+ i.total_vendido+'</td><td>'+ i.comercio_id+'</td><td>'+ i.updated+'</td><td>'+ i.id+'</td><td></td></tr>');
					count++;
				});
				list_values();
			},
			error: function(e) {
			}
		});*/

		var estado=$("#estado option:selected").val();
		var publicada=$("#publicada option:selected").val();
		var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/admin/lineas/productos');?>';
		//var url2='<?php echo('http://'.$_SERVER[HTTP_HOST].$_SERVER['REQUEST_URI'])?>';

		if(estado==''){
			window.location.href=url;
		}
		else{
			var_get='?estado='+estado+'&publicada='+publicada;
			window.location.href = url+var_get;
		}
	});

	$("#publicada").change(function()
	{
		var var_get='';
		var estado=$("#estado option:selected").val();
		var publicada=$("#publicada option:selected").val();
		var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/admin/lineas/productos');?>';
		//var url='<?php echo('http://'.$_SERVER[HTTP_HOST].$_SERVER['REQUEST_URI'])?>';
		if(publicada==''){
			window.location.href=url;
		}
		else{
			var_get='?estado='+estado+'&publicada='+publicada;
			window.location.href = url+var_get;
		}
	});


	$(".ver_oferta").click(function()
	{
		var url=$(this).attr('value');
		var win = window.open(url, '_blank');
		win.focus();
	});




	function init(){
        $('.delete_item').click(function(){
            var r = confirm('Desea eliminar el registro?');
            if(!r){
                return false;
            }
        });
    }
</script>