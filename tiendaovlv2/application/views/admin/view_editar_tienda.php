<div id="c_admin">
	<div class="container" style="width: 100%">
		<div class="row-fluid">
			<div class="span12">
				<div class="content" style="padding-left: 0;margin-left: 30px; margin-right: 30px;" >

				<!-- Start Content -->
					<div class="breadcrumb">
						<a href="<?php echo URL::site('admin/lineas/tiendas/linea'); ?>">Tiendas <?php //echo $linea->code; ?></a> / <strong>Agregar tienda</strong>
					</div>

					<form id="form_registro_tienda" action="" method="POST" enctype='multipart/form-data' name="form_registro_producto">

						<input type="hidden" name="tienda_id" value="<?php echo $tienda->id; ?>"/>
						<h3 class="label_separador">Informacion de la tienda</h3>
						<table width="100%" cellpadding="5">
							<tr>
								<td align="right" valign="top" width="25%">Nombre de la tienda</td>
								<td><input type="text" name="name" value="<?php echo $tienda->name ?>" /></td>
							</tr>
							<tr>
								<td align="right" valign="top" width="25%">Código</td>
								<td><input type="text" name="code" value="<?php echo $tienda->code ?>" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">Orden</td>
								<td><input type="text" name="order" class="input-small" value="<?php echo $tienda->order ?>"></td>
							</tr>
							<tr>
								<td align="right" valign="top">Descripción de la tienda <span class="label">opcional</span></td>
								<td><textarea rows="" cols="" name="desription" class="input-block-level"><?php echo $tienda->description ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Comisión OVL</td>
								<td valign="top"><div class="input-append"><input type="text" name="comision_ovl" class="span1" style="margin: 0;" value="<?php echo $tienda->comision_ovl ?>" /><span class="add-on">%</span></div></td>
							</tr>
						</table>
						<p style="text-align: right;"><button type="submit" class="btn btn-danger">Crear tienda</button></p>
					</form>
				<!-- End Content -->
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(initPage);

	function initPage()
	{
		$('.open_galery').click(openGalery);
	}

	function openGalery()
	{
		var target = $(this);
		$.colorbox({href:"<?php echo URL::site('qdmedia/home/snippet'); ?>", width:740, height:700, onComplete:function(){
			qdmedia_onSelect = function(_data, _html){
                //alert(_data);
				$.colorbox.close();
                target.parent().find('input').val(_data);
				target.parent().find('.thumbnail').html('<img src="' + _html.attr('src') + '"/>');
			};
		}});
	}
</script>