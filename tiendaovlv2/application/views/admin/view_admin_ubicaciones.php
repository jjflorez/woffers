<div id="c_admin">
    <div class="container" style="width: 100%;">
        <div class="row-fluid">
            <div class="span12">
                <div class="content" >
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="<?php echo URL::site('admin/categorias/'); ?>">Ubicaciones</a></li>

                    </ul>
                    <div class="pull-right">
                        <a href="<?php echo URL::site('admin/ubicaciones/agregarpais/')?>" class="btn btn-mini">
							<span class="icon-plus-sign">
							</span>
                            Agregar País
                        </a>
                    </div>
                    <h4 style="height: ">
                        Paises
                    </h4>

                    <?php foreach ($paises as $key=> $item) {
                        $row = $item->as_array();
                        $row['actions'] =
                            Html::anchor('admin/ubicaciones/provincias/'.$row['id'],'Ver provincias') . ' | '.
                            Html::anchor('admin/ubicaciones/editarpais/'.$row['id'], __('edit')) . ' | '.
                            Html::anchor('admin/ubicaciones/eliminar/'.$row['id'], 'Eliminar', array('class' => 'delete_item'));
                        $row['nombre'] = $item->nombre;
                        $row['key']=$key+1;

                        $data[] = $row;
                    }
                    $column_list = array(
                        'key' => array('label' => __('Id')),
                        'nombre' => array('label' => __('Nombre')),

                        //'logo' => array('label' => 'Logo', 'sortable' => false),
                        //'status' => array('label' => 'Estado', 'sortable' => true),
                        'actions' => array('label' => __('actions'), 'sortable' => false)
                    );

                    $datatable = new Helper_Datatable(
                        $column_list,
                        array('paginator' => true, 'class' => 'table table-bordered table-striped', 'sortable' => 'true', 'default_sort' => 'username')
                    );
                    $datatable->values($data);
                    try{
                        if($paging!=null){
                            echo $paging->render();
                        }
                    }catch(Exception $ex){}

                    echo $datatable->render();

                    try{
                        if($paging!=null){
                            echo $paging->render();
                        }
                    }catch(Exception $ex){}
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(init);

    function init(){
        $('.delete_item').click(function(){
            var r = confirm('¿Esta seguro que desea eliminar este país?');
            if(!r){
                return false;
            }
        });
    }
</script>