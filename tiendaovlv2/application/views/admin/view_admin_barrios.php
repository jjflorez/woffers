<div id="c_admin">
    <div class="container" style="width: 100%">
        <div class="row-fluid">
            <div class="span12">
                <div class="content" >
                    <div class="breadcrumb">
                        <a href="<?php echo URL::site('admin/ubicaciones/ubicaciones/'); ?>">
                            Paises
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/provincias/'.$provincia_id); ?>">
                            Provincias
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/ciudades/'.$ciudad_id); ?>">
                            Ciudades
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/distritos/'.$distrito_id); ?>">
                            Distritos
                        </a>
                        /
                        <strong>
                            Barrios
                        </strong>
                    </div>
                    <div class="pull-right">
                        <a href="<?php echo URL::site('admin/ubicaciones/agregarbarrio/'.$ubicacion_id)?>" class="btn btn-mini">
							<span class="icon-plus-sign">
							</span>
                            Agregar Barrio
                        </a>
                    </div>
                    <h4 style="height: ">
                        Barrios
                    </h4>
                    <?php $temp_provincia=$provincias->as_array();
                    if(count($temp_provincia)==0)
                        echo '<div class="alert alert-info">No se encontraron barrios.</div>';
                    ?>

                    <?php if(count($temp_provincia)!=0) {?>
                        <?php foreach ($provincias as $key=> $item) {
                            $row = $item->as_array();
                            $row['actions'] =

                                Html::anchor('admin/ubicaciones/editarbarrio/' . $row['id'], __('edit')) . ' | ' .
                                Html::anchor('admin/ubicaciones/eliminar/' . $row['id'], 'Eliminar', array('class' => 'delete_item'));
                            $row['nombre'] = $item->nombre;
                            $row['key']=$key+1;
                            $data[] = $row;
                        }
                        $column_list = array(
                            'key' => array('label' => __('id')),
                            'nombre' => array('label' => __('Nombre')),
                            'actions' => array('label' => __('actions'), 'sortable' => false)
                        );

                        $datatable = new Helper_Datatable(
                            $column_list,
                            array('paginator' => true, 'class' => 'table table-bordered table-striped', 'sortable' => 'true', 'default_sort' => 'username')
                        );
                        $datatable->values($data);
                        try {
                            if ($paging != null) {
                                echo $paging->render();
                            }
                        } catch (Exception $ex) {
                        }

                        echo $datatable->render();

                        try {
                            if ($paging != null) {
                                echo $paging->render();
                            }
                        } catch (Exception $ex) {
                        }
                        ?>
                    <?php }?>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(init);

    function init(){
        $('.delete_item').click(function(){
            var r = confirm('¿Esta seguro que desea eliminar el barrio?');
            if(!r){
                return false;
            }
        });
    }
</script>