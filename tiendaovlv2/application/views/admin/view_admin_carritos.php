<div id="c_admin">
	<div class="container" style="width: 100%">
		<div class="row-fluid">
			<div class="span12">
				<div class="content" >
					<ul class="nav nav-tabs">
						<li class="active"><a href="<?php
								if(isset($comercio)==true){
									if(isset($status)==true){
										echo URL::site('admin/carritos/'.$comercio."/".$status);
									}else{
										echo URL::site('admin/carritos/'.$comercio."/all/");
									}
								}else{
									if(isset($status)==true){
										echo URL::site('admin/carritos/all/'.$status);
									}else{
										echo URL::site('admin/carritos/all/all');
									}
								}
						?>">Compras</a></li>
					</ul>
					<div class="pull-right">
						<?php /* ?>
						<a href="<?php echo URL::site('admin/productos/agregar/'.$linea)?>" class="btn btn-mini">
							<span class="icon-plus-sign">
							</span>
							Agregar producto
						</a>
						<?php /**/ ?>
					</div>
					<div class="pull-right">
						<form action="<?php echo URL::base(); ?>admin/carritos/compras/" method="get" id="form_productos_view">
							<select class="select_list" name="status" id="status_combo_view">
								<?php
									if($status=='all' || $status==''){
										$select="selected";
									}
									$option='<option value="all" '.$select.'>'.
												'Seleccione un estado '.
											'</option>';
									echo $option;
									foreach($estados as $item){
										$select='';
										if($item['id']==$status){
											$select="selected";
										}
										$option='<option '.$select.' value="'.$item['id'].'" >'.
													$item['name'].' '.
												'</option>';
										echo $option;

									}
								?>
							</select>
						</form>
					</div>


					<h4 style="height: ">Compras</h4>

					<?php
						// format data for DataTable
						$data = array();
						foreach ($compras as $i=>$item) {
							$row = $item->as_array();
							$row['actions'] = Html::anchor('admin/productos/editar/' . $item->id, __('edit')) . ' | '.
											  Html::anchor('admin/productos/eliminar/' . $item->id, 'Eliminar', array('class' => 'delete_item'));
							$row['user'] = $extra[$i]['usuario'];
							$row['direccion'] = $extra[$i]['direccion'];
							$row['estado'] = $extra[$i]['status'];
							$data[] = $row;
						}

						$column_list = array(
							'id' => array('label' => __('id')),
							'user' => array('label' =>"Usuario", 'sortable' => true),
							'direccion' => array('label' =>"Direccion", 'sortable' => true),
							'factura' => array('label' => 'Factura', 'sortable' => false),
							'total_price' => array('label' => 'Precio Total', 'sortable' => true),
							'pago' => array('label' => 'Fecha de Pago', 'sortable' => true),
							'actions' => array('label' => __('actions'), 'sortable' => false)
						);

						$datatable = new Helper_Datatable(
								$column_list,
								array(
									'paginator' => true,
									'class' => 'table table-bordered table-striped',
									'sortable' => 'true',
									'default_sort' => 'user'
								)
						);
						$datatable->values($data);
						try{
							echo $paging->render();
						}catch(Exception $ex){

						}
						echo $datatable->render();
						try{
							echo $paging->render();
						}catch(Exception $ex){

						}

					?>
				</div>
			</div>
		</div>

	</div>
</div>
<script type="text/javascript">
<?php
	//url de redireccion de form
	$url=URL::base().'admin/carritos/compras/';
	if(isset($comercio)==true){
		$url.=$comercio."/"	;
	}else{
		$url.="all/";
	}
?>
	/**
	 * Click handler for button of form comercio
	 * @access public
	 * @return void
	 **/
	function status_combo_view_change(event){
		event.preventDefault();
		var status=$('#status_combo_view').val();
		if(status==''){
			alert('Seleccione un estado');
			return;
		}
		window.location='<?php echo $url; ?>'+status;
		return;
	}

	$(
		function(){
			//adding hanlder for comercio form
			$('#status_combo_view').change(status_combo_view_change);
			var status=$('#status_combo_view').val();
			var page=parseInt(<?php echo $page; ?>);
			//generar urls de paginacion
			var contador=0;
			$('.btn-toolbar a').each(
				function(index,item){
					contador++;
					return;
				}
			);
			var limit=parseInt(((contador-8)/2))+1;
			console.log(limit);
			var url_page="<?php echo $url; ?>"+status+"/";
			//arreglo a paginado de elementos de carrito
			$('.btn-toolbar a').each(
				function(index,item){
					contador++
					var html=$(item).html();
					var last=$(item).attr('rel');
					if(html=='Primero'){
						item.href=url_page+'1/';
						return;
					}
					if(html=='<span class="icon-chevron-left"></span>'){
						item.href=url_page+'1/';
						return;
					}
					if(last=='last'){
						item.href=url_page+limit+'/';
						return;
					}
					if(html=='<span class="icon-chevron-right"></span>'){
						item.href=url_page+limit+'/';
						return;
					}
					item.href=url_page+parseInt(html)+'/';
					return;
				}
			);
			return;
		}
	);

    $(document).ready(init);

    function init(){
        $('.delete_item').click(function(){
            var r = confirm('Desea eliminar el registro?');
            if(!r){
                return false;
            }
        });
    }
</script>