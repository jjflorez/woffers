<div id="c_admin">
    <div class="container" style="width: 100%">
        <div class="row-fluid">
            <div class="span12">
                <div class="content" style="padding-left: 0;margin-left: 30px; margin-right: 30px;" >

                    <!-- Start Content -->
                    <div class="breadcrumb">
                        <a href="<?php echo URL::site('admin/ubicaciones/'); ?>">
                            Paises
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/provincias/'.$provincia_id); ?>">
                            Provincias
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/ciudades/'.$ciudad_id); ?>">
                            Ciudades
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/distritos/'.$distrito_id); ?>">
                            Distritos
                        </a>
                        /
                        <strong>
                            Agregar Distrito
                        </strong>
                    </div>

                    <form id="form_registro_comercio" action="<?php echo URL::site('admin/ubicaciones/savedistrito/'.$distrito_id); ?>"
                          method="POST" enctype='multipart/form-data' name="form_registro_comercio">
                        <!-- Start segunda columna -->
                        <input type="hidden" name="created" value="<?php echo date('Y-m-d h:m:s'); ?>"/>
                        <h3 class="label_separador">Información del distrito</h3>
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td align="right" valign="top" width="25%">Nombre del distrito</td>
                                <td><input id="name" type="text" name="name" /></td>
                            </tr>
                            <tr>
                                <td align="right" valign="top" width="25%"></td>
                                <td style="display: inline-flex;">
                                    <input id="default" name="default" type="checkbox"> <label for="default" style="cursor: pointer"> En caso de que no exista distrito marcar esta opción</label>
                                </td>
                            </tr>
                        </table>
                        <p style="text-align: right;">
                            <button type="submit" class="btn btn-danger">
                                Crear distrito
                            </button>
                        </p>
                    </form>
                    <!-- End Content -->
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#default').click(function()
        {
            if( $('#default').is(':checked')){
                $('#name').prop('disabled',true);
            }
            else{
                $('#name').prop('disabled',false);
            }
        });
    });
</script>