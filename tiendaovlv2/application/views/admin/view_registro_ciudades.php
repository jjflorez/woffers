<div id="c_admin">
    <div class="container" style="width: 100%">
        <div class="row-fluid">
            <div class="span12">
                <div class="content" style="padding-left: 0;" >

                    <!-- Start Content -->
                    <div class="breadcrumb">
                        <a href="<?php echo URL::site('admin/ubicaciones/'); ?>">
                            Paises
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/provincias/'.$provincia_id); ?>">
                            Provincias
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/ciudades/'.$ubicacion_id); ?>">
                            Ciudades
                        </a>
                        /
                        <strong>
                            Agregar Ciudad
                        </strong>
                    </div>

                    <form id="form_registro_comercio" action="<?php echo URL::site('admin/ubicaciones/saveciudad/'.$ubicacion_id); ?>"
                          method="POST" enctype='multipart/form-data' name="form_registro_comercio">
                        <!-- Start segunda columna -->
                        <input type="hidden" name="created" value="<?php echo date('Y-m-d h:m:s'); ?>"/>
                        <h3 class="label_separador">Información la ciudad</h3>
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td align="right" valign="top" width="25%">Nombre la ciudad</td>
                                <td><input type="text" name="name" /></td>
                            </tr>
                        </table>
                        <p style="text-align: right;">
                            <button type="submit" class="btn btn-danger">
                                Crear Ciudad
                            </button>
                        </p>
                    </form>
                    <!-- End Content -->
                </div>
            </div>
        </div>
    </div>
</div>

