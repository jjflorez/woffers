<div id="c_admin">
	<div class="container" style=" width: 100%;">
		<div class="row-fluid">
			<!--<div class="span2">
				<?php echo View::factory('admin/view_admin_menu'); ?>
			</div>-->
			<div class="span12">
				<div class="content" >
					<div class="pull-right">

						<form action="<?php echo URL::base(); ?>admin/lineas/productos/" method="get" id="form_productos_view">
							<!--<select class="select_list" name="comercio" id="comercio_combo_view">
								<?php
							if($comercio=='all' || $comercio==''){
								$select="selected";
							}
							$option='<option value="all" '.$select.'>'.
								'Seleccione un comercio '.
								'</option>';
							echo $option;
							?>
							</select>-->
						</form>
					</div>
					<br>
					<br>
					<div class="span12">
						Buscar oferta
						<select id="ofertas">
							<option value="" selected>Todos</option>
							<?php
							foreach($ofertas as $item):
								$select='';
								if($item->id==$get_oferta){
									$select="selected";
								}
								?>

								<option <?php echo($select)?>  value="<?php echo $item->id ?>"><?php echo $item->oferta_titulo ?></option>
							<?php endforeach; ?>
						</select>

						Estado
						<select id="estado">
							<option value="" selected>Todos</option>
							<option <?php if($_GET['estado']=='1')echo('selected')?> value="1" >Vendido</option>
							<option <?php if($_GET['estado']=='2')echo('selected')?> value="2">Verificado</option>
							<option <?php if($_GET['estado']=='3')echo('selected')?> value="3">Caducado</option>
							<option <?php if($_GET['estado']=='4')echo('selected')?> value="4">Cancelado</option>
						</select>
					</div>
					<br>
					<br>
					<br>
					<table id="ofertas" class="table table-bordered table-striped">
						<thead>
						<tr>
							<th>#</th>
							<th>Código de cupón</th>
							<th>Oferta</th>
							<th>Nombre del comprador</th>
							<th>Nombre Usuario</th>
							<th>Periodo de validez</th>
							<th>Fecha de compra</th>
							<th>Fecha de consumo</th>
							<th>Codigo de validacion</th>
							<th>Estado</th>
							<th>ID</th>

						</tr>
						</thead>
						<tbody  id="ofertas_body">

						<?php foreach ($cupones as $k=>$p) {
							$k++;
							$td1='<td>'.$k.'</td>';
							$td2='<td>'.$p['codigo_cupon'].'</td>';
							$td3='<td>'.$p['oferta'].'</td>';
							$td4='<td>'.$p['name'].'</td>';
							$td5='<td>'.$p['user'].'</td>';
							$td6='<td>'.$p['tiempo_validez'].'</td>';
							$td7='<td>'.$p['fecha_compra'].'</td>';

							switch($p['estado']){
								case 1:
									$td10= '<td style="color: #345ee8;">Vendido</td>';
									$td9='<td style="color: #345ee8;">'.$p['codigo_validacion'].'</td>';
									$td8='<td style="color: #345ee8;">'.$p['fecha_consumo'].'</td>';
								break;
								case 2:
									$td10= '<td style="color: #419f53;">Verificado</td>';
									$td9='<td style="color: #419f53;">'.$p['codigo_validacion'].'</td>';
									$td8='<td style="color: #419f53;">'.$p['fecha_consumo'].'</td>';
									break;
								case 3:
									$td10= '<td style="color: #f44d48;">Caducado</td>';
									$td9='<td style="color: #f44d48;">'.$p['codigo_validacion'].'</td>';
									$td8='<td style="color: #f44d48;">'.$p['fecha_consumo'].'</td>';
									break;
								case 4:
									$td10= '<td style="color: #e874e6;">Cancelado</td>';
									$td9='<td style="color: #e874e6;">'.$p['codigo_validacion'].'</td>';
									$td8='<td style="color: #e874e6;">'.$p['fecha_cancelacion'].'</td>';
									break;
							}
							$td11='<td>'.$p['id'].'</td>';
							$td='<tr>'.$td1.$td2.$td3.$td4.$td5.$td6.$td7.$td8.$td9.$td10.$td11.'</tr>';
							echo($td);
						}?>

						</tbody>
					</table>


				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var selected;
	<?php
        //url de redireccion de form
        $url=URL::base().'admin/lineas/productos/';
        if($linea!='')	{
            $url.=$linea.'/';
        }else{
            $url.='all/';
        }
    ?>
	/**
	 * Click handler for button of form comercio
	 * @access public
	 * @return void
	 **/



	//array de checks ids
	function list_values(){
		$(".list_values").click(function(){
			var chkArray = [];
			$(".list_values:checked").each(function() {
				chkArray.push($(this).val());
			});
			selected = chkArray.join(',');
		});
	}
	list_values();

	$("#ofertas").change(function()
	{
		var var_get='';
		var ofertas=$("#ofertas option:selected").val();
		var estado=$("#estado option:selected").val();
		var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/admin/lineas/cupones');?>';
		if(ofertas==''){
			window.location.href=url;
		}
		else{
			var_get='?ofertas='+ofertas+'&estado='+estado;
			window.location.href = url+var_get;
		}
	});

	$("#estado").change(function()
	{
		var var_get='';
		var ofertas=$("#ofertas option:selected").val();
		var estado=$("#estado option:selected").val();
		var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/admin/lineas/cupones');?>';
		if(estado==''){
			window.location.href=url;
		}
		else{
			var_get='?ofertas='+ofertas+'&estado='+estado;
			window.location.href = url+var_get;
		}
	});

</script>