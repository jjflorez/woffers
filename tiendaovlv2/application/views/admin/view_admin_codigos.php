<div id="c_admin">
    <div class="container" style="width: 100%">
        <div class="row-fluid">
            <div class="span12">
                <div class="content" >
                    <div class="breadcrumb">
                        <a href="<?php echo URL::site('admin/ubicaciones/'); ?>">
                            Paises
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/provincias/'.$provincia_id); ?>">
                            Provincias
                        </a>
                        /
                        <a href="<?php echo URL::site('admin/ubicaciones/ciudades/'.$ciudad_id); ?>">
                            Ciudades
                        </a>
                        /
                        <strong>
                            Códigos postales
                        </strong>
                    </div>
                    <div class="pull-right">
                        <a href="<?php echo URL::site('admin/ubicaciones/agregarcodigo/'.$ubicacion_id)?>" class="btn btn-mini">
							<span class="icon-plus-sign">
							</span>
                            Agregar código postal
                        </a>
                    </div>
                    <h4 style="height: ">
                        Códigos postales
                    </h4>
                    <?php $temp_codigos=$codigos->as_array();
                    if(count($temp_codigos)==0) {
                        echo '<div class="alert alert-info">No se encontraron codigos postales.</div>';
                    }else {
                        ?>


                        <?php foreach ($codigos as $key => $item) {

                            $row = $item->as_array();
                            $row['actions'] =
                                Html::anchor('admin/ubicaciones/editarcodigo/' . $row['id'], __('edit')) . ' | ' .
                                Html::anchor('admin/ubicaciones/eliminarcodigo/' . $row['id'], 'Eliminar', array('class' => 'delete_item'));
                            $row['codigo'] = $item->codigo;
                            $row['key'] = $key + 1;
                            $data[] = $row;
                        }
                        $column_list = array(
                            'key' => array('label' => __('id')),
                            'codigo' => array('label' => __('codigo')),
                            'actions' => array('label' => __('actions'), 'sortable' => false)
                        );

                        $datatable = new Helper_Datatable(
                            $column_list,
                            array('paginator' => true, 'class' => 'table table-bordered table-striped', 'sortable' => 'true', 'default_sort' => 'username')
                        );
                        $datatable->values($data);
                        try {
                            if ($paging != null) {
                                echo $paging->render();
                            }
                        } catch (Exception $ex) {
                        }

                        echo $datatable->render();

                        try {
                            if ($paging != null) {
                                echo $paging->render();
                            }
                        } catch (Exception $ex) {
                        }
                    }?>


                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(init);

    function init(){
        $('.delete_item').click(function(){
            var r = confirm('¿Esta seguro que desea eliminar el codigo postal?');
            if(!r){
                return false;
            }
        });
    }
</script>