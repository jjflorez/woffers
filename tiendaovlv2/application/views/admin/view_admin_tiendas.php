<div id="c_admin">
	<div class="container" style="width: 100%">
		<div class="row-fluid">
			<div class="span12">
				<div class="content" >
					<ul class="nav nav-tabs">

						<li><a href="<?php echo URL::site('admin/lineas/productos/'.$linea); ?>">Productos</a></li>
						<li class="active"><a href="<?php echo URL::site('admin/lineas/tiendas/'.$linea); ?>">Tiendas</a></li>
					</ul>
					<div class="pull-right">
						<a href="<?php echo URL::site('admin/tiendas/agregar/'.$linea)?>" class="btn btn-mini"><span class="icon-plus-sign"></span> Agregar tienda</a>
					</div>
					<h4 style="height: ">Productos en offers</h4>

					<?php
						// format data for DataTable
						$data = array();
						foreach ($tiendas as $file) {
							$row = $file->as_array();
							$row['actions'] = Html::anchor('admin/tiendas/editar/' . $row['id'], __('edit')) . ' | ' . Html::anchor('admin/tiendas/eliminar/' . $row['id'], 'Eliminar', array('class' => 'delete_item'));
							$data[] = $row;
						}

						$column_list = array(
							'id' => array('label' => __('id')),
							'name' => array('label' => __('name')),
							'code' => array('label' => __('description'), 'sortable' => false),
							'comision_ovl' => array('label' => 'Comisión', 'sortable' => false),
							'actions' => array('label' => __('actions'), 'sortable' => false)
						);

						$datatable = new Helper_Datatable($column_list, array('paginator' => true, 'class' => 'table table-bordered table-striped', 'sortable' => 'true', 'default_sort' => 'username'));
						$datatable->values($data);

						echo $paging->render();
						echo $datatable->render();
						echo $paging->render();
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(init);

    function init(){
        $('.delete_item').click(function(){
            var r = confirm('Desea eliminar el registro?');
            if(!r)
                return false;
        });
    }
</script>