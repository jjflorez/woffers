<div class="eddy-cont">
    <div class="eddy-cols-x">

        <div class="eddy-col-9">
            <div id="conferencias">
            <div class="bread bread_2">
                <ul>
                    <li><a href="#">promociona tu negocio</a></li>
                    <li><a class="active" href="#">Contacto para comercios</a></li>
                </ul>
            </div>

            <h3>Contacto para comercios</h3>
            <p>Ponte en contacto con One Vision Life y resolveremos cualquier duda que tengas. El equipo de One Vision Life te responderá en la mayor brevedad posible y te dará la mejor solución.</p>

            <img src="<?php echo URL::base();?>public/img/contacto_comercios.jpg" >
            </div>
        </div>
        <div class="eddy-col-8">
            <?php echo View::factory('ci/view_sidebar')->set('var_sidebar','')?>
        </div>
    </div>
</div>

<style type="text/css">
    #conferencias h3{
        color: #A9BC46;
    }

    #conferencias .titulo{
        color: #98b206;
        text-align: center;
        text-shadow: 1px 1px 1px #444;
    }
</style>