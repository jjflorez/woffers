<div id="c_admin">
	<div class="container">
		<div id="eddy-nav-x">
            <ul>
                <!--<li class="active">
                    <a href="<?php echo URL::site('admin/cupones/'); ?>">
                        Ofertas del dia
                    </a>
                </li>-->
                <li><a href="<?php echo URL::site('comercios'); ?>">Inicio</a></li>
                <li><a href="<?php echo URL::site('comercios/perfil'); ?>">Perfil</a></li>
                <li><a  href="javascript:void(0);">Ventas</a></li>
                <li><a  href="javascript:void(0);">Crear Oferta</a></li>
                <li><a  href="javascript:void(0);">Herramientas</a></li>
                <li><a  href="javascript:void(0);">Soporte</a></li>
                <li><a class="active" href="<?php echo URL::site('comercios/cupones'); ?>">listacupones</a></li>
            </ul>
        </div>
		<div class="row-fluid">
			<?php /* ?>
			<div class="span3">
				<?php echo View::factory('admin/view_admin_menu'); ?>
			</div>
			<?php /**/ ?>
			<div class="span12">
				<div class="content" >
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="<?php echo URL::site($url); ?>">
								Cupones
							</a>
						</li>
						<?php /* ?>
						   <li>
						   <a href="<?php echo URL::site('admin/cupones/permanentes'); ?>">
						   Ofertas permanentes
						   </a>
						   </li>
						   <?php */?>
					</ul>

					<h4 style="height: ">Cupones</h4>

					<h1>
						Actualizar el estado del cup&oacute;n
					</h1>
					<p>
						<form action="<?php echo URL::site($url); ?>" method=post>
							<input type="hidden" name="token" value="buscar"/>
							<div class="row-fluid">
								<div class="span2">
									<label>
										Codigo de Cupon :
									</label>
								</div>
								<input type="text" name="codigo_cupon" value="" class="span2"/>
								<div class="span2">
									<input type="submit" name="button" value="Buscar Cupon" class="btn btn-primary">
								</div>
							</div>

						</form>
					</p>
					<p>
						<form action="<?php echo URL::site($url); ?>" method="post">
							<input type="hidden" name="token" value="oferta"/>
							<div class="row-fluid">
								<?php //echo $oferta; ?>
								<select name="oferta" class="span6">
									<?php foreach($ofertas as $i=>$o){ ?>
										<?php foreach($o as $j=>$k){ ?>
											<?php
												$select="";
												if($k->id==$oferta){
													$select=" selected ";
												}
											?>
											<option <?php echo $select; ?> value="<?php echo $k->id; ?>">
												<?php echo $k->oferta_titulo;?>
											</option>
										<?php } ?>
									<?php } ?>
								</select>
								<div class="span6">
									<input type="submit" value="Ir" class="btn btn-primary"/>
								</div>
							</div>
						</form>
					</p>

						<center>
							<strong>
								Lista de Cupones
							</strong>
						</center>
						<br>
						<table class="table table-striped table-bordered table-hover">
							<tr>
								<th>

								</th>
								<?php /*?>
								<th>
									Id
								</th>
								<?php /**/ ?>
								<th>
									Nombre
								</th>
								<th>
									C&oacute;digo de Cup&oacute;n
								</th>
								<th>
									Fecha de compra
								</th>
								<th>
									C&oacute;digo de Validaci&oacute;n
								</th>
								<th>
									Estado
								</th>
							</tr>
                            <?php
								foreach($cupones as $i=>$K){

							?>
                                <tr>
                                    <td>
                                        <?php echo $cupones[$i]['numero']; ?>
                                    </td>
                                    <?php /*?>
                                    <td>
                                        <?php echo $cupones[$i]['producto_id']; ?>
                                    </td>
                                    <?php /**/?>
                                    <td>
                                        <?php echo $cupones[$i]['nombre']; ?>
                                    </td>
                                    <td>
                                        <?php echo $cupones[$i]['codigo_cupon']; ?>
                                    </td>
                                    <td>
                                        <?php echo $cupones[$i]['fecha_compra']; ?>
                                    </td>
                                    <td>
                                        <?php if($cupones[$i]['validated']=='1'){ ?>
                                            <?php echo $cupones[$i]['codigo_validacion']; ?>
                                            <?php /* ?>
												<strong>Validado</strong>
                                           	<?php /**/ ?>
                                        <?php }else{ ?>
                                            <form  action="<?php echo URL::site($url_validar); ?>" method="post">
                                                <input type="hidden" name="id" value="<?php echo $cupones[$i]['id']; ?>">
                                                <input type="hidden" name="url" value="<?php echo $url; ?>">
                                                <input type="text" name="codigo_validacion">
                                                <button class="btn"  id="btn_validador_<?php echo $cupones[$i]['id']; ?>"> >> </button>
                                            </form>
                                        <?php } ?>

                                    </td>
                                    <td>
                                        <?php echo $cupones[$i]['valido']; ?>
                                    </td>
                                </tr>
                            <?php

								}
							?>

						</table>
						<?php /*?>
						<center>
                            <form  action="<?php echo URL::base("http"); ?>admin/descargas/cupones" method="post">
                                <button class="btn">
                                    Descargar Lista de Cupones
                                </button>
                            </form>

						</center>
						<?php /***/ ?>


				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(init);
    function init(){}
</script>