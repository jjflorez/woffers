<div id="c_admin">
	<div class="container">
		<div id="eddy-nav-x">
            <ul>
                <!--<li class="active">
                    <a href="<?php echo URL::site('admin/cupones/'); ?>">
                        Ofertas del dia
                    </a>
                </li>-->
                <li><a href="<?php echo URL::site('comercios'); ?>">Inicio</a></li>
                <li><a href="<?php echo URL::site('comercios/perfil'); ?>">Perfil</a></li>
                <li><a  href="javascript:void(0);">Ventas</a></li>
                <li><a  href="javascript:void(0);">Crear Oferta</a></li>
                <li><a  href="javascript:void(0);">Herramientas</a></li>
                <li><a  href="javascript:void(0);">Soporte</a></li>
                <li><a class="active" href="<?php echo URL::site('comercios/cupones'); ?>">listacupones</a></li>
            </ul>
        </div>
		<div class="row-fluid">
			<?php /*?>
			<div class="span3">
				<?php //echo View::factory('admin/view_admin_menu'); ?>
			</div>
			<?php /**/ ?>
			<div class="span12">
				<div class="content" >
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="<?php echo URL::base()."/comercios/cupones/";?>">
							Lista de Cupones
							</a>
						</li>
					</ul>
					<div class="pull-right">
						<a href="<?php echo URL::site('comercios/cupones/agregar/'); ?>" class="btn btn-mini">
							<span class="icon-plus-sign">
							</span>
							Agregar cupon
						</a>
					</div>
					<?php /*?>
					<div class="pull-right">
						<form action="<?php echo URL::base(); ?>admin/lineas/productos/" method="get" id="form_productos_view">
							<select class="select_list" name="comercio" id="comercio_combo_view">
								<?php
									if($comercio=='all' || $comercio==''){
										$select="selected";
									}
									$option='<option value="all" '.$select.'>'.
														'Seleccione un comercio '.
													'</option>';
									echo $option;
									foreach($comercios as $item){
										$select='';
										if($item->id==$comercio){
											$select="selected";
										}
										$option='<option '.$select.' value="'.$item->id.'" >'.
													$item->name.' '.
												'</option>';
										echo $option;
									}
								?>
							</select>
						</form>
					</div>
					<?php /**/ ?>

					<h4 style="height: ">Lista de Cupones Comercio </h4>

					<?php
				// format data for DataTable
				$data = array();
				foreach ($cupones as $cupon) {
					$row = $cupon;
					$row['actions'] = Html::anchor(
									'comercioc/cupones/editar/'.
									$row['id'], __('edit')).
									' | '.
									Html::anchor(
									'comercios/cupones/eliminar/'.
									 $row['id'], 'Eliminar',
									 array('class' => 'delete_item')
									 );
					$row['tienda'] = $cupon['tienda']->name;
					if(isset($cupon['comercio']['name'])==true){
						$row['comercio_nombre'] = $cupon['comercio']['name'];
					}
					$row['img_externa'] = '<img src="' . URL::base() . $cupon['comercio']['logo']->url_path . '" style="width:30px;" />';
					$data[] = $row;
				}

				$column_list = array(
						'id' => array('label' => __('id')),
						'oferta_titulo' => array('label' => __('name')),
						'oferta_descripcion' => array('label' => __('description'), 'sortable' => false),
						'tienda' => array('label' => 'Tienda', 'sortable' => false),
						'comercio_nombre' => array('label' => 'Comercio', 'sortable' => false),
						'img_externa' => array('label' => 'Imagen', 'sortable' => false, 'align'=>'center'),
						//'actions' => array('label' => __('actions'), 'sortable' => false)
					);



				$datatable = new Helper_Datatable(
						$column_list,
						array(
							'paginator' => true,
							'class' => 'table table-bordered table-striped',
							'sortable' => 'true',
							'default_sort' => 'id'
							)
					);
				$datatable->values($data);
				try{
					if(isset($paging)==true){
						echo $paging->render();
					}
				}catch(Exception $ex){

				}
				try{
					echo $datatable->render();
				}catch(Exception $ex){

				}
				try{
					if(isset($paging)==true){
						echo $paging->render();
					}
				}catch(Exception $ex){

				}

				?>
				</div>
			</div>
		</div>

	</div>
</div>
<script type="text/javascript">
	<?php
	//url de redireccion de form
	$url=URL::base().'admin/lineas/productos/';
	if($linea!='')	{
		$url.=$linea.'/';
	}else{
		$url.='all/';
	}
	//echo "id=\"".$type."\";";
	?>
	$(
		function(){
			//adding hanlder for comercio form
			$('#<? echo $type; ?>').addClass('active');
		}
	);
	/**
	 * Click handler for button of form comercio
	 * @access public
	 * @return void
	 **/
	function comercio_combo_view_change(event){
		event.preventDefault();
		var comercio=$('#comercio_combo_view').val();
		if(comercio==''){
			alert('Seleccione un comercio');
			return;
		}
		window.location='<?php echo $url; ?>'+comercio;
		return;
	}

	$(
		function(){
			//adding hanlder for comercio form
			$('#comercio_combo_view').change(comercio_combo_view_change);
			return;
		}
	);

    $(document).ready(init);

    function init(){
        $('.delete_item').click(function(){
            var r = confirm('Desea eliminar el registro?');
            if(!r){
                return false;
            }
        });
    }
</script>