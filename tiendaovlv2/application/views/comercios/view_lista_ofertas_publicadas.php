<div id="c_admin">
	<div class="container">
		<div id="eddy-nav-x">
            <ul>
                <li>
                	<a href="<?php echo URL::site('comercios'); ?>">
                		Inicio
                	</a>
                </li>
                <li>
                	<a href="<?php echo URL::site('comercios/perfil'); ?>">
                		Perfil
                	</a>
                </li>
                <li>
                	<a  href="javascript:void(0);">
                		Ventas
                	</a>
                </li>
                <li>
                	<a  href="javascript:void(0);">
                		Crear Oferta
                	</a>
                </li>
                <li>
                	<a  href="javascript:void(0);">
                		Herramientas
                	</a>
                </li>
                <li>
                	<a  href="javascript:void(0);">
                		Soporte
                	</a>
                </li>
                <li>
                	<a class="active" href="<?php echo URL::site('comercios/cupones'); ?>">
                			Lista Cupones
                	</a>
                </li>
            </ul>
        </div>
        <br/>
        <br/>
        <div class="container">
			<div class="row">
				<div class="span4">
					<h2>
						Gestion de Ofertas
					</h2>
				</div>
				<div class="span1">
					<a href="<?php echo URL::site('comercios/ofertas/aprobar'); ?>"
							class="span1 btn btn-active"
							style="height:50px;"
							>
						Aprobar oferta
					</a>
				</div>
				<div class="span1">
					<a href="<?php echo URL::site('comercios/ofertas/cancelar'); ?>"
							class="span1 btn btn-active"
							style="height:50px;"
							>
						Cancelar oferta
					</a>
				</div>
				<div class="span1">
					<a href="<?php echo URL::site('comercios/ofertas/confirmar'); ?>"
							class="span1 btn btn-active"
							style="height:50px;"
							>
						Confirmar oferta
					</a>
				</div>
				<div class="span1">
					<a href="<?php echo URL::site('comercios/ofertas/publicar'); ?>"
							class="span1 btn btn-active"
							style="height:50px;"
							>
						Publicar
					</a>
				</div>
				<div class="span1">
					<a href="<?php echo URL::site('comercios/ofertas/despublicar'); ?>"
							class="span1 btn btn-active"
							style="height:50px;"
							>
						Despublicar
					</a>
				</div>
				<div class="span1">
					<a href="<?php echo URL::site('comercios/ofertas/editar'); ?>"
							class="span1 btn btn-active"
							style="height:50px;"
							>
						Editar
					</a>
				</div>
				<div class="span1">
					<a href="<?php echo URL::site('comercios/ofertas/borrar'); ?>"
							class="span1 btn btn-active"
							style="height:50px;"
							>
						Borrar
					</a>
				</div>
				<div class="span1">
					<a href="<?php echo URL::site('comercios/ofertas/nueva'); ?>"
							class="span1 btn btn-active"
							style="height:50px;"
							>
						Nuevo
					</a>
				</div>
			</div>
			<br/>
			<?php // finish row ?>
			<?php // init row  form?>
			<div>
				<div class="row">
					<div class="span1">
						<p>
							<label>
								C&oacute;digo de oferta:
							</label>
						</p>
						<input class="span1" type="text" name="codigo" id="codigo_data" value="<?php if($post['codigo']!='-1'){ echo $post['codigo']; }?>" />
					</div>
					<?php /*?>
					<div class="span1">
						<p>
							<label>
								Nombre:
							</label>
						</p>
						<input type="text" class="span1" name="nombre" id="nombre_data" value="<?php if($post['nombre']!='-1'){ echo $post['nombre']; }?>" />
					</div>
					<?php /* */ ?>
					<div class="span1">
						<p>
							<label>
								Autor
							</label>
						</p>
						<select class="span1" name="autor" id="autor_data">
							<?php foreach($autores as $i=>$autor){?>
								<?php if($autor['id']==$post['autor']){ ?>
									<option value="<?php echo $autor['id']; ?>" selected>
										<?php echo $autor['name'];?>
									</option>
								<?php }else{?>
									<option value="<?php echo $autor['id']; ?>">
										<?php echo $autor['name'];?>
									</option>
								<?php } ?>
							<?php } ?>
						</select>
						<script>
							$(
								function(){
									$("#autor_data").change(
										function(){
											var value=$(this).val();
											var formdata=load_data();
											$.ajax(
												{
													type:'post',
													url:"<?php echo URL::site("comercios/ofertas/filtros")?>",
													data:formdata,
													success:function(data){
														data=$.parseJSON(data);
														if(data.success=='true'){
															window.location='<?php echo URL::site("comercios/ofertas/publicadas")?>';
														}
														return;
													}
												}
											);
										}
									);
								}
							);
						</script>
					</div>
					<div class="span1">
						<p>
							<label>
								Publicado
							</label>
						</p>
						<select class="span1" name="publicado" id="publicado_data">
							<?php foreach($publicados as $i=>$publicado){?>
								<?php if($publicado['id']==$post['publicado']){ ?>
									<option value="<?php echo $publicado['id']; ?>" selected>
										<?php echo $publicado['name']; ?>
									</option>
								<?php }else{ ?>
									<option value="<?php echo $publicado['id']; ?>">
										<?php echo $publicado['name']; ?>
									</option>
								<?php } ?>
							<?php }?>
						</select>
						<script>
							$(
								function(){
									$("#publicado_data").change(
										function(){
											var value=$(this).val();
											var formdata=load_data();
											$.ajax(
												{
													type:'post',
													url:"<?php echo URL::site("comercios/ofertas/filtros")?>",
													data:formdata,
													success:function(data){
														data=$.parseJSON(data);
														if(data.success=='true'){
															window.location='<?php echo URL::site("comercios/ofertas/publicadas")?>';
														}
														return;
													}
												}
											);
										}
									);
								}
							);
						</script>
					</div>
					<div class="span1">
						<p>
							<label>
								Tipo de oferta
							</label>
						</p>
						<select class="span1" name="pago" id="pago_data" >
							<?php foreach($pagos as $i=>$pago){?>
								<?php if($pago['id']==$post['pago']){ ?>
									<option value="<?php echo $pago['id']; ?>" selected>
										<?php echo $pago['name'];?>
									</option>
								<?php }else{ ?>
									<option value="<?php echo $pago['id']; ?>">
										<?php echo $pago['name'];?>
									</option>
								<?php } ?>
							<?php }?>
						</select>
						<script>
							$(
								function(){
									$("#pago_data").change(
										function(){
											var value=$(this).val();
											var formdata=load_data();
											$.ajax(
												{
													type:'post',
													url:"<?php echo URL::site("comercios/ofertas/filtros")?>",
													data:formdata,
													success:function(data){
														data=$.parseJSON(data);
														if(data.success=='true'){
															window.location='<?php echo URL::site("comercios/ofertas/publicadas")?>';
														}
														return;
													}
												}
											);
										}
									);
								}
							);
						</script>
					</div>
					<div class="span1">
						<p>
							<label>
								Provincia
							</label>
						</p>
						<select class="span1" name="provincia" id="provincia_data" >
							<option value="-1">
								Seleccione una opcion
							</option>
							<?php foreach($provincias as $i=>$provincia){?>
								<?php if($provincia->id==$post['provincia']){ ?>
									<option value="<?php echo $provincia->id; ?>" selected>
										<?php echo $provincia->nombre;?>
									</option>
								<?php }else{ ?>
									<option value="<?php echo $provincia->id; ?>">
										<?php echo $provincia->nombre;?>
									</option>
								<?php } ?>
							<?php }?>
						</select>
						<script>
							$(
								function(){
									$("#provincia_data").change(
										function(){
											var value=$(this).val();
											var formdata=load_data();
											$.ajax(
												{
													type:'post',
													url:"<?php echo URL::site("comercios/ofertas/filtros")?>",
													data:formdata,
													success:function(data){
														data=$.parseJSON(data);
														if(data.success=='true'){
															window.location='<?php echo URL::site("comercios/ofertas/publicadas")?>';
														}
														return;
													}
												}
											);
										}
									);
								}
							);
						</script>
					</div>
					<div class="span1">
						<p>
							<label>
								Ciudad
							</label>
						</p>
						<select class="span1" name="ciudad" id="ciudad_data" >
							<option value="-1">
								Seleccione una opcion
							</option>
							<?php foreach($ciudades as $i=>$ciudad){?>
								<?php if($ciudad->id==$post['ciudad']){ ?>
									<option value="<?php echo $ciudad->id; ?>" selected>
										<?php echo $ciudad->nombre;?>
									</option>
								<?php }else{ ?>
									<option value="<?php echo $ciudad->id; ?>">
										<?php echo $ciudad->nombre;?>
									</option>
								<?php } ?>
							<?php }?>
						</select>
						<script>
							$(
								function(){
									$("#ciudad_data").change(
										function(){
											var value=$(this).val();
											var formdata=load_data();
											$.ajax(
												{
													type:'post',
													url:"<?php echo URL::site("comercios/ofertas/filtros")?>",
													data:formdata,
													success:function(data){
														data=$.parseJSON(data);
														if(data.success=='true'){
															window.location='<?php echo URL::site("comercios/ofertas/publicadas")?>';
														}
														return;
													}
												}
											);
										}
									);
								}
							);
						</script>
					</div>
					<div class="span1">
						<p>
							<label>
								Distrito
							</label>
						</p>
						<select class="span1" name="distrito" id="distrito_data" >
							<option value="-1">
								Seleccione una opcion
							</option>
							<?php foreach($distritos as $i=>$distrito){?>
								<?php if($distrito->id==$post['distrito']){ ?>
									<option value="<?php echo $distrito->id; ?>" selected>
										<?php echo $distrito->nombre;?>
									</option>
								<?php }else{ ?>
									<option value="<?php echo $distrito->id; ?>">
										<?php echo $distrito->nombre;?>
									</option>
								<?php } ?>
							<?php }?>
						</select>
						<script>
							$(
								function(){
									$("#distrito_data").change(
										function(){
											var value=$(this).val();
											var formdata=load_data();
											$.ajax(
												{
													type:'post',
													url:"<?php echo URL::site("comercios/ofertas/filtros")?>",
													data:formdata,
													success:function(data){
														data=$.parseJSON(data);
														if(data.success=='true'){
															window.location='<?php echo URL::site("comercios/ofertas/publicadas")?>';
														}
														return;
													}
												}
											);
										}
									);
								}
							);
						</script>
					</div>
					<div class="span1">
						<p>
							<label>
								Barrio
							</label>
						</p>
						<select class="span1" name="barrio" id="barrio_data" >
							<option value="-1">
								Seleccione una opcion
							</option>
							<?php foreach($barrios as $i=>$barrio){?>
								<?php if($barrio->id==$post['barrio']){ ?>
									<option value="<?php echo $barrio->id; ?>" selected>
										<?php echo $barrio->nombre;?>
									</option>
								<?php }else{ ?>
									<option value="<?php echo $barrio->id; ?>">
										<?php echo $barrio->nombre;?>
									</option>
								<?php } ?>
							<?php }?>
						</select>
						<script>
							$(
								function(){
									$("#barrio_data").change(
										function(){
											var value=$(this).val();
											var formdata=load_data();
											$.ajax(
												{
													type:'post',
													url:"<?php echo URL::site("comercios/ofertas/filtros")?>",
													data:formdata,
													success:function(data){
														data=$.parseJSON(data);
														if(data.success=='true'){
															window.location='<?php echo URL::site("comercios/ofertas/publicadas")?>';
														}
														return;
													}
												}
											);
										}
									);
								}
							);
						</script>
					</div>
					<?php //categoria ?>
					<div class="span1">
						<p>
							<label>
								Categoria
							</label>
						</p>
						<select class="span1" name="categoria" id="categoria_data" >
							<option value="-1">
								Seleccione una opcion
							</option>
							<?php foreach($categorias as $i=>$categoria){?>
								<?php if($categoria->id==$post['categoria']){ ?>
									<option value="<?php echo $categoria->id; ?>" selected >
										<?php echo $categoria->nombre;?>
									</option>
								<?php }else{ ?>
									<option value="<?php echo $categoria->id; ?>">
										<?php echo $categoria->nombre;?>
									</option>
								<?php } ?>
							<?php }?>
						</select>
						<script>
							$(
								function(){
									$("#categoria_data").change(
										function(){
											var value=$(this).val();
											var formdata=load_data();
											$.ajax(
												{
													type:'post',
													url:"<?php echo URL::site("comercios/ofertas/filtros")?>",
													data:formdata,
													success:function(data){
														data=$.parseJSON(data);
														if(data.success=='true'){
															window.location='<?php echo URL::site("comercios/ofertas/publicadas")?>';
														}
														return;
													}
												}
											);
										}
									);
								}
							);
						</script>
					</div>
					<?php /**/?>


					<?php /* ?>
					<div class="span1">
						<p>
							<label>
								Estado
							</label>
						</p>
						<select class="span1" name="estado" id="estado_data">
						</select>
					</div>
					<?php /**/?>
					<div class="span1">
						<br>
						<a href="<?php echo URL::site("comercios/ofertas/buscar"); ?>"
							id="buscar_btn"
							class="btn span1">
							Buscar
						</a>
					</div>
					<div class="span1">
						<br>
						<a href="<?php echo URL::site("comercios/ofertas/limpiar"); ?>"
							id="limpiar_btn"
							class="btn span1">
							Limpiar
						</a>
					</div>
				</div>
			</div>
			<?php // finish row form ?>
			<script>
				var ids=new Array();
			</script>
			<table class="table table-striped table-bordered table-hover">
				<tr>
					<th>

					</th>
					<th>
						#
					</th>
					<th>
						Codigo de Oferta
					</th>
					<th>
						Nombre
					</th>
					<th>
						Total Vendido
					</th>
					<th>
						Publicado
					</th>
					<th>
						Comienza
					</th>
					<th>
						Finaliza
					</th>
					<th>
						Autor
					</th>
					<th>
						Comercio
					</th>
					<th>
						Actualizado
					</th>
					<th>
						Id
					</th>
				</tr>
				<?php foreach($ofertas as $i=>$oferta){?>
					<tr>
						<td>
							<input
								type="checkbox"
								name="oferta_<?php echo $oferta['id']; ?>"
								value="<?php echo $oferta['id']; ?>"
								id="oferta_<?php echo $oferta['id']; ?>"
							/>
							<script>
								$(
									function(){
										ids.push('<?php echo $oferta['id']; ?>');
										return;
									}
								);
							</script>
						</td>
						<td>
							<?php echo $oferta['numero']; ?>
						</td>
						<td>
							<?php echo $oferta['codigo']; ?>
						</td>
						<td>
							<?php echo $oferta['nombre']; ?>
						</td>
						<td>
							<?php echo $oferta['total']; ?>
						</td>
						<td>
							<?php echo $oferta['publicado']; ?>
						</td>
						<td>
							<?php echo $oferta['comienza']; ?>
						</td>
						<td>
							<?php echo $oferta['finaliza']; ?>
						</td>
						<td>
							<?php echo $oferta['autor']; ?>
						</td>
						<td>
							<?php echo $oferta['comercio']; ?>
						</td>
						<td>
							<?php echo $oferta['actualizado']; ?>
						</td>
						<td>
							<?php echo $oferta['id']; ?>
						</td>
					</tr>
				<?php }?>
			</table>
		</div>
	</div>
</div>

<script>
/*carga de filtros de datos de busqueda*/
function load_data(){
	var data={};
	var codigo='';
	try{
		codigo=$("#codigo_data").val();
	}catch(ex){
		codigo='';
	}
	/*var nombre='';
	try{
		nombre=$("#nombre_data").val();
	}catch(ex){
		nombre='';
	}*/
	var autor='';
	try{
		autor=$("#autor_data").val();
	}catch(ex){
		autor='';
	}
	var publicado='';
	try{
		publicado=$("#publicado_data").val();
	}catch(ex){
		publicado='';
	}
	var pago='';
	try{
		pago=$("#pago_data").val();
	}catch(ex){
		pago='';
	}
	var provincia='';
	try{
		provincia=$("#provincia_data").val();
	}catch(ex){
		provincia='';
	}
	var ciudad='';
	try{
		ciudad=$("#ciudad_data").val();
	}catch(ex){
		ciudad='';
	}
	var distrito='';
	try{
		distrito=$("#distrito_data").val();
	}catch(ex){
		distrito='';
	}
	var barrio='';
	try{
		barrio=$("#barrio_data").val();
	}catch(ex){
		barrio='';
	}
	var categoria='';
	try{
		categoria=$("#categoria_data").val();
	}catch(ex){
		categoria='';
	}
	data={
		'codigo':codigo,
		'autor':autor,
		'publicado':publicado,
		'pago':pago,
		'provincia':provincia,
		'ciudad':ciudad,
		'distrito':distrito,
		'barrio':barrio,
		'categoria':categoria
	};
	return data;
}
function launch_check(id){
	$("#oferta_"+id).click(
		function(evt){
			var url='<?php echo URL::site('comercios/ofertas/check'); ?>';
			var flag=true;
			var checked=$("#oferta_"+id).prop('checked');
			if(checked==false){
				url='<?php echo URL::site('comercios/ofertas/uncheck'); ?>';
			}else{
				url='<?php echo URL::site('comercios/ofertas/check'); ?>';
			}
			$.ajax(
				{
					type:'post',
					url: url,
					data: {
						id: id
					}
				}
			).done(
				function(data){
					data=$.parseJSON(data);
					if(data.success=='true'){

					}else{
						try{
							evt.preventDefault();
						}catch(ex){}
						try{console.log("Hubo un error en el proceso...");}catch(ex){}
					}
					return;
				}
			);

			return;
		}
	);
	return;
}
function clean_data(){
	$("#codigo_data").val('');
	$("#autor_data").val('');
	$("#publicado_data").val('');
	$("#pago_data").val('');
	$("#provincia_data").val('');
	$("#ciudad_data").val('');
	$("#distrito_data").val('');
	$("#barrio_data").val('');
	$("#categoria_data").val('');
}
//inicio de main
	$(
		function(){
			for(var id in ids){
				launch_check(ids[id]);
			}
			$('#buscar_btn').click(
				function(evt){
					var formdata=load_data();
					$.ajax(
						{
							type:'post',
							url:"<?php echo URL::site("comercios/ofertas/filtros")?>",
							data:formdata,
							success:function(data){
								data=$.parseJSON(data);
								if(data.success=='true'){
									window.location='<?php echo URL::site("comercios/ofertas/publicadas")?>';
								}
								return;
							}
						}
					);
					evt.preventDefault();
					return;
				}
			);

			$('#limpiar_btn').click(
				function(evt){
					$.ajax(
						{
							type:'post',
							url:"<?php echo URL::site("comercios/ofertas/limpiar_filtros")?>",
							success:function(data){
								data=$.parseJSON(data);
								if(data.success=='true'){
									clean_data();
									window.location='<?php echo URL::site("comercios/ofertas/publicadas")?>';
								}
								return;
							}
						}
					);
					evt.preventDefault();
					return;
				}
			);
		}
	);
</script>