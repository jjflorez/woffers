<div id="c_comercio">

	<div class="eddy-aux">
        <div id="eddy-nav-x">
            <ul>
                <li><a href="<?php echo URL::site('comercios'); ?>">Inicio</a></li>
                <li><a class="active" href="<?php echo URL::site('comercios/perfil'); ?>">Perfil</a></li>
                <li><a  href="javascript:void(0);">Ventas</a></li>
                <li><a  href="javascript:void(0);">Crear Oferta</a></li>
                <li><a  href="javascript:void(0);">Herramientas</a></li>
                <li><a  href="javascript:void(0);">Soporte</a></li>
                <li><a  href="<?php echo URL::site('comercios/cupones'); ?>">listacupones</a></li>
            </ul>
        </div>
        <div class="eddy-cont eddy-box-1" >
            <div class="eddy-cols">
            <div class="eddy-col-1">
                <div class="links_nav">
                    <a class="first"  href="<?php echo URL::site('comercios/perfil'); ?>">Datos del comercio</a>
                    <a class="active" href="javascript:void(0);"><span class="sig">></span> Datos de pago</a>
                    <a  href="<?php echo URL::site('comercios/new_pass'); ?>">Cambiar contraseña</a>
                </div>
            </div>
            <div class="eddy-col-2">
                <div class="bord-1">
                    <h3 class="ttl-x">Datos del comercio</h3>
                <div class="eddy-box-1">
                    <p>Introduce tus datos de pago para que One Vision Life pueda ingresarte el importe correspondiente a los cupones vendidos en tus campañas realizadas.</p>
                    <p>Recuerda que One Vision Life realiza los pagos los Martes de cada semana.</p>
                    <div class="eddy-panel-x eddy-box-1">
                        <h3>Transferencia bancaria:</h3>
                        <div class="eddy-tbl-1 feat-tbl-z">
                            <div class="eddy-cols">
                                <div class="eddy-col-3">
                                    Nombre completo del titular de la cuenta
                                    <input type="text">
                                </div>
                                <div class="eddy-col-3">
                                    Nombre completo del titular de la cuenta<br>
                                    <div class="cuenta">
                                        <input class="n_1" type="text"><input class="n_1" type="text"><input class="n_2" type="text"><input class="n_3" type="text">
                                    </div>
                                </div>
                            </div>
                            <a class="eddy-btn-x btn-feat-x">Guardar</a>
                        </div>
                        <p>Juan Pérez Gómez  |  Banco Sabadell  |  2130-3245-56-0039008789</p>



                    </div>




                </div>
            </div>
            </div>
        </div>
        </div>
</div>