<?php
if(!isset($producto))	$producto = new Model_Producto();
?>
<div id="c_admin">
	<div class="container">
		<div id="eddy-nav-x">
            <ul>
                <!--<li class="active">
                    <a href="<?php echo URL::site('admin/cupones/'); ?>">
                        Ofertas del dia
                    </a>
                </li>-->
                <li><a href="<?php echo URL::site('comercios'); ?>">Inicio</a></li>
                <li><a href="<?php echo URL::site('comercios/perfil'); ?>">Perfil</a></li>
                <li><a  href="javascript:void(0);">Ventas</a></li>
                <li><a  href="javascript:void(0);">Crear Oferta</a></li>
                <li><a  href="javascript:void(0);">Herramientas</a></li>
                <li><a  href="javascript:void(0);">Soporte</a></li>
                <li><a class="active" href="<?php echo URL::site('comercios/cupones'); ?>">listacupones</a></li>
            </ul>
        </div>
		<div class="row-fluid">
			<?php /*?>
			   <div class="span3">
			   <?php //echo View::factory('admin/view_admin_menu'); ?>
			   </div>
			   <?php /**/ ?>
			<div class="span12">
				<div class="content" style="padding-left: 0;" >

				<!-- Start Content -->
					<div class="breadcrumb">
						<a href="<?php echo URL::site('comercios/cupones'); ?>">
						Cupones
						</a>
						 / agregar cupon /
					</div>

					<form id="form_registro_producto" action="<?php echo URL::site('comercios/cupones/guardar/'); ?>" method="POST" name="form_registro_producto">
						<!-- Start segunda columna -->
						<?php if($linea == 'permanentes'){ ?>
                        	<h3 class="label_separador">Informaci&oacute;n de la ciudad y categor&iacute;a</h3>

							<!-- Campos ubicacion -->
                        	<table width="100%" cellpadding="3" border="0">
								<tr>
									<td align="right" valign="top" width="25%">Pais</td>
									<td>
										<select class="ubicacion" name="oferta_ubicacion_pais" id="oferta_ubicacion_pais" target="oferta_ubicacion_provincia">
											<option>Seleccione uno</option>
											<?php foreach($paises as $item){
												$selected = '';
												if($producto->oferta_ubicacion_pais == $item->id) $selected = 'selected="selected"';
												echo '<option '.$selected.'value="' . $item->id .'">' . $item->nombre . '</option>';
											} ?>
										</select>
									</td>
									<td align="right">Distrito</td>
									<td>
                                        <select class="ubicacion" name="oferta_ubicacion_distrito"  id="oferta_ubicacion_distrito" target="oferta_ubicacion_barrio">
                                            <option value="-1">Seleccione uno</option>
                                            <?php
							if(isset($distritos)){
								foreach($distritos as $item){
									$selected = '';
									if($producto->oferta_ubicacion_distrito == $item->id) $selected = 'selected="selected"';
									echo '<option '.$selected.'value="' . $item->id .'">' . $item->nombre . '</option>';
								}} ?>
                                        </select>
									</td>
								</tr>
								<tr>
									<td align="right" valign="top" width="25%">Provincia</td>
									<td>
                                        <select class="ubicacion" name="oferta_ubicacion_provincia" id="oferta_ubicacion_provincia" target="oferta_ubicacion_ciudad" >
                                            <option value="-1">Seleccione uno</option>
                                            <?php
							if(isset($provincias)){
								foreach($provincias as $item){
									$selected = '';
									if($producto->oferta_ubicacion_provincia == $item->id) $selected = 'selected="selected"';
									echo '<option '.$selected.'value="' . $item->id .'">' . $item->nombre . '</option>';
								}} ?>
                                        </select>
                                    </td>
                                    <td align="right">Barrio</td>
                                    <td>
                                        <select class="ubicacion" name="oferta_ubicacion_barrio"  id="oferta_ubicacion_barrio" target="">
                                            <option value="-1">Seleccione uno</option>
                                            <?php
							if(isset($barrios)){
								foreach($barrios as $item){
									$selected = '';
									if($producto->oferta_ubicacion_barrio == $item->id) $selected = 'selected="selected"';
									echo '<option '.$selected.'value="' . $item->id .'">' . $item->nombre . '</option>';
								}} ?>
                                        </select>
                                    </td>
								</tr>
								<tr>
									<td align="right" valign="top" width="25%">Ciudad</td>
									<td>
                                        <select class="ubicacion" name="oferta_ubicacion_ciudad" id="oferta_ubicacion_ciudad" target="oferta_ubicacion_distrito">
                                            <option>Seleccione uno</option>
                                            <?php foreach($ciudades as $item){
                                            	$selected = '';
                                            	if($producto->oferta_ubicacion_ciudad == $item['id']){
                                            		$selected = 'selected="selected"';
                                            		echo '<option '.$selected.'value="' . $item['id'] .'">' . $item['nombre'] . '</option>';
                                            	}

                                            } ?>
                                        </select>
                                    </td>
                                    <td align="right">Categor�a</td>
                                    <td>
                                        <select name="oferta_categoria_id">
                                            <option>Seleccione una categor�a</option>
                                            <?php
												foreach($categorias as $cat)
												{
													$selected_cat = '';
													if($producto->oferta_categoria_id == $cat->id) $selected_cat = 'selected="selected"';
													echo '<option '.$selected_cat.' value="' . $cat->id . '">' . $cat->nombre . '</option>';
												}
											?>
                                        </select>
                                    </td>
								</tr>
							</table>

						<?php }else{ ?>

                        <h3 class="label_separador">Informaci&oacute;n de la ciudad</h3>
                        <table width="100%" cellpadding="5" border="0">
                            <tr>
                                <td align="right" valign="top" width="25%">Pais</td>
                                <td>
                                    <select class="ubicacion" name="oferta_ubicacion_pais" id="oferta_pais" target="oferta_ciudad">
                                        <option>Seleccione uno</option>
										<?php
											foreach($paises as $item):
											$selec0 = '';
											if($producto->oferta_ubicacion_pais == $item->id)
												$selec0 = 'selected="selected"';
										?>
										<option <?php echo $selec0 ?> value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option>
										<?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top" width="25%">Oferta Nacional</td>
                                <td>
                                    <?php
							$disable = 'disabled="disabled"';
							if($producto->oferta_nacional === '1'): ?>
                                    <label class="radio inline"><input type="radio" name="oferta_nacional" value="1" checked="checked" class="oferta_nacional"/> Si</label>
                                    <label class="radio inline"><input type="radio" name="oferta_nacional" value="0" class="oferta_ciudad"/> No</label>

                                    <?php elseif($producto->oferta_nacional === '0'):
                                    	$disable = ''; ?>
                                    <label class="radio inline"><input type="radio" name="oferta_nacional" value="1" class="oferta_nacional"/> Si</label>
                                    <label class="radio inline"><input type="radio" name="oferta_nacional" value="0" checked="checked" class="oferta_ciudad"/> No</label>

                                    <?php else: ?>
                                    <label class="radio inline"><input type="radio" name="oferta_nacional" value="1" checked="checked" class="oferta_nacional"/> Si</label>
                                    <label class="radio inline"><input type="radio" name="oferta_nacional" value="0" class="oferta_ciudad"/> No</label>

                                    <?php endif; ?>

                                    <?php //echo $producto->oferta_nacional ?>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ciudad: <select class="ubicacion" name="oferta_ubicacion_ciudad" id="oferta_ciudad" target="" <?php echo $disable ?> style="margin-bottom: 0;">
                                            <option value="-1">Seleccione la ciudad</option>
                                            <?php
												foreach($ciudades as $item):
													$selec = '';
													if($producto->oferta_ubicacion_ciudad == $item['id']) $selec = 'selected="selected"';
											?>
                                        		<option <?php echo $selec ?> value="<?php echo $item['id'] ?>"><?php echo $item['nombre'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                </td>
                            </tr>
                        </table>

						<?php } ?>
						<input type="hidden" name="comercio_id" value="<?php echo $comercio_id; ?>"/>
						<?php /*?>
							<h3 class="label_separador">Informaci&oacute;n del comercio</h3>
						<?php /**/ ?>
						<table width="100%" cellpadding="5">
							<?php /* ?>
							   <tr>
							   <td align="right" valign="top" width="25%">Comercio</td>
							   <td>
							   		<input type="hidden" name="comercio_id" value="<?php echo $comercio_id; ?>"/>
							   		<strong>
							   			<?php echo $comercio; ?>
							   		</strong>
							   <?php /* ?>
							   <select class="comercio" name="comercio_id" id="comercio_id" target="comercio_id">
							   		<option>
							   			Seleccione uno
							   		</option>
							   <?php
							   foreach($seleccion_comercios as $item){
								   $select = '';
								   if($producto->comercio_id == $item->id){
								   		$select = 'selected';
								   }
							   ?>
							   		<option <?php echo $select; ?> value="<?php echo $item->id; ?>">
							   			<?php echo $item->name; ?>
							   		</option>
							   <?php } ?>
							   </select>
							   <?php /**/ ?>
							   </td>
							   </tr>
							   <?php /**/ ?>
							<?php /*?>
							<tr>
								<td align="right" valign="top" width="25%">Nombre del comercio</td>
								<td><input type="text" name="comercio_nombre" value="<?php echo $producto->comercio_nombre; ?>"></p></td>
							</tr>
							<tr>
								<td align="right" valign="top">Direcci&oacute;n del comercio</td>
								<td><textarea rows="" cols="" name="comercio_direccion" class="input-block-level"><?php echo $producto->comercio_direccion; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Descripci&oacute;n del comercio</td>
								<td><textarea rows="" cols="" name="comercio_descripcion" id="comercio_descripcion" class="input-block-level"><?php echo $producto->comercio_descripcion; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Cargar Logo</td>
								<td>
									<input type="hidden" name="comercio_logo_id" value="<?php echo $producto->comercio_logo_id; ?>">
									<?php if($producto->comercio_logo_id != ''){ ?>
									<div class="thumbnail span4"><img src="<?php echo URL::base() . $producto->comercio_imagen->url_path ?>" /></div>
									<?php }else{ ?>
									<div class="thumbnail span4"></div>
									<?php } ?>
									<a href="javascript:void(0);" class="btn btn-mini open_galery"><span class="icon-camera"></span> Seleccionar imagen</a> </td>
							</tr>
							<?php /**/?>
						</table>

						<!-- Separador -->

						<br/>
						<h3 class="label_separador">Informaci&oacute;n de la oferta</h3>
						<table width="100%" cellpadding="5" border="0">
                            <?php
								if($linea == 'permanentes'){
									$_display = 'style="display: none;"';
								}else{
									$_display = '';
								}
							?>

							<tr <?php echo $_display ?>>
                                <td align="right" valign="top" width="25%">Tipo de tienda</td>
                                <td>
                                    <select name="tienda_id" class="inline">
                                        <!--<option value="">Seleccione una tienda</option>-->
                                        <?php
											foreach($tiendas as $tienda){
												if($producto->tienda_id == $tienda->id)
													echo '<option selected="selected" value="'.$tienda->id.'">'.$tienda->name.'</option>';
												else
													echo '<option value="'.$tienda->id.'">'.$tienda->name.'</option>';
											}
										?>
                                    </select>
                                    &nbsp;&nbsp;&nbsp;
                                    Tipo de oferta :
                                    <?php /*?> Cupon
                                    <input type="hidden" name="oferta_tipo" value="<?php echo "cupon";?>">
                                    <?php /**/ ?>
									<?php

									?>
									<select name="oferta_tipo" class="inline span3" readonly="readonly">
                                    <?php
											switch($linea)
											{
												case 'offers':
												case 'travel':
												case 'coaching':
												case 'permanentes':
													echo '<option value="cupon" selected="selected">Cup&oacute;n</option>';
													break;
												case 'outlet':
													echo '<option value="producto" selected="selected">Cup&oacute;n de producto</option>';
													break;
											}

									?>
	                                    <option value="">Seleccione uno</option>
	                                    <option value="producto" <?php echo ($linea==''); ?>>Producto</option>
	                                    <option value="cupon">Cup&oacute;n</option>
	                                    <option value="cupon_producto">Cup&oacute;n de producto</option>
                                	</select>
                                	<?php /**/  ?>
                                </td>
                            </tr>

							<tr>
								<td align="right" valign="top">Titulo de la oferta</td>
								<td><input type="text" maxlength="95" name="oferta_titulo" value="<?php echo $producto->oferta_titulo; ?>" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">Destacamos</td>
								<td><textarea rows="" cols="" name="oferta_destacamos" class="input-block-level"><?php echo $producto->oferta_destacamos; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Que incluye la oferta</td>
								<td><textarea rows="" cols="" name="oferta_que_incluye" class="input-block-level"><?php echo $producto->oferta_que_incluye; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Descripci&oacute;n de la oferta</td>
								<td><textarea rows="" cols="" name="oferta_descripcion" class="input-block-level"><?php echo $producto->oferta_descripcion; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Condiciones</td>
								<td><textarea rows="" cols="" name="oferta_condiciones" class="input-block-level"><?php echo $producto->oferta_condiciones; ?></textarea></td>
							</tr>
							<tr>
								<td align="right" valign="top">Ubicaci&oacute;n</td>
								<td valign="middle">
									Latitud: <input type="text" name="oferta_ubicacion_lat" class="input-small" value="<?php echo $producto->oferta_ubicacion_lat; ?>" />&nbsp;&nbsp;&nbsp;
									Longitud: <input type="text" name="oferta_ubicacion_lon" class="input-small" value="<?php echo $producto->oferta_ubicacion_lon; ?>" />
								</td>
							</tr>

							<!-- Inicio despleagables -->
							<!--
							<tr>
								<td align="right" valign="top">&nbsp;</td>
								<td><h4>Desplegable 1</h4></td>
							</tr>
							<tr>
								<td align="right" valign="top">Titulo desplegable</td>
								<td><input type="text" name="" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">Opci&oacute;n 1</td>
								<td><input type="text" name="" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">Opci&oacute;n 2</td>
								<td><input type="text" name="" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">&nbsp;</td>
								<td><h4>Desplegable 2</h4></td>
							</tr>
							<tr>
								<td align="right" valign="top">Titulo desplegable</td>
								<td><input type="text" name="" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">Opci&oacute;n 1</td>
								<td><input type="text" name="" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">Opci&oacute;n 2</td>
								<td><input type="text" name="" /></td>
							</tr>
							-->
							<!-- End  despleagables ############################# -->


							<tr>
								<td align="right" valign="top">Imagen externa</td>
								<td>
									<input type="hidden" name="oferta_imagen_id" value="<?php echo $producto->oferta_imagen_id; ?>">
									<div class="thumbnail span4"><img src="<?php echo URL::base() . $producto->oferta_imagen->url_path; ?>" /> </div>
									<a href="javascript:void(0);" class="btn btn-mini open_galery"><span class="icon-camera"></span> Seleccionar imagen</a>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">Imagenes internas</td>
								<td>
									<div id="lista_externas">
										<?php
foreach($producto->productofile->find_all() as $file){
	echo '<div>
			<input type="hidden" name="imagen_externa[]" value="' . $file->file_id . '">
			<div class="thumbnail span4"><img src="' . URL::base() . $file->file->url_path . '" /></div>
			<a href="javascript:void(0);" class="btn btn-mini open_galery"><span class="icon-camera"></span> Seleccionar imagen</a> <a href="#" class="">&times;</a>
		</div>';
}
?>
										<div>
											<input type="hidden" name="imagen_externa[]" value="">
											<div class="thumbnail span4"></div>
											<a href="javascript:void(0);" class="btn btn-mini open_galery"><span class="icon-camera"></span> Seleccionar imagen</a> <a href="#" class="">&times;</a>
										</div>
									</div>
									<a href="javascript:void(0);" class="btn btn-mini btn-success add_image"><span class="icon-plus-sign icon-white"></span> Agregar imagen</a>
								</td>
							</tr>

						</table>
						<!-- Tercera tabla -->
						<br/>
						<h3 class="label_separador">Informaci&oacute;n de descuento</h3>
						<table width="100%" cellpadding="5">
							<tr>
								<td align="right" valign="top" width="25%">Comisi&oacute;n OVL:</td>
								<td valign="top">
									<?php
										$valor_comision = $producto->oferta_comision_ovl;
										$input_attr = '';
										if($producto->oferta_comision_ovl == -1 || $producto->oferta_comision_ovl == ''){
											$valor_comision = $producto->tienda->comision_ovl;
											$input_attr = 'disabled="disabled"';
										}
									?>
									<input rel="tooltip" title="Para cambiar click en editar, caso contrario se usar� el valor de la tienda" id="oferta_comision_ovl" type="text" <?php echo $input_attr; ?> name="oferta_comision_ovl" class="input-small" value="<?php echo $valor_comision; ?>" /> <button class="btn btn-mini activar_descuento" type="button"><span class="icon-edit"></span> Editar</button>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">PVP:</td><td><input type="text" name="oferta_precio_venta" id="oferta_precio_venta" class="input-small" value="<?php echo $producto->oferta_precio_venta; ?>" /> �</td>
							</tr>
							<tr>
								<td align="right" valign="top">Descuento OVL:</td><td><input type="text" name="oferta_descuento" id="oferta_descuento" class="input-small" value="<?php echo $producto->oferta_descuento; ?>" /> %</td>
							</tr>
							<tr>
								<td align="right" valign="top">Precio OVL:</td>
                                <td><span class="badge badge-warning oferta_precio_ovl"><?php echo $producto->get_oferta_precio_final(); ?>�</span></td>
							</tr>
							<tr>
								<td align="right" valign="top">Ahorro:</td>
                                <td><span class="badge badge-warning oferta_ahorro"><?php echo $producto->get_oferta_ahorro(); ?>�</span></td>
							</tr>
						</table>


						<h3 class="label_separador">Informaci&oacute;n del tiempo y periodo de validez</h3>
						<table width="100%" cellpadding="5">
							<tr>
								<td align="right" valign="middle" width="25%">Tiempo de la oferta:</td>
								<td>
									<div class="row-fluid">
										<div class="span6">
											Fecha Inicio
											<br/>

												<input type="text" class="t" name="oferta_publicacion" id="oferta_publicacion"
													value="<?php echo ($producto->oferta_publicacion) ?
															date('d/m/Y', strtotime($producto->oferta_publicacion)) :
															date('d/m/Y');
?>" />

										</div>
										<div class="span6">
											Fecha Finalizaci&oacute;n
											<br/>
											<div id="div_duracion">
												<input type="text" class="t" name="oferta_duracion" id="oferta_duracion"
													value="<?php echo ($producto->oferta_duracion) ?
															date('d/m/Y', strtotime($producto->oferta_duracion)) :
															date('d/m/Y');
?>" />
											</div>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">Periodo de validez:</td>
								<td>
									<div class="row-fluid">
										<div class="span6">
											<?php if($linea == 'permanentes') {?>
												<label class="checkbox">
													<input type="checkbox" class="inline" />
														Fecha limite consumo
													</label>
											<?php } ?>
											<div id="div_validez">
												<input type="text" class="t" name="tiempo_validez" id="tiempo_validez"
													value="<?php echo ($producto->tiempo_validez) ?
															date('d/m/Y', strtotime($producto->tiempo_validez)) :
															date('d/m/Y');
?>" />
											</div>
										</div>
										<?php if($linea == 'permanentes') {?>
											<div class="span6">
												<label class="checkbox"><input type="checkbox" class="checkbox inline" /> Tiempo para consumir</label>
												<input type="text" class="t" name="tiempo_consumir" id="tiempo_consumir"
													value="<?php echo date('d/m/Y', strtotime($producto->tiempo_validez)); ?>" />
											</div>
										<?php } ?>
									</div>
								</td>
							</tr>

						</table>
                        <hr>
                        <p style="text-align: right">
                        	<?php if($checkbox=='true'){?>
	                            <input type="checkbox" name="estado" value="2" <?php echo ($producto->status==2) ? 'checked="checked"' : ''; ?> />
	                            Guardar como borrador
                            <?php }else{ ?>
                            	<input type="hidden" name="estado" value="2" />
                            <?php }?>
                        </p>
						<?php if($producto->id == ''){ ?>
							<p style="text-align: right;">
								<button type="submit" class="btn btn-danger">
									Agregar Cupon
								</button>
							</p>
						<?php }else{ ?>
                            <p style="text-align: right;"><button type="submit" class="btn btn-danger">Guardar cambios</button> </p>
                            <p>Previsualizar: <strong> <a href="<?php echo URL::base('http').'preview/index/'.$linea.'/'.$producto->id; ?>" target="_blank"><?php echo URL::base('http').'preview/index/'.$linea.'/'.$producto->id; ?></a> </strong> </p>
                            <p>URL: <strong> <a href="<?php echo URL::base('http').$linea.'/producto/'.$producto->id; ?>" target="_blank"><?php echo URL::base('http').$linea.'/producto/'.$producto->id; ?></a> </strong> </p>
						<?php } ?>
					</form>
				<!-- End Content -->
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var publicacion_date=new Date();
	var duracion_date=new Date();
	var meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

    $(document).ready(initPage);
    var _param = '<?php echo Kohana_Request::$current->param('param1') ?>';
    var _linea = '<?php echo $linea ?>';

	var rules = {
		'comercio_nombre' : {required:true, minlength:2},
		'comercio_direccion' : {required:true, minlength:2},
		'comercio_descripcion' : {required:true, minlength:2},
		'tienda_id': 'required',
		'oferta_titulo': {required:true, minlength:2},
		'oferta_destacamos': {required:true, minlength:2},
		'oferta_que_incluye': {required:true, minlength:2},
		'oferta_descripcion': {required:true, minlength:2},
		'oferta_condiciones': {required:true, minlength:2},
		'oferta_ubicacion': 'required',
		'oferta_imagen_id': 'required',
		'oferta_comision_ovl': {number:true, max:100},
		'oferta_precio_venta': {number:true, required:true},
		'oferta_descuento': {number:true, required:true, max:100},
		'oferta_duracion': 'required',
		'tiempo_validez': 'required'
	};
	/**
	 *
	 * @access public
	 * @return void
	 **/
	function publicacion_datepicker(){
		//console.log("hola mundo");
		var val=$('#oferta_publicacion').val();
		if(val==null || val==''){
			val=(new Date()).valueOf();
		}
		$('#oferta_publicacion').datepicker(
			{
				dateFormat:'dd/mm/yy',
				monthNames:meses,
				minDate:0,
				current:(new Date()).valueOf(),
				format: 'd/m/Y',
				onRender:function(date){
					publicacion_date=date;
					var now=new Date();
					return  {
								disabled: (date.valueOf()<now.valueOf())
							}
				}
			}
		);
		$('#oferta_publicacion').val(val);

		$('#oferta_publicacion').change(
			function(){
				publicacion_date=$('#oferta_publicacion').val();
				publicacion_date = new Date(publicacion_date.replace(/(\d{2})\/(\d{2})\/(\d{4})/,'$3-$2-$1'));
				return;
			}
		);
	}

	/**
	 *
	 * @access public
	 * @return void
	 **/
	function duracion_datepicker(){
		var val=$('#oferta_duracion').val();
		if(val==null || val==''){
			val=(publicacion_date).valueOf();
		}
		//$('#div_duracion').html( $('#div_duracion').data('html') );
		$('#oferta_duracion').datepicker(
			{
				dateFormat:'dd/mm/yy',
				monthNames:meses,
				minDate:0,
				format: 'd/m/Y',
				onRender:function(date){
					duracion_date=date;
					return  {
								disabled: (date.valueOf()<publicacion_date.valueOf())
							}
				}
			}
		);
		$('#oferta_duracion').val(val);
		$('#oferta_duracion').change(
			function(){
				duracion_date=$('#oferta_duracion').val();
				duracion_date = new Date(duracion_date.replace(/(\d{2})\/(\d{2})\/(\d{4})/,'$3-$2-$1'));
				if((duracion_date.valueOf())<=(publicacion_date.valueOf())){
					alert("Fecha incorrecta, ingrese otra fecha de duracion");
				}
				return;
			}
		);
	}

	/**
	 *
	 * @access public
	 * @return void
	 **/
	function validez_datepicker(){
		var val=$('#tiempo_validez').val();
		if(val==null || val==''){
			val=(duracion_date).valueOf();
		}
		//$('#div_validez').html($('#div_validez').data('html'));
		$('#tiempo_validez').datepicker(
			{
				dateFormat:'dd/mm/yy',
				minDate:0,
				monthNames:meses,
				format: 'd/m/Y',
				onRender:function(date){
					return  {
								disabled: (date.valueOf()<duracion_date.valueOf())
							}
				}
			}
		);
		$('#tiempo_validez').val(val);
		$('#tiempo_validez').change(
			function(){
				validez_date=$('#tiempo_validez').val();
				validez_date = new Date(validez_date.replace(/(\d{2})\/(\d{2})\/(\d{4})/,'$3-$2-$1'));
				if((validez_date.valueOf())<=(duracion_date.valueOf())){
					alert("Fecha incorrecta, ingrese otra fecha de validez");
				}
				return;
			}
		);
	}

	function initPage()
	{
		$('.open_galery').live('click', openGalery);
		$('.add_image').click(addImage);


		/*datepickers a validar*/

		publicacion_datepicker();
		/*$('#oferta_duracion').prop('disabled','true');
		$('#tiempo_validez').prop('disabled','true');*/

		//$('#div_duracion').data('html',$('#div_duracion').html());
		duracion_datepicker();
		//$('#div_validez').data('html',$('#div_validez').html());
		validez_datepicker();

		/*fin validacion*/


		$('.activar_descuento').click(activarComisionOVL);

		$('#form_registro_producto').validate({rules: rules,invalidHandler:null,submitHandler: onSubmitForm	});

		//CKEDITOR.replace( 'comercio_descripcion');
		CKEDITOR.replace( 'oferta_destacamos');
		CKEDITOR.replace( 'oferta_que_incluye');
		CKEDITOR.replace( 'oferta_descripcion');
		CKEDITOR.replace( 'oferta_condiciones');
		//CKEDITOR.replace( 'comercio_direccion');

		$('#oferta_descuento').keyup(updateMontos);
		$('#oferta_precio_venta').keyup(updateMontos);

        $('.radio_oferta').click(checkOferta);
        $('#ciudad').change(changeCiudad);

		$('.oferta_nacional').click(CLICK_UBICACION_handler);
		$('.oferta_ciudad').click(CLICK_UBICACION_handler);

		$('.ubicacion').change(CHANGE_SELECT_handler);

	}

	function CHANGE_SELECT_handler()
	{
		updateListUbicacion($(this).val(), '#' + $(this).attr('target'));
	}

	function addImage()
	{
		var html = '<div>';
		html += '<div class="thumbnail span4"></div>';
		html += '<input type="hidden" name="imagen_externa[]" value="">';
		html += '<a href="javascript:void(0);" class="btn btn-mini open_galery"><span class="icon-camera"></span> Seleccionar imagen</a> <a href="#" class="">&times;</a>';
		html += '</div>';

		$('#lista_externas').append(html);
	}

	function CLICK_UBICACION_handler()
	{
		var target = $(this);

		switch (target.attr('class'))
		{
			case 'oferta_nacional':
				$('#oferta_ciudad').attr('disabled', 'disabled');
				break;
			case 'oferta_ciudad':
				$('#oferta_ciudad').removeAttr('disabled');
				break;
		}
	}

	function updateMontos()
	{
		if($('#oferta_descuento').val() != '' && $('#oferta_precio_venta').val() != '')
		{
			var ahorroOVL = ($('#oferta_precio_venta').val() / 100 ) * $('#oferta_descuento').val();
			var precioFinal = Math.round($('#oferta_precio_venta').val() - ahorroOVL);
			var precioFinal2 = $('#oferta_precio_venta').val() - ahorroOVL;

            $('.oferta_ahorro').text(($('#oferta_precio_venta').val() - precioFinal) + '�');
            $('.oferta_precio_ovl').text(precioFinal.toString() + '�');

            /*
            precioFinal3 = Math.round(precioFinal2 * 10) / 10;
            var _ahorro = $('#oferta_precio_venta').val() - precioFinal3;
            $('.oferta_ahorro').text((Math.round(_ahorro * 10) / 10).toFixed(2) + '�');
			$('.oferta_precio_ovl').text(precioFinal3.toFixed(2).toString() + '�');*/
		}
	}

	function onSubmitForm(form)
	{
		form.submit();
	}

	function activarComisionOVL()
	{
		if($('#oferta_comision_ovl').attr('disabled') == 'disabled'){
			$('#oferta_comision_ovl').removeAttr('disabled');
			$('#oferta_comision_ovl').focus();
			$(this).html('<span class="icon-minus"></span> Desactivar');
		}else{
			$('#oferta_comision_ovl').attr('disabled', 'disabled');
			$(this).html('<span class="icon-edit"></span> Editar');
		}
	}

	function openGalery()
	{
		var target = $(this);
		$.colorbox({href:"<?php echo URL::site('qdmedia/home/snippet'); ?>", width:740, height:590, onComplete:function(){
			qdmedia_onSelect = function(_data, _html){
                //alert(_data);
				$.colorbox.close();
                target.parent().find('input').val(_data);
				target.parent().find('.thumbnail').html('<img src="' + _html.attr('src') + '"/>');
			};
		}});
		return false;
	}

	function toogleContent(_id)
	{
		if($(_id).attr('display') == 'none')
			$(_id).slideDown();
		else
			$(_id).slideUp();
	}

    function checkOferta(){

        if($(this).val() == 0){
            $('#sel_ciudad').show();
        }else{
            $('#sel_ciudad').hide();
        }
    }

	function updateListUbicacion($_id, $_target)
	{
        if(_linea == 'permanentes'){
        //if(_param == 'permanentes'){
            _url = 'api/ubicaciones/get/';
        }else{
            _url = 'api/ubicaciones/getciudades/';
        }
		qd.core.LoadData.load(_url + $_id, null, function(_data){
			if(_data.data.length > 0)	$($_target).html('<option value="-1">Seleccionar una opci&oacute;n</option>');
			else						$($_target).html('<option value="-1">No hay resultados</option>');
			for(i in _data.data)
			{
				var it = _data.data[i];
				$($_target).append('<option value="' +  it.id + '">' + it.nombre + '</option>');
			}
		})
	}

    function changeCiudad(){
        //alert($(this).val());
    }

</script>