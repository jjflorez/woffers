<div class="eddy-aux">
        <div class="eddy-cont" >
            <div class="eddy-cols-x">
                <div class="eddy-col-1">
                    <div class="aviso-x bord-1">
                        <p class="ttl">AVISOS<img class="puntero" src="<?php echo URL::base();?>public/img/comercio/puntero.png"></p>
                        <p>Nuevo material de promoción para este semestre: “landing OVL”</p>
                        <p>Nuevo material de promoción para este semestre: “landing OVL”</p>
                    </div>
                </div>
                <div class="eddy-col-2">
                    <div class="bord-1 row-feat-x">
                        <h3 class="ttl-x">Datos del comercio</h3>
                        <div class="eddy-box-1">

                            <p>A través de tu Oficina Virtual de Comercio puedes administrar tu cuenta de One Vision Life. Puedes modificar tus datos de perfil, cambiar tu contraseña de acceso, llevar el control de tus ofertas y de tus ventas...</p>
                            <p>Como Comercio, también tienes acceso a todas las ventajas que tienen el resto de usuarios de One Vision Life. Puedes comprar en nuestras tiendas, ganar por recomendar One Vision Life a tus amigos, participar en sorteos...</p>
                            <p>Para gestionar tu cuenta de Usuario, ver tus compras, tus datos de envío... tan solo tienes que hacer click en el botón del menú “Of. Virtual Usuario”.</p>
                        </div>
                    </div>
                    <div class="eddy-cols row-feat-x">
                        <div class="eddy-col-6 bord-1">
                            <h3 class="ttl-x feat-ttl">Información de mi cuenta</h3>
                            <div class="eddy-box-1">
                                <p><strong>Eres Socio Oro | Rango Máster</strong></p>
                                <p><strong>URL Promoción:</strong> http://onevisionlife.com/ovl_matrix1<br><strong>Código Agencia Virtual de Viajes:</strong> 5avo2324</p>
                            </div>
                        </div>
                        <div class="eddy-col-6 bord-1">
                            <h3 class="ttl-x feat-ttl">Información de mi sponsor</h3>
                            <div class="eddy-box-1">
                                <p><strong>Eres Socio Oro | Rango Máster</strong></p>
                                <p>admin One Vision Life<br>gersonm17@gmail.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>