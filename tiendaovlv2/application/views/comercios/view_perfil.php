<div class="eddy-aux">
        <div class="eddy-cont" >
            <div class="eddy-cols-x">
            <div class="eddy-col-1">
                <div class="links_nav">
                    <a  href="javascript:void(0);" class="first active"><span class="sig">></span> Datos del comercio</a>
                    <a  href="<?php echo URL::site('comercios/datos_pago'); ?>">Datos de pago</a>
                    <a  href="<?php echo URL::site('comercios/new_pass'); ?>">Cambiar contraseña</a>
                </div>
            </div>
            <div class="eddy-col-2 ">
                <div class="bord-1">
                    <h3 class="ttl-x">Datos del comercio</h3>
                <div class="eddy-box-1">
                    <h3 class="ttl-y">DATOS DE FACTURACIÓN:</h3>
                    <p>El sexo, nombre, apellidos, DNI y país no se pueden modificar. Si quieres cambiar alguno de estos datos, envíanos un email a <a href="javascript:void(0);">soporte@onevisionlife.com</a></p>
                    <div class="eddy-tbl-1 feat-tbl-x">
                        <div class="eddy-cols row-feat-x">
                            <div class="eddy-col-4">Sexo<span class="sig">*</span><br>Nombre<span class="sig">*</span><br>1r Apellido<span class="sig">*</span><br>2o Apellido<span class="sig">*</span><br>DNI<span class="sig">*</span><br>Fecha nacimiento<span class="sig">*</span><br></div>
                            <div class="eddy-col-3">H<br>Juan<br>Péres<br>2o Apellido<br>DNI<br>
                                <div class="fech"><input type="text"> / <input type="text"> / 19<input type="text"></div>
                            </div>
                            <div class="eddy-col-4"><br>Dirección<span class="sig">*</span><br>Código Postal<span class="sig">*</span><br>Ciudad<span class="sig">*</span><br>Provincia<span class="sig">*</span><br>País<span class="sig">*</span><br></div>
                            <div class="eddy-col-3">
                                <div class="alert-x eddy-alig-r">(<span class="sig">*</span>campos obligatorios)</div>
                                <input type="text"><br><input type="text"><br><input type="text"><br>
                                <select name="IDPAISSOCIEDAD" id="IDPAISSOCIEDAD" class="valid">
                                    <option value="243" iva="0"> España - Canarias</option><option value="244" iva="0"> España - Ceuta</option><option value="245" iva="0"> España - Melilla</option><option value="209" iva="0">Yemen</option><option value="207" iva="0">Zambia</option><option value="206" iva="0">Zimbabue</option></select><br>
                                Pais<br>
                            </div>
                        </div>
                        <div class="eddy-alig-r">
                            <a class="eddy-btn-x">Guardar cambios</a>
                        </div>
                    </div>
                    <h3 class="ttl-y">DATOS GENERALES:</h3>
                    <p>El país no se pueden modificar. Si quieres cambiar este dato, envíanos un email a <a href="javascript:void(0);">soporte@onevisionlife.com</a></p>
                    <div class="ttl-z row-feat-x">Datos del comercio:<div class="alert-x feat-posc-x">(<span class="sig">*</span>campos obligatorios)</div></div>
                    <div class="eddy-tbl-1 feat-tbl-x">
                        <div class="eddy-cols row-feat-x">
                            <div class="eddy-col-4"><div class="doble-ln">Nombre<span class="sig">*</span> comercial</div>Teléfono<span class="sig">*</span><br>Web<span class="sig">*</span><br>Dirección<span class="sig">*</span><br></div>
                            <div class="eddy-col-3"><input type="text"><br><div class="telf"><input type="text"><input type="text"></div><input type="text"><br><input type="text"></div>
                            <div class="eddy-col-4">Código Postal<span class="sig">*</span><br>Ciudad<span class="sig">*</span><br>Provincia<span class="sig">*</span><br>País<span class="sig">*</span></div>
                            <div class="eddy-col-3">
                                <input type="text"><br><input type="text"><br>
                                <select name="IDPAISSOCIEDAD" id="IDPAISSOCIEDAD" class="valid">
                                    <option value="243" iva="0"> España - Canarias</option><option value="244" iva="0"> España - Ceuta</option><option value="245" iva="0"> España - Melilla</option><option value="209" iva="0">Yemen</option><option value="207" iva="0">Zambia</option><option value="206" iva="0">Zimbabue</option></select><br>
                                Pais<br>
                            </div>
                        </div>
                    </div>
                    <div class="ttl-z row-feat-x">Persona de contacto:<div class="alert-x feat-posc-x">(<span class="sig">*</span>campos obligatorios)</div></div>
                    <div class="eddy-tbl-1 feat-tbl-x ">
                        <div class="eddy-cols row-feat-x">
                            <div class="eddy-col-4">Nombre<span class="sig">*</span><br>1r Apellido<span class="sig">*</span><br>2o Apellido<span class="sig">*</span><br></div>
                            <div class="eddy-col-3"><input type="text"><br><input type="text"><br><input type="text"></div>
                            <div class="eddy-col-4">Email<span class="sig">*</span><br>Repetir Email<span class="sig">*</span><br>Teléfono<span class="sig">*</span><br></div>
                            <div class="eddy-col-3"><input type="text"><br><input type="text"><br><input type="text"></div>
                        </div>
                        <div class="eddy-alig-r">
                            <a class="eddy-btn-x">Guardar cambios</a>
                        </div>
                    </div>

                    <h3 class="ttl-y">DATOS PÚBLICOS:</h3>
                    <p>En la web personal de promoción y en el momento en que alguien se registra a través de tu link, se muestra de forma predeterminada tu nombre comercial, Email y país. Si deseas modificar alguno de estos datos, escoge entre las siguientes opciones:</p>
                    <div class="eddy-tbl-1 feat-tbl-y">
                        <div class="eddy-cols row-feat-x">
                            <div class="eddy-col-1"><input type="radio" name="radio_1" value="si"> Si <input type="radio" checked name="radio_1" value="no"> No<br><input type="radio" name="radio_2" value="si"> Si <input type="radio" checked name="radio_2" value="no"> No</div>
                            <div class="eddy-col-0">Ocultar Email<br>Ocultar nombre comercial y mostrar mi nombre de usuario:<span class="user">matrix1</span></div>
                        </div>
                        <div class="eddy-alig-r">
                            <a class="eddy-btn-x">Guardar cambios</a>
                        </div>
                    </div>

                </div>
            </div>
            </div>
        </div>
        </div>
</div>