<div id="c_admin">
	<div class="container">
		<div id="eddy-nav-x">
            <ul>
                <!--<li class="active">
                    <a href="<?php echo URL::site('admin/cupones/'); ?>">
                        Ofertas del dia
                    </a>
                </li>-->
                <li><a href="<?php echo URL::site('comercios'); ?>">Inicio</a></li>
                <li><a href="<?php echo URL::site('comercios/perfil'); ?>">Perfil</a></li>
                <li><a  href="javascript:void(0);">Ventas</a></li>
                <li><a  href="javascript:void(0);">Crear Oferta</a></li>
                <li><a  href="javascript:void(0);">Herramientas</a></li>
                <li><a  href="javascript:void(0);">Soporte</a></li>
                <li><a class="active" href="<?php echo URL::site('comercios/cupones'); ?>">listacupones</a></li>
            </ul>
        </div>
		<div class="row-fluid">
			<?php /* ?>
			<div class="span3">
				<?php echo View::factory('admin/view_admin_menu'); ?>
			</div>
			<?php /**/ ?>
			<div class="span12">
				<div class="content" >
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="<?php echo URL::site('comercios/cupones/'); ?>">
								Ofertas del dia
							</a>
						</li>
						<?php /* ?>
						   <li>
						   <a href="<?php echo URL::site('admin/cupones/permanentes'); ?>">
						   Ofertas permanentes
						   </a>
						   </li>
						   <?php */?>
					</ul>

					<h4 style="height: ">Ofertas del dia</h4>

					<h1>
						Lista Codigos Cup&oacute;n
					</h1>

					<p>
						En One Vision Life todos los cupones tienen asociados  un N&uacute;mero de referencia,
						Un C&oacute;digo de Cup&oacute;n y un C&oacute;digo de Validaci&oacute;n.
					</p>
					<ul>
						<li> El N&uacute;mero de Referencia es un c&oacute;digo que nos indica de qu&eacute; oferta se trata.
						<li> El C&oacute;digo de Cup&oacute;n es el numero de identificaci&oacute;n de cada cup&oacute;n vendido.
						<li> El c&oacute;digo de validaci&oacute;n es el n&uacute;mero asociado a cada c&oacute;digo de cup&oacute;n necesario para poder validarlo.
					</ul>
					<p>
						En el momento en que un usuario canjea un cup&oacute;n, el comercio debe mandar un email a
						<a href="mailto:soporte@onevisionlife.com">soporte@onevisionlife.com</a> indicando el
						c&oacute;digo der cup&oacute;n y el c&oacute;digo de Validaci&oacute;n correspondiente
						para que One vision Life pueda realizar el pago de los cupones validados.
					</p>
					<div >
						<strong>
							Oferta del dia
						</strong>
						<p>
							<strong>
								50% de Dto. ? Date un homenaje sin salir de casa! Paella + botella de vino take away.
							</strong>

						</p>
						<p>
							<div class="row">
								<div class="span6">
									<strong>
										N&uacute;mero de referencia:
									</strong>
									74674626576
								</div>
								<div class="span6">
									<strong>
										Periodo de Validez:
									</strong>
									hasta 23/7/2013
								</div>
							</div>
						</p>
						<center>
							<strong>
								Lista de Cupones Canjeados
							</strong>
						</center>
						<table class="table table-striped table-bordered table-hover">
							<tr>
								<th>

								</th>
								<th>
									Nombre
								</th>
								<th>
									C&oacute;digo de Cup&oacute;n
								</th>
								<th>
									C&oacute;digo de Validaci&oacute;n
								</th>
							</tr>
                            <?php
								foreach($cupones as $i=>$K){

							?>
                                <tr>
                                    <td>
                                        <?php echo $cupones[$i]['numero']; ?>
                                    </td>
                                    <td>
                                        <?php echo $cupones[$i]['nombre']; ?>
                                    </td>
                                    <td>
                                        <?php echo $cupones[$i]['codigo_cupon']; ?>
                                    </td>
                                    <td>
                                        <?php if($cupones[$i]['status']==10){ ?>
                                            <?php echo $cupones[$i]['codigo_validacion']; ?> <strong>Validado</strong>
                                        <?php }else{ ?>
                                            <form  action="<?php echo URL::base("http"); ?>comercios/cupones/validar/" method="post">
                                                <input type="hidden" name="id" value="<?php echo $cupones[$i]['id']; ?>">
                                                <input type="text" name="codigo_validacion">
                                                <button class="btn"  id="btn_validador_<?php echo $cupones[$i]['id']; ?>"> >> </button>
                                            </form>
                                        <?php } ?>

                                    </td>
                                </tr>
                            <?php

								}
							?>

						</table>
						<center>
                            <form  action="<?php echo URL::base("http"); ?>admin/descargas/cupones" method="post">
                                <button class="btn">
                                    Descargar Lista de Cupones
                                </button>
                            </form>

						</center>



				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(init);
    function init(){}
</script>