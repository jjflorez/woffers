<div id="c_comercio">

	<div class="eddy-aux">
        <div id="eddy-nav-x">
            <ul>
                <li><a href="<?php echo URL::site('comercios'); ?>">Inicio</a></li>
                <li><a class="active" href="<?php echo URL::site('comercios/perfil'); ?>">Perfil</a></li>
                <li><a  href="javascript:void(0);">Ventas</a></li>
                <li><a  href="javascript:void(0);">Crear Oferta</a></li>
                <li><a  href="javascript:void(0);">Herramientas</a></li>
                <li><a  href="javascript:void(0);">Soporte</a></li>
                <li><a  href="<?php echo URL::site('comercios/cupones'); ?>">listacupones</a></li>
            </ul>
        </div>
        <div class="eddy-cont eddy-box-1" >
            <div class="eddy-cols">
            <div class="eddy-col-1">
                <div class="links_nav">
                    <a class="active first" href="javascript:void(0);"><span class="sig">></span>Ofertas del día</a>
                    <div class="sub">
                        <a  href="javascript:void(0);">Campañas activas</a>
                        <a class="active" href="javascript:void(0);">Campañas en proceso</a>
                    </div>
                    <a href="javascript:void(0);">Productos</a>
                    <a href="javascript:void(0);">Ofertas permanentes</a>
                    <a href="javascript:void(0);">Histórico de campañas</a>
                </div>
            </div>
            <div class="eddy-col-2">
            <div class="bord-1">
                <h3 class="ttl-x">Ofertas del día | Campañas en proceso</h3>
                <div class="eddy-padd-1 row-feat-x">
                    <p>En Campañas en proceso puedes ver las <strong>Ofertas del día</strong> que ya se han publicado en One Vision Life, así como el periodo de validez en el que los usuarios que han comprado cupones tienen tiempo para acudir a tu comercio a canjearlos.</p>
                    <p>En el botón de <strong>“Ver códigos de cupón”</strong>, ves la lista con los códigos de cupón vendidos y puedes validar los cupones que ya hayan sido canjeados.</p>
                    <p>Una vez que haya finalizado el periodo de validez de la Campaña, la Oferta se archivará en la pestaña de Histórico de campañas.</p>
                    <div class="eddy-panel-y row-feat-x">
                        <div class="eddy-cols">
                            <div class="eddy-col-8"><img src="<?php echo URL::base() ?>public/img/comercio/img0.jpg"/></div>
                            <div class="eddy-col-0">
                                <div class="ttl">
                                <p>OFERTA DEL DÍA<br>50% Dto. ¡Date un homenaje sin salir de casa! Paella + botella de vino take away</p>
                                <div class="prec">Precio: 12€</div>
                                </div>
                                <div class="eddy-padd-2 fech">
                                    Fecha de publicación: 24/05/2012<br>
                                    Fecha de publicación: 24/05/2012
                                    <div class="eddy-padd-2">Fecha de finalización: 27/05/2012</div>
                                </div>
                            </div>
                        </div>
                        <div class="ttl2 eddy-padd-1">
                            <span class="fisrt">Número de Referencia:</span> Cupones vendidos:
                        </div>
                        <div class="cont">
                            <div class="eddy-cols">
                                <div class="eddy-col-1">
                                    546789765
                                </div>
                                <div class="eddy-col-0">
                                    121
                                </div>
                            </div>
                            <a class="eddy-btn-x feat-posc-x">Ver códigos de cupón</a>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        </div>
</div>