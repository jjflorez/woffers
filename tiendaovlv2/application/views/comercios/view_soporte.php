

<div class="eddy-aux">
        <div class="eddy-cont eddy-box-1" >
            <div class="eddy-cols">
            <div class="eddy-col-1">
                <div class="links_nav">
                    <a class="active" href="javascript:void(0);"><span class="sig">></span>Formulario</a>
                    <a href="javascript:void(0);">FAQ’s Oficina Virtual Comercio</a>
                </div>
            </div>
            <div class="eddy-col-2">
            <div class="bord-1">
                <h3 class="ttl-x">Cambiar contraseña</h3>
                <div class="eddy-box-1 row-feat-x">
                    <p>¿Tienes alguna duda sobre tu Oficina Virtual de Comercio? Consulta nuestras <a href="javascript:void(0);">FAQ’s</a> y sinó resolvemos tus dudas, rellena el formulario de contacto y nos pondremos en contacto contigo lo antes posible.</p><br>
                    <div class="eddy-tbl-1 feat-tbl-w">
                        <div class="eddy-cols">
                            <div class="eddy-col-7">Nombre y apellidos<span class="sig">*</span><span class="sig"></span><br><input type="text"></div>
                            <div class="eddy-col-7">Email<span class="sig">*</span><br><input type="text"></div>
                            <div class="eddy-col-7">Telefono<br><input type="text"></div>
                        </div>
                        <br>
                        <p>
                            Mensaje<span class="sig">*</span>
                            <textarea cols="50" rows="5"></textarea>
                        </p>
                        <p class="cond_general">
                            <input type="checkbox" name="Checkbox">Acepto las <a href="javascript:void(0);">condiciones generales</a> y la <a href="javascript:void(0);">política de privacidad</a>
                        </p>
                        <a class="eddy-btn-x btn-feat-x">Enviar</a>
                    </div>
                </div>
            </div>
            </div>
        </div>
        </div>
</div>