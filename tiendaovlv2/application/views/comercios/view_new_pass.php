<div class="eddy-cont eddy-box-1" >
    <div class="eddy-cols">
    <div class="eddy-col-1">
        <div class="links_nav">
            <a class="first"  href="<?php echo URL::site('comercios/perfil'); ?>">Datos del comercio</a>
            <a  href="<?php echo URL::site('comercios/datos_pago'); ?>">Datos de pago</a>
            <a class="active" href="javascript:void(0);"><span class="sig">></span>Cambiar contraseña</a>
        </div>
    </div>
    <div class="eddy-col-2">
    <div class="bord-1">
        <h3 class="ttl-x">Cambiar contraseña</h3>
        <div class="eddy-box-1 row-feat-x">
            <p>Ingresa tu actual y nueva contraseña.</p>
            <div class="eddy-tbl-1 feat-tbl-x">
                <div class="eddy-cols">
                    <div class="eddy-col-0">Contraseña actual<span class="sig">*</span><br>Contraseña nueva<span class="sig">*</span><br>Repetir contraseña nueva<span class="sig">*</span><br></div>
                    <div class="eddy-col-3"><input type="text"><br><input type="text"><br><input type="text"> </div>
                </div>
                    <a class="eddy-btn-x btn-feat-x">Guardar cambios</a>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
