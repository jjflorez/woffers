<?php
/**
 * div class = pagination
 *	span .previous .disabled
 *	span .current
 *	a
 *	a .next
 */
?>

<div class="btn-toolbar">
	<div class="btn-group">
		<?php if ($first_page !== FALSE): ?>
		<a class="btn" href="<?php echo HTML::chars($page->url($first_page)) ?>" rel="first"><?php echo __('first') ?></a>
		<?php else: ?>
		<a class="disabled btn"><?php echo __('first') ?></a>
		<?php endif ?>

		<?php if ($previous_page !== FALSE): ?>
		<a class="previous btn" href="<?php echo HTML::chars($page->url($previous_page)) ?>"
		   rel="prev"><span class="icon-chevron-left"></span></a>
		<?php else: ?>
		<a class="previous btn disabled"><span class="icon-chevron-left"></span></a>
		<?php endif ?>

		<?php for ($i = 1; $i <= $total_pages; $i++): ?>

		<?php if ($i == $current_page): ?>
			<span class="current btn btn-primary"><?php echo $i ?></span>
			<?php else: ?>
			<a class="btn" href="<?php echo HTML::chars($page->url($i)) ?>"><?php echo $i ?></a>
			<?php endif ?>

		<?php endfor ?>

		<?php if ($next_page !== FALSE): ?>
		<a class="next btn" href="<?php echo HTML::chars($page->url($next_page)) ?>" rel="next"><span class="icon-chevron-right"></span></a>
		<?php else: ?>
		<a class="next btn disabled"><span class="icon-chevron-right"></span></a>
		<?php endif ?>

		<?php if ($last_page !== FALSE): ?>
		<a class="btn" href="<?php echo HTML::chars($page->url($last_page)) ?>" rel="last"><?php echo __('last') ?></a>
		<?php else: ?>
		<a class="disabled btn"><?php echo __('last') ?></a>
		<?php endif ?>
	</div>
</div>
