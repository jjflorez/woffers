<?php
$controlador = Kohana_Request::$current->controller();
?>
<!--<div id="c_travel">
	<div id="container_lineas">
		<ul id="pestanas_lineas">
			<?php
			foreach($tiendas as $key => $t){

				if($t->code == Kohana_Request::$current->param('param1')){
                    echo '<li class="selected"><a href="'. URL::site($controlador.'/tienda/' . $t->code) .'">' . $t->name . '</a></li>';
                }else{
                    if(Kohana_Request::$current->param('param1') == "" && $key == 0)
                        echo '<li class="selected"><a href="'. URL::site($controlador.'/tienda/' . $t->code) .'">' . $t->name . '</a></li>';
                    else
                        echo '<li><a href="'. URL::site($controlador.'/tienda/' . $t->code) .'">' . $t->name . '</a></li>';
                }

                if($key == 0)
                    echo '<li><a href="'. URL::site('village/') .'">Ofertas permanentes</a></li>';
			}
			?>
		</ul>
	</div>
    -->
	<div class="container">
		<div id="colum_produc_a">
			<ul class="lista_productos_general cont_travel">
                <?php
                $_ids = array();
                $_count = 0;
                foreach($productos as $p)
                {
                    $_duracion = strtotime($p->oferta_duracion) * 1000;

                    $_ids[$_count]["p_id"] = $p->id;
                    $_ids[$_count]["p_duracion"] = $_duracion;
                    ?>
                    <li>
						<ul>
							<li>
								<span class="dto"><?php echo $p->oferta_descuento; ?>% Dto.</span>
								<span class="descripcion"><?php  echo $p->get_txtmuestra($p->oferta_titulo,100); ?></span>
							</li>
							<li class="img">
								<span></span>
                                <div>
                                    <img border="0" alt="" src="<?php echo URL::base() . $p->oferta_imagen->url_path; ?>">
                                </div>

							</li>
							<li class="caracteristicas">
								<ul>
									<li>
										<span class="ver_plan">
											<span class="prec">
												<!--<span class="prec_2"><?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
												</span>-->
												<?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
											</span>
											<a href="<?php echo URL::site('travel/producto/' . $p->id ); ?>"></a>
										</span>
										<span>Precio:</span>
										<span class="precio"><?php echo $p->oferta_precio_venta; ?>€</span>
									</li>
									<li>
										<span class="redes">
                                            <img src="<?php echo URL::base() ?>public/img/reloj.gif">
                                            <span class="rest">Tiempo restante:</span>
                                            <span class="tiempo_restante">
                                                <?php if(!$p->isExpired()){ ?>
                                                <div style="width: 140px; margin: 0 auto; padding-left: 5px">
                                                    <div id="defaultCountdown<?php echo $p->id ?>"></div>
                                                </div>
                                                <?php }else{ ?>
                                                <div class="badge badge-error">Está oferta ya no está disponible</div>
                                                <?php } ?>
                                            </span>
										</span>
									</li>
								</ul>
							</li>
						</ul>
					</li>
					<?php
                    $_count ++;
				}
				?>
			</ul>
		</div>
		<div id="colum_produc_b">
			<?php echo View::factory('ci/view_sidebar')->set('var_sidebar','')?>
		</div>
	</div>
<!--</div>-->
<?php
    echo Html::script('public/js/jquery.countdown.js');
?>

<script type="text/javascript">
    var obj = jQuery.parseJSON('<?php echo json_encode($_ids) ?>');

    $(document).ready(init);

    function init(){

        jQuery.each(obj, function(i, val) {

            var austDay = new Date();
            austDay = new Date(val.p_duracion);
            $('#defaultCountdown'+val.p_id).countdown({until: austDay, format: 'dHM'});
        });
    }
</script>