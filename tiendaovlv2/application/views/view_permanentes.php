 <?php
    $controlador = Kohana_Request::$current->controller();
?>

<style>
    .lista_productos_general.permanentes_general > li{
        height: 285px !important;
    }
    .titulo_c{
        font-family: Arial;
        font-weight: bold;
        color: #757373;
        font-size: 12px;
        margin: 8px;
    }
    #filtro{}
    #filtro td{
        padding: 6px 0;
    }
    #filtro .t_border{
        border-left: solid 1px #aeaeae;
        padding-left: 14px;
    }

</style>

    <!-- Inicio contenedor -->
    <div class="container" style="padding-bottom: 146px;">
        <div id="colum_produc_a">
            <!-- Inicio filtros -->

            <?php if($controlador !="outlet"){ ?>

            <div class="filtros_ofertas">

                    <table id="filtro" width="100%" border="0">
                        <tr>
                            <td colspan="4" class="titulo">Filtrar ofertas &nbsp;&nbsp;&nbsp;&nbsp;
                                <img src="<?php echo URL::base() ?>public/img/icons_village.jpg" alt="">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Provincia</td>
                            <td><div class="select_list" id="oferta_ubicacion_provincia"></div></td>
                            <td>Categorias</td>
                            <td>
                                <select id="categoria" >
                                    <option disabled selected>Seleccione uno</option>
                                    <?php
                                    foreach($categorias as $item):
                                        $selec0 = '';
                                        if($producto->oferta_categoria_id == $item->id)
                                            $selec0 = 'selected="selected"';
                                        ?>
                                        <option <?php echo $selec0 ?> value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Ciudad</td>
                            <td><div class="select_list" id="oferta_ubicacion_ciudad"></div></td>
                            <td>Subcategoría</td>
                            <td>
                                <div id="subcategoria">
                                    <select id="subcategorias">
                                        <option disabled selected>Seleccione uno</option>
                                        <?php
                                        foreach($subcategorias as $item):
                                            $selec0 = '';
                                            if($producto->oferta_categoria_id == $item->id)
                                                $selec0 = 'selected="selected"';
                                            ?>
                                            <option <?php echo $selec0 ?> value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>

            </div>

            <!-- Fin filtros -->

            <?php }else{ ?>
            <li>
                <h2>Filtra las ofertas:</h2>
                <a href="javascript:void(0);" class="list_op_busqueda">
                    <input id="categoria" type="hidden" >
                </a>
                Categoria

            </li>
            <li class="second">
                <a href="javascript:void(0);" class="list_op_busqueda">
                    <input id="precio" type="hidden" >
                </a>
                Precio

            </li>
            <li>
                <div class="buscador_op">
                    <input id="txt_busc" type="text">
                    <a href="javascript:void(0);"></a>
                </div>
                <a href="javascript:void(0);" class="list_op_busqueda">
                    <input id="ordenar" type="hidden" >
                </a>
                Ordenar

            </li>
            <?php }?>

            <!-- Listado de productos -->

            <?php
            if(count($productos) == 0)
                echo '<p class="alert alert-danger" style="margin:0 10px;"><span class="icon-comment"></span> No se encontraron productos con estos filtros, prueba otra combinación</p>';
            ?>
            <ul class="lista_productos_general cont_<?php echo $controlador; ?> permanentes_general">
                <?php
                foreach($productos as $p)
                {
                    ?>
                    <li>
                        <ul>
                            <li>
                                <span class="dto"><?php echo $p->oferta_descuento; ?>% Dto.</span>
                                <span class="descripcion"><?php
                                    $text_cortado=90;
                                    if($controlador=="outlet"){
                                        $text_cortado=49;
                                    }
                                    echo $p->get_txtmuestra($p->oferta_titulo,$text_cortado);?></span>


                            </li>
                            <li class="img">
                                <span></span>
                                <div>
                                    <img border="0" alt="" src="<?php echo URL::base() . $p->oferta_imagen->url_path; ?>">
                                </div>
                            </li>
                            <li class="caracteristicas">
                                <ul>
                                    <li>
										<span class="ver_plan">
											<span class="prec">
                                                <?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
											</span>
											<a href="<?php echo URL::site($controlador.'/producto/' . $p->id ); ?>"></a>
										</span>
                                        <span>Precio:</span>
                                        <br/>
                                        <span class="precio"><?php echo $p->oferta_precio_venta; ?>€</span>
                                    </li>

                                    <li>
                                        <p class="titulo_c"><?php echo $p->comercio_nombre ?></p>
                                        <!--
										<span class="redes">
                                                <?php if($controlador!="outlet"){?>
                                                <?php }?>
                                            <a class="twiter"></a>
												<a class="mail"></a>
												<a class="megusta"></a>
                                            <span id="alerta_producto_op">
                                            </span>
										</span>
										-->
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <div id="colum_produc_b">
            <?php echo View::factory('ci/view_sidebar')->set('var_sidebar','')?>
        </div>
    </div>
<!--</div>-->
<?php
$catJSON = array();
foreach($categorias as $cat) array_push($catJSON, $cat->as_array());

$provJSON = array();
foreach($provincias as $pr)	array_push($provJSON, $pr->as_array());

$ciudadJSON = array();
foreach($ciudades as $ci)	array_push($ciudadJSON, $ci->as_array());

$distritoJSON = array();
foreach($distritos as $di)	array_push($distritoJSON, $di->as_array());

$barrioJSON = array();
foreach($barrios as $ba)	array_push($barrioJSON, $ba->as_array());
?>

<script type="text/javascript">
    $('#categoria').change(function(){
        var option = $(this).find('option:selected').val();
        var url = '<?php echo('http://'.$_SERVER['SERVER_NAME'].$GLOBALS['url_to'].'/api/comercio/findsubcategoria/');?>';
        $.ajax({
            url:url+option,
            type: 'GET',
            data:'',
            dataType: 'json',
            success: function(data) {
                $('#subcategorias').html('');
                var count=0;
                if((data.data.length)==0)
                {
                    $('#subcategoria').html('');
                    $('#subcategoria').append('<select id="subcategorias"><option disabled selected>Seleccione uno</option></select>');
                }

                data.data.forEach(function( i ) {
                    if(count==0){
                        $('#subcategoria').html('');
                        $('#subcategoria').append('<select id="subcategorias" ><option disabled selected>Seleccione uno</option></select>');
                    }
                    $('#subcategorias').append('<option value="'+i.id+'">'+ i.nombre+'</option>');
                    count=count+1;
                });
            },
            error: function(e) {
            }
        });
    });


    $(document).ready(init);

    var filterValues = new Array();
    filterValues['oferta_categoria_id'] = '<?php echo isset($_GET['oferta_categoria_id'])?$_GET['oferta_categoria_id']:''; ?>';
    filterValues['oferta_ubicacion_provincia'] = '<?php echo isset($_GET['oferta_ubicacion_provincia'])?$_GET['oferta_ubicacion_provincia']:''; ?>';
    filterValues['oferta_ubicacion_ciudad'] = '<?php echo isset($_GET['oferta_ubicacion_ciudad'])?$_GET['oferta_ubicacion_ciudad']:''; ?>';
    filterValues['oferta_ubicacion_distrito'] = '<?php echo isset($_GET['oferta_ubicacion_distrito'])?$_GET['oferta_ubicacion_distrito']:''; ?>';
    filterValues['oferta_ubicacion_barrio'] = '<?php echo isset($_GET['oferta_ubicacion_barrio'])?$_GET['oferta_ubicacion_barrio']:''; ?>';


    var categoriasJSON = '<?php echo str_replace("'", "\\'", json_encode($catJSON)); ?>';
    var provinciaJSON = '<?php echo str_replace("'", "\\'", json_encode($provJSON)); ?>';
    var ciudadJSON = '<?php echo str_replace("'", "\\'", json_encode($ciudadJSON)); ?>';
    var distritoJSON = '<?php echo str_replace("'", "\\'", json_encode($distritoJSON)); ?>';
    var barrioJSON = '<?php echo str_replace("'", "\\'", json_encode($barrioJSON)); ?>';

    function init(){
        //list_op_ovl();
        initSelectListFiltros();

        setDataList('oferta_categoria_id', JSON.parse(categoriasJSON));
        setDataList('oferta_ubicacion_provincia', JSON.parse(provinciaJSON));
        setDataList('oferta_ubicacion_ciudad', JSON.parse(ciudadJSON));
        setDataList('oferta_ubicacion_distrito', JSON.parse(distritoJSON));
        setDataList('oferta_ubicacion_barrio', JSON.parse(barrioJSON));
    }

    function setDataList(_id, _data)
    {
        var target = $('#' + _id);
        target.find('.select_options').html('');
        target.find('.select_options').html('<li><a href="javascript:void(0);" id="-1">Todos</a></li>');
        for(i in _data)
        {
            target.find('.select_options').append('<li><a href="javascript:void(0);" id="' + _data[i].id + '">' + _data[i].nombre + '</a></li>');
            if(filterValues[_id] == _data[i].id)
            {
                target.find('.select_label').text(_data[i].nombre);
                target.find('input').val(_data[i].id);
            }
        }
    }

    function initSelectListFiltros()
    {

        $('.select_list').each(function(){
            var target = $(this);
            target.html('');
            target.append('<a href="javascript:void(0);" class="select_label">Selecciona</a>');
            target.append('<input type="hidden" name="' + target.attr('id') + '" value="" />');
            target.append('<ul class="select_options"><li><a href="javascript:void(0);" id="">No hay opciones</a></li></ul>');

            target.find('.select_label').click(function()
            {
                $("body").unbind('click');
                target.find('.select_options').slideDown('fast', function()
                {
                    $("body").click(eventBodySelect);

                    target.find('.select_options a').click(function()
                    {
                        target.find('.select_options').slideUp('fast');
                        $("body").unbind('click');
                        if($(this).attr('id') != '')
                        {
                            target.find('.select_label').text($(this).text());
                            target.find('input').val($(this).attr('id'));
                            if(target.attr('id') == 'oferta_ubicacion_provincia')
                            {
                                $('#oferta_ubicacion_ciudad input').val('');
                                $('#oferta_ubicacion_distrito input').val('');
                                $('#oferta_ubicacion_barrio input').val('');
                            }
                            if(target.attr('id') == 'oferta_ubicacion_ciudad')
                            {
                                $('#oferta_ubicacion_distrito input').val('');
                                $('#oferta_ubicacion_barrio input').val('');
                            }
                            SUBMIT_form();
                        }
                    });

                });

            });

        });
        /*<div class="select_list" id="oferta_categoria_id">
                <a href="#" class="select_label">dfgsdfg</a>
                <ul class="select_options">
                <li><a href="">asdfas</a> </li>
                <li><a href="">asdfas</a> </li>
                </ul>
        </div>*/
    }

    function SUBMIT_form()
    {
        $('#form_filtros').submit();
    }

    function eventBodySelect()
    {
        $('.select_options').slideUp('fast');
        $("body").unbind('click');
    }

</script>