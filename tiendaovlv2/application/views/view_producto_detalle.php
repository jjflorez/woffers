<?php
	$controlador = Kohana_Request::$current->controller();
	$current_line = Kohana_Request::current()->controller();

   // echo $producto->tienda->code;
    //echo "<pre>"; print_r($producto->as_array()); die();
?>
<style type="text/css">
    .titulo_com{
        color: #680034;
        border-bottom: solid 1px;
        margin: 10px;
    }
	hr{
		margin:0;
		border: 0;
		border-top: 2px solid #680034;
	}
</style>
<div id="c_<?php echo $controlador; ?>">

    <div class="container" style="padding-bottom: 146px;">
	<div id="colum_produc_a">


        <div id="detalles_productos_ovl">
			<!--contentido-->
            <div class="producto <?php echo $current_line; ?>">
                <span class="descripcion">
                    <span><?php echo $producto->oferta_descuento; ?>% Dto.</span>
                    <?php echo $producto->oferta_titulo; ?>
                </span>
                <?php
                    $file = $producto->productofile->find();
                    $_interna = $file->file->url_path;
                ?>
                <ul>
					<li id="img_det_pro">
                        <img style="width: 440px;" src="<?php echo URL::base() . $_interna; ?>" alt="<?php echo $producto->oferta_titulo; ?>">
                    </li>
					<li>
						<ul>
							<li>
								<span class="precio">
									<!--<span class="precio2"><?php echo $producto->get_oferta_precio_final(); ?><span class="signo">€</span></span>-->
									<?php echo $producto->get_oferta_precio_final(); ?><span class="signo">€</span>
								</span>
								<a class="btn_comprar" id="<?php echo $producto->id; ?>" href="<?php echo URL::site('carrito/add_producto/'. $producto->id ); ?>"></a>
							</li>
							<li class="mostrar_precio">
								<span>Precio <span><?php echo $producto->oferta_precio_venta; ?>€</span></span>
								<span class="n2">Dto <span><?php echo $producto->oferta_descuento; ?>%</span></span>
								<span>Ahorro <span><?php echo $producto->get_oferta_ahorro(); ?>€</span></span>
							</li>

							<!-- Detalle de fecha -->
							<?php if($current_line != 'village'){ ?>
							<li id="tiempo_detalle">
								<?php if(!$producto->isExpired()){ ?>
									<div id="defaultCountdown"></div>
								<?php }else{ ?>
									<div class="badge badge-error">Está oferta ya no está disponible</div>
								<?php } ?>
							</li>
							<?php } ?>

							<li id="regalo_content">
								<div class="opciones" style="display: none;">
								<?php
								if($current_line == "outlet"){
								?>
								<ul>
									<li>
										Color<br/>
										Talla
									</li>
									<li>
										<select>
											<option>Seleccionar</option>
											<option>Seleccionar1</option>
											<option>Seleccionar2</option>
										</select>
										<br/>
										<select>
											<option>Seleccionar</option>
											<option>Seleccionar1</option>
											<option>Seleccionar2</option>
										</select>
									</li>
								</ul>

								<?php
								}else{
								?>
									Opciones
									<select>
										<option>Seleccionar</option>
										<option>Seleccionar1</option>
										<option>Seleccionar2</option>
									</select>
									<br/>
                                    <!--
									<img src="<?php /* echo URL::base(); */ ?>public/img/flechas_regalo.jpg" /> Conoce otras ofertas de este comercio
									<br/>-->
								<?php } ?>
								</div>
								<br/>
                                <!--
								<p>
									<img src="<?php echo URL::base();?>public/img/flechas_regalo.jpg"> Conoce más ofertas de este comercio<br/>
									<img src="<?php echo URL::base();?>public/img/regalo_regalo.jpg"> ¡Regálaselo a un amigo!
								</p>
								-->
								<span>
                                    <!--
									<a class="twiter" href="javascript:void(0);"></a>
									<a class="mail" href="javascript:void(0);"></a>
									<a class="me_gusta" href="javascript:void(0);"></a>
									-->
								</span>
							</li>
						</ul>
					</li>
                </ul>
            </div>
			<!--fin contentido-->
			<ul>
				<li>
					<span>
						<span>
							<?php echo ucwords($producto->comercio_nombre); ?>
						</span>
						<p class="active">
							<?php echo $producto->comercio_direccion; ?>
						</p>
					</span>

					<?php echo($logo)?>


					<div id="img_empresa_producto">
						<img style="width: 230px;" src="<?php echo URL::base() . $imagen_interna; ?>" />
					</div>
					<div class="contenido descripcion active">
						<?php echo $producto->comercio_descripcion; ?>
					</div>
				</li>

				<li class="content_descripciones">

					<ul class="tabs_detalle">
						<li class="active"><a id="descripcion" href="javascript:void(0);">Descripción</a></li>
						<li><a id="condiciones" href="javascript:void(0);">Condiciones</a></li>
						<li><a id="ubicacion" href="javascript:void(0);">Ubicación</a></li>
					</ul>
					<div class="content_tabs">
						<div class="contenido descripcion active">
							<h2>Destacamos</h2>
							<?php echo $producto->oferta_destacamos; //nl2br ?>
							<br/>
							<h2>¿Qu&eacute; incluye esta oferta?</h2>
							<?php echo $producto->oferta_que_incluye; ?>
							<br/>
							<h2>Descripci&oacute;n de la oferta</h2>
							<?php echo $producto->oferta_descripcion; ?>
						</div>
						<div class="contenido condiciones"><?php echo $producto->oferta_condiciones; ?></div>
						<div class="contenido ubicacion">
							<div id="map_canvas" style="height: 380px; width: 450px; display: block;"></div>
						</div>
					</div>

					<div class="content_tabs">
						<div class="contenido condiciones"><?php echo $producto->oferta_condiciones; ?></div>
						<div class="contenido ubicacion">
							<div id="map_canvas" style="height: 380px; width: 450px; display: block;"></div>
						</div>
					</div>

				</li>

			</ul>

            <?php if($controlador == 'village' && $otros->count() > 0): ?>
            <div>
                <h4 class="titulo_com">Más ofertas de este comercio:</h4>

                <?php $controlador = 'village'; ?>
                <ul class="lista_productos_general cont_<?php echo $controlador; ?> permanentes_general">
                    <?php

                    foreach($otros as $p)
                    {
                        ?>
                        <li>
                            <ul>
                                <li>
                                    <span class="dto"><?php echo $p->oferta_descuento; ?>% Dto.</span>
                        <span class="descripcion"><?php
                            $text_cortado=90;
                            if($controlador=="outlet"){
                                $text_cortado=49;
                            }
                            echo $p->get_txtmuestra($p->oferta_titulo,$text_cortado);?></span>

                                </li>
                                <li class="img">
                                    <span></span>
                                    <div>
                                        <img border="0" alt="" src="<?php echo URL::base() . $p->oferta_imagen->url_path; ?>">
                                    </div>
                                </li>
                                <li class="caracteristicas">
                                    <ul>
                                        <li>
                                <span class="ver_plan">
                                    <span class="prec">
                                        <?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
                                    </span>
                                    <a href="<?php echo URL::site($controlador.'/producto/' . $p->id ); ?>"></a>
                                </span>
                                            <span>Precio:</span>
                                            <br/>
                                            <span class="precio"><?php echo $p->oferta_precio_venta; ?>€</span>
                                        </li>

                                        <li>
                                            <p class="titulo_c"><?php echo $p->comercio_nombre ?></p>
                                            <!--
                                <span class="redes">
                                        <?php if($controlador!="outlet"){?>
                                        <?php }?>
                                    <a class="twiter"></a>
                                        <a class="mail"></a>
                                        <a class="megusta"></a>
                                    <span id="alerta_producto_op">
                                    </span>
                                </span>
                                -->
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <?php endif; ?>
			<div style="margin-left: 10px;margin-right: 10px">
				<h3 style="color: #680034;">Más ofertas de este comercio</h3>
				<hr>

				<ul id="lista_productos_general" class="lista_productos_general cont_village permanentes_general">
					<?php
					foreach($productos as $p)
					{
						?>
						<li>
							<ul>
								<li>
									<span class="dto"><?php echo $p->oferta_descuento; ?>% Dto.</span>
                                <span class="descripcion"><?php
									$text_cortado=90;
									if($controlador=="outlet"){
										$text_cortado=49;
									}
									echo $p->get_txtmuestra($p->oferta_titulo,$text_cortado);?></span>


								</li>
								<li class="img">
									<span></span>
									<div>
										<img border="0" alt="" src="<?php echo URL::base() . $p->oferta_imagen->url_path; ?>">
									</div>
								</li>
								<li class="caracteristicas">
									<ul>
										<li>
										<span class="ver_plan">
											<span class="prec">
                                                <?php echo $p->get_oferta_precio_final(); ?><span style="font-size:24px;">€</span>
											</span>
											<a href="<?php echo URL::site($controlador.'/producto/' . $p->id ); ?>"></a>
										</span>
											<span>Precio:</span>
											<br/>
											<span class="precio"><?php echo $p->oferta_precio_venta; ?>€</span>
										</li>

										<li>
											<p class="titulo_c"><?php echo $p->comercio_nombre ?></p>
											<!--
										<span class="redes">
                                                <?php if($controlador!="outlet"){?>
                                                <?php }?>
                                            <a class="twiter"></a>
												<a class="mail"></a>
												<a class="megusta"></a>
                                            <span id="alerta_producto_op">
                                            </span>
										</span>
										-->
										</li>

									</ul>
								</li>
							</ul>
						</li>
						<?php
					}
					?>
				</ul>
			</div>
        </div>
    </div>


	<div id="colum_produc_b">
		<?php echo View::factory('ci/view_sidebar'); ?>
	</div>
    </div>
</div>

<?php
    //echo Html::style('public/css/jquery.countdown.css');
    echo Html::script('public/js/jquery.countdown.js');

    $fecha = new DateTime($producto->oferta_duracion);
    $_format = strtotime($producto->oferta_duracion) * 1000;

?>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    var _ofer_lat = '<?php echo $producto->oferta_ubicacion_lat; ?>';
    var _ofer_lon = '<?php echo $producto->oferta_ubicacion_lon; ?>';

	$(document).ready(init);

	$tabs_antiguo_activo = "";

	function init(){
		$('.tabs_detalle a').click(setTab);

		$('.btn_comprar').click(comprar);

        var austDay = new Date();
        austDay = new Date(<?php echo $_format ?>);
        $('#defaultCountdown').countdown({until: austDay});
	}

	function setTab()
	{
		$('.tabs_detalle li').removeClass('active');
		$(this).parent().addClass('active');

		$('.content_tabs .contenido').removeClass('active');
		$('.content_tabs .' + $(this).attr('id')).addClass('active');

		if($(this).attr('id') == 'ubicacion'){
            if(_ofer_lat != "" && _ofer_lon != ""){
                startMap();
            }
        }
	}

	function startMap()
	{
		//var position = new google.maps.LatLng(<?php echo $producto->oferta_ubicacion_lat; ?>, <?php echo $producto->oferta_ubicacion_lon; ?>);
		var position = new google.maps.LatLng(_ofer_lat, _ofer_lon);
		var mapOptions = {
			zoom: 16,
			center: position,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

		var marker = new google.maps.Marker({
			position: position,
			map: map,
			title: "<?php echo $producto->comercio_nombre; ?>"
		});

		var infowindow = new google.maps.InfoWindow({
			content: "<h2><?php echo $producto->comercio_nombre; ?></h2><p>Usa el mapa para ubicar mejor la dirección del comercio</p>"
		});

		infowindow.open(map,marker);
	}

	function comprar()
	{
		$(this).effect('transfer', { to: "#abrir_carrito", className: "transfer-effect" });
		qd.app.Carrito.agregarItemCarrrito($(this).attr('id'));
		return false;
	}
</script>