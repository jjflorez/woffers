<?php
	$model_productos = new Model_Producto();
	$productos_noregalo = $carrito->detalleproducto->where('is_gift', '=', 0)->find_all();
?>

<div id="c_offers">
<!--<div class="container">-->
	<div id="content_cesta">
		<div class="titulo">
		Mi cesta
		</div>
		<div class="cesta">
            <div id="tpv_frame" style="display: none;">
                <iframe src="" name="pasarela_frm" id="pasarela_frm" frameborder="0" width="870" height="770"></iframe>
            </div>
            <div id="cesta_frm">

	        <h2>Mi Cesta</h2>
			<ul class="my_cesta style_content_cesta"  cellspacing="0" cellpadding="0">
				<li class="titulo_colum">
					<span class="colum1">Tipo</span>
					<span class="colum2">Detalle de tu compra</span>
					<span class="colum3">Cantidad</span>
					<span class="colum4">Regalo</span>
					<span class="colum5">Precio</span>
					<span class="colum6">Subtotal</span>
					<span class="colum7">Acciones</span>
				</li>
				<?php
					$productos_cero=0;
                    $producto_direccion = 0;

					foreach($productos as $item):
						if($productos_cero==0){$productos_cero=1;}
				?>
				<li class="elementos">
					<span class="colum1"><?php
                        switch ($item->producto->oferta_tipo) {
                            case "cupon_producto":
                                echo "CP";
                                break;
                            case "producto":
                                echo "P";
                                $producto_direccion ++;
                                break;
                            case "cupon":
                                echo "C";
                                break;
                            default;
                                echo $item->producto->oferta_tipo;
                                break;
                        }

                        ?></span>

					<span class="colum2"><?php echo $model_productos->get_txtmuestra($item->producto->oferta_titulo, 60); ?></span>

					<span class="colum3">
						<?php
							$_cantidad = $item->count_producto($carrito_id, $item->producto_id);
						?>
						<select id="<?php echo $item->producto_id ?>" name="cantidad" class="cantidad">
							<?php
								for($x=1; $x<=10; $x++):
									$_selected = 'selected="selected"';
							?>
								<option <?php echo ($_cantidad == $x) ? $_selected : ""; ?> value="<?php echo $x ?>"><?php echo $x ?></option>
							<?php endfor; ?>
						</select>
					</span>
					<span class="colum4"><?php echo $item->count_gift($carrito_id, $item->producto_id) ?></span>
					<span class="colum5"><?php echo $item->producto->get_oferta_precio_final(); ?> €</span>
					<span class="colum6"><?php echo ($_cantidad * $item->producto->get_oferta_precio_final()) . " €" ?></span>
					<span class="colum7"><a href="<?php echo url::site() . 'carrito/delete_producto/' . $carrito_id . '/' . $item->producto_id; ?>">Eliminar</a> <?php /* echo $item->producto_id*/ ?></span>
				</li>


				<?php endforeach;
				if($productos_cero==0){
					echo "<p class='badge badge-error'>Tu carrito está vacío.</p>";
				}
				?>

				<li class="totales top">
					TOTAL<span><?php echo $total_monto; ?> €</span>
				</li>

				<li class="totales" style="display: none;">
					<img src="<?php echo URL::base();?>public/img/cesta/quees.png" >GASTOS DE ENVÍO
					<span>4.5 €</span>
				</li>

				<li class="totales">
					<div class="ovl_credits">
						TIENES <span>50 OVL CREDITS</span> DISPONIBLES
					</div>
					<img src="<?php echo URL::base();?>public/img/cesta/quees.png" >UTILIZAR OVL CREDITS
					<span>
					<img src="<?php echo URL::base();?>public/img/cesta/coins.png">
					<input type="text" disabled="disabled" title="Muy pronto podrás usar está opción" name="ovl_credits">
					</span>
				</li>

				<li class="totales pago">
					PRECIO TOTAL <span><?php echo $total_monto; ?> €</span>
				</li>
			</ul>

			<div class="agregar_producto">
				Recuerda imprimir tu vale o descargatelo directamente a tu movil.
				<br>
				Debe presentarlo a la hora de canjearlo.
				<a href="<?php echo URL::site(); ?>">&nbsp;</a>
			</div>

			<!-- Regalos ##################### -->

	        <h2>Mis regalos</h2>

			<ul id="mis_regalos" class="style_content_cesta">

				<!--contenido regalos a enviar-->
				<li class="detalle_regalos">
					<h2>Detalles de tus regalos</strong></h2>
					<div>
						<?php
							foreach($prod_regalos as $regalo)
							{
								if($regalo->direccion_id > 0)
									echo '<p><img src="' . URL::base() . 'public/img/regalo_regalo.jpg">' . $regalo->producto->oferta_titulo . '<span> regalado a: ' . $regalo->direccion->nombre . ' ' . $regalo->direccion->apellidos .'</span> <a href="' . URL::site('carrito/delete_gift/' . $regalo->id) . '">eliminar</a></p>';
								else
									echo '<p><img src="' . URL::base() . 'public/img/regalo_regalo.jpg">' . $regalo->producto->oferta_titulo . '<span> regalado a: ' . $regalo->regalo_nombre . ' ' . $regalo->regalo_email.'</span> <a href="' . URL::site('carrito/delete_gift/' . $regalo->id) . '">eliminar</a></p>';
							}
						?>
					</div>
				</li>

				<!---->
				<li class="select_producto">
					<div class="select_element">
						<?php
							if(count($productos_noregalo) == 0)
							{
								echo '<p class="alert alert-info" style="margin: 0 15px;">No tienes más productos para regalar, agrega algunos</p>';
							}else{
						?>

						<img src="<?php echo URL::base();?>public/img/cesta/smile.jpg">Puedes hacer hasta <strong><?php echo count($productos_noregalo); ?> regalos</strong>. Selecciona el producto o cupón que deseas regalar
						<select id="cb_regalos">
							<option value="0">-- Seleccione regalo --</option>
							<?php
								foreach($productos_noregalo as $p)
								{
									echo '<option type="'. $p->producto->oferta_tipo .'" value="'. $p->id .'">' . $p->producto->oferta_titulo . '</option>';
								}
							?>
						</select>

						<?php
							}
						?>
					</div>

					<div id="form-regalo" class="registro">
						<p>Indicanos a quién quieres regalarle el producto:</p>
						<span class="campos_obligatorios">(<span>*</span>Campos obligatorios)</span>
						<form id="frm_gift_dir" action="" method="post">
							<div>
								Tipo de Direccion:
								<select name="tipo_direccion">
									<option value="">Tipo de direccion</option>
									<option value="Particular">Particular</option>
									<option value="Oficina">Oficina</option>
								</select> <br />
								Empresa: <input type="text" name="empresa" /> <br />
								Nombre: <input type="text" name="nombre" /> <br />
								Apellidos: <input type="text" name="apellidos" /> <br />
								Telefono: <input type="text" name="telefono" />
							</div>
							<div>
								Direccion: <input type="text" name="direccion" /> <br />

								Codigo Postal: <input type="text" name="codigo_postal" /> <br />
								Poblacion: <input type="text" name="poblacion" /> <br />
								Provincia:
								<select name="provincia">
									<option value="0">provincia 001</option>
									<option value="1">provincia 002</option>
									<option value="2">provincia 003</option>
									<option value="3">provincia 004</option>
									<option value="4">provincia 005</option>
								</select> <br />
								Pais: <input type="text" name="pais" />
							</div>
							<input type="submit" value="&nbsp;" />
						</form>
					</div>

					<form id="frm_gift_send_cupon" action="<?php echo URL::site('/carrito/update_detalle_producto'); ?>" method="post">
						<ul id="form-cupon">
							<input type="hidden" id="id_detalle_producto_regalo" name="id_detalle_producto" />
							<input type="hidden" value="1" name="is_gift" />
							<p><input id="radio_cupon_ovl" name="regalo_send" value="1" type="radio" checked="checked">One Vision Life envía el Cupón regalo<br></p>
							<p><input id="radio_cupon_yo" name="regalo_send" value="0" type="radio">Yo mismo entrego el Cupón regalo</p>


							<li class="envia_ovl registro">
								<p>Indicanos a quién quieres regalarle el Cupón:</p>
								<div>
									Nombre y apellidos<input name="regalo_nombre" type="text"><br>
									Mensaje<textarea name="regalo_mensaje" rows="" cols=""></textarea>
								</div>
								<div>
									Email<input type="text" name="regalo_email" />
								</div>
								<p><input type="submit" value="" /></p>
							</li>

							<li class="envia_yo">
								<p>Indicanos a quién quieres regalarle el Cupón:</p>
								Nombre y apellidos<input id="regalo_nombre2" type="text" name="">
								<input type="submit" value="" />
							</li>
						</ul>
					</form>
				</li>
			</ul>

		<!-- Datos de envío -->
<?php if($producto_direccion != 0): ?>
        <h2>Mis datos de envio</h2>

		<div id="mis_datos_envio" class="style_content_cesta">
	        <p>Detalles y configuracion de tus datos de envío</p>

			<div class="important">
				Importante!<br/>Todos los productos que no hayas seleccionado como regalos, seran enviados a la direccion postal que selecciones a continuacion.
			</div>

			<div class="direcciones_envio">
				<table width="100%" class="table table-condensed">
					<thead>
						<th width="15">&nbsp;</th>
						<th width="90">Tipo</th>
						<th width="90">Nombre</th>
						<th>Dirección</th>
					</thead>
					<tbody class="lista_direcciones">
				<?php
                //echo "<pre>"; print_r($direcciones2); die();
				foreach($direcciones2 as $d)
				{
					echo '<tr>';
					if($carrito->direccion_id == $d->id)
						echo '<td><input type="radio" name="direccion_id" checked="checked" value="'. $d->id .'"></td><td><strong>' . $d->tipo_direccion . '</strong></td><td>' . $d->nombre .'</td><td>' . $d->direccion . '</td>';
					else
						echo '<td><input type="radio" name="direccion_id" value="'. $d->id .'"></td><td><strong>' . $d->tipo_direccion . '</strong></td><td>' . $d->nombre .'</td><td>' . $d->direccion . '</td>';
					echo '</tr>';
				}
				?>
					</tbody>
				</table>
				<a href="javascript:void(0);" class="btn btn_agregar_direccion">Agregar dirección</a>
			</div>

			<ul class="style_content_cesta add_direccion_envio" style="display: none;">
				<li class="registrados my_cesta">
					<span class="titulo_colum">
						<span class="colum1">Tipo</span>
						<span class="colum2">Dirección</span>
						<span class="colum3">C.P.</span>
						<span class="colum4">Población</span>
						<span class="colum5">Provincia</span>
						<span class="colum6">País</span>
						<span class="colum7">Acciones</span>
					</span>
					<span class="elementos">
						<span class="colum1"><input type="radio">Particular</span>
						<span class="colum2">c/ Mayor n. 33 1o 4a</span>
						<span class="colum3">08904</span>
						<span class="colum4">Barcelona</span>
						<span class="colum5">Barcelona</span>
						<span class="colum6">España</span>
						<span class="colum7"><a href="javascript:void (0);">Eliminar</a></span>
					</span>
					<a class="editar" href="javascript:void(0);"></a>
					<a class="agregar_nueva" href="javascript:void(0);"></a>
				</li>

				<li class="registro">
					<span class="campos_obligatorios">(<span>*</span>Campos obligatorios)</span>
					<form id="frm_dir" action="" method="post">
					<div>
						Tipo de Direccion:
						<select name="tipo_direccion">
							<option value="">Tipo de direccion</option>
							<option value="Particular">Particular</option>
							<option value="Oficina">Oficina</option>
						</select> <br />
						Empresa: <input type="text" name="empresa" /> <br />
						Nombre: <input type="text" name="nombre" /> <br />
						Apellidos: <input type="text" name="apellidos" /> <br />
						Telefono: <input type="text" name="telefono" />
					</div>
					<div>
							Direccion: <input type="text" name="direccion" /> <br />

							Codigo Postal: <input type="text" name="codigo_postal" /> <br />
							Poblacion: <input type="text" name="poblacion" /> <br />
							Provincia:
							<select name="provincia">
								<option value="0">provincia 001</option>
								<option value="1">provincia 002</option>
								<option value="2">provincia 003</option>
								<option value="3">provincia 004</option>
								<option value="4">provincia 005</option>
							</select> <br />
							Pais: <input type="text" name="pais" />

					</div>
						<a href="javascript:void(0);" class="btn btn_cancelar_agregar_direccion">Cancelar</a>
						<input type="submit" value="" />
					</form>
				</li>
			</ul>
		</div>
<?php endif; ?>
        <!-- fin Datos de envío -->

<!--
        <h2>Datos de Facturacion</h2>

		<div id="content_facturacion" class="style_content_cesta">
            <input type="radio" name="_fact" value="0" checked="checked" /> No quiero factura<br/>
            <span><input type="radio" name="_fact" value="1" /> Si quiero factura</span>

		<div>
			<p>One Vision Life solo puede hacerte factura por los productos que has comprado. Si has adquirido un cupón canjeable por un servicio y deseas obtener una factura, debes pedírsela al comercio en el momento de disfrutar del cupón.<br/>
				A continuación puedes ver los datos que utilizaremos para hacerte la factura. Rellena los campos vacíos si fuera necesario.</p>
            <div id="form-facturacion" class="style_content_cesta">
				<span class="campos_obligatorios">(<span>*</span>Campos obligatorios)</span>
                <form id="frm_fac" action="" method="post">

					<div>
						Nombre:  <br />
						1r Apellido:  <br />
						2o Apellido: <br />
						DNI:  <br />
						Direccion:
					</div>

					<div  class="colum02">
						<input type="text" name="f_nombre" id="f_nombre" /><br />
						<input type="text" name="f_apellido1" id="f_apellido1" /><br />
						<input type="text" name="f_apellido2" id="f_apellido2" /><br />
						<input type="text" name="f_dni" id="f_dni" /><br />
						<input type="text" name="f_direccion" id="f_direccion" />
					</div>
					<div>
						Codigo Postal:<br />
						Poblacion:<br />
						Provincia:<br />
						Pais:<br />
						<br />
					</div>
					<div class="colum02">
						 <input type="text" name="f_codigo_postal" id="f_codigo_postal" /><br />
						 <input type="text" name="f_poblacion" id="f_poblacion" /><br />
						 <input type="text" name="f_provincia" id="f_provincia" /><br />
						 <input type="text" name="f_pais" id="f_pais" /><br /> <br/>
					</div>
                    <br><br>
                    <input type="submit" value="Actualizar Datos">
                </form>
            </div>
		</div>
		</div>
-->
		<!-- Formulario de -->
		<?php /*?>
        <!--<form id="form_enviar" action="http://ouanda.h3m.com/~ixl030ba/securepayment/" method="post">-->
		<?php /**/ ?>
		<?php /*?>
        	<form id="form_enviar" action="http://securepayment.onevisionlife.com/pasarela/" method="post" target="_self">
		<?php /**/ ?>
		<form id="form_enviar" action="<?php echo URL::base(); ?>pasarela/pre_evaluar_compra/" method="post" target="_self">
			<input type="hidden" id="response" name="response" value="<?php echo URL::site('cesta/felicidades', 'http'); ?>" />
			<input type="hidden" id="id_carr" name="id_carr" value="<?php echo $carrito_id; ?>" />
			<input type="hidden" id="total" name="total" value="<?php echo $total_monto; ?>" />
			<input type="hidden" id="idusuario" name="idusuario" value="<?php echo $usuario_id ?>" />
			<input type="hidden" id="orden" name="orden" value="" />
			<input type="hidden" id="email" name="email" value="<?php echo $usuario->email ?>" />
			<input type="hidden" id="nombre" name="nombre" value="<?php echo $usuario->name ?>" />
			<input type="hidden" id="apellidos" name="apellidos" value="<?php echo $usuario->last_name ?>" />

                <!-- Init Array de valores, no tiene limite -->
            <?php
                $_count = 0;
                foreach($productos as $key => $item):
                    $_cantidad = $item->count_producto($carrito_id, $item->producto_id);
            ?>
                <input type="hidden" name="productos[<?php echo $key; ?>][nombre]" value="<?php echo $item->producto->oferta_titulo . ' (x ' . $_cantidad . ')'; ?>" />
                <input type="hidden" name="productos[<?php echo $key; ?>][descripcion]" value="<?php echo Text::limit_chars(strip_tags($item->producto->oferta_descripcion)); ?>" />
                <input type="hidden" name="productos[<?php echo $key; ?>][monto]" value="<?php echo ($_cantidad * $item->monto); ?>" />
            <?php
                $_count ++;
                endforeach;
            ?>
                <!-- En caso de que sea de un país de la unión europea agregar IVA como un producto -->
                <input type="hidden" name="productos[<?php echo $_count ?>][nombre]" value="IVA" />
                <input type="hidden" name="productos[<?php echo $_count ?>][descripcion]" value="Impuesto unión europea" />
                <input type="hidden" name="productos[<?php echo $_count ?>][monto]" value="0" />
        </form>

        <h2>Formas de pago</h2>
			<div id="formas_pago" >
				<div class="style_content_cesta">
					<input type="radio" name="pago" checked="checked" /> Tarjeta de credito
				</div>

				<input id="btn_pagar" type="submit" value="" />
			</div>
        </div>
		</div><!--end .cesta -->
		</div>

<!--</div>-->

</div>

<script type="text/javascript">
    var producto_direccion = <?php echo $producto_direccion; ?>;
    var id_carrito = '<?php echo $carrito_id; ?>';
    var base_url = '<?php echo url::site(); ?>';
    //var back_url = "http://dev.onevisionlife.es/backoffice/api/user/get/";
    var back_url = "http://dev.onevisionlife.es/backoffice/api/user/get/<?php echo $usuario->username; ?>";
    //var back_url = "http://localhost/backoffice/api/user/get/gersonm";
    //var back_url_doc = "http://localhost/backoffice/api/user/updatedoc/";
    var back_url_doc = "http://dev.onevisionlife.es/backoffice/api/user/updatedoc/";
    var id_persona = "";
    var _factura;

    $(document).ready(init);
	var $radio_regalo = 0;

    function init(){

		$("#cb_regalos").change(function(){

			$('#id_detalle_producto_regalo').val($(this).val());

			var type;
			$("#cb_regalos option:selected").each(function () {
				type = $(this).attr('type');
			});

			if(type == "cupon"){
				$("#form-cupon").show("fast");
				$("#form-regalo").hide("fast");
			}
			else
			{
				$("#form-cupon").hide("fast");
				$("#form-regalo").show("fast");
			}
		})

		$("#form-cupon p input[type=radio]").click(function(){
			if($(this).attr("id")=="radio_cupon_yo"){
				$("#form-cupon .envia_ovl").hide("fast");
				$("#form-cupon .envia_yo").show("fast");
                $("#regalo_nombre2").attr("name", "regalo_nombre");

            }else{
				$("#form-cupon .envia_yo").hide("fast");
				$("#form-cupon .envia_ovl").show("fast");
                $("#regalo_nombre2").attr("name", "");
			}
		})

        /*$("#form-regalo").hide();*/
        $("#content_facturacion > div").hide();

        $("#radio_regalo").click(function(){

			if($radio_regalo==0){
				$("#radio_regalo").attr('checked', true);
				$radio_regalo=1;
				$("#mis_regalos .select_producto").show('slow');
			}
			else
			{
				$("#radio_regalo").attr('checked', false);
				$radio_regalo=0;
				$("#mis_regalos .select_producto").hide('slow');
			}
        });

        $(".cantidad").change(update_cantidad);
        $("#frm_gift_dir").submit(save_frm_gift);
        $("#frm_dir").submit(save_frm);
        $("#btn_pagar").click(send_pagar);

        $('input:radio[name=_fact]').click(function () {
            var id = $('input:radio[name=_fact]:checked').val();
            _factura = id;

            if(_factura == 0){
                $("#content_facturacion > div").hide("show");

            }else if(_factura == 1){
                $("#content_facturacion > div").show("show");
            }
            update_factura(_factura);
            //alert("change " + id);
        });

		$('.btn_agregar_direccion').click(switch_direccionRegalo);
		$('.btn_cancelar_agregar_direccion').click(switch_direccionRegalo);

        get_data();
        $('#frm_fac').submit(updateDoc);
    }

	var addRegaloIsShow = false;
	function switch_direccionRegalo()
	{
		if(addRegaloIsShow)
		{
			$('.direcciones_envio').slideDown();
			$('.add_direccion_envio').slideUp();
		}else{
			$('.direcciones_envio').slideUp();
			$('.add_direccion_envio').slideDown();
		}

		addRegaloIsShow = !addRegaloIsShow;
	}

    function update_cantidad(){
        var id_producto = $(this).attr('id');
        var cantidad = $(this).val();

        $.get(base_url + '/carrito/update_cantidad/'+id_carrito+'/'+id_producto+'/'+cantidad+'/', function (data) {
            window.location = base_url + 'cesta/';
        });
    }

    function save_frm_gift(){
        var id_producto = $("#cb_regalos").val(); //alert("hola " + id_producto);
        var _form = $("#frm_gift_dir").serialize(); //alert(_form);

        if(id_producto > 0){
            $.post(base_url + 'carrito/add_gift_direccion/'+id_carrito+'/'+id_producto+'/', _form, function (data) {
                window.location = base_url + 'cesta/';
            });
        }else{
            //console.log("no va");
        }
        return false;
    }

    function save_frm(){

        $.post(base_url + 'carrito/add_direccion/'+id_carrito+'/', $("#frm_dir").serialize(), function (data) {
            window.location = base_url + 'cesta/';
        });

        return false;
    }

    function send_pagar(){
        var _direccionSelected = false;
        var _factura = $('input:radio[name=_fact]:checked').val();

        if(producto_direccion != 0){
            $('.lista_direcciones input[name=direccion_id]').each(function(){
                if($(this).attr('checked'))
                    _direccionSelected = true;
            });

            if(_direccionSelected){
                if(_update_direccion($('.lista_direcciones input[checked=checked]').val())){
                    //$('#cesta_frm').hide();
                    //$('#tpv_frame').show();
                    //window.scrollTo(0, 0);

                    $("#form_enviar").submit();

                    /*
                    var _form = $("#form_enviar");
                    $.post(_form.attr("action"), _form.serialize(), function(data){
                        alert(data);
                    });*/
                }
            }else{
                alert("Por favor selecciona una dirección de envío");
            }
        }else{
            $("#form_enviar").submit();
        }
    }

    function _update_direccion(id_direccion){
        $.post(base_url + 'carrito/update_direccion/'+id_direccion+'/'+id_carrito+'/', function (data) {});

        return true;
    }

    function get_data(){

        $.get(back_url, function(_data){
            if(_data.data.length > 0){
                var _user = _data.data[0];
                id_persona = _user.IDPERSONA;
                $('#f_nombre').attr('value', _user.NOMBRES);
                $('#f_apellido1').attr('value', _user.APEPATERNO);
                $('#f_apellido2').attr('value', _user.APEMATERNO);
                $('#f_dni').attr('value', _user.NUMERODOCUMENTO);
                $('#f_direccion').attr('value', _user.DIRECCION);

                $('#f_codigo_postal').attr('value', _user.CODIGOPOSTAL);
                $('#f_poblacion').attr('value', _user.CIUDAD);
                $('#f_provincia').attr('value', _user.PROVINCIA);
                $('#f_pais').attr('value', _user.NOMBREPAIS);
            }
        }, "json");
    }

    function update_factura(_factura){
        $.post(base_url + 'carrito/update_factura/', {ic:id_carrito, f:_factura});
    }

    function updateDoc(){
        var _dni = $('#f_dni').val();
        if(_dni != ""){
            $.post(back_url_doc + id_persona, {num_doc: $('#f_dni').val()}, function(data){
            });
        }else{
            alert("Ingrese un DNI valido");
        }


        return false;
    }
</script>