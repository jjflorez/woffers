<div class="container">
	<h1>Scaffolding</h1>
	<p>Selecciones una tabla para realizar las acciones de mantenimiento</p>
	<div class="row">
		<div class="span3">
			<h4>Tablas disponibles</h4>
			<ul class="nav nav-pills nav-stacked" id="lista_tablas">
			<?php
				foreach($tablas as $tabla)
				{
					echo '<li><a href="' . URL::site('scaffolding/table/' . $tabla) . '" id="' . $tabla . '">' . Inflector::humanize($tabla) . '</a></li>';
				}
			?>
			</ul>
		</div>
		<div class="span9">
		</div>
	</div>

</div> <!-- /container -->


<script type="text/javascript">

	$(document).ready(initPage);

	function initPage()
	{
		qd.app.SelectList('#lista_tablas', CLICK_LIST_handler);
	}

	function CLICK_LIST_handler()
	{

	}

</script>