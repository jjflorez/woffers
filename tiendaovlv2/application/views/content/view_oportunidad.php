<div id="c_home">
    <div id="container_lineas">
        <ul id="pestanas_lineas">
            <li><a href="<?php echo URL::site('sobre/que-es'); ?>">Sobre One Vision Life</a></li>
            <li class="selected"><a href="#">Oportunidad de Negocios</a></li>
            <li><a href="<?php echo URL::site('content'); ?>/comercios">One Vision Life Bussiness</a></li>
        </ul>
    </div>
    <div class="container">
        <div id="tipo-socios" style="padding: 10px 20px;">
            <div class="bread">
                <ul>
                    <li><a href="#">Gana con nosotros</a></li>
                    <li><a class="active" href="<?php echo URL::site('oportunidad/oportunidad_de_negocio'); ?>">Tipos de socios</a></li>
                    <li><a href="<?php echo URL::site('oportunidad/conferencias'); ?>">Conferencias</a></li>
                </ul>
            </div>
            <h3>Tipos de Socios</h3>
            <p>One Vision Life es un Club con un concepto revolucionario, que busca que todos sus socios ganen beneficios desde el primer momento que se registran.</p>

            <div class="col left2">
                <div class="gratis right2">GRATIS</div>
                <div class="clear2"></div>

                <div class="cont-col">
                    <table cellpadding="0">
                        <tr>
                            <td width="226" bgcolor="#eee">
                                <h3>MEMBRESÍA</h3>
                            </td>
                            <td width="228">
                                <table class="tableg" border="1" width="100%">
                                    <tr>
                                        <th width="114">Socio Bronce</th>
                                        <th>Socio Bronce VIP</th>
                                    </tr>
                                    <tr>
                                        <td><img src="<?php echo URL::base();?>public/img/bronce_icon.jpg"></td>
                                        <td><img src="<?php echo URL::base();?>public/img/broncevip_icon.jpg"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="col left2" style="margin-left: 40px;">
                <div class="depago">DE PAGO</div>

                <div class="cont-col">
                    <div style="width: 454px;">
                        <table class="tableo" border="1" width="100%">
                            <tr>
                                <th width="114">Socio Plata</th>
                                <th width="114">Socio Plata<br> VIP</th>
                                <th width="114">Socio Oro</th>
                                <th width="114">Socio Platino</th>
                            </tr>
                            <tr>
                                <td><img src="<?php echo URL::base();?>public/img/plata_icon.jpg"></td>
                                <td><img src="<?php echo URL::base();?>public/img/platavip_icon.jpg"></td>
                                <td><img src="<?php echo URL::base();?>public/img/oro_icon.jpg"></td>
                                <td><img src="<?php echo URL::base();?>public/img/platino_icon.jpg"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clear2"></div>

    <div class="bar">
        <h4>INFORMACIÓN DEL SOCIO</h4>
    </div>

            <div class="col left2">

                <div class="cont-col">
                    <table cellpadding="0">
                        <tr>
                            <td width="226" bgcolor="#eee">
                                <table cellpadding="4">
                                    <tr>
                                        <td><strong>¿Qué tipo de socio soy?</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong>¿Cómo cobro mis ganancias por recomendar?</strong></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="228">
                                <table class="tableg" border="1" width="100%">
                                    <tr>
                                        <td width="114">Socio Cliente</td>
                                        <td>Socio de Negocio</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>PUNTOS</strong><br>para cobrar
                                        </td>
                                        <td>
                                            <strong>COMISIONES</strong><br>en Euros
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="col left2" style="margin-left: 40px;">
                <div class="cont-col">
                    <div style="width: 454px;">
                        <table class="tableo" border="1" width="100%">
                            <tr>
                                <td width="114">Socio Bronce</td>
                                <td width="114">Socio Bronce VIP</td>
                                <td width="114">Socio Bronce VIP</td>
                                <td width="114">Socio Bronce VIP</td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>COMISIONES</strong><br>en Euros
                                </td>
                                <td><strong>COMISIONES</strong><br>en Euros</td>
                                <td><strong>COMISIONES</strong><br>en Euros</td>
                                <td><strong>COMISIONES</strong><br>en Euros</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clear2"></div>

    <div class="bar">
        <h4>VENTAJAS</h4>
    </div>
            <div class="col left2">
                <div class="left2" style="width: 226px; border-top: 2px solid #fff;">
                    <table class="tablv1" width="100%">
                        <tr><td class="tit">OFERTA EN NUESTRAS TIENDAS</td></tr>
                        <tr><td>Tu propia Oficina VirtualS</td></tr>
                        <tr><td>Ofertas en servicios y productos</td></tr>
                        <tr><td>Sorteos y promociones</td></tr>

                        <tr><td class="tit">PLATAFORMAS DE RESORTS</td></tr>
                        <tr><td>Pltaforma de resorts</td></tr>

                        <tr><td class="tit">FORMACIONES ESCLUSIVAS</td></tr>
                        <tr><td>Ciclos de conferencias</td></tr>
                        <tr class="line2"><td>Face to Face mensual<br>Coaching con nuestros formadores</td></tr>
                        <tr class="line2"><td>Teleseminario mensual<br>Coaching con nuestros formadores</td></tr>
                        <tr><td>Formaciones exclusivas Socio Platino</td></tr>

                        <tr class="line2"><td class="tit">GANANCIAS POR <br>RECOMENDAR A TUS AMIGOS</td></tr>
                        <tr class="line2"><td>Ganas por nuevas membresias de tus amigos</td></tr>
                        <tr class="line2"><td>Consigue puntos por nuevas membreaias, para comprar en tiendas</td></tr>
                        <tr><td>Ganas por las compras de tus amigos</td></tr>
                        <tr class="line2"><td>Ganas por las ventas de los comercios que recomiendas</td></tr>
                        <tr class="line2"><td>Ganas por las renovaciones de las membresias</td></tr>
                        <tr><td>Ganas bonos exclusivos</td></tr>
                        <tr class="line2"><td>Ganas un porcentaje de las ventas mundiales</td></tr>
                        <tr><td>Ganancias exclusivas platino</td></tr>

                        <tr class="gtr1">
                            <td>
                                <strong>COSTE MEMBRESIA</strong> <br>
                                <i>* ALTA RESTO DE PAISES SIN IVA</i><br>
                                <i>* ALTA UNION EUROPEA CON IVA</i>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="left2" style="width: 228px;">
                    <table class="tablv2" border="1" width="100%">
                        <tr><td class="tit" colspan="2">&nbsp;</td></tr>
                        <tr><td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td><td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td></tr>
                        <tr class="odd"><td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td><td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td></tr>
                        <tr><td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td><td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td></tr>

                        <tr><td class="tit" colspan="2">&nbsp;</td></tr>
                        <tr><td>B</td><td>B</td></tr>

                        <tr><td class="tit" colspan="2">&nbsp;</td></tr>
                        <tr><td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td><td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td></tr>
                        <tr class="odd line2"><td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td><td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td></tr>
                        <tr class="line2"><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr class="odd"><td></td><td></td></tr>

                        <tr class="line2"><td class="tit" colspan="2">&nbsp;</td></tr>
                        <tr class="odd line2"><td>3 Niveles</td><td>3 Niveles</td></tr>
                        <tr class="line2"><td>12 Niveles</td><td>12 Niveles</td></tr>
                        <tr class="odd"><td>12 Niveles</td><td>12 Niveles</td></tr>
                        <tr class="line2"><td>12 Niveles</td><td>12 Niveles</td></tr>
                        <tr class="odd line2"><td></td><td></td></tr>
                        <tr><td></td><td></td></tr>
                        <tr class="odd line2"><td></td><td></td></tr>
                        <tr><td></td><td></td></tr>

                        <tr class="gtr2">
                            <td>
                                <input type="radio" name="tipo"><br>
                                <strong>0 €</strong><br>
                                <strong>0 €</strong>
                            </td>
                            <td><input type="radio" name="tipo"><br>
                                <strong>0 €</strong><br>
                                <strong>0 €</strong></td>
                        </tr>
                    </table>
                </div>
                <div class="clear2"></div>
                <div style="height: 130px; padding-top: 40px; text-align: right;">
                    <img src="<?php echo URL::base();?>public/img/registrate_btn.jpg" >
                </div>
            </div>

            <div class="col left2" style="margin-left: 40px;">
                <table class="tablv2 tablv3" border="1" width="100%">
                    <tr><td class="tit" colspan="4">&nbsp;</td></tr>
                    <tr>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                    </tr>
                    <tr class="odd">
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                    </tr>
                    <tr>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                    </tr>

                    <tr><td class="tit" colspan="4">&nbsp;</td></tr>
                    <tr>
                        <td>B</td><td>B</td><td>B</td><td>B</td></tr>

                    <tr><td class="tit" colspan="4">&nbsp;</td></tr>
                    <tr>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                    </tr>
                    <tr class="odd line2">
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td width="114"><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                    </tr>
                    <tr class="line2">
                        <td>2 meses</td>
                        <td>6 meses</td>
                        <td>6 meses</td>
                        <td>12 meses</td>
                    </tr>
                    <tr class="odd">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                    </tr>

                    <tr class="line2"><td class="tit" colspan="4">&nbsp;</td></tr>
                    <tr class="odd line2">
                        <td>3 Niveles</td>
                        <td>3 Niveles</td>
                        <td>3 Niveles</td>
                        <td>3 Niveles</td>
                    </tr>
                    <tr class="line2">
                        <td>14 Niveles</td>
                        <td>14 Niveles</td>
                        <td>Hasta 20 Niveles</td>
                        <td>Hasta 20 Niveles</td>
                    </tr>
                    <tr class="odd">
                        <td>14 Niveles</td>
                        <td>14 Niveles</td>
                        <td>Hasta 20 Niveles</td>
                        <td>Hasta 20 Niveles</td>
                    </tr>
                    <tr class="line2">
                        <td>14 Niveles</td>
                        <td>14 Niveles</td>
                        <td>Hasta 20 Niveles</td>
                        <td>Hasta 20 Niveles</td>
                    </tr>
                    <tr class="odd line2">
                        <td>14 Niveles</td>
                        <td>14 Niveles</td>
                        <td>Hasta 20 Niveles</td>
                        <td>Hasta 20 Niveles</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                    </tr>
                    <tr class="odd line2">
                        <td></td>
                        <td></td>
                        <td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                        <td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><img src="<?php echo URL::base();?>public/img/check_icon.gif"></td>
                    </tr>
                    <tr class="otr">
                        <td><input type="radio" name="tipo"><br>
                            <strong>20 €</strong><br>
                            <strong>20 €</strong></td>
                        <td><input type="radio" name="tipo"><br>
                            <strong>70 €</strong><br>
                            <strong>70 €</strong></td>
                        <td><input type="radio" name="tipo"><br>
                            <strong>190 €</strong><br>
                            <strong>190 €</strong></td>
                        <td><img src="<?php echo URL::base();?>public/img/q_icon.gif" ><br>
                            <strong>600 €</strong><br>
                            <strong>600 €</strong></td>
                    </tr>
                </table>
                <div style="height: 130px; padding-top: 40px; text-align: center;">
                    <img src="<?php echo URL::base();?>public/img/registrate2_btn.jpg" >
                </div>
            </div>
            <div class="clear2"></div>
        <div id="masinfo">
            <div style="width: 338px;" class="left2">
                <i>* IVA: Impuesto solo aplicable a paises de Unión Europea (18%).<br>
                    Las altas de socio en las islas Canarias estan exentas de IVA.</i>
            </div>
            <div style="width: 290px;" class="left2">
                <i>** RENOVACIÓN ANUAL<br>
                    Unión Europea, un importe de 50€ + IVA (59€).<br>
                    Resto de Paises un importe de 50€
                </i>
            </div>
            <div style="height: 100px; width: 314px;" class="left2">
                <img src="<?php echo URL::base();?>public/img/info_btn.jpg" >
            </div>
            <div class="clear2"></div>
        </div>
        <h3>Acerca de los tipos de socios</h3>

        <ul id="acerca">
            <li>
                <div class="question">
                    <p><strong>¿Puedo empezar gratis y luego ir subiendo a otra membresia superior?</strong></p>
                </div>
            </li>
            <li>
                <div class="answer">
                    <p><i>Sí, puedes <span>empezar totalmente gratis,</span> y luego escoger la membresia que más te apetezca para empezar a ganar más con OVL</i></p>
                </div>
            </li>
            <li>
                <div class="question">
                    <p><strong>¿Puedo empezar como Socio Cliente y luego cambiarme a Socio de Negocio?</strong></p>
                </div>
            </li>
            <li>
                <div class="answer">
                    <p><i>Sí, puedes <span>empezar</span> como Socio Cliente solamente (Bronce), <span>y luego cambiarte a Socio de Negocio</span> en cualquiera de las membresias que le ofrecemos, ya sea la gratuita (Bronce Vip) o las de pago (Plata, Plata Vip, Oro o Platino)</i></p>
                </div>
            </li>
            <li>
                <div class="question">
                    <p><strong>¿Puedo darme de alta directamente como Socio Platino?</strong></p>
                </div>
            </li>
            <li>
                <div class="answer">
                    <p><i><span>No, no es posible registrarse como Socio Platino de Inicio.</span> Solo puede accederse a Socio Platino una vez te has registrado como Socio Oro primero. De esta manera estás multiplicando tus ingresos y avanzando de una manera segura.</i></p>
                </div>
            </li>
            <li>
                <div class="question">
                    <p><strong>¿Qué diferencia hay entre Socio Cliente y Socio de Negocio?</strong></p>
                </div>
            </li>
            <li>
                <div class="answer">
                    <p><i>El <span>Socio Cliente</span> solo <span>gana puntos</span> (Bronce)<br>
                        El <span>Socio de Negocio gana comisiones en metálico</span> y tambien puede <span>ganar puntos</span> (Bronce Vip, Plata, Plata Vip, Oro y Platino)</i></p>
                </div>
            </li>
            <li>
                <div class="question">
                    <p><strong>Tengo dudas acerca de la ventaja "Ganancias por recomendar a tus amigos"</strong></p>
                </div>
            </li>
            <li>
                <div class="answer">
                    <p><i>Puede ir al apartado <span>"Gana con nosotros"</span>, donde te explicamos con más detalle las diferentes formas que tienes de ganar. Si además <span>quieres ampliar la información,</span> ves al apartado <span>"Conferencias"</span> y allí te indican las horas para asistir a una presentación en directo, donde podrás hacer preguntas para aclarar todas tus dudas.</i></p>
                </div>
            </li>
        </ul>
        </div>
    </div>
</div>

<style type="text/css">
    /*.left2{
        float: left;
    }
    .right2{
        float: right;
    }
    .clear2{
        clear: both;
    }
    #tipo-socios{

    }
    #tipo-socios h3{
        color: #A9BC46;
    }

    #tipo-socios td h3{
        color: #535353;
        margin-left: 10px;
    }

    .bar{
        background: #D0E377;
        border: 1px solid #C0C3D2;
        border-radius: 2px;
        height: 30px;
        margin: 12px 0 12px -4px;
        width: 948px;
    }
    .bar h4{
        color: #848b7b;
        margin: 7px;
    }

    .bread{
        background: url("../public/img/breadcrumb1.jpg") no-repeat;
        height: 30px;
        margin-bottom: 10px;
        width: 302px;
    }

    .bread ul{
        margin: 0;
        padding: 0;
        list-style: none;
    }
    .bread ul li{
        display: inline-block;
        margin: 5px 10px;
    }
    .bread ul li a{
        color: #585858;
        font-size: 11px;
        text-decoration: none;
    }
    .bread ul li a:hover{
        color: #a9bc46;
    }
    .bread ul li .active{
        color: #a9bc46;
    }

    .gratis{
        background: #D0E377;
        font-weight: bold;
        text-align: center;
        width: 228px;
    }
    .depago{
        background: #FECB4E;
        font-weight: bold;
        text-align: center;
        width: 454px;
    }
    .col{
        width: 454px;
    }
    .cont-col{
        margin-top: 10px;
    }
    .tableg{
        border: solid 3px #D0E377;
    }

    .tableg td, .tableg th{
        border: solid 1px #D0E377;
        text-align: center;
        padding: 4px 0px;
    }
    .tableg td{
        background: #fff;
    }
    .tableg th{
        background: #F6F9E4;
    }

    .tableo{
        border: solid 3px #FECB4E;
    }

    .tableo td, .tableo th{
        border: solid 1px #FECB4E;
        text-align: center;
        padding: 4px 0px;
    }
    .tableo td{
        background: #fff;
    }
    .tableo th{
        background: #FFF5DC;
    }

    .tablv1, .tablv1 td, .tablv2 td{
        border: solid 1px #D2D2D2;
    }
    .tablv1 td, .tablv2 td{
        height: 30px;
    }
    .tablv1 td{
        background: #eee;
        padding-left: 6px;
    }
    .tablv1 .tit{
        background: #D1D9F0;
    }

    .tablv2{
        border: solid 3px #D0E377;
    }
    .tablv2 td{
        text-align: center;
    }
    .tablv2 .tit{
        background: #E0E8FF;
    }
    .tablv3{
        border: solid 3px #FECB4E !important;
    }
    .odd{
        background: #F8F8F8;
    }
    .otr{
        background: #FFF5DC;
        border-top: solid 3px #FECB4E;
        height: 78px;
    }
    .gtr1 td{
        background: #E7ECCC;
        height: 78px;
    }
    .gtr1 td i{
        font-size: 10px;
    }
    .gtr2{
        background: #F4FAD8;
        border-top: solid 3px #D0E377;
        height: 78px;
    }

    #masinfo{
        font-size: 11px;
    }

    #acerca{
        margin: 0;
        padding: 0;
        list-style: none;
    }
    #acerca .question, #acerca .answer{
        border: solid 1px #c0c3d2;
        border-radius: 5px;
        height: 34px;
        margin-bottom: 6px;
    }
    #acerca .question{
        padding-left: 28px;
        background: url("../public/img/q2_icon.gif") no-repeat 5px 5px #f4f4f4;
    }
    #acerca .question p, #acerca .answer p{
        padding: 0;
        margin: 6px;
    }
    #acerca .answer{
        margin-left: 40px;
        width: 760px;
    }*/
</style>