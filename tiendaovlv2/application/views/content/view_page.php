<?php
$controlador = Kohana_Request::$current->controller();
$p = Kohana_Request::$current->param('param1');
?>
<div id="c_home">
    <div id="container_lineas">
        <ul id="pestanas_lineas">
            <li class="selected"><a href="#">Sobre One Vision Life</a></li>
            <li><a href="<?php echo URL::site('oportunidad/gana-con-nosotros'); ?>">Oportunidad de Negocios</a></li>
            <li><a href="<?php echo URL::site('content'); ?>/comercios">One Vision Life Bussiness</a></li>
        </ul>
    </div>
    <div class="container container_home">
        <div class="row-fluid">
            <div id="colum_produc_a">
                <br>
                <div class="bread bread_3">
                    <ul>
                        <li><a <?php echo ($p == 'que-es') ? 'class="active"' : ''; ?> href="<?php echo URL::site('sobre/que-es'); ?>">¿Qué es One Vision Life?</a></li>
                        <li><a <?php echo ($p == 'como-funciona') ? 'class="active"' : ''; ?> href="<?php echo URL::site('sobre/como-funciona'); ?>">¿Cómo funciona?</a></li>
                        <li><a <?php echo ($p == 'vision-solidaria') ? 'class="active"' : ''; ?> href="<?php echo URL::site('sobre/vision-solidaria'); ?>">Visión solidaria</a></li>
                    </ul>
                </div>

                <?php echo $page->contenido; ?>
            </div>
            <div id="colum_produc_b">
                <?php echo View::factory('ci/view_sidebar')->set('var_sidebar','')?>
            </div>
        </div>
    </div>
</div>