<div id="c_home">
    <div id="container_lineas">
        <ul id="pestanas_lineas">
            <li><a href="<?php echo URL::site('sobre/que-es'); ?>">Sobre One Vision Life</a></li>
            <li class="selected"><a href="#">Oportunidad de Negocios</a></li>
            <li><a href="<?php echo URL::site('content'); ?>/comercios">One Vision Life Bussiness</a></li>
        </ul>
    </div>

    <div class="container">
        <div id="colum_produc_a">
            <div id="conferencias">
            <div class="bread">
                <ul>
					<li><a href="<?php echo URL::site('oportunidad/gana-con-nosotros'); ?>">Gana con nosotros</a></li>
					<li><a href="<?php echo URL::site('oportunidad/tipos-socios'); ?>">Tipos de socios</a></li>
					<li><a class="active" href="<?php echo URL::site('oportunidad/conferencias'); ?>">Conferencias</a></li>
                </ul>
            </div>

            <h3>Conferencias</h3>
            <p>Si quieres conocer mejor cómo funciona One Vision Life y saber cómo obtener ingresos extras solo por recomendarnos a tus amigos, asiste a una de nuestras conferencias de negocio.</p>

            <h1 class="titulo">¡Aprovecha esta oportunidad!</h1>
                <div id="content_conferencias">
                    <div class="sala">
                        <ul>
                            <li><strong>Día: </strong>todos los días</li>
                            <li><strong>Hora: </strong>22:30 hora española <br>(consulta <a href="#">hora mundial</a>)</li>
                            <li><strong>Sala de Conferencias: </strong><a href="#">haz click aquí</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="colum_produc_b">
            <?php echo View::factory('ci/view_sidebar')->set('var_sidebar','')?>
        </div>
    </div>
</div>