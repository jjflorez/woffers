<div id="c_home">

    <div id="container_lineas">
        <ul id="pestanas_lineas">
            <li><a href="<?php echo URL::site('sobre/que-es'); ?>">Sobre One Vision Life</a></li>
            <li class="selected"><a href="#">Oportunidad de Negocios</a></li>
            <li><a href="<?php echo URL::site('content'); ?>/comercios">One Vision Life Bussiness</a></li>
        </ul>
    </div>

    <div class="container">
        <div id="colum_produc_a">
            <div id="conferencias">
            <div class="bread">
                <ul>
                    <li><a class="active" href="<?php echo URL::site('oportunidad/gana-con-nosotros'); ?>">Gana con nosotros</a></li>
                    <li><a href="<?php echo URL::site('oportunidad/tipos-socios'); ?>">Tipos de socios</a></li>
                    <li><a href="<?php echo URL::site('oportunidad/conferencias'); ?>">Conferencias</a></li>
                </ul>
            </div>

            <h3>Gana con nosotros</h3>
            <p>¿Cuantas veces has recomendado a tus amigos algo que te ha gustado? ¿Y cuantas veces te han pagado por ello?
                Ahora en One Vision Life por recomendar como haces cada día en persona o en las redes sociales vas a ganar, y no solo una vez, ¡para siempre!</p>

                <img src="<?php echo URL::base();?>public/img/comoganas_content.jpg" >
                <p style="text-align: right;">
                    <a href="<?php echo URL::site('oportunidad/tipos-socios'); ?>"><img src="<?php echo URL::base();?>public/img/btn_ventajas.jpg" ></a>
                </p>
            </div>
        </div>
        <div id="colum_produc_b">
            <?php echo View::factory('ci/view_sidebar')->set('var_sidebar','')?>
        </div>
    </div>
</div>