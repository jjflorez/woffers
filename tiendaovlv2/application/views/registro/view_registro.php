<div id="c_home">
	<div class="container container_home">
		<h1>Registro</h1>
		<p>Hazte socio de One Vision LIfe y disfrita de los miles de beneficios que tenemos para tu. Y accede a las mejores ofertas, ocio, formación y grandes oportunidades.<br/>
			Para registrarte, especifica si eres persona, empresa o autónomo y rellena los datos del siguiente formulario ¡Y ya comienzas a ganar!</p>
		<p>Recuerad que te esás registrando como usuarios, si eres <strong>un comercio</strong> haz <a href="#">clic aquí</a> para registrarte</p>
		<div class="register-steps">
			<a href="#1" class="first-step">1</a>
			<a href="#2" class="lasta-step">2</a>
		</div>
		<div class="row-fluid">
			<div class="span3">
				<h3>Datos de tu sponsor</h3>
			</div>
			<div class="span9">
				<div class="register-block">
					<h4>Te estás registrnado como: Socio Oro</h4>
					<p>Si quieres cambiar tu membresia puedes hacerlo desde aquí</p>
				</div>
			</div>
		</div>
	</div>
</div>