<?php
/**
 * Created by JetBrains PhpStorm.
 * User: GersonM
 * Date: 07/04/12
 * Time: 02:01 AM
 * To change this template use File | Settings | File Templates.
 */

class Quickdev_Api extends Controller
{
	public function before()
	{
		$app_client = 'ID_APP_CLIENTE';

		/*if(!isset($_REQUEST['token'])){
			echo '<h1>Access denied!</h1>';
			echo '<p>This section is only for partners</p>';
			echo '<pre>' . md5(sha1('passUserApp') . $app_client) . '</pre>';
			exit;
		}*/

		/*if($_SERVER['REMOTE_ADDR'] != '216.224.174.59' && $_SERVER['REMOTE_ADDR'] != '127.0.0.1' && $_SERVER['REMOTE_ADDR'] != '190.43.56.87' && $_SERVER['REMOTE_ADDR'] != '169.254.10.89')
		{
			if(isset($_SERVER["HTTP_REFERER"])){
				if(strpos($_SERVER['HTTP_REFERER'], 'onevisionlife.com') === false)
				{
					redirect('http://onevisionlife.com');
				}
			}
		}*/

		if(isset($_SERVER['HTTP_REFERER'])){
			$host_referencia = $_SERVER['HTTP_REFERER'];
			$segmentos = explode('/', $host_referencia);
			if(count($segmentos) >= 2){
				$this->response->headers('Access-Control-Allow-Origin', 'http://'. $segmentos[2]);
				$this->response->headers('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
			}
		}

		parent::before();
	}

	public function checkPermissions()
	{
		return true;
	}

	public function makeResponse(Quickdev_Response $_response)
	{
		if($this->request->is_ajax()){
			header("Content-Type: application/json");
			$this->response->body($_response->renderJSON());
		}else{
			if(isset($_REQUEST['doredirect']))
			{
				$type = 'info';
				switch($_response->status->code)
				{
					case 200:
						$type = 'info';
						break;
					case 400:
						$type = 'danger';
						break;
					case 300:
						$type = 'danger';
						break;
				}

				$extra = '';

				if(Kohana::$environment == Kohana::DEVELOPMENT){
					$extra .= '<br/><pre>' . Kohana_Debug::dump($_response->status->details, false) . '</pre><br/>';
					$extra .= $_response->status->debug_string;
				}

				Message::add($type, $_response->status->description . $extra);
				$this->request->redirect($this->request->referrer());
			}else{
				header("Content-Type: application/json");
				$this->response->body($_response->renderJSON());
				return;
			}
		}
	}


	//Acciones generales

	public function insertUpdate($_validation, $_model, $_response = null)
	{
		$model = ORM::factory($_model);
		if($_response == null)
			$response = new Quickdev_Response();
		else
			$response = $_response;
      
		if($_validation->check())
		{           
			$model->values($_validation->data());
			$result = $model->save();
			if($result)
			{
				$response->data = $model->as_array();
				$response->data['id'] = $model->id;
			}else{
				$response->status->setStatus('ERROR');
			}
		}else{
			$response->status->setStatus('PARAMS');
			$response->status->details = $_validation->errors(null, FALSE);
		}

		return $response;
	}
}
