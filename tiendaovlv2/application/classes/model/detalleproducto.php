<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Detalleproducto extends ORM
{
	protected $_belongs_to = array(
		'carrito' => array(),
		'producto' => array(),
        'direccion' => array(),
	);

    public function count_producto($id_carrito, $id_producto)
    {
        $query = DB::select(array('COUNT("producto_id")', 'total_producto'))
                    ->from('detalleproductos')
                    ->where('producto_id', '=', $id_producto)
                    ->and_where('carrito_id', '=', $id_carrito);
        $_count = $query->execute()->get('total_producto');

        return $_count;
        //return Debug::vars((string) $query);
    }

    public function total_monto($id_carrito)
    {
        $query = DB::select(array('SUM("monto")', 'total_monto'))
                    ->from('detalleproductos')
                    ->where('carrito_id', '=', $id_carrito);

        $_total = $query->execute()->get('total_monto');

        return $_total;
    }

    public function count_gift($id_carrito, $id_producto)
    {
        $query = DB::select(array('COUNT("is_gift")', 'total_gift'))
            ->from('detalleproductos')
            ->where('producto_id', '=', $id_producto)
            ->and_where('carrito_id', '=', $id_carrito)
            ->and_where('is_gift', '=', 1);
        $_count = $query->execute()->get('total_gift');

        return $_count;
        //return Debug::vars((string) $query);
    }
}