<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_ProductoFile extends ORM
{
	protected $_belongs_to = array(
		'file' => array(),
	);

	public static function cleanImages($_productoid)
	{
		$m_prof = new Model_ProductoFile();
		$res = $m_prof->where('producto_id', '=', $_productoid)->find_all();
		foreach($res as $reg)
		{
			//echo $reg->producto_id;
			$reg->delete();
		}
	}

}