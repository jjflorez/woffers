<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Coaching extends Controller_Template
{
	public $template = 'ci/view_template';
	private $_tiendas = NULL;
	private $_pathlinea = 'coaching';
	private $_linea_id = 4;

	public function before()
	{
		$m_tiendas = new Model_Tienda();
		$this->_tiendas = $m_tiendas->where('linea_id', '=', $this->_linea_id)->find_all();

		parent::before();

		$res_provincias = ORM::factory('ubicacion')->and_where_open()->and_where('parent_id', '=', 1)->and_where('status', '=', 1)->and_where_close()->find_all();

		$m_ciudades = new Model_Ubicacion();

		$m_ciudades->or_where_open();
		foreach($res_provincias as $pro)
			$m_ciudades->or_where('parent_id', '=', $pro->id);

		$m_ciudades->or_where_close();

		$m_ciudades->and_where('status', '=', 1);
		$m_ciudades->order_by('nombre', 'asc');
		$ciudades = $m_ciudades->find_all();

		$session = Session::instance();
		$current = 7;
        if(!$session->get('id_ciudad'))
            $session->set('id_ciudad', $current);

		if($session->get('id_ciudad'))
			$current = ORM::factory('ubicacion')->where('id', '=', $session->get('id_ciudad'))->find();

		$this->template->provincias_selected = $current;
		$this->template->provincias = $res_provincias;
		$this->template->lista_ciudades = $ciudades;
	}

	public function action_index()
	{
		$m_productos = new Model_Producto();
        $session = Session::instance();

        if($session->get('id_ciudad'))
            $id_ciudad = $session->get('id_ciudad');

		$productos = array();
		if(count($this->_tiendas) > 0)
		{
			$productos = $m_productos->where('tienda_id', '=', $this->_tiendas[0]->id)
                ->and_where_open()
                ->where('oferta_ubicacion_ciudad', '=', $id_ciudad)
                ->or_where('oferta_ubicacion_ciudad', '=', '-2')
                ->and_where_close()
				->where('oferta_publicacion', '<=', date('Y-m-d h:i:s'))
				->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))
                ->where('status', '=', 1)
				->order_by('id', 'desc')->find_all();
		}
        $this->template->menu = View::factory('ci/view_menu')->set('tiendas', $this->_tiendas);
		$this->template->content = View::factory('view_' . $this->_pathlinea)
			->set('productos', $productos)
			->set('tiendas', $this->_tiendas);

	}

	public function action_tienda()
	{
		$vista = 'view_' . $this->_pathlinea;
		$tienda_code = $this->request->param('param1');
		$tienda = ORM::factory('tienda')->where('code', '=', $tienda_code)->find();

		$m_productos = new Model_Producto();

		$productos = array();
		if($tienda->loaded())
		{
			$productos = $m_productos->where('tienda_id', '=', $tienda->id)
				->where('oferta_publicacion', '<=', date('Y-m-d h:i:s'))
				->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))
				->order_by('id', 'desc')->find_all();
			if($tienda->code == 'permanente')
			{
				$vista = 'view_permanentes';
			}
		}else{
			$this->request->redirect($this->_pathlinea);
		}

		$this->template->content = View::factory($vista)
			->set('productos', $productos)
			->set('tiendas', $this->_tiendas);
	}

	public function action_producto()
	{
		$producto_id = $this->request->param('param1');
		$producto = new Model_Producto($producto_id);

		if($producto->loaded())
		{


			$this->template->content = View::factory('view_producto_detalle')
				->set('producto', $producto)
				->set('tiendas', $this->_tiendas);
		}else{
			$this->request->redirect($this->_pathlinea);
		}
	}

} // End Home
