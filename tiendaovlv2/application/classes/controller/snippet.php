<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Snippet extends Controller
{

	public function action_index()
	{
		$this->template->t_content = View::factory('view_play');
	}

    public function action_share($_codevideo = null)
    {
		$m_uvideo = new Model_Uservideo();

		if($_codevideo == null)
			$_codevideo = $_GET['video'];

		$m_uvideo->where('code', '=', $_codevideo);
		$user_video = $m_uvideo->find_all();

		$view = View::factory('snippets/sp_compartir')
				->set('uservideo', $user_video[0]);

		$this->response->body($view);
    }

	public function action_minicesta()
	{
		$m_carrito = new Model_Carrito();

		$carrito = null;
		if(Auth::instance()->logged_in()){
			$carrito = $m_carrito->where('user_id','=',Auth::instance()->get_user()->id)->where('status', '=', 0)->find();
		}else{
			if(Session::instance()->get('IDCARRITO'))
				$carrito = $m_carrito->where('id','=', Session::instance()->get('IDCARRITO'))->find();

		}


		$view = View::factory('snippets/sp_mini_cesta')
			->set('carrito', $carrito);

		$this->response->body($view);
	}

} // End Home
