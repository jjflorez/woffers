<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cesta extends Controller_Template
{
    public $template = 'ci/view_template';
    private $_redirect_cesta = 'http://dev.onevisionlife.es/tiendaovlv2/cesta/ok';

    public function before()
    {
		parent::before();

		$res_provincias = ORM::factory('ubicacion')->and_where_open()->and_where('parent_id', '=', 1)->and_where('status', '=', 1)->and_where_close()->find_all();

		$m_ciudades = new Model_Ubicacion();

		$m_ciudades->or_where_open();
		foreach($res_provincias as $pro)
			$m_ciudades->or_where('parent_id', '=', $pro->id);

		$m_ciudades->or_where_close();

		$m_ciudades->and_where('status', '=', 1);
		$m_ciudades->order_by('nombre', 'asc');
		$ciudades = $m_ciudades->find_all();

		$session = Session::instance();
		$current = 7;
        if(!$session->get('id_ciudad'))
            $session->set('id_ciudad', $current);

		if($session->get('id_ciudad'))
			$current = ORM::factory('ubicacion')->where('id', '=', $session->get('id_ciudad'))->find();

		$this->template->provincias_selected = $current;
		$this->template->provincias = $res_provincias;
		$this->template->lista_ciudades = $ciudades;
    }

    public function action_index()
    {
        $session = Session::instance();

		$carrito_id = $session->get('IDCARRITO');

		if(Auth::instance()->logged_in())
			$id_usuario = Auth::instance()->get_user()->id;
		else
            $this->request->current()->redirect('session/generate'); //$this->request->redirect('user/login');

		if(!$carrito_id){
        	if (Auth::instance()->logged_in()){
        	    $id_usuario = Auth::instance()->get_user()->id;
				$carrito_pendiente = ORM::factory('carrito')->where('user_id','=',Auth::instance()->get_user()->id)->where('status', '=', 0)->find();
				$carrito_id = $carrito_pendiente->id;
        	}
		}

        //if($carrito_id){

            $m_detalle = new Model_Detalleproducto();
            $_detalle = $m_detalle->where('carrito_id', '=', $carrito_id)
                ->group_by('producto_id')
                ->order_by('id')
                ->find_all();

            $_total_monto = $m_detalle->total_monto($carrito_id);

            $_direcciones = ORM::factory('direccion')->where('carrito_id', '=', $carrito_id)->find_all();
            $_direcciones2 = ORM::factory('direccion')->where('user_id', '=', $id_usuario)->find_all();

            //producto regalo
            $_pro_regalo = $m_detalle->where('carrito_id', '=', $carrito_id)
                ->and_where('is_gift', '=', 1)
                ->find_all();

            $_usuario = ORM::factory('user', $id_usuario);

            $this->template->content = View::factory('view_cesta')
                ->set('carrito_id', $carrito_id)
                ->set('carrito', new Model_Carrito($carrito_id))
                ->set('productos', $_detalle)
                ->set('total_monto', $_total_monto)
                ->set('direcciones', $_direcciones)
                ->set('direcciones2', $_direcciones2)
                ->set('prod_regalos', $_pro_regalo)
                ->set('usuario_id', $id_usuario)
                ->set('usuario', $_usuario);
        //}
    }

    public function action_prueba()
    {
        if(Auth::instance()->logged_in()){
            $id_usuario = Auth::instance()->get_user()->id;

            $m_carrito = new Model_Carrito();
            $carritos = $m_carrito->where('user_id', '=', $id_usuario)->find_all();

            $m_detalle_pro = new Model_Detalleproducto();

            foreach($carritos as $_car){
                $_car = $_car->as_array();
                $m_detalle_pro->or_where('carrito_id', '=', $_car['id']);
            }
            $m_detalle_pro->order_by('carrito_id');

            $_productos = $m_detalle_pro->find_all();

            echo $m_detalle_pro->last_query();
            foreach($_productos as $_pro){
                echo "<pre>";
                print_r($_pro->as_array());
            }

            foreach($carritos as $_car){
                $_car = $_car->as_array();
                $carrito_id = $_car['id'];
                /*echo $carrito_id;
                echo "<pre>";
                print_r($_car);*/
            }

        }else{
            $id_usuario = NULL;
        }

        echo "hola " . $id_usuario;
    }

	public function action_felicidades()
	{
		$usuario_id = $this->request->post('idusuario');
		$total = $this->request->post('total');
		$email = $this->request->post('email');
		$num_orden = $this->request->post('num_orden');
		$codigo = $this->request->post('codigo'); //Respuesta del TPV
		$carrito_id = $this->request->post('id_carr');

		$carrito = new Model_Carrito($carrito_id);

		if($carrito->loaded())
		{
			if($this->checkStatusTPV($codigo))
			{
				$carrito->status = Quickdev_Status::ACTIVE;
			}else{
				$carrito->status = Quickdev_Status::ERROR;
			}

			if($carrito->user_id == $usuario_id){
				$carrito->save();
			}

		}

		$this->template->content = View::factory('view_cesta_felicidades');
	}

	private function checkStatusTPV($_codigo)
	{
		return TRUE;
	}

    public function action_error()
    {
        //if($_POST) echo "<pre>"; print_r($_POST);
        //echo "<pre>"; print_r($_SERVER); die();
        $_id = $this->request->param("param1");
        $_carrito = ORM::factory('carrito', $_id);

        if($_carrito->id){

            //temporal
            $session = Session::instance();
            $session->delete('IDCARRITO');

            $_carrito->status = 1;
            $_carrito->pago = date('Y-m-d H:i:s');
            $_carrito->update();

            $productos = ORM::factory('detalleproducto')->where('carrito_id', '=', $_carrito->id)->find_all();
            $productos_array = array();
            $total_comision = 0;

            foreach($productos as $p){
                $productos_array[] = $p->id;

                $pro = ORM::factory('producto', $p->producto_id);

                if($pro->oferta_comision_ovl == -1 || $pro->oferta_comision_ovl == ''){
                    $total_comision += ($pro->get_oferta_precio_final() * $pro->tienda->comision_ovl) / 100;
                }else{
                    $total_comision += ($pro->get_oferta_precio_final() * $pro->oferta_comision_ovl) / 100;
                }
            }

            $this->_addcomision($total_comision, $_carrito->id);

            $_ids = urlencode(serialize($productos_array));

            $_redirect = $this->_redirect_cesta;
            $_url = 'http://dev.onevisionlife.es/backoffice_user/document/send_oferta/?i='.$_ids.'&redirect='.$_redirect;

            $this->request->redirect($_url);
        }

        $this->template->content = View::factory('view_cesta_error');
    }

    public function action_confirm()
    {
        $_id = $this->request->param("param1");
        $_carrito = ORM::factory('carrito', $_id);

        if($_carrito->id){
            $_carrito->status = 1;
            $_carrito->pago = date('Y-m-d H:i:s');
            $_carrito->update();
        }
        $this->template->content = View::factory('view_cesta_confirm');
    }

    public function action_ok()
    {
        echo $this->request->referrer();
        $this->template->content = View::factory('view_cesta_confirm');/*
        if($this->request->referrer() == $this->_redirect_cesta){
           $this->template->content = View::factory('view_cesta_confirm');
        }else{
           $this->request->redirect('/');
        }*/
    }

    private function _addcomision($comision=0, $idcompra=0)
    {
        $username = Auth::instance()->get_user()->username;
        //$url = "http://localhost/backoffice/api/bono/add/".$username."/".$comision."/".$idcompra;

        $url = "http://dev.onevisionlife.es/backoffice/api/bono/add/".$username."/".$comision."/".$idcompra;
        $valor1 = "valor0001";
        $valor2 = "valor0002";
        $parametros_post = 'parametro1='.urlencode($valor1).'&parametro2='.urlencode($valor2);

        $sesion = curl_init($url);
        curl_setopt ($sesion, CURLOPT_POST, true);
        curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post);
        curl_setopt($sesion, CURLOPT_HEADER, false);
        curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec($sesion);
        curl_close($sesion);

        var_dump($respuesta);
    }
}