<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Carrito extends Controller_Template
{
	public $template = 'ci/view_template';

	public function before()
	{
		$session = Session::instance();

		$inactividad = time() - $session->get('LastUpdateCarrito');

		//Tiempo de duración del carrito para usuarios no logueados
		$tiempo_duracion_carrito = 120;

		if($inactividad > $tiempo_duracion_carrito)
		{
			$session->delete('IDCARRITO');
		}else{
			$carrito = new Model_Carrito($session->get('IDCARRITO'));

			/*if($carrito->loaded())
				$session->set('LastUpdateCarrito', time());
			else
				$session->delete('IDCARRITO');*/
		}

		parent::before();
	}

	public function action_index()
	{
        $session = Session::instance();
        if($session->get('IDCARRITO')){
            echo "IDCARRITO: " . $session->get('IDCARRITO') . ' - ' . (time() - $session->get('LastUpdateCarrito'));
			$carrito = new Model_Carrito($session->get('IDCARRITO'));
			if($carrito->loaded())
			{
				echo '<br/>Existe en base de datos';

				$m_detalle = new Model_Detalleproducto();
				$lista = $m_detalle->where('carrito_id', '=', $session->get('IDCARRITO'))->find_all();
				echo '<ul>';
				if(count($lista) == 0) echo '<li>No hay productos en el carrito</li>';
				foreach($lista as $l)
				{
					echo '<li>' . $l->id . ' ' . $l->producto->oferta_titulo . '</li>';
				}
				echo '</ul>';
			}else{
				echo '<br/>No existe en base de datos';
			}
        }else{
			echo '<br/>No se ha creado un carrito';
		}

		$this->auto_render = false;
	}

    public function action_show()
    {
        $session = Session::instance();
/*
        $_xx = ORM::factory('detalleproducto', 27);

        echo $_xx->direccion->direccion;
        echo "<hr>";*/

        if($session->get('IDCARRITO')){
            $carrito_id = $session->get('IDCARRITO');

            $m_detalle = new Model_Detalleproducto();
            $_detalle = $m_detalle->where('carrito_id', '=', $carrito_id)
                        ->group_by('producto_id')
                        ->find_all();

            $_total_monto = $m_detalle->total_monto($carrito_id);
			echo $m_detalle->count_gift(4, 2) . "<br>";

            foreach($_detalle as $item){

                //$item->cantidad = $m_detalle->count_producto($carrito_id, $item->producto_id);

                echo $item->id . " | " . $item->carrito_id . " | " . $item->producto_id . " | " . $item->codigo_cupon
                    . " | " . $item->monto
                    . " | " . $item->carrito->id
                    . " || " . $item->producto->oferta_titulo
                    . " || " . $item->count_producto($carrito_id, $item->producto_id)
                    . "<br>";

            }
            echo count($_detalle);

            $_direcciones = ORM::factory('direccion')->where('carrito_id', '=', $carrito_id)->find_all();

            foreach($_direcciones as $_dir){
                //echo "<p>" . $_dir->direccion . "</p>";
            }


            $this->template->content = View::factory('carrito/view_carrito_show')
                                        ->set('carrito_id', $carrito_id)
                                        ->set('productos', $_detalle)
                                        ->set('total_monto', $_total_monto)
                                        ->set('direcciones', $_direcciones);

            //echo date('Y-m-d H:i:s');
        }
    }

    //http://localhost/tiendaovlv2/carrito/add_producto/1
    public function action_add_producto()
    {
        $session = Session::instance();
		$carrito = NULL;
        $id_producto = $this->request->param('param1');
		$id_usuario = NULL;
		$id_carrito = $session->get('IDCARRITO');

        if (Auth::instance()->logged_in()){
            $id_usuario = Auth::instance()->get_user()->id;
			if($id_carrito){
				//Model_Carrito::cleanOld($id_usuario);
				$carrito = new Model_Carrito($id_carrito);
				if($carrito->loaded())
				{
					$carrito->user_id = $id_usuario;
                    $carrito->created = date('Y-m-d H:i:s');
					$carrito->save();
				}
			}else{
				$carrito = ORM::factory('carrito')->where('user_id', '=', $id_usuario)->where('status', '=', Quickdev_Status::PENDING)->find();
				$id_carrito = $carrito->id;
			}
        }

        if($id_carrito){
            $_producto = ORM::factory('producto', $id_producto);

            if($_producto->loaded()){

                $_detalle = ORM::factory('detalleproducto');
                $_detalle->carrito_id = $id_carrito;
                $_detalle->producto_id = $_producto->id;
                $_detalle->codigo_referencia = $_producto->num_referencia;
                $_detalle->codigo_cupon = $this->u_codigo('c');
                $_detalle->codigo_validacion = $this->u_codigo('v');
                $_detalle->is_gift = 0;
                $_detalle->added = date('Y-m-d H:i:s');
                $_detalle->status = 1;
                $_detalle->monto = $_producto->get_oferta_precio_final();

                $_detalle->save();
            }
        }else{
            $_carrito = ORM::factory('carrito');
            $_carrito->user_id = $id_usuario;
            $_carrito->total_price = 0;
            $_carrito->created = date('Y-m-d H:i:s');
            $_carrito->status = 0;
            $_carrito->save();

            $session->set('IDCARRITO', $_carrito->id);
			$session->set('LastUpdateCarrito', time());

			$_producto = ORM::factory('producto', $id_producto);
            if($_producto->loaded()){
                $_detalle = ORM::factory('detalleproducto');
                $_detalle->carrito_id = $session->get('IDCARRITO');
                $_detalle->producto_id = $_producto->id;
                $_detalle->codigo_referencia = $_producto->num_referencia;
                $_detalle->codigo_cupon = $this->u_codigo('c');
                $_detalle->codigo_validacion = $this->u_codigo('v');
                $_detalle->is_gift = 0;
                $_detalle->added = date('Y-m-d H:i:s');
                $_detalle->status = 1;
                $_detalle->monto = $_producto->get_oferta_precio_final();

                $_detalle->save();
            }
        }

        //$this->_update_total($session->get('IDCARRITO'));
        if($session->get('IDCARRITO') == "")
            $this->_update_total($id_carrito);
        else
            $this->_update_total($session->get('IDCARRITO'));

		if($this->request->is_ajax())
		{
			$this->auto_render = false;
			$r = new Quickdev_Response();
			$r->data = $_detalle;
			echo $r->renderJSON();
		}else{
			$this->request->redirect('cesta');
		}
    }

    //http://localhost/tiendaovlv2/carrito/update_cantidad/3/2/1
    public function action_update_cantidad()
    {
        $carrito_id = $this->request->param('param1');
        $producto_id = $this->request->param('param2');
        $cantidad = $this->request->param('param3');

        $m_detalle = new Model_Detalleproducto();
        $count = $m_detalle->count_producto($carrito_id, $producto_id);

        if($cantidad > $count){
            $cant_agrega = $cantidad - $count;
            //echo "es mayor, hay que agregar ".$cant_agrega." productos<br>";

            $_producto = ORM::factory('producto', $producto_id);

            for($i=0; $i<$cant_agrega; $i++){
                $_detalle = ORM::factory('detalleproducto');
                $_detalle->carrito_id = $carrito_id;
                $_detalle->producto_id = $producto_id;
                $_detalle->codigo_referencia = $_producto->num_referencia;
                $_detalle->codigo_cupon = $this->u_codigo('c');
                $_detalle->codigo_validacion = $this->u_codigo('v');
                $_detalle->is_gift = 0;
                $_detalle->added = date('Y-m-d H:i:s');
                $_detalle->status = 1;
                $_detalle->monto = ($_producto->get_oferta_precio_final());

                $_detalle->save();
                //echo "hola " . $i ."<br>";
            }
        }else if($cantidad < $count){
            $cant_borra = $count - $cantidad;
            //echo "es menor, hay que borrar ".$cant_borra." productos<br>";

            $productos = $m_detalle->where('producto_id', '=', $producto_id)->and_where('carrito_id', '=', $carrito_id)->find_all();

            foreach($productos as $item){
                if($cant_borra > 0){
                    $_pro = ORM::factory('detalleproducto', $item->id);
                    $_pro->delete();

                    $cant_borra --;
                }
            }
        }

        $this->_update_total($carrito_id);

        //$carrito_id, $producto_id, $cantidad
    }

    //http://localhost/tiendaovlv2/carrito/add_gift_direccion/4/2
    public function action_add_gift_direccion()
    {
        $carrito_id = $this->request->param('param1');
        $producto_id = $this->request->param('param2');

        $session = Session::instance();

        if($session->get('IDUSUARIO')){
            $id_usuario = $session->get('IDUSUARIO');
        }else{
            $id_usuario = NULL;
        }

        $m_direccion = new Model_Direccion();
        $m_direccion->user_id = $id_usuario;//Session::instance()->get('IDUSUARIO');
        $m_direccion->empresa = $_POST['empresa'];
        $m_direccion->nombre = $_POST['nombre'];
        $m_direccion->apellidos = $_POST['apellidos'];
        $m_direccion->telefono = $_POST['telefono'];
        $m_direccion->direccion = $_POST['direccion'];
        $m_direccion->tipo_direccion = $_POST['tipo_direccion'];
        $m_direccion->codigo_postal = $_POST['codigo_postal'];
        $m_direccion->poblacion = $_POST['poblacion'];
        $m_direccion->provincia = $_POST['provincia'];
        $m_direccion->pais_id = -1;

        if($m_direccion->save()){
            //echo "last id " . $m_direccion->id;

            $m_detallepro = new Model_Detalleproducto();
            $_pro = $m_detallepro->where('carrito_id', '=', $carrito_id)
                ->and_where('producto_id', '=', $producto_id)
                ->and_where('is_gift', '=', 0)
                ->find();

            if($_pro->id){
                $_detalle = ORM::factory('detalleproducto', $_pro->id);
                $_detalle->direccion_id = $m_direccion->id;
                $_detalle->is_gift = 1;
                $_detalle->save();
            }echo $carrito_id . " - hola - ".$_POST['empresa'];
        }

        $this->auto_render = false;
    }

    //http://localhost/tiendaovlv2/carrito/add_direccion/4
    public function action_add_direccion()
    {
        $carrito_id = $this->request->param('param1');

        $m_direccion = new Model_Direccion();
		$_POST['user_id'] = Auth::instance()->get_user()->id;
		$m_direccion->values($_POST);

        if($m_direccion->save()){
            echo json_encode($m_direccion);
			$carrito = new Model_Carrito($carrito_id);
			$carrito->direccion_id = $m_direccion->id;
			$carrito->update();
        }

        $this->auto_render = false;
    }

    //http://localhost/tiendaovlv2/carrito/update_direccion/10/5
    public function action_update_direccion()
    {
        $direccion_id = $this->request->param('param1');
        $carrito_id = $this->request->param('param2');

        $dir = ORM::factory('direccion')->where('carrito_id', '=', $carrito_id)->find();

        if ($dir->loaded()){
            $dir->carrito_id = 0;

            $dir->save();
        }

        $direccion = ORM::factory('direccion', $direccion_id);
        $direccion->carrito_id = $carrito_id;

        $direccion->save();

        $this->auto_render = false;
    }

    //http://localhost/tiendaovlv2/carrito/delete_producto/2/1
    public function action_delete_producto()
    {
        $carrito_id = $this->request->param('param1');
        $producto_id = $this->request->param('param2');

        $m_detalle = new Model_Detalleproducto();
        $_productos = $m_detalle->where('carrito_id', '=', $carrito_id)
                                ->and_where('producto_id', '=', $producto_id)
                                ->find_all();

        foreach($_productos as $_pro){
            $_dir = "";
            if($_pro->direccion_id > 0){
                $_dir = $_pro->direccion_id;
                ORM::factory('direccion', $_pro->direccion_id)->delete();
            }
            $_pro->delete();
        }

        $this->_update_total($carrito_id);

		if($this->request->is_ajax())
		{
			$this->auto_render = false;
			$r = new Quickdev_Response();
			//$r->data =;
			echo $r->renderJSON();
		}else{
			$this->request->redirect('cesta');
		}
    }

    private function _update_total($carrito_id)
    {
        //echo "hola " . $carrito_id ;
        $m_detalle = new Model_Detalleproducto();
        $_total = $m_detalle->total_monto($carrito_id);

        $_carrito = ORM::factory('carrito', $carrito_id);
        $_carrito->total_price = $_total;

        $_carrito->save();
        //echo "CARRITP :" . $_total;
    }

	public function action_update_detalle_producto()
	{
		$id_detalle = $this->request->post('id_detalle_producto');

		$detalle = new Model_Detalleproducto($id_detalle);

		//print_r($this->request->post());die();

		if($detalle->loaded())
		{
			/*$detalle->regalo_send = $this->request->post('oferta_send');
			$detalle->regalo_nombre = $this->request->post('oferta_nombre');
			$detalle->regalo_email = $this->request->post('oferta_email');
			$detalle->regalo_mensaje = $this->request->post('oferta_mensaje');*/

			$detalle->values($this->request->post());
			$detalle->update();
		}

		$this->request->redirect('cesta');
	}

	public function action_delete_gift()
	{
		$id_detalle = $this->request->param('param1');

		$detalle = new Model_Detalleproducto($id_detalle);
		if($detalle->loaded())
		{
			$detalle->is_gift = 0;
			$detalle->direccion_id = -1;
			$detalle->save();
		}

		$this->request->redirect('cesta');
	}
/*
    public function action_versession()
    {
        echo Session::instance()->get('nombre');
        Session::instance()->delete('nombre');
    }
*/
    private function u_codigo($type)
    {
        $str = strtoupper(uniqid());
        if($type == 'c'){
            return 'CC'. substr($str, 0, 10);
        }else{
            return 'CV'. time();
        }
    }

    public function action_update_factura()
    {
        if($_POST){
            $_id = $this->request->post('ic');
            $_f = $this->request->post('f');
            $_carrito = ORM::factory('carrito', $_id);

            if($_carrito->id){
                $_carrito->factura = $_f;
                $_carrito->update();
            }
        }

        $this->auto_render = false;
    }
}