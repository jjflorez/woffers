<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Controller_Template
{
	public $template = 'ci/view_template';

	public function before()
	{
		parent::before();

		$res_provincias = ORM::factory('ubicacion')->and_where_open()->and_where('parent_id', '=', 1)->and_where('status', '=', 1)->and_where_close()->find_all();

		$m_ciudades = new Model_Ubicacion();

		$m_ciudades->or_where_open();
		foreach($res_provincias as $pro)
			$m_ciudades->or_where('parent_id', '=', $pro->id);

		$m_ciudades->or_where_close();

		$m_ciudades->and_where('status', '=', 1);
		$m_ciudades->order_by('nombre', 'asc');
		$ciudades = $m_ciudades->find_all();

		$session = Session::instance();
		$current = 7;

        if(!$session->get('id_ciudad'))
            $session->set('id_ciudad', $current);

		if($session->get('id_ciudad'))
			$current = ORM::factory('ubicacion')->where('id', '=', $session->get('id_ciudad'))->find();

		$this->template->provincias_selected = $current;
		$this->template->provincias = $res_provincias;
		$this->template->lista_ciudades = $ciudades;
	}

	public function action_index()
	{
        if($this->request->query('u')){
            $_u = $this->request->query('u');

            $user = ORM::factory('User')->where('username', '=', $_u)->find();

            if($user->loaded()){
                $_url = "http://dev.onevisionlife.es/backoffice/api/user/get/" . $user->username;
                $_data = json_decode(file_get_contents($_url));
                $_data = $_data->data[0];

                $_datos = $_data->NOMBRES ." ". $_data->APEPATERNO ." ". $_data->APEMATERNO ." | ". $_data->EMAIL ." | ".
                            $_data->NOMBREPAIS ." | ".
                            $_data->TIPOMEMBRESIA;

                //Cookie::set('user_email', $user->email);
                Cookie::set('user_datos', $_datos);

                $this->request->redirect('/');
            }
        }

        $m_productos = new Model_Producto();
        $session = Session::instance();

        if($session->get('id_ciudad'))
            $id_ciudad = $session->get('id_ciudad');

		$t_offers = ORM::factory('tienda')->where('linea_id', '=', 1)->find();
		$t_outlet = ORM::factory('tienda')->where('linea_id', '=', 2)->find();
		$t_travel = ORM::factory('tienda')->where('linea_id', '=', 3)->find();
		$t_coaching= ORM::factory('tienda')->where('linea_id', '=', 4)->find();
        $t_village = ORM::factory('tienda')->where('code', '=', 'permanente')->find();

        $offers = $m_productos->where('tienda_id', '=', $t_offers->id)
                            ->and_where_open()
                            ->where('oferta_ubicacion_ciudad', '=', $id_ciudad)
                            ->or_where('oferta_ubicacion_ciudad', '=', '-2')
                            ->and_where_close()
                            ->where('oferta_publicacion', '<=', date('Y-m-d h:i:s'))
                            ->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))
                            ->where('status', '=', 1)
                            ->order_by('id', 'desc')->find_all();

        $outlet = $m_productos->where('tienda_id', '=', $t_outlet->id)->and_where_open()->where('oferta_ubicacion_ciudad', '=', $id_ciudad)->or_where('oferta_ubicacion_ciudad', '=', '-2')->and_where_close()->where('oferta_publicacion', '<=', date('Y-m-d h:i:s'))->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('status', '=', 1)->order_by('id', 'desc')->find_all();
        $travel = $m_productos->where('tienda_id', '=', $t_travel->id)->and_where_open()->where('oferta_ubicacion_ciudad', '=', $id_ciudad)->or_where('oferta_ubicacion_ciudad', '=', '-2')->and_where_close()->where('oferta_publicacion', '<=', date('Y-m-d h:i:s'))->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('status', '=', 1)->order_by('id', 'desc')->find_all();
        $coaching = $m_productos->where('tienda_id', '=', $t_coaching->id)->and_where_open()->where('oferta_ubicacion_ciudad', '=', $id_ciudad)->or_where('oferta_ubicacion_ciudad', '=', '-2')->and_where_close()->where('oferta_publicacion', '<=', date('Y-m-d h:i:s'))->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('status', '=', 1)->order_by('id', 'desc')->find_all();

        $village = $m_productos->where('tienda_id', '=', $t_village->id)->and_where_open()->where('oferta_ubicacion_ciudad', '=', $id_ciudad)->or_where('oferta_ubicacion_ciudad', '=', '-2')->and_where_close()->where('oferta_publicacion', '<=', date('Y-m-d h:i:s'))->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('status', '=', 1)->limit(3)->order_by('id', 'desc')->find_all();

		$this->template->content = View::factory('view_home')
            ->set('offers_p', $offers)
            ->set('outlet_p', $outlet)
            ->set('travel_p', $travel)
            ->set('coaching_p', $coaching)
            ->set('village_p', $village);
	}

    public function action_set_ciudad()
    {
        $_id = $this->request->param('param1');
        $_controller = $this->request->param('param2');

        //echo $_id . "<br>" . $_controller; die();
        $session = Session::instance();
        $session->set('id_ciudad', $_id);

        $this->request->redirect('/'.$_controller);
    }

	public function action_producto()
	{
		$id_pro = $this->request->param('param1');

		$producto = new Model_Producto($id_pro);
		if($producto->loaded())
		{
			$linea = $producto->tienda->linea;
			$this->request->redirect($linea->url_path . 'producto/' . $id_pro);
		}else{
			$this->request->redirect('/');
		}
	}

    public function action_login()
    {
        $this->template->content = View::factory('view_login');
    }
/*
    public function action_tipos_socios()
    {
        $this->template->content = View::factory('home/view_tipos_socios');
    }
    public function action_como_funcionaaa()
    {
        $this->template->content = View::factory('home/view_comofunciona');
    }
    public function action_gana_con_nosotros()
    {
        $this->template->content = View::factory('home/view_gana');
    }
    public function action_conferencias()
    {
        $this->template->content = View::factory('home/view_conferencias');
    }
*/
    public function action_content()
    {
        $_url = $this->request->param('param1');

        $_page = ORM::factory('page')->where('url', '=', $_url)->find();

        if($_page->loaded()){
            $this->template->content = View::factory('view_page')
                ->set('page', $_page);
        }
    }


} // End Home
