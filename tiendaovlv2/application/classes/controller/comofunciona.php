<?php defined('SYSPATH') or die('No direct script access.');

class Controller_comofunciona extends Controller_Template
{

    public $template = 'ci/view_template';
    private $_tiendas = NULL;
    private $_pathlinea = 'offers';
    private $_linea_id = 1;


    public function before()
    {
        parent::before();

        $res_provincias = ORM::factory('ubicacion')->where('parent_id', '=', 1)->find_all();

        $provincias = array();
        foreach($res_provincias as $pro)
        {
            $nuevo = $pro->as_array();

            $res_ciudades = ORM::factory('ubicacion')->where('parent_id', '=', $pro->id)->find_all();
            $nuevo['ciudades'] = array();
            foreach($res_ciudades as $ciu){
                array_push($nuevo['ciudades'], $ciu->as_array());
            }

            array_push($provincias, $nuevo);
        }

        $session = Session::instance();
        $current = 7;
        if(!$session->get('id_ciudad'))
            $session->set('id_ciudad', $current);

        if($session->get('id_ciudad'))
            $current = ORM::factory('ubicacion')->where('parent_id', '=', $pro->id)->find();

        $this->template->provincias_selected = $current;
        $this->template->provincias = $provincias;

    }

    public function action_index()
	{
        $this->template->content = View::factory('view_comofunciona');
    }



} // End Home
