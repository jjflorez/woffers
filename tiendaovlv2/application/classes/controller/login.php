<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Login extends Controller
{
	public $template = 'ci/view_login';

	public function before()
	{
		if(Auth::instance()->logged_in('admin'))
			$this->request->redirect('admin/');

		return parent::before();
	}

	/// url :  login/
	public function action_index()
	{
		$view = View::factory('ci/view_login');
		$page=$view->render();
		$this->request->response=$page;
		echo $this->request->response;
	}
	/// url :  login/auth/
    public function action_auth()
    {
    	$login_url='login/';

    	if( $this->request->post('l_user') ){
    		$post = $this->request->post();
    		$success = Auth::instance()->login($post['l_user'], $post['l_pass'],true);

			if ($success)
			{
				// Login successful, send to app
				try{
					$usuario = ORM::factory('User')->where('username', '=' ,$post['l_user'])->find();
					$session = Session::instance();
					Auth::instance()->force_login($post['l_user'], TRUE);
					Kohana_Cookie::set('TOVLID', $usuario->id);
					if(!Auth::instance()->logged_in()){
						$this->request->redirect('login/');
					}else{
						$this->request->redirect('admin/');
					}
				}catch (ORM_Validation_Exception $e){
					echo $e->getMessage()."<pre>";
					print_r($e->errors());
				}
			}
			else
			{
				// Login failed, send back to form with error message
				$this->request->redirect($login_url);
			}
    	}else{
    		$this->request->redirect($login_url);
    	}
    }


}
