
<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Pasarela extends Controller_Template
{
    public $template = 'ci/view_blank';
    //private $_redirect_cesta = 'http://dev.onevisionlife.es/tiendaovlv2/cesta/ok';

    public function before()
    {
		parent::before();
    }
	/*pasarela no funcional*/
    public function action_index()
    {
    	$data=$this->request->post('productos');
    	$producto=array();
    	$productos=array();
    	foreach($data as $i=>$p){
    		$producto['nombre']=$p['nombre'];
    		$producto['descripcion']=$p['descripcion'];
    		$producto['monto']=$p['monto'];
    		$productos[]=$producto;
    	}
    	$post=array(
    		'codigo' => '1',
    		'response' => $this->request->post('response'),
    		'id_carr' => $this->request->post('id_carr'),
    		'idusuario' => $this->request->post('idusuario'),
    		'orden' => $this->request->post('orden'),
    		'email' => $this->request->post('email'),
    		'nombre' => $this->request->post('nombre'),
    		'apellidos' => $this->request->post('apellidos'),
    		'productos' => $productos
    	);
    	$_POST=$post;
    	$request = Request::factory($post['response'])->method(Request::POST)->post($post);
    	$this->template->content = $request->execute();
    }
    /*
    	preevaluacion de datos enviados a pasarela
	*/
	public function action_pre_evaluar_compra()
	{
		$data=$this->request->post('productos');
		$producto=array();
		$productos=array();
		foreach($data as $i=>$p){
			$producto['nombre']=$p['nombre'];
			$producto['descripcion']=$p['descripcion'];
			$producto['monto']=$p['monto'];
			$productos[]=$producto;
		}
		$post=array(
			'codigo' => '1',
			'response' => $this->request->post('response'),
			'id_carr' => $this->request->post('id_carr'),
			'idusuario' => $this->request->post('idusuario'),
			'orden' => $this->request->post('orden'),
			'email' => $this->request->post('email'),
			'nombre' => $this->request->post('nombre'),
			'apellidos' => $this->request->post('apellidos'),
			'productos' => $productos
		);
		$_POST=$post;

		$carrito = ORM::factory('carrito')->where("id",'=',$post['id_carr'])->find();
		if(isset($carrito)==true){
			$id=$carrito->id;
			$productos = ORM::factory('detalleproducto')->where('carrito_id','=',$post['id_carr'])->find_all();
			$cantidad=count($productos);
			if($cantidad>0){
				$request = Request::factory($post['response'])->method(Request::POST)->post($post);
				$this->template->content = $request->execute();
			}else{
				Message::add('error', 'No hay productos en la cesta');
				$this->request->redirect($this->request->referrer());
			}

		}else{
			Message::add('error', 'No se pudo procesar compra');
			$this->request->redirect($this->request->referrer());
		}

	}
}
