<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Comercios_Ofertas extends Controller_Template
{
	public $template = 'ci/view_template';
	public function action_publicadas(){

		$session=Session::instance();

		$codigo=$session->get("ofertas_codigo");
		if($codigo==NULL){
			$codigo='-1';
		}
		$nombre=$session->get("ofertas_nombre");
		if($nombre==NULL){
			$nombre='-1';
		}
		$ciudad=$session->get("ofertas_ciudad");
		if($ciudad==NULL){
			$ciudad='-1';
		}
		$categoria=$session->get("ofertas_categoria");
		if($categoria==NULL){
			$categoria='-1';
		}
		$publicado=$session->get("ofertas_publicado");
		if($publicado==NULL){
			$publicado='-1';
		}
		$autor=$session->get("ofertas_autor");
		if($autor==NULL){
			$autor='-1';
		}
		$pago=$session->get("ofertas_pago");
		if($pago==NULL){
			$pago='-1';
		}

		$post=array(
			"codigo"=>$codigo,
			"autor"=>$autor,
			"publicado"=>$publicado,
			"pago"=>$pago,
			"provincia"=>$provincia,
			"ciudad"=>$ciudad,
			"barrio"=>$barrio,
			"distrito"=>$distrito,
			"categoria"=>$categoria
		);
		//echo json_encode($post);
		$query=ORM::factory('producto');

		if($codigo!='-1'){
			$query=$query->where("num_referencia","=",$codigo);
		}
		if($nombre!='-1'){
			$query=$query->where("oferta_titulo","like","%".$nombre."%");
		}
		if($ciudad!='-1'){
			$query=$query->where("oferta_ubicacion_ciudad","=",$ciudad);
		}
		if($categoria!='-1'){
			$query=$query->where("oferta_categoria_id","=",$categoria);
		}

		if($publicado!='-1'){
			$query=$query->where("status","=",$publicado);
		}

		if($autor!='-1'){
			$query=$query->where("autor_flag","=",$autor);
		}

		if($pago!='-1'){
			if($pago=='1'){
				$query=$query->where("oferta_precio_venta","=",'0');
			}else{
				if($pago=='2'){
					$query=$query->where("oferta_precio_venta",">",'0');
				}
			}

		}

		$ofertas=$query->find_all();
		$data=array();
		//$data=$ofertas->as_array();
		$count=1;
		foreach($ofertas as $i=>$oferta){
			$comercio=ORM::factory("comercio")->where("id","=",$oferta->comercio_id)->find();
			$data[$i]['codigo']=$oferta->num_referencia;
			$data[$i]['nombre']=$oferta->oferta_titulo;
			$data[$i]['publicado']='';
			$publicacion=explode(" ",$oferta->oferta_publicacion);
			$data[$i]['comienza']=$publicacion[0];
			$duracion=explode(" ",$oferta->oferta_duracion);
			$data[$i]['finaliza']=$duracion[0];
			$data[$i]['comercio']=$comercio->name;
			$actualizacion=explode(" ",$oferta->oferta_actualizacion);
			$data[$i]['actualizado']=$actualizacion[0];
			if($oferta->autor_flag==NULL || $oferta->autor_flag=='0'){
				$data[$i]['autor']=$comercio->name;
			}else{
				$data[$i]['autor']='admin';
			}
			$data[$i]['id']=$oferta->id;
			$data[$i]['numero']=$count;
			$count++;
		}

		$ciudades=ORM::factory('ubicacion')->where('parent_id','>','1')->find_all();
		$categorias=ORM::factory('categoria')->find_all();

		$publicados=array(
			array(
				'id'=>-1,
				'name'=>"Seleccionar una opcion"
			),
			array(
				'id'=>1,
				'name'=>"Borrador(edicion)"
			),
			array(
				'id'=>2,
				"name"=>"Borrador(finalizado)"
			),
			array(
				'id'=>3,
				'name'=>"Publicado(pendiente aprobacion)"
			),
			array(
				'id'=>4,
				'name'=>"Publicado(Aprobado)"
			),
			array(
				'id'=>5,
				'name'=>"Publicado(En venta)"
			),
			array(
				'id'=>6,
				'name'=>"Publicado(Finalizado)"
			)
		);
		$autores=array(
			array(
				'id'=>-1,
				'name'=>"Seleccionar una opcion"
			),
			array(
				'id'=>1,
				'name'=>"Admin"
			),
			array(
				'id'=>0,
				'name'=>"Comercio"
			)
		);
		$pagos=array(
			array(
				'id'=>-1,
				'name'=>"Seleccionar una opcion"
			),
			array(
				'id'=>1,
				'name'=>"Gratis"
			),
			array(
				'id'=>2,
				'name'=>"De Pago"
			)
		);

		$this->template->content = View::factory('comercios/view_lista_ofertas_publicadas')
				->set('ciudades',$ciudades)
				->set("categorias",$categorias)
				->set("publicados",$publicados)
				->set("autores",$autores)
				->set("pagos",$pagos)
				->set("ofertas",$data)
				->set("post",$post);
	}
	/*public function action_buscar_publicadas(){

		$session=Session::instance();

		$codigo=$session->get("ofertas_codigo");
		if($codigo==NULL){
			$codigo='-1';
		}
		$nombre=$session->get("ofertas_nombre");
		if($nombre==NULL){
			$nombre='-1';
		}
		$ciudad=$session->get("ofertas_ciudad");
		if($ciudad==NULL){
			$ciudad='-1';
		}
		$categoria=$session->get("ofertas_categoria");
		if($categoria==NULL){
			$categoria='-1';
		}
		$publicado=$session->get("ofertas_publicado");
		if($publicado==NULL){
			$publicado='-1';
		}
		$autor=$session->get("ofertas_autor");
		if($autor==NULL){
			$autor='-1';
		}

		$query=ORM::factory('producto');

		if($codigo!='-1'){
			$query=$query->where("num_referencia","=",$codigo);
		}
		if($nombre!='-1'){
			$query=$query->where("oferta_titulo","like","%".$codigo."%");
		}
		if($ciudad!='-1'){
			$query=$query->where("oferta_ubicacion_ciudad","=",$ciudad);
		}
		if($categoria!='-1'){
			$query=$query->where("oferta_categoria_id","=",$categoria);
		}

		if($publicado!='-1'){
			$query=$query->where("status","=",$publicado);
		}

		if($autor!='-1'){
			$query=$query->where("autor","=",$autor);
		}
		$ofertas=$query->find_all();

		$ciudades=ORM::factory('ubicacion')->where('parent_id','>','1')->find_all();
		$categorias=ORM::factory('categoria')->find_all();

		$publicados=array(
			array(
				'id'=>-1,
				'name'=>"Seleccionar una opcion"
			),
			array(
				'id'=>1,
				'name'=>"Borrador(edicion)"
			),
			array(
				'id'=>2,
				"name"=>"Borrador(finalizado)"
			),
			array(
				'id'=>3,
				'name'=>"Publicado(pendiente aprobacion)"
			),
			array(
				'id'=>4,
				'name'=>"Publicado(Aprobado)"
			),
			array(
				'id'=>5,
				'name'=>"Publicado(En venta)"
			),
			array(
				'id'=>6,
				'name'=>"Publicado(Finalizado)"
			)
		);
		$autores=array(
			array(
				'id'=>-1,
				'name'=>"Seleccionar una opcion"
			),
			array(
				'id'=>1,
				'name'=>"Admin"
			),
			array(
				'id'=>0,
				'name'=>"Comercio"
			)
		);
		$pagos=array(
			array(
				'id'=>-1,
				'name'=>"Seleccionar una opcion"
			),
			array(
				'id'=>1,
				'name'=>"Gratis"
			),
			array(
				'id'=>2,
				'name'=>"De Pago"
			)
		);


		$this->template->content = View::factory('comercios/view_lista_ofertas_publicadas')
				->set('ciudades',$ciudades)
				->set("categorias",$categorias)
				->set("publicados",$publicados)
				->set("autores",$autores)
				->set("pagos",$pagos)
				->set("ofertas",$ofertas);
	}*/
	public function action_filtros(){
		try{
			$codigo=$this->request->post('codigo');
		}catch(Exception $ex){
			$codigo='-1';
		}
		if($codigo==''){
			$codigo='-1';
		}

		try{
			$nombre=$this->request->post('nombre');
		}catch(Exception $ex){
			$nombre='-1';
		}
		if($nombre==''){
			$nombre='-1';
		}

		try{
			$ciudad=$this->request->post('ciudad');
		}catch(Exception $ex){
			$ciudad='-1';
		}
		if($ciudad==''){
			$ciudad='-1';
		}

		try{
			$categoria=$this->request->post('categoria');
		}catch(Exception $ex){
			$categoria='-1';
		}
		if($categoria==''){
			$categoria='-1';
		}

		try{
			$publicado=$this->request->post('publicado');
		}catch(Exception $ex){
			$publicado='-1';
		}
		if($publicado==''){
			$publicado='-1';
		}

		try{
			$autor=$this->request->post('autor');
		}catch(Exception $ex){
			$autor='-1';
		}
		if($autor==''){
			$autor='-1';
		}

		try{
			$pago=$this->request->post('pago');
		}catch(Exception $ex){
			$pago='-1';
		}
		if($pago==''){
			$pago='-1';
		}

		$session=Session::instance();

		$session->set("ofertas_codigo",$codigo);
		$session->set("ofertas_nombre",$nombre);
		$session->set("ofertas_ciudad",$ciudad);
		$session->set("ofertas_categoria",$categoria);
		$session->set("ofertas_publicado",$publicado);
		$session->set("ofertas_autor",$autor);
		$session->set("ofertas_pago",$pago);

		die( json_encode(array("success"=>"true")));
	}

	public function action_limpiar_filtros(){

		$session=Session::instance();

		$session->set("ofertas_codigo","");
		$session->set("ofertas_nombre","");
		$session->set("ofertas_ciudad","");
		$session->set("ofertas_categoria","");
		$session->set("ofertas_publicado","");
		$session->set("ofertas_autor","");
		$session->set("ofertas_pago","");

		die( json_encode(array("success"=>"true")));

	}

	public function action_check(){
		$session=Session::instance();
		$id=$this->request->post('id');
		$session->set("oferta_".$id,"true");
		die( json_encode(array("success"=>"true")));
	}
	public function action_uncheck(){
		$session=Session::instance();
		$id=$this->request->post('id');
		$session->set("oferta_".$id,"false");
		die( json_encode(array("success"=>"true")));
	}
}