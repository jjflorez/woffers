<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Comercios_Cupones extends Controller_Template
{
	public $template = 'ci/view_template';

	/*public function action_index()
	{
		$controller='cupones';
		try{
			$comercio_id=$this->request->query('comercio');
		}catch(Exception $ex){
			$comercio_id=-1;
		}
		if($comercio_id==''){
			$comercio_id=-1;
		}

		$user_id = Auth::instance()->get_user()->id;

		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();

		if($comercio_id!=-1){
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->where("id","=",$comercio_id)->find_All();
		}else{
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->find_All();
		}
		$count=1;
		foreach($comercios as $k=>$comercio){
			$productos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->find_All();
			$lista_productos=array();
			foreach($productos->as_array() as $l=>$p){
				$p=$p->as_array();
				$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
				$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
				$p['numero']=$count;
				$p['tienda']=$tienda;
				$p['comercio']=array();
				$p['comercio']=$comercio->as_array();
				try{
					$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
				}catch(Exception $ex){
					$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
				}
				//echo $imagen->id."<br>";
				if(isset($imagen->id)==false){
					$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
				}
				//echo json_encode($imagen);
				$p['comercio']['logo']=$imagen;
				if(isset($p['comercio']['name'])==false){
					$p['comercio']['name']=$p['comercio_nombre'];
				}
				$lista_productos[]=$p;
				$count++;

			}
			foreach($lista_productos as $index=>$val){
				foreach($comercio as $i=>$c){
					$lista_productos[$index][$i]=$c;
				}
			}
		}
		echo json_encode($lista_productos);
		$comercios = ORM::factory('comercio')->find_all();

		$this->template->content = View::factory('comercios/view_lista_cupones')
			->set("cupones",$lista_productos)
			->set("comercios",$comercios)
			->set("type",$controller);

	}*/

	public function action_agregar()
	{
		$user_id = Auth::instance()->get_user()->id;
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->find_All();

		$comercio_data = ORM::factory('comercio')->where("user_id","=",$usuario->id)->find();

		$m_tiendas = new Model_Tienda();
		$m_linea = new Model_Linea();
		$m_ubicacion = new Model_Ubicacion();

		//$linea = $m_linea->where('code', '=', $this->request->param('param1'))->find();
		$tiendas = $m_tiendas->find_all();

		$provincias = $m_ubicacion->where('parent_id', '=', 1)->find_all();

		$ciudades_list = array();
		foreach($provincias as $item){
			$ciudades = $m_ubicacion->where('parent_id', '=', $item->id)->find_all();

			foreach($ciudades as $ciudad){
				$ciudad = $ciudad->as_array();
				$ciudades_list[] = $ciudad;
			}
		}

		$comercios_todos= ORM::factory('comercio')->find_all();
		$this->template->content = View::factory('comercios/view_agregar_cupon')
			->set('comercios', $comercios_todos)
			->set('comercio_id', $comercio_data->id)
			->set('comercio', $comercio_data->name)
			->set('seleccion_comercios', $comercios)
			->set('ciudades', $ciudades_list)
			->set('paises', $m_ubicacion->where('parent_id', '=', '-1')->find_all())
			->set('categorias', ORM::factory('categoria')->find_all())
			->set('tiendas', $tiendas);
	}

	public function action_editar()
	{
		$id_producto = $this->request->param('param1');

		//echo $id_producto."<br>";

		if($id_producto)
		{
			$producto = ORM::factory('producto')->where('id', '=', $id_producto)->find();
			//echo $producto->id."<br>";
			if($producto->loaded())
			{
				$m_tiendas = new Model_Tienda();
				$tiendas = $m_tiendas->where('linea_id', '=', $producto->tienda->linea_id)->find_all();

				$provincias = ORM::factory('ubicacion')->where('parent_id', '=', 1)->find_all();

				$ciudades_list = array();
				foreach($provincias as $item){
					$ciudades = ORM::factory('ubicacion')->where('parent_id', '=', $item->id)->find_all();

					foreach($ciudades as $ciudad){
						$ciudad = $ciudad->as_array();
						$ciudades_list[] = $ciudad;
					}
				}


				$distritos = ORM::factory('ubicacion')->where('parent_id', '=', $producto->oferta_ubicacion_ciudad)->find_all();
				$barrios = ORM::factory('ubicacion')->where('parent_id', '=', $producto->oferta_ubicacion_distrito)->find_all();
				$comercios = ORM::factory('comercio')->find_all();

				$this->template->content = View::factory('comercios/view_agregar_cupon')
					->set('tiendas', $tiendas)
					->set('linea', $producto->tienda->linea->code)
					->set('ciudades', ORM::factory('ubicacion')->where('parent_id', '=', 1)->find_all())
					->set('comercios', $comercios)
					->set('provincias', $provincias)
					->set('ciudades', $ciudades_list)
					->set('distritos', $distritos)
					->set('barrios', $barrios)
					->set('paises', ORM::factory('ubicacion')->where('parent_id', '=', '-1')->find_all())
					->set('categorias', ORM::factory('categoria')->find_all())
					->set('producto', $producto)
					->set('check', 'false');
			}else{
				$this->request->redirect($this->request->referrer());
			}
		}else{
			$this->request->redirect($this->request->referrer());
		}
	}

	public function action_guardar()
	{

		$comercio_id=$_POST['comercio_id'];

		$comercio=ORM::factory('comercio')->where("id","=",$comercio_id)->find();
		if(isset($comercio->address)==true){
			$_POST['comercio_direccion']=$comercio->address;
		}else{
			$_POST['comercio_direccion']='';
		}
		if(isset($comercio->description)==true){
			$_POST['comercio_descripcion']=$comercio->description;
		}else{
			$_POST['comercio_descripcion']='';
		}


		$mdl_producto = new Model_Producto();

		//echo "<pre>"; print_r($_POST); die();

		$_POST['user_id'] = Auth::instance()->get_user()->id;
		$_POST['status'] = 1;
		$_POST['created'] = date('Y-m-d h:i:s');

		if($this->request->post('oferta_duracion')){
			$fd = explode('/',  $_POST['oferta_duracion'] );
			if(count($fd) > 2) $_POST['oferta_duracion'] = $fd[2] . '-'. $fd[1] . '-' . $fd[0] . ' 01:01:01';
		}else{
			$_POST['oferta_duracion'] = date('Y-m-d h:i:s');
		}

		if($this->request->post('tiempo_validez')){
			$fd = explode('/',  $_POST['tiempo_validez'] );
			if(count($fd) > 2) $_POST['tiempo_validez'] = $fd[2] . '-'. $fd[1] . '-' . $fd[0] . ' 01:01:01';
		}else{
			$_POST['tiempo_validez'] = date('Y-m-d h:i:s');
		}

		if($this->request->post('oferta_publicacion')){
			$fd = explode('/',  $_POST['oferta_publicacion'] );
			if(count($fd) > 2) $_POST['oferta_publicacion'] = $fd[2] . '-'. $fd[1] . '-' . $fd[0] . ' 00:00:00';
		}else{
			$_POST['oferta_publicacion'] = date('Y-m-d h:i:s');
		}

		$fechas_validas='false';
		$publicacion =date($_POST['oferta_publicacion']);
		$duracion= date($_POST['oferta_duracion']);
		$validez = date($_POST['tiempo_validez']);

		//echo $publicacion."<br>";
		//echo $duracion."<br>";
		//echo $validez."<br>";

		if($publicacion<$duracion && $duracion<$validez){
			$fechas_validas='true';
		}else{
			Message::add('error', 'Corrija las fechas');
		}

		//echo $fechas_validas."<br>";

		//////////////////////////////////////////////////////////////////////////////
		$post = Validation::factory($_POST);
		$post->rule('comercio_id', 'not_empty');
		//$post->rule('comercio_direccion', 'not_empty');
		//$post->rule('comercio_descripcion', 'not_empty');
		$post->rule('oferta_titulo', 'not_empty');
		$post->rule('oferta_descripcion', 'not_empty');
		$post->rule('oferta_destacamos', 'not_empty');
		$post->rule('oferta_que_incluye', 'not_empty');
		$post->rule('tienda_id', 'not_empty');
		$post->rule('oferta_valor', 'numeric');

		if ($post->check() && $fechas_validas=='true')
		{
			$producto_id = -1;
			$redirect = '';
			if($this->request->post('id') != '')
			{
				$p = new Model_Producto($this->request->post('id'));
				if($p->loaded())
				{
					$p->values($_POST);

					if($this->request->post('oferta_nacional') == 1){
						$p->oferta_ubicacion_ciudad = -2;
					}

					if($this->request->post('estado')){
						$p->status = $this->request->post('estado');
					}else{
						$p->status = 1;
					}

					$p->update();
					$producto_id = $this->request->post('id');
					Message::add('Mensaje', 'Datos guardados exitosamente');
					$redirect = 'admin/productos/editar/' . $this->request->post('id');
				}
			}else{
				$mdl_producto->values($post->data());

				if($this->request->post('oferta_nacional') == 1){
					$mdl_producto->oferta_ubicacion_ciudad = -2;
				}

				$mdl_producto->save();
				$producto_id = $mdl_producto->id;

				$mdl_producto->num_referencia = $this->_get_num_ref($producto_id);
				$mdl_producto->update();

				$redirect = 'comercios/cupones/';
			}

			//Actualizar imagenes externas
			Model_ProductoFile::cleanImages($producto_id);
			foreach($_POST['imagen_externa'] as $img)
			{
				if($img != ''){
					$m_pro_file = new Model_ProductoFile();
					$m_pro_file->values(array('producto_id'=>$producto_id, 'file_id'=>$img));
					$m_pro_file->save();
				}
			}

			$this->request->redirect($redirect);
		}else{
			Message::add('error', 'Por favor complete los campos del formulario');
			if($this->request->post('id') != '')
				$this->request->redirect('comercios/cupones/editar/' . $this->request->post('id'));
			else
				$this->request->redirect('comercios/cupones/');
		}
	}
	public function action_eliminar()
	{
		$id_producto = $this->request->param('param1');
		if($id_producto)
		{
			$producto = new Model_Producto($id_producto);

			if($producto->loaded())
			{
				foreach($producto->productofile->find_all() as $file){
					$file->delete();
				}

				$producto->delete();
				Message::add('info', 'Producto eliminado correctamente');
				$this->request->redirect($this->request->referrer());
			}else{
				Message::add('error', 'No se pudo eliminar el producto');
				$this->request->redirect($this->request->referrer());
			}
		}else{
			$this->request->redirect($this->request->referrer());
		}
	}
	private function _get_num_ref($producto_id)
	{
		return "ES-".date('md').$producto_id.date('y');
	}

	public function action_index()
	{
		$controller='vendidos';
		try{
			$comercio_id=$this->request->query('comercio');
		}catch(Exception $ex){
			$comercio_id=-1;
		}
		if($comercio_id==''){
			$comercio_id=-1;
		}
		try{
			$user_id = Auth::instance()->get_user()->id;
		}catch(Exception $ex){
			$user_id=-1;
		}
		$user_id=trim($user_id);
		if($user_id==''){
			$user_id=-1;
		}
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen(trim($usuario->id))==0){
			$usuario->id=-1;
		}

		if($comercio_id!=-1){
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->where("id","=",$comercio_id)->find_All();
		}else{
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->find_All();
		}

		foreach($comercios as $k=>$comercio){
			$count=1;
			$productos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->find_All();
			$lista_productos=array();
			foreach($productos->as_array() as $l=>$p){
				$p=$p->as_array();
				$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
				foreach($detalles->as_array() as $id=>$var){
					if(isset($var->carrito_id)==true){
						$carrito_id=$var->carrito_id;
						$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
						if(strlen($carrito->pago)>=19){
							$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
							$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
							$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
							$p['numero']=$count;
							$p['status']=$var->status;
							$p['codigo_cupon']=$var->codigo_cupon;
							//echo $usuario->name."<br>";
							if($var->is_gift=='1'){
								$p['nombre']=$var->regalo_nombre;
							}else{
								$p['nombre']=$usuario->name;
							}
							$p['codigo_validacion']=$var->codigo_validacion;
							$p['tienda']=$tienda;
							$p['comercio']=array();
							$p['comercio']=$comercio->as_array();
							if(strlen($comercio->logo_id)>0){
								try{
									$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
								}catch(Exception $ex){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
							}
							if(isset($imagen->id)==false){
								$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
							}
							$p['comercio']['logo']=$imagen;
							if(isset($p['comercio']['name'])==false){
								$p['comercio']['name']=$p['comercio_nombre'];
							}
							$p['id']=$var->id;
							$lista_productos[]=$p;
							$count++;
						}
					}
				}
			}
			/*foreach($lista_productos as $index=>$val){
				foreach($comercio as $i=>$c){
					$lista_productos[$index][$i]=$c;
				}
			}/**/
		}
		$comercios = ORM::factory('comercio')->find_all();
		$this->template->content = View::factory('comercios/view_lista_cupones')
			->set("cupones",$lista_productos)
			->set("comercios",$comercios)
			->set("type",$controller);

	}
	public function action_canjeados()
	{
		$controller='canjeados';
		try{
			$comercio_id=$this->request->query('comercio');
		}catch(Exception $ex){
			$comercio_id=-1;
		}
		if($comercio_id==''){
			$comercio_id=-1;
		}

		try{
			$user_id = Auth::instance()->get_user()->id;
		}catch(Exception $ex){
			$user_id=-1;
		}
		$user_id=trim($user_id);
		if($user_id==''){
			$user_id=-1;
		}
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen(trim($usuario->id))==0){
			$usuario->id=-1;
		}

		if($comercio_id!=-1){
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->where("id","=",$comercio_id)->find_All();
		}else{
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->find_All();
		}

		foreach($comercios as $k=>$comercio){

			$productos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->find_All();
			$lista_productos=array();
			$count=1;
			foreach($productos->as_array() as $l=>$p){
				$p=$p->as_array();
				$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
				foreach($detalles->as_array() as $id=>$var){
					if(isset($var->carrito_id)==true){
						$carrito_id=$var->carrito_id;
						$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
						if(strlen($carrito->pago)>=19 && $var->status=='10'){//10 es el estado para los cupones validados
							$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
							$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
							$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
							$p['tienda']=$tienda;
							$p['comercio']=array();
							$p['comercio']=$comercio->as_array();
							$p['numero']=$count;
							$p['status']=$var->status;
							$p['codigo_cupon']=$var->codigo_cupon;
							//echo $usuario->name."<br>";
							if($var->is_gift=='1'){
								$p['nombre']=$var->regalo_nombre;
							}else{
								$p['nombre']=$usuario->name;
							}
							$p['codigo_validacion']=$var->codigo_validacion;
							if(strlen($comercio->logo_id)>0){
								try{
									$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
								}catch(Exception $ex){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
							}
							if(isset($imagen->id)==false){
								$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
							}
							$p['comercio']['logo']=$imagen;
							if(isset($p['comercio']['name'])==false){
								$p['comercio']['name']=$p['comercio_nombre'];
							}
							$p['id']=$var->id;
							$lista_productos[]=$p;
							$count++;
						}
					}
				}
			}
			/*foreach($lista_productos as $index=>$val){
				foreach($comercio as $i=>$c){
					$lista_productos[$index][$i]=$c;
				}
			}/**/
		}
		$comercios = ORM::factory('comercio')->find_all();
		$this->template->content = View::factory('comercios/view_lista_cupones_canjeados')
			->set("cupones",$lista_productos)
			->set("comercios",$comercios)
			->set("type",$controller);

	}
	public function action_comprados()
	{
		$controller='comprados';
		try{
			$user_id = Auth::instance()->get_user()->id;
		}catch(Exception $ex){
			$user_id=-1;
		}
		$user_id=trim($user_id);
		if($user_id==''){
			$user_id=-1;
		}
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen(trim($usuario->id))==0){
			$usuario->id=-1;
		}
		$carritos = ORM::factory('carrito')->where("user_id","=",$usuario->id)->find_All();
		$lista_productos=array();
		$count=1;
		foreach($carritos as $k=>$carrito){
			$carrito=$carrito->as_array();
			//echo json_encode($carrito['id'])."<br>";
			$detalles = ORM::factory('detalleproducto')->where("carrito_id","=",$carrito['id'])->find_all();
			foreach($detalles->as_array() as $d=>$detalle){
				$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
				$detalle=$detalle->as_array();
				$producto=ORM::factory('producto')->where("id","=",intval($detalle['producto_id']))->find();
				$p=$producto->as_array();
				$p['numero']=$count;
				$p['status']=$var->status;
				$p['codigo_cupon']=$var->codigo_cupon;
				if($var->is_gift=='1'){
					$p['nombre']=$var->regalo_nombre;
				}else{
					$p['nombre']=$usuario->name;
				}
				$p['codigo_validacion']=$var->codigo_validacion;
				$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
				$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
				$p['tienda']=$tienda;
				$p['comercio']=array();
				$p['comercio']=$comercio->as_array();
				if(strlen($comercio->logo_id)>0){
					try{
						$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
					}catch(Exception $ex){
						$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
					}
				}
				if(isset($imagen->id)==false){
					$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
				}
				$p['comercio']['logo']=$imagen;
				if(isset($p['comercio']['name'])==false){
					$p['comercio']['name']=$p['comercio_nombre'];
				}
				$p['id']=$var->id;
				if(strlen($p['id'])>0){
					$lista_productos[]=$p;
					$count++;
				}
			}
		}
		$comercios = ORM::factory('comercio')->find_all();
		$this->template->content = View::factory('comercios/view_lista_cupones_comprados')
			->set("cupones",$lista_productos)
			->set("comercios",$comercios)
			->set("type",$controller);

	}
	public function action_validar(){
		$codigo_validacion=$_POST['codigo_validacion'];
		$url=$_POST['url'];
		$m_d = new Model_Detalleproducto($_POST['id']);
		if($codigo_validacion==$m_d->codigo_validacion){
			$m_d->validated=TRUE;//validacion con estado 10
		}
		$m_d->save();
		$this->request->redirect($url);
	}

	public function action_publicar(){
		$codigo_validacion=$_POST['codigo_validacion'];
		$url=$_POST['url'];
		$m_d = new Model_Detalleproducto($_POST['id']);
		$m_d->status=1;//validacion con estado 10
		$m_d->save();
		$this->request->redirect($url);
	}

	public function action_buscar_publicados(){
		$url='comercios/cupones/buscar_publicados';
		$url_validar='comercios/cupones/validar';
		$url_publicar='comercios/cupones/publicar';
		$url_agregar='comercios/cupones/agregar';
		$checkbox='false';
		$controller='buscar';
		try{
			$token=$this->request->post('token');
		}catch(Exception $ex){
			$token="buscar";
		}
		$token=trim($token);
		if($token==''){
			$token='buscar';
		}
		try{
			$codigo=$this->request->post('codigo_cupon');
		}catch(Exception $ex){
			$codigo=-1;
		}
		$codigo=trim($codigo);
		if($codigo==''){
			$codigo=-1;
		}
		//echo $codigo."<br>";
		try{
			$comercio_id=$this->request->query('comercio');
		}catch(Exception $ex){
			$comercio_id=-1;
		}
		if($comercio_id==''){
			$comercio_id=-1;
		}
		try{
			$user_id = Auth::instance()->get_user()->id;
		}catch(Exception $ex){
			$user_id=-1;
		}
		$user_id=trim($user_id);
		if($user_id==''){
			$user_id=-1;
		}
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen(trim($usuario->id))==0){
			$usuario->id=-1;
		}

		if($comercio_id!=-1){
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->where("id","=",$comercio_id)->find_All();
		}else{
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->find_All();
		}
		try{
			$oferta=$this->request->post('oferta');
		}catch(Exception $ex){
			$oferta=-1;
		}
		$oferta=trim($oferta);
		if($oferta==''){
			$oferta=-1;
		}

		foreach($comercios as $k=>$comercio){
			$count=1;
			$productos_validos=array();
			$productos_no_validos=array();
			if($token=='buscar'){
				$productos_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("status","=",1)->where("tiempo_validez",">",DB::expr("NOW()"))->find_All();
				$productos_no_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("status","=",1)->where("tiempo_validez","<=",DB::expr("NOW()"))->find_All();
			}elseif($token=='oferta'){
				$productos_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("status","=",1)->where("id","=",$oferta)->where("tiempo_validez",">",DB::expr("NOW()"))->find_All();
				$productos_no_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("status","=",1)->where("id","=",$oferta)->where("tiempo_validez","<=",DB::expr("NOW()"))->find_All();
			}
			/*$productos_linea = ORM::factory('producto')
					->where("comercio_id","=",$comercio->id)
					->where("tienda_id","=",'1')->find_All();
			$all_products[]=$productos_linea->as_array();*/
			$lista_productos=array();
			foreach($productos_validos->as_array() as $l=>$p){
				$p=$p->as_array();
				if($codigo==-1){
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
				}else{
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("codigo_cupon","=",$codigo)->find_all();
				}
				foreach($detalles->as_array() as $id=>$var){
					if(isset($var->carrito_id)==true){
						$carrito_id=$var->carrito_id;
						$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
						if(strlen($carrito->pago)>=19){
							$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
							$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
							$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
							$p['numero']=$count;
							$p['status']=$var->status;
							$p['codigo_cupon']=$var->codigo_cupon;
							//echo $usuario->name."<br>";
							if($var->is_gift=='1'){
								$p['nombre']=$var->regalo_nombre;
							}else{
								$p['nombre']=$usuario->name;
							}
							if(strlen($carrito->created)>0){
								$p['fecha_compra']=$carrito->created;
							}
							$date=explode(" ",$p['fecha_compra']);
							$p['fecha_compra']=$date[0];
							$p['codigo_validacion']=$var->codigo_validacion;
							$p['tienda']=$tienda;
							$p['comercio']=array();
							$p['comercio']=$comercio->as_array();
							$p['valido']='no caducado';
							if(strlen($comercio->logo_id)>0){
								try{
									$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
								}catch(Exception $ex){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
							}
							if(isset($imagen->id)==false){
								$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
							}
							$p['comercio']['logo']=$imagen;
							if(isset($p['comercio']['name'])==false){
								$p['comercio']['name']=$p['comercio_nombre'];
							}
							$p['producto_id']=$p['id'];
							$p['detalle_id']=$var->id;
							//$p['id']=$var->id;
							$p['validated']=$var->validated;
							$lista_productos[]=$p;
							$count++;
						}
					}
				}
			}

			foreach($productos_no_validos->as_array() as $l=>$p){
				$p=$p->as_array();
				if($codigo==-1){
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
				}else{
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("codigo_cupon","=",$codigo)->find_all();
				}
				foreach($detalles->as_array() as $id=>$var){
					if(isset($var->carrito_id)==true){
						$carrito_id=$var->carrito_id;
						$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
						if(strlen($carrito->pago)>=19){
							$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
							$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
							$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
							$p['numero']=$count;
							$p['status']=$var->status;
							$p['codigo_cupon']=$var->codigo_cupon;
							//echo $usuario->name."<br>";
							if($var->is_gift=='1'){
								$p['nombre']=$var->regalo_nombre;
							}else{
								$p['nombre']=$usuario->name;
							}
							if(strlen($carrito->created)>0){
								$p['fecha_compra']=$carrito->created;
							}
							$date=explode(" ",$p['fecha_compra']);
							$p['fecha_compra']=$date[0];
							$p['codigo_validacion']=$var->codigo_validacion;
							$p['tienda']=$tienda;
							$p['comercio']=array();
							$p['comercio']=$comercio->as_array();
							$p['valido']='caducado';
							if(strlen($comercio->logo_id)>0){
								try{
									$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
								}catch(Exception $ex){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
							}
							if(isset($imagen->id)==false){
								$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
							}
							$p['comercio']['logo']=$imagen;
							if(isset($p['comercio']['name'])==false){
								$p['comercio']['name']=$p['comercio_nombre'];
							}
							$p['producto_id']=$p['id'];
							$p['detalle_id']=$var->id;
							//$p['id']=$var->id;
							$p['validated']=$var->validated;
							$lista_productos[]=$p;
							$count++;
						}
					}
				}
			}
		}
		$comercios = ORM::factory('comercio')->find_all();
		$this->template->content = View::factory('comercios/view_lista_cupones_publicados')
			->set("cupones",$lista_productos)
			->set("comercios",$comercios)
			->set("type",$controller)
			->set("url",$url)
			->set("url_validar",$url_validar)
			->set("url_agregar",$url_agregar)
			->set("oferta",$oferta);
	}

	public function action_buscar_no_publicados(){
		$url='comercios/cupones/buscar_no_publicados';
		$url_validar='comercios/cupones/validar';
		$url_publicar='comercios/cupones/publicar';
		$url_agregar='comercios/cupones/agregar';
		$controller='buscar';
		try{
			$token=$this->request->post('token');
		}catch(Exception $ex){
			$token="buscar";
		}
		$token=trim($token);
		if($token==''){
			$token='buscar';
		}
		try{
			$codigo=$this->request->post('codigo_cupon');
		}catch(Exception $ex){
			$codigo=-1;
		}
		$codigo=trim($codigo);
		if($codigo==''){
			$codigo=-1;
		}
		//echo $codigo."<br>";
		try{
			$comercio_id=$this->request->query('comercio');
		}catch(Exception $ex){
			$comercio_id=-1;
		}
		if($comercio_id==''){
			$comercio_id=-1;
		}
		try{
			$user_id = Auth::instance()->get_user()->id;
		}catch(Exception $ex){
			$user_id=-1;
		}
		$user_id=trim($user_id);
		if($user_id==''){
			$user_id=-1;
		}
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen(trim($usuario->id))==0){
			$usuario->id=-1;
		}

		if($comercio_id!=-1){
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->where("id","=",$comercio_id)->find_All();
		}else{
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->find_All();
		}
		try{
			$oferta=$this->request->post('oferta');
		}catch(Exception $ex){
			$oferta=-1;
		}
		$oferta=trim($oferta);
		if($oferta==''){
			$oferta=-1;
		}

		foreach($comercios as $k=>$comercio){
			$count=1;
			$productos_validos=array();
			$productos_no_validos=array();
			if($token=='buscar'){
				$productos_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("status","=",0)->where("tiempo_validez",">",DB::expr("NOW()"))->find_All();
				$productos_no_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("status","=",0)->where("tiempo_validez","<=",DB::expr("NOW()"))->find_All();
			}elseif($token=='oferta'){
				$productos_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("status","=",0)->where("id","=",$oferta)->where("tiempo_validez",">",DB::expr("NOW()"))->find_All();
				$productos_no_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("status","=",0)->where("id","=",$oferta)->where("tiempo_validez","<=",DB::expr("NOW()"))->find_All();
			}
			/*$productos_linea = ORM::factory('producto')
			   ->where("comercio_id","=",$comercio->id)
			   ->where("tienda_id","=",'1')->find_All();
			   $all_products[]=$productos_linea->as_array();*/
			$lista_productos=array();
			foreach($productos_validos->as_array() as $l=>$p){
				$p=$p->as_array();
				if($codigo==-1){
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
				}else{
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("codigo_cupon","=",$codigo)->find_all();
				}
				foreach($detalles->as_array() as $id=>$var){
					if(isset($var->carrito_id)==true){
						$carrito_id=$var->carrito_id;
						$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
						if(strlen($carrito->pago)>=19){
							$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
							$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
							$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
							$p['numero']=$count;
							$p['status']=$var->status;
							$p['codigo_cupon']=$var->codigo_cupon;
							//echo $usuario->name."<br>";
							if($var->is_gift=='1'){
								$p['nombre']=$var->regalo_nombre;
							}else{
								$p['nombre']=$usuario->name;
							}
							if(strlen($carrito->created)>0){
								$p['fecha_compra']=$carrito->created;
							}
							$date=explode(" ",$p['fecha_compra']);
							$p['fecha_compra']=$date[0];
							$p['codigo_validacion']=$var->codigo_validacion;
							$p['tienda']=$tienda;
							$p['comercio']=array();
							$p['comercio']=$comercio->as_array();
							$p['valido']='no caducado';
							if(strlen($comercio->logo_id)>0){
								try{
									$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
								}catch(Exception $ex){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
							}
							if(isset($imagen->id)==false){
								$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
							}
							$p['comercio']['logo']=$imagen;
							if(isset($p['comercio']['name'])==false){
								$p['comercio']['name']=$p['comercio_nombre'];
							}
							$p['producto_id']=$p['id'];
							$p['detalle_id']=$var->id;
							$p['id']=$var->id;
							$p['validated']=$var->validated;
							$lista_productos[]=$p;
							$count++;
						}
					}
				}
			}

			foreach($productos_no_validos->as_array() as $l=>$p){
				$p=$p->as_array();
				if($codigo==-1){
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
				}else{
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("codigo_cupon","=",$codigo)->find_all();
				}
				foreach($detalles->as_array() as $id=>$var){
					if(isset($var->carrito_id)==true){
						$carrito_id=$var->carrito_id;
						$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
						if(strlen($carrito->pago)>=19){
							$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
							$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
							$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
							$p['numero']=$count;
							$p['status']=$var->status;
							$p['codigo_cupon']=$var->codigo_cupon;
							//echo $usuario->name."<br>";
							if($var->is_gift=='1'){
								$p['nombre']=$var->regalo_nombre;
							}else{
								$p['nombre']=$usuario->name;
							}
							if(strlen($carrito->created)>0){
								$p['fecha_compra']=$carrito->created;
							}
							$date=explode(" ",$p['fecha_compra']);
							$p['fecha_compra']=$date[0];
							$p['codigo_validacion']=$var->codigo_validacion;
							$p['tienda']=$tienda;
							$p['comercio']=array();
							$p['comercio']=$comercio->as_array();
							$p['valido']='caducado';
							if(strlen($comercio->logo_id)>0){
								try{
									$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
								}catch(Exception $ex){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
							}
							if(isset($imagen->id)==false){
								$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
							}
							$p['comercio']['logo']=$imagen;
							if(isset($p['comercio']['name'])==false){
								$p['comercio']['name']=$p['comercio_nombre'];
							}
							$p['producto_id']=$p['id'];
							$p['detalle_id']=$var->id;
							$p['id']=$var->id;
							$p['validated']=$var->validated;
							$lista_productos[]=$p;
							$count++;
						}
					}
				}
			}
		}
		$comercios = ORM::factory('comercio')->find_all();
		$this->template->content = View::factory('comercios/view_lista_cupones_publicados')
			->set("cupones",$lista_productos)
			->set("comercios",$comercios)
			->set("type",$controller)
			->set("url",$url)
			->set("url_validar",$url_validar)
			->set("url_agregar",$url_agregar)
			->set("checkbox",$checkbox)
			->set("oferta",$oferta);
	}

	public function action_buscar(){//cupones vendidos
		$url='comercios/cupones/buscar';
		$url_validar='comercios/cupones/validar';
		$controller='buscar';
		try{
			$token=$this->request->post('token');
		}catch(Exception $ex){
			$token="buscar";
		}
		$token=trim($token);
		if($token==''){
			$token='buscar';
		}
		try{
			$codigo=$this->request->post('codigo_cupon');
		}catch(Exception $ex){
			$codigo=-1;
		}
		$codigo=trim($codigo);
		if($codigo==''){
			$codigo=-1;
		}
		//echo $codigo."<br>";
		try{
			$comercio_id=$this->request->query('comercio');
		}catch(Exception $ex){
			$comercio_id=-1;
		}
		if($comercio_id==''){
			$comercio_id=-1;
		}
		try{
			$user_id = Auth::instance()->get_user()->id;
		}catch(Exception $ex){
			$user_id=-1;
		}
		$user_id=trim($user_id);
		if($user_id==''){
			$user_id=-1;
		}
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen(trim($usuario->id))==0){
			$usuario->id=-1;
		}

		if($comercio_id!=-1){
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->where("id","=",$comercio_id)->find_All();
		}else{
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->find_All();
		}
		try{
			$oferta=$this->request->post('oferta');
		}catch(Exception $ex){
			$oferta=-1;
		}
		$oferta=trim($oferta);
		if($oferta==''){
			$oferta=-1;
		}

		foreach($comercios as $k=>$comercio){
			$count=1;
			$productos_validos=array();
			$productos_no_validos=array();
			if($token=='buscar'){
				$productos_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("tiempo_validez",">",DB::expr("NOW()"))->find_All();
				$productos_no_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("tiempo_validez","<=",DB::expr("NOW()"))->find_All();
			}elseif($token=='oferta'){
				$productos_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("id","=",$oferta)->where("tiempo_validez",">",DB::expr("NOW()"))->find_All();
				$productos_no_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("id","=",$oferta)->where("tiempo_validez","<=",DB::expr("NOW()"))->find_All();
			}
			$productos_linea = ORM::factory('producto')
					->where("comercio_id","=",$comercio->id)
					->where("tienda_id","=",'1')->find_All();
			$all_products[]=$productos_linea->as_array();
			$lista_productos=array();
			foreach($productos_validos->as_array() as $l=>$p){
				$p=$p->as_array();
				if($codigo==-1){
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
				}else{
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("codigo_cupon","=",$codigo)->find_all();
				}
				foreach($detalles->as_array() as $id=>$var){
					if(isset($var->carrito_id)==true){
						$carrito_id=$var->carrito_id;
						$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
						if(strlen($carrito->pago)>=19){
							$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
							$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
							$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
							$p['numero']=$count;
							$p['status']=$var->status;
							$p['codigo_cupon']=$var->codigo_cupon;
							//echo $usuario->name."<br>";
							if($var->is_gift=='1'){
								$p['nombre']=$var->regalo_nombre;
							}else{
								$p['nombre']=$usuario->name;
							}
							if(strlen($carrito->created)>0){
								$p['fecha_compra']=$carrito->created;
							}
							$date=explode(" ",$p['fecha_compra']);
							$p['fecha_compra']=$date[0];
							$p['codigo_validacion']=$var->codigo_validacion;
							$p['tienda']=$tienda;
							$p['comercio']=array();
							$p['comercio']=$comercio->as_array();
							$p['valido']='no caducado';
							if(strlen($comercio->logo_id)>0){
								try{
									$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
								}catch(Exception $ex){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
							}
							if(isset($imagen->id)==false){
								$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
							}
							$p['comercio']['logo']=$imagen;
							if(isset($p['comercio']['name'])==false){
								$p['comercio']['name']=$p['comercio_nombre'];
							}
							$p['producto_id']=$p['id'];
							$p['detalle_id']=$var->id;
							$p['id']=$var->id;
							$p['validated']=$var->validated;
							$lista_productos[]=$p;
							$count++;
						}
					}
				}
			}

			foreach($productos_no_validos->as_array() as $l=>$p){
				$p=$p->as_array();
				if($codigo==-1){
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
				}else{
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("codigo_cupon","=",$codigo)->find_all();
				}
				foreach($detalles->as_array() as $id=>$var){
					if(isset($var->carrito_id)==true){
						$carrito_id=$var->carrito_id;
						$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
						if(strlen($carrito->pago)>=19){
							$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
							$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
							$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
							$p['numero']=$count;
							$p['status']=$var->status;
							$p['codigo_cupon']=$var->codigo_cupon;
							//echo $usuario->name."<br>";
							if($var->is_gift=='1'){
								$p['nombre']=$var->regalo_nombre;
							}else{
								$p['nombre']=$usuario->name;
							}
							if(strlen($carrito->created)>0){
								$p['fecha_compra']=$carrito->created;
							}
							$date=explode(" ",$p['fecha_compra']);
							$p['fecha_compra']=$date[0];
							$p['codigo_validacion']=$var->codigo_validacion;
							$p['tienda']=$tienda;
							$p['comercio']=array();
							$p['comercio']=$comercio->as_array();
							$p['valido']='caducado';
							if(strlen($comercio->logo_id)>0){
								try{
									$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
								}catch(Exception $ex){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
							}
							if(isset($imagen->id)==false){
								$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
							}
							$p['comercio']['logo']=$imagen;
							if(isset($p['comercio']['name'])==false){
								$p['comercio']['name']=$p['comercio_nombre'];
							}
							$p['producto_id']=$p['id'];
							$p['detalle_id']=$var->id;
							$p['id']=$var->id;
							$p['validated']=$var->validated;
							$lista_productos[]=$p;
							$count++;
						}
					}
				}
			}
		}
		$comercios = ORM::factory('comercio')->find_all();
		$this->template->content = View::factory('comercios/view_lista_cupones_buscar')
			->set("cupones",$lista_productos)
			->set("comercios",$comercios)
			->set("ofertas",$all_products)
			->set("type",$controller)
			->set("url",$url)
			->set("url_validar",$url_validar)
			->set("oferta",$oferta);
	}
	public function action_buscar_consumidos(){//cupones consumidos
		$url='comercios/cupones/buscar_consumidos';
		$url_validar='comercios/cupones/validar';
		$controller='buscar';
		try{
			$token=$this->request->post('token');
		}catch(Exception $ex){
			$token="buscar";
		}
		$token=trim($token);
		if($token==''){
			$token='buscar';
		}
		try{
			$codigo=$this->request->post('codigo_cupon');
		}catch(Exception $ex){
			$codigo=-1;
		}
		$codigo=trim($codigo);
		if($codigo==''){
			$codigo=-1;
		}
		//echo $codigo."<br>";
		try{
			$comercio_id=$this->request->query('comercio');
		}catch(Exception $ex){
			$comercio_id=-1;
		}
		if($comercio_id==''){
			$comercio_id=-1;
		}
		try{
			$user_id = Auth::instance()->get_user()->id;
		}catch(Exception $ex){
			$user_id=-1;
		}
		$user_id=trim($user_id);
		if($user_id==''){
			$user_id=-1;
		}
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen(trim($usuario->id))==0){
			$usuario->id=-1;
		}

		if($comercio_id!=-1){
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->where("id","=",$comercio_id)->find_All();
		}else{
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->find_All();
		}
		try{
			$oferta=$this->request->post('oferta');
		}catch(Exception $ex){
			$oferta=-1;
		}
		$oferta=trim($oferta);
		if($oferta==''){
			$oferta=-1;
		}
		foreach($comercios as $k=>$comercio){
			$count=1;
			$productos_validos=array();
			$productos_no_validos=array();
			if($token=='buscar'){
				$productos_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("tiempo_validez",">",DB::expr("NOW()"))->find_All();
				$productos_no_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("tiempo_validez","<=",DB::expr("NOW()"))->find_All();
			}elseif($token=='oferta'){
				$productos_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("id","=",$oferta)->where("tiempo_validez",">",DB::expr("NOW()"))->find_All();
				$productos_no_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("id","=",$oferta)->where("tiempo_validez","<=",DB::expr("NOW()"))->find_All();
			}
			$productos_linea = ORM::factory('producto')
					->where("comercio_id","=",$comercio->id)
					->where("tienda_id","=",'1')->find_All();
			$all_products[]=$productos_linea->as_array();
			$lista_productos=array();
			foreach($productos_validos->as_array() as $l=>$p){
				$p=$p->as_array();
				if($codigo==-1){
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
				}else{
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("codigo_cupon","=",$codigo)->find_all();
				}
				foreach($detalles->as_array() as $id=>$var){
					if(isset($var->carrito_id)==true){
						$carrito_id=$var->carrito_id;
						$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
						$true=true;
						if(strlen($carrito->pago)>=19 && $var->validated==$true){
							$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
							$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
							$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
							$p['numero']=$count;
							$p['status']=$var->status;
							$p['codigo_cupon']=$var->codigo_cupon;
							//echo $usuario->name."<br>";
							if($var->is_gift=='1'){
								$p['nombre']=$var->regalo_nombre;
							}else{
								$p['nombre']=$usuario->name;
							}
							if(strlen($carrito->created)>0){
								$p['fecha_compra']=$carrito->created;
							}
							$date=explode(" ",$p['fecha_compra']);
							$p['fecha_compra']=$date[0];
							$p['codigo_validacion']=$var->codigo_validacion;
							$p['tienda']=$tienda;
							$p['comercio']=array();
							$p['comercio']=$comercio->as_array();
							$p['valido']='no caducado';
							if(strlen($comercio->logo_id)>0){
								try{
									$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
								}catch(Exception $ex){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
							}
							if(isset($imagen->id)==false){
								$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
							}
							$p['comercio']['logo']=$imagen;
							if(isset($p['comercio']['name'])==false){
								$p['comercio']['name']=$p['comercio_nombre'];
							}
							$p['producto_id']=$p['id'];
							$p['detalle_id']=$var->id;
							$p['id']=$var->id;
							$p['validated']=$var->validated;
							//echo $p['validated']."<br>";
							$lista_productos[]=$p;
							$count++;
						}
					}
				}
			}

			foreach($productos_no_validos->as_array() as $l=>$p){
				$p=$p->as_array();
				if($codigo==-1){
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
				}else{
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("codigo_cupon","=",$codigo)->find_all();
				}
				foreach($detalles->as_array() as $id=>$var){
					if(isset($var->carrito_id)==true){
						$carrito_id=$var->carrito_id;
						$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
						$true=true;
						if(strlen($carrito->pago)>=19 && $var->validated==$true){
							$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
							$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
							$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
							$p['numero']=$count;
							$p['status']=$var->status;
							$p['codigo_cupon']=$var->codigo_cupon;
							//echo $usuario->name."<br>";
							if($var->is_gift=='1'){
								$p['nombre']=$var->regalo_nombre;
							}else{
								$p['nombre']=$usuario->name;
							}
							if(strlen($carrito->created)>0){
								$p['fecha_compra']=$carrito->created;
							}
							$date=explode(" ",$p['fecha_compra']);
							$p['fecha_compra']=$date[0];
							$p['codigo_validacion']=$var->codigo_validacion;
							$p['tienda']=$tienda;
							$p['comercio']=array();
							$p['comercio']=$comercio->as_array();
							$p['valido']='caducado';
							if(strlen($comercio->logo_id)>0){
								try{
									$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
								}catch(Exception $ex){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
							}
							if(isset($imagen->id)==false){
								$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
							}
							$p['comercio']['logo']=$imagen;
							if(isset($p['comercio']['name'])==false){
								$p['comercio']['name']=$p['comercio_nombre'];
							}
							$p['producto_id']=$p['id'];
							$p['detalle_id']=$var->id;
							$p['id']=$var->id;
							$p['validated']=$var->validated;
							$lista_productos[]=$p;
							$count++;
						}
					}
				}
			}
		}
		$comercios = ORM::factory('comercio')->find_all();
		$this->template->content = View::factory('comercios/view_lista_cupones_buscar')
			->set("cupones",$lista_productos)
			->set("comercios",$comercios)
			->set("ofertas",$all_products)
			->set("type",$controller)
			->set("url",$url)
			->set("url_validar",$url_validar)
			->set("oferta",$oferta);
	}
/*
	public function action_buscar_()
	{
		$controller='buscar';
		try{
			$token=$this->request->post('token');
		}catch(Exception $ex){
			$token="buscar";
		}
		$token=trim($token);
		if($token==''){
			$token='buscar';
		}
		try{
			$codigo=$this->request->post('codigo_cupon');
		}catch(Exception $ex){
			$codigo=-1;
		}
		$codigo=trim($codigo);
		if($codigo==''){
			$codigo=-1;
		}
		//echo $codigo."<br>";
		try{
			$comercio_id=$this->request->query('comercio');
		}catch(Exception $ex){
			$comercio_id=-1;
		}
		if($comercio_id==''){
			$comercio_id=-1;
		}
		try{
			$user_id = Auth::instance()->get_user()->id;
		}catch(Exception $ex){
			$user_id=-1;
		}
		$user_id=trim($user_id);
		if($user_id==''){
			$user_id=-1;
		}
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen(trim($usuario->id))==0){
			$usuario->id=-1;
		}

		if($comercio_id!=-1){
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->where("id","=",$comercio_id)->find_All();
		}else{
			$comercios = ORM::factory('comercio')->where("user_id","=",$usuario->id)->find_All();
		}
		try{
			$oferta=$this->request->post('oferta');
		}catch(Exception $ex){
			$oferta=-1;
		}
		$oferta=trim($oferta);
		if($oferta==''){
			$oferta=-1;
		}
		if($token=='buscar'){ //caso de token de buscar
			foreach($comercios as $k=>$comercio){
				$count=1;
				$productos_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("tiempo_validez",">",DB::expr("NOW()"))->find_All();
				$productos_no_validos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("tiempo_validez","<=",DB::expr("NOW()"))->find_All();
				$productos_linea = ORM::factory('producto')
						->where("comercio_id","=",$comercio->id)
						->where("tienda_id","=",'1')->find_All();
				$all_products[]=$productos_linea->as_array();
				$lista_productos=array();
				foreach($productos_validos->as_array() as $l=>$p){
					$p=$p->as_array();
					if($codigo==-1){
						$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
					}else{
						$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("codigo_cupon","=",$codigo)->find_all();
					}

					//echo json_encode($detalles->as_array());
					foreach($detalles->as_array() as $id=>$var){
						if(isset($var->carrito_id)==true){
							$carrito_id=$var->carrito_id;
							$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
							if(strlen($carrito->pago)>=19){
								$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
								$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
								$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
								$p['numero']=$count;
								$p['status']=$var->status;
								$p['codigo_cupon']=$var->codigo_cupon;
								//echo $usuario->name."<br>";
								if($var->is_gift=='1'){
									$p['nombre']=$var->regalo_nombre;
								}else{
									$p['nombre']=$usuario->name;
								}
								if(strlen($carrito->created)>0){
									$p['fecha_compra']=$carrito->created;
								}
								$date=explode(" ",$p['fecha_compra']);
								$p['fecha_compra']=$date[0];
								$p['codigo_validacion']=$var->codigo_validacion;
								$p['tienda']=$tienda;
								$p['comercio']=array();
								$p['comercio']=$comercio->as_array();
								$p['valido']='no caducado';
								if(strlen($comercio->logo_id)>0){
									try{
										$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
									}catch(Exception $ex){
										$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
									}
								}
								if(isset($imagen->id)==false){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
								$p['comercio']['logo']=$imagen;
								if(isset($p['comercio']['name'])==false){
									$p['comercio']['name']=$p['comercio_nombre'];
								}
								$p['id']=$var->id;
								$lista_productos[]=$p;
								$count++;
							}
						}
					}
				}

				foreach($productos_no_validos->as_array() as $l=>$p){
					$p=$p->as_array();
					if($codigo==-1){
						$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
					}else{
						$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("codigo_cupon","=",$codigo)->find_all();
					}

					//echo json_encode($detalles->as_array());
					foreach($detalles->as_array() as $id=>$var){
						if(isset($var->carrito_id)==true){
							$carrito_id=$var->carrito_id;
							$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
							if(strlen($carrito->pago)>=19){
								$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
								$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
								$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
								$p['numero']=$count;
								$p['status']=$var->status;
								$p['codigo_cupon']=$var->codigo_cupon;
								//echo $usuario->name."<br>";
								if($var->is_gift=='1'){
									$p['nombre']=$var->regalo_nombre;
								}else{
									$p['nombre']=$usuario->name;
								}
								if(strlen($carrito->created)>0){
									$p['fecha_compra']=$carrito->created;
								}
								$date=explode(" ",$p['fecha_compra']);
								$p['fecha_compra']=$date[0];
								$p['codigo_validacion']=$var->codigo_validacion;
								$p['tienda']=$tienda;
								$p['comercio']=array();
								$p['comercio']=$comercio->as_array();
								$p['valido']='caducado';
								if(strlen($comercio->logo_id)>0){
									try{
										$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
									}catch(Exception $ex){
										$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
									}
								}
								if(isset($imagen->id)==false){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
								$p['comercio']['logo']=$imagen;
								if(isset($p['comercio']['name'])==false){
									$p['comercio']['name']=$p['comercio_nombre'];
								}
								$p['id']=$var->id;
								$lista_productos[]=$p;
								$count++;
							}
						}
					}
				}
			}
		}else{	//caso de token de oferta
			if($token=='oferta'){
				$all_products=array();
				foreach($comercios as $k=>$comercio){
					$count=1;
					$productos = ORM::factory('producto')->where("comercio_id","=",$comercio->id)->where("tiempo_validez",">",DB::expr("NOW()"))->find_all();
					//echo ORM::factory('producto')->last_query();
					$productos_linea = ORM::factory('producto')
						->where("comercio_id","=",$comercio->id)
						->where("tienda_id","=",'1')->find_All();
					$all_products[]=$productos_linea->as_array();

					$lista_productos=array();
					foreach($productos->as_array() as $l=>$p){
						$p=$p->as_array();
						if($oferta==-1){
							$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
						}else{
							$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("producto_id","=",$oferta)->find_all();
						}
						foreach($detalles->as_array() as $id=>$var){
							if(isset($var->carrito_id)==true){
								$carrito_id=$var->carrito_id;
								$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
								if(strlen($carrito->pago)>=19){
									$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
									$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
									$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
									$p['numero']=$count;
									$p['status']=$var->status;
									$p['codigo_cupon']=$var->codigo_cupon;
									if(strlen($carrito->created)>0){
										$p['fecha_compra']=$carrito->created;
									}
									$date=explode(" ",$p['fecha_compra']);
									$p['fecha_compra']=$date[0];
									if($var->is_gift=='1'){
										$p['nombre']=$var->regalo_nombre;
									}else{
										$p['nombre']=$usuario->name;
									}
									$p['codigo_validacion']=$var->codigo_validacion;
									$p['tienda']=$tienda;
									$p['comercio']=array();
									$p['comercio']=$comercio->as_array();
									if(strlen($comercio->logo_id)>0){
										try{
											$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
										}catch(Exception $ex){
											$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
										}
									}
									if(isset($imagen->id)==false){
										$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
									}
									$p['comercio']['logo']=$imagen;
									if(isset($p['comercio']['name'])==false){
										$p['comercio']['name']=$p['comercio_nombre'];
									}
									$p['id']=$var->id;
									$lista_productos[]=$p;
									$count++;
								}
							}
						}
					}
				}
			}else{
				$comercios=array();
				$lista_productos=array();
			}
		}
		//$all_products=$all_products->as_array();
//		echo json_encode($all_products);

		$comercios = ORM::factory('comercio')->find_all();
		$this->template->content = View::factory('comercios/view_lista_cupones_buscar')
			->set("cupones",$lista_productos)
			->set("comercios",$comercios)
			->set("ofertas",$all_products)
			->set("type",$controller);

	}/**/


	public function action_aprobacion_ofertas(){
		$url='comercios/cupones/aprobacion_ofertas';
		$url_validar='comercios/cupones/validar';
		$url_publicar='comercios/cupones/publicar';
		$url_agregar='comercios/cupones/agregar';
		$controller='aprobacion_ofertas';
		try{
			$token=$this->request->post('token');
		}catch(Exception $ex){
			$token="buscar";
		}
		$token=trim($token);
		if($token==''){
			$token='buscar';
		}
		try{
			$codigo=$this->request->post('codigo_cupon');
		}catch(Exception $ex){
			$codigo=-1;
		}
		$codigo=trim($codigo);
		if($codigo==''){
			$codigo=-1;
		}
		//echo $codigo."<br>";
		try{
			$comercio_id=$this->request->query('comercio');
		}catch(Exception $ex){
			$comercio_id=-1;
		}
		if($comercio_id==''){
			$comercio_id=-1;
		}
		try{
			$user_id = Auth::instance()->get_user()->id;
		}catch(Exception $ex){
			$user_id=-1;
		}
		$user_id=trim($user_id);
		if($user_id==''){
			$user_id=-1;
		}
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen(trim($usuario->id))==0){
			$usuario->id=-1;
		}

		$comercios = ORM::factory('comercio')->find_All();
		try{
			$oferta=$this->request->post('oferta');
		}catch(Exception $ex){
			$oferta=-1;
		}
		$oferta=trim($oferta);
		if($oferta==''){
			$oferta=-1;
		}

		foreach($comercios as $k=>$comercio){
			$count=1;
			$productos_validos=array();
			$productos_no_validos=array();
			if($token=='buscar'){
				$productos_validos = ORM::factory('producto')->where("status","=",'0')->
					where("tiempo_validez",">",DB::expr("NOW()"))->find_All();
				$productos_no_validos = ORM::factory('producto')->where("status","=",'0')->
					where("tiempo_validez","<=",DB::expr("NOW()"))->find_All();
			}elseif($token=='oferta'){
				$productos_validos = ORM::factory('producto')->where("status","=",'0')->
					where("id","=",$oferta)->where("tiempo_validez",">",DB::expr("NOW()"))->find_All();
				$productos_no_validos = ORM::factory('producto')->where("status","=",'0')->
					where("id","=",$oferta)->where("tiempo_validez","<=",DB::expr("NOW()"))->find_All();
			}
			/*$productos_linea = ORM::factory('producto')
			   ->where("comercio_id","=",$comercio->id)
			   ->where("tienda_id","=",'1')->find_All();
			   $all_products[]=$productos_linea->as_array();*/
			$lista_productos=array();
			foreach($productos_validos->as_array() as $l=>$p){
				$p=$p->as_array();
				if($codigo==-1){
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
				}else{
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("codigo_cupon","=",$codigo)->find_all();
				}
				foreach($detalles->as_array() as $id=>$var){
					if(isset($var->carrito_id)==true){
						$carrito_id=$var->carrito_id;
						$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
						//if(strlen($carrito->pago)>=19){
							$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
							$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
							$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
							$p['numero']=$count;
							$p['status']=$var->status;
							$p['codigo_cupon']=$var->codigo_cupon;
							//echo $usuario->name."<br>";
							if($var->is_gift=='1'){
								$p['nombre']=$var->regalo_nombre;
							}else{
								$p['nombre']=$usuario->name;
							}
							if(strlen($carrito->created)>0){
								$p['fecha_compra']=$carrito->created;
							}
							$date=explode(" ",$p['fecha_compra']);
							$p['fecha_compra']=$date[0];
							$p['codigo_validacion']=$var->codigo_validacion;
							$p['tienda']=$tienda;
							$p['comercio']=array();
							$p['comercio']=$comercio->as_array();
							$p['valido']='no caducado';
							if(strlen($comercio->logo_id)>0){
								try{
									$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
								}catch(Exception $ex){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
							}
							if(isset($imagen->id)==false){
								$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
							}
							$p['comercio']['logo']=$imagen;
							if(isset($p['comercio']['name'])==false){
								$p['comercio']['name']=$p['comercio_nombre'];
							}
							$p['producto_id']=$p['id'];
							$p['detalle_id']=$var->id;
							$p['id']=$var->id;
							$p['validated']=$var->validated;
							$lista_productos[]=$p;
							$count++;
						//}
					}
				}
			}

			foreach($productos_no_validos->as_array() as $l=>$p){
				$p=$p->as_array();
				if($codigo==-1){
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->find_all();
				}else{
					$detalles = ORM::factory('detalleproducto')->where("producto_id","=",$p['id'])->where("codigo_cupon","=",$codigo)->find_all();
				}
				foreach($detalles->as_array() as $id=>$var){
					if(isset($var->carrito_id)==true){
						$carrito_id=$var->carrito_id;
						$carrito = ORM::factory('carrito')->where("id","=",$carrito_id)->find();
						//if(strlen($carrito->pago)>=19){
							$usuario=ORM::factory('user')->where('id',"=",$carrito->user_id)->find();
							$tienda = ORM::factory('tienda')->where("id","=",$p['tienda_id'])->find();
							$comercio = ORM::factory('comercio')->where("id","=",$p['comercio_id'])->find();
							$p['numero']=$count;
							$p['status']=$var->status;
							$p['codigo_cupon']=$var->codigo_cupon;
							//echo $usuario->name."<br>";
							if($var->is_gift=='1'){
								$p['nombre']=$var->regalo_nombre;
							}else{
								$p['nombre']=$usuario->name;
							}
							if(strlen($carrito->created)>0){
								$p['fecha_compra']=$carrito->created;
							}
							$date=explode(" ",$p['fecha_compra']);
							$p['fecha_compra']=$date[0];
							$p['codigo_validacion']=$var->codigo_validacion;
							$p['tienda']=$tienda;
							$p['comercio']=array();
							$p['comercio']=$comercio->as_array();
							$p['valido']='caducado';
							if(strlen($comercio->logo_id)>0){
								try{
									$imagen= ORM::factory('file')->where("id","=",$comercio->logo_id)->find();
								}catch(Exception $ex){
									$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
								}
							}
							if(isset($imagen->id)==false){
								$imagen=ORM::factory('file')->where("id","=",$p->comercio_logo_id)->find();
							}
							$p['comercio']['logo']=$imagen;
							if(isset($p['comercio']['name'])==false){
								$p['comercio']['name']=$p['comercio_nombre'];
							}
							$p['producto_id']=$p['id'];
							$p['detalle_id']=$var->id;
							$p['id']=$var->id;
							$p['validated']=$var->validated;
							$lista_productos[]=$p;
							$count++;
						//}
					}
				}
			}
		}
		$comercios = ORM::factory('comercio')->find_all();
		$this->template->content = View::factory('comercios/view_lista_cupones_aprobacion')
			->set("cupones",$lista_productos)
			->set("comercios",$comercios)
			->set("type",$controller)
			->set("url",$url)
			->set("url_validar",$url_validar)
			->set("url_agregar",$url_agregar)
			->set("checkbox",$checkbox)
			->set("oferta",$oferta);
	}
}