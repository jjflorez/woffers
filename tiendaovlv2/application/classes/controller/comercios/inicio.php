<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Comercios_Inicio extends Controller_Template
{
    public $template = 'ci/view_template';

    public function action_index()
    {
        $this->template->content = View::factory('comercios/view_inicio');
    }
}