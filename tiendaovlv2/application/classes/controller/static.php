<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Static extends Controller_Template
{
    public $template = 'ci/view_template';

    public function action_index()
	{

	}

    public function action_oportunidad()
    {
        $_url = $this->request->param('section');

		switch ($_url){
			case 'gana-con-nosotros':
				$this->template->content = View::factory('home/view_gana');
				break;
			case 'tipos-socios':
				$this->template->content = View::factory('home/view_tipos_socios');
				break;
			case 'conferencias':
				$this->template->content = View::factory('home/view_conferencias');
				break;
			default:
				$this->request->redirect('oportunidad/gana-con-nosotros');
		}
    }

	public function action_sobre()
	{
		$_url = $this->request->param('section');

		$_page = ORM::factory('page')->where('url', '=', $_url)->find();

		if($_page->loaded()){
			$this->template->content = View::factory('home/view_page')
				->set('page', $_page);
		}

		/*switch ($_url){
			case 'que-es-ovl':
				$this->template->content = View::factory('content/view_gana');
				break;
			case 'como-funciona':
				$this->template->content = View::factory('content/view_tipos_socios');
				break;
			case 'vision-solidaria':
				$this->template->content = View::factory('content/view_conferencias');
				break;
			default:
				$this->request->redirect('sobre/que-es-ovl');
		}*/
	}
}