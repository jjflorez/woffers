<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_Template
{
    public $template = 'ci/view_template';

    public function action_login()
	{
		$this->template->content = View::factory('view_login');
	}

    public function action_logout()
    {
        $session = Session::instance();
        $session->delete('IDCARRITO');

        Kohana_Cookie::delete('TOVLID');
        Auth::instance()->logout();

        $_url = "http://dev.onevisionlife.es/backoffice/api/session/check/?action=logout&redirect=http://dev.onevisionlife.es/tiendaovlv2/";
        $this->request->redirect($_url);
    }

}