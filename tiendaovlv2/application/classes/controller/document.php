<?php
/*
Modified in tienda for christian portilla pauca
Mail: xhrist14n@gmail.com

*/

?>
<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Document extends Controller
{
	//public $template = 'ci/view_template';
	private $_pdf_filename;
	private $_files = array();

	public function before()
	{
		$redirect = $this->request->query('redirect');
		if($redirect)
		if(!Kohana_Auth::instance()->logged_in())
		{
			Kohana_Cookie::set('next_url', urlencode($_SERVER['HTTP_REFERER']));
			$this->request->current()->redirect('session/generate');
		}
		else
			if(!Kohana_Auth::instance()->logged_in())	$this->request->current()->redirect('session/generate');
		parent::before();
	}

	public function action_index()
	{
		$id_producto = $_GET['idpr'];
		$id_pedido = $_GET['idpe'];

		$m_usuario = new Model_User();
		//$ruta = 'http://'.$_SERVER['SERVER_NAME'].'/app_server/index.php/api/compras/producto/' . $id_pedido . '/'. $id_producto;
		$ruta = 'http://'.$_SERVER['SERVER_NAME'].'/tiendaovlv2/index.php/api/compras/producto/' . $id_pedido . '/'. $id_producto;

		$oferta = json_decode(file_get_contents($ruta));

		$usuario = $m_usuario->where('id', '=', Kohana_Auth::instance()->get_user()->id)->find();

		$config = array(
			'author'   => 'OneVisionLife',
			'title'    => 'Cupon',
			'subject'  => 'Oferta',
			'name'     => 'OVLcupon_' . Text::random().'.pdf', // name file pdf
		);

		/*$this->response->body(View::factory('pdf/view_cupon')
		   ->set('usuario', $usuario)
		   ->set('oferta', $oferta->data)
		   );*/
		echo '<pre>';
		print_r($oferta);
		echo '</pre>';

		//$view = View_PDF::factory('pdf/view_cupon', $config)
		//->set('dados', )
		//	->render();
	}

	public function action_oferta()
	{
		//if(Auth::instance()->logged_in()){
		$id_producto = $this->request->param('param1');
		$id_carrito = $this->request->param('param2');
		$this->_create_pdf($id_producto, 'I',$id_carrito);
		/*}else{
		   $this->request->current()->redirect('http://dev.onevisionlife.es/tiendaovlv2/');
		   }*/
	}

	public function action_send_oferta()
	{
		$_productos = unserialize(urldecode($this->request->query('i')));

		//echo "<pre>"; print_r($_productos); die();

		foreach($_productos as $_pro){
			$this->_send($_pro);
		}

		$id_producto = $this->request->param('param1');
		$redirect = $this->request->query('redirect');
		$id_carrito = $this->request->query('redirect');

		$this->request->redirect($redirect);
	}

	private function _send($id_pro)
	{
		$ruta = 'http://dev.onevisionlife.es/tiendaovlv2/index.php/api/compras/oferta/'. $id_pro;
		$oferta = json_decode(file_get_contents($ruta));
		//echo "<pre>"; print_r($oferta); die();

		$m = new Mailer();

		if($oferta->data->is_gift){

			if($oferta->data->regalo_send){//ovl

				$_cont = '<p style="font-size: 13px;"><b style="color: #738915;">'.$oferta->data->carrito->user->name.'</b> te ha regalado el siguiente cup�n y te escrito el siguiente mensaje</p>';
				$_cont .= '<p style="font-size: 13px; color: #738915; padding: 0 45px; text-align: center;">"'.$oferta->data->regalo_mensaje.'"</p>';
				$_cont .= '<p style="font-size: 13px;">Puedes <b>encontrar</b> tu <b>cup�n adjunto en este mismo correo.</b><br><br>Solo tienes que imprimirlo, seguir las instrucciones y... �Disfrutar!</p>';

				$_email = $oferta->data->regalo_email;
				$_nombre = $oferta->data->regalo_nombre;

				$template = View::factory('view_mail')
				                ->set('titulo', '�EN HORA BUENA, HAS RECIBIDO UN REGALO!')
				                ->set('contenido', $_cont);

				$_mensaje = $template;
				$_subject = "�Has recibido un regalo de One Vision Life!";

				//email usuario ovl
				$_msg = '<p style="font-size: 13px;">Ya puedes encontrar tu cup�n en el area de <b style="color: #738915;">Compras</b> de tu <b style="color: #738915;">Oficina Virtual</b>. Recuerda que aparecera como <b>Regalado.</b></p>';
				$_msg .= '<p style="font-size: 13px;">One Vision Life ha enviado a la persona indicada el cup�n que escogiste y lo recibir� en su correo para descarg�rselo y ... �Disfutar!</p>';

				$_email2 = "david.kundalini@gmail.com";//$oferta->data->carrito->user->email;
				$_nombre2 = $oferta->data->carrito->user->name;
				$_subject2 = "�Gracias por regalar con One Vision Life!";

				$template2 = View::factory('view_mail')
				    ->set('titulo', '�GRACIAS POR REGALAR CUPONES EN ONE VISION LIFE!')
				    ->set('contenido', $_msg);
				$_mensaje2 = $template2;
				$_file2 = "";

				$m->send($_email2, $_nombre2, $_subject2, $_mensaje2, $_file2);

			}else{//yp mismo

				$_cont = '<p style="font-size: 13px;">Ya puedes encontrar tu cup�n en el area de <b style="color: #738915;">Compras</b> de tu <b style="color: #738915;">Oficina Virtual</b>. Recuerda que aparecera como <b>Regalado.</b></p>';
				$_cont .= '<p style="font-size: 13px;">Tambi�n puedes <b>encontrar</b> tu <b>cup�n adjunto en este mismo correo para regal�rselo a quien tu quieras.</b></p>';
				$_cont .= '<p style="font-size: 13px;">Puedes hacer dos cosas:</p>';
				$_cont .= '<p style="text-align: center;"><br><img src="http://dev.onevisionlife.es/backoffice_user/public/img/email_btn.jpg" alt=""></p>';

				$_email = "david.kundalini@gmail.com";//$oferta->data->carrito->user->email;
				$_nombre = $oferta->data->carrito->user->name;

				$template = View::factory('view_mail')
				    ->set('titulo', '�GRACIAS POR REGALAR CUPONES EN ONE VISION LIFE!')
				    ->set('contenido', $_cont);

				$_mensaje = $template;
				$_subject = "�Gracias por regalar con One Vision Life!";
			}


		}else{//
			$_email = "david.kundalini@gmail.com";//$oferta->data->carrito->user->email;
			$_nombre = $oferta->data->carrito->user->name;
			$_subject = "�Gracias por comprar en One Vision Life! ";

			$_contenido = '<p style="font-size: 13px;">Puedes encontrar tu cup�n en el area de <b style="color: #738915;">Compras</b> de tu <b style="color: #738915;">Oficina Virtual</b>. Recuerda que aparecera como <b>Disponible</b> para utilizarlo cuando t� quieras</p>';
			$_contenido .= '<p style="font-size: 13px;">Tambi�n puedes <b>encontrar</b> tu <b>cup�n adjunto en este mismo correo</b></p>';
			$_contenido .= '<p style="font-size: 13px;">Solo tienes que imprimirlo, seguir las instrucciones y ... �Disfrutrar!</p>';

			$template = View::factory('view_mail')
			                ->set('titulo', '�GRACIAS POR COMPRAR EN ONE VISION LIFE!')
			                ->set('contenido', $_contenido);

			$_mensaje = $template;
		}

		try {
			$this->_create_pdf($id_pro, 'F',NULL);
		} catch (Exception $e) {
			echo "error"; die();
		}

		//$_file = 'temp/'.$this->_pdf_filename;
		$_file = 'temp/'.$this->_files[$id_pro];

		$m->send($_email, $_nombre, $_subject, $_mensaje, $_file);

		if(file_exists($_file)) unlink($_file);
	}

	private function _create_pdf($id_producto,$_destino,$id_carrito)
	{

		if($id_carrito=null){
			$id_carrito=-1;
		}
		date_default_timezone_set("Europe/Madrid");
		$m_usuario = new Model_User();
		$ruta = 'http://dev.onevisionlife.es/tiendaovlv2/index.php/api/compras/oferta/'. $id_producto."/".$id_carrito;
		//$ruta = URL::base("http").'api/compras/oferta/' . $id_producto."/".$id_carrito;

		$oferta = json_decode(file_get_contents($ruta));

		//echo json_encode($oferta); die();

		$usuario = $m_usuario->where('id', '=', Kohana_Auth::instance()->get_user()->id)->find();

		$config = array(
		    'author'   => 'OneVisionLife',
		    'title'    => 'Cupon',
		    'subject'  => 'Oferta',
		    'name'     => 'OVLcupon_' . Text::random().'.pdf', // name file pdf
		);

		$fontsize_big = 15;
		$fontsize_normal = 10.5;
		$fontsize_small = 10.5;

		$img1_x = 54;
		$img1_y = 109;
		$img2_x = 295;
		$img2_y = 303;

		$col1_x = 49;
		$col1_y = 233;
		$col1A_x = $col1_x;
		$col1A_y = $col1_y + 85;

		$col2_x = 295;
		$col2_y = 233;
		$col2A_x = $col2_x;
		$col2A_y = $col2_y + 85;

		$ln_header_x = 210;
		$ln_header_y = 109;

		$boder = 0;
		ini_set("memory_limit","80M");

		$this->response->headers('Content-type', 'application/pdf');
		try{
			$pdf = new TCPDF('P', 'px', 'A4');
		}catch(Exception $ex){
			$pdf=null;
		}


		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->addPage();
		$pdf->setFontStretching(98);
		//TODO: --------------------- Fondo del documento
		//echo URL::base() . 'public/img/pdf/bg_cupon_v2.jpg';
		//die();
		$image_path=URL::base("http").'public/img/pdf/bg_cupon_v2.jpg';
		$file_headers=@get_headers($image_path);
		if( trim($file_headers[0])!='HTTP/1.1 404 Not Found' ){
			$pdf->Image( URL::base("http").'public/img/pdf/bg_cupon_v2.jpg', 41, 27);
		}

		//echo json_encode($oferta);
		//die();

		//TODO: --------------------- Header
		$image_path=trim($oferta->data->producto->oferta_imagen->url_path);
		$file_headers=@get_headers($image_path);
		if(trim($file_headers[0])!='HTTP/1.1 404 Not Found' && $image_path!=URL::base("http") && $image_path!=''){
			try{
				$pdf->Image($oferta->data->producto->oferta_imagen->url_path, $img1_x, $img1_y, 144, 101, '', '', '', false, 300, '', false, false, 1, true);
			}catch(Exception $ex){
				echo "Problemas con imagen de oferta"."<br";

			}
		}
		$pdf->setFontSize(14);
		$pdf->SetTextColor(17,107,92);
		$pdf->SetFont('', 'B', 14, '', false);

		if($oferta->data->is_gift == 1){
			$pdf->Text($ln_header_x, $ln_header_y, "CUP�N REGALO");
		}else{
			if(isset($oferta->data->producto->oferta_tipo)==true){
				$pdf->Text($ln_header_x, $ln_header_y, strtoupper($oferta->data->producto->oferta_tipo));
			}

			$pdf->Text($ln_header_x, $ln_header_y, "CUP�N");
		}

		$pdf->MultiCell(300, 50, $oferta->data->producto->oferta_descuento . "% Dto. " . $oferta->data->producto->oferta_titulo, $boder, 'L', false, 1, $ln_header_x, $ln_header_y + 20);

		$pdf->SetTextColor(135,135,134);

		if($oferta->data->is_gift == 1){
			//$pdf->MultiCell(300, 15, 'icon', $boder, 'L', false, 1, $ln_header_x, $ln_header_y + 78);
			$image_path=URL::base('http') . 'public/img/pdf/regalo.jpg';
			$file_headers=@get_headers($image_path);
			if(trim($file_headers[0])!='HTTP/1.1 404 Not Found' && $image_path!=URL::base("http") ){
				$pdf->Image(URL::base('http').'public/img/pdf/regalo.jpg', $ln_header_x + 5, $ln_header_y + 75, 28, 35, '', '', '', false, 300, '', false, false, 0, true);
			}
			$pdf->SetFont('', '', 10.5, '', false);
			$pdf->setCellHeightRatio(1.2);
			$_txt = "De: <b>".$oferta->data->carrito->user->name." ".
			         $oferta->data->carrito->user->last_name .
			         "</b> <br>Para: <b>".$oferta->data->regalo_nombre.
			         "</b> <br> ";
			$pdf->writeHTMLCell(300, 0, $ln_header_x + 42, $ln_header_y + 78, $_txt, 0, 0, 0, true, 'L', true);
			$pdf->writeHTMLCell($w, $h, $x, $y);
			$pdf->MultiCell();
		}else{
			$pdf->MultiCell(300, 15, 'Precio ' . $oferta->data->monto . '�', $boder, 'L', false, 1, $ln_header_x, $ln_header_y + 70);
		}

		//TODO: --------------------- Bloque 2
		$pdf->SetFont('', 'B', 10.5, '', false);
		$pdf->setFontSize($fontsize_normal);
		$pdf->SetTextColor(50,50,50);
		if($oferta->data->carrito->pago!=''){
			$f_inicio = $oferta->data->carrito->pago;
		}else{
			$f_inicio = "";
		}

		$f_fin = $oferta->data->producto->tiempo_validez;

		$pdf->MultiCell(200, 50, $oferta->data->carrito->user->name . ' ' . $oferta->data->carrito->user->last_name, 0, 'L', false, 1, $col1_x + 50, $col1_y-1);
		//$pdf->MultiCell(220, 20, 'del ' . $f_inicio . ' al ' . $f_fin, $boder, 'L', false, 1, $col1_x, $col1_y + 35);
		$pdf->SetFont('', '', 10.5, '', false);
		/*Si el periodo no esta definido es debido a que no se hizo el pago correspondiente*/
		if($sf_inicio!=''){
			$_periodo = 'del <b>' . $f_inicio . '</b> al <b>' . $f_fin . '</b>';
		}else{
			$_periodo='No definido';
		}

		$pdf->writeHTMLCell(220, 0, 49, 233+33, $_periodo, 0, 0, 0, true, 'L', true);

		$pdf->SetFont('', 'B', 10.5, '', false);
		$pdf->MultiCell(150, 20, strtoupper($oferta->data->codigo_referencia), $boder, 'L', false, 1, $col2_x + 115, $col2_y-1);

		$pdf->MultiCell(150, 20, strtoupper($oferta->data->codigo_cupon), $boder, 'L', false, 1, $col2_x + 115, $col2_y + 16);
		$pdf->MultiCell(150, 20, strtoupper($oferta->data->codigo_validacion), $boder, 'L', false, 1, $col2_x + 115, $col2_y + 34);


		//TODO: --------------------- Bloque 3
		$pdf->SetFont('', '', '', '', false);
		$pdf->setFontSize(10);

		$pdf->setFontStretching(98);

		$pdf->setCellHeightRatio(0.8);
		$pdf->MultiCell(220, 50, $oferta->data->producto->comercio_nombre, $boder, 'L', false, 1, $col1A_x, $col1A_y, true, 2, true, false);
		$pdf->MultiCell(220, 50, $oferta->data->producto->comercio_direccion, $boder, 'L', false, 1, $col1A_x, $col1A_y + 15, true, 2, true, false);

		$image_path=trim($oferta->data->producto->comercio_imagen->url_path);
		if($image_path!=''){
			$file_headers=@get_headers($image_path);
			if(trim($file_headers[0])!='HTTP/1.1 404 Not Found' && $image_path!=URL::base("http") ){
				try{
					$pdf->Image($oferta->data->producto->comercio_imagen->url_path, $img2_x, $img2_y, 241, 82, '', '', '', true, 300, '', false, false, 0, 'CM');
				}catch(Exception $ex){
					echo "Problemas con imagen de comercio"."<br";
				}
			}
		}

		$pdf->setCellHeightRatio(1.3);
		//TODO: --------------------- Bloque 4
		$pdf->setFontSize(8);
		$pdf->MultiCell(220, 80, $oferta->data->producto->oferta_condiciones, $boder, 'L', false, 1, $col2A_x, $col2A_y + 115, true, 2, true, false);
		$pdf->MultiCell(220, 60, $oferta->data->producto->oferta_que_incluye, $boder, 'L', false, 1, $col1A_x, $col1A_y + 115, true, 2, true, false);
		//$pdf->MultiCell(220, 60, $oferta->data->producto->oferta_que_incluye, 1, 'L', false, 2, null, null, true, 1, true, true, 80);//, 1, 'L', true, 1, $col1A_x, $col1A_y + 115, true, 1, true, true,2,'T',true);
		$pdf->MultiCell(80, 50, $como_usar, $boder, 'L', false, 1, $col1A_x, $col1A_x, true, 2, true);

		//$pdf->Output('OVL_' . Text::random() . '.pdf', 'I');
		$_filename = 'OVL_' . Text::random() . '.pdf';
		$this->_pdf_filename = $_filename;
		$this->_files[$id_producto] = $_filename;
		$pdf->Output('temp/' . $_filename, $_destino);
	}
}