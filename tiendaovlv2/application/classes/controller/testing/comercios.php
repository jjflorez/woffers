<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Testing_Comercios extends Controller_Template
{
	public $template = 'ci/view_template';

	public function action_index()
	{
		$usuarios = ORM::factory('user')->find_All();

		$this->template->content = View::factory('testing/view_comercios')
			->set('usuarios',$usuarios);
	}
	public function action_login(){
		$user_id=$this->request->param('param1');
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen($usuario->id)>0){
			$session = Session::instance();
			Auth::instance()->force_login($usuario->username, TRUE);
			Kohana_Cookie::set('TOVLID', $usuario->id);
			$this->request->redirect('comercios/cupones');
		}
	}
	public function action_login_vendidos(){
		$user_id=$this->request->param('param1');
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen($usuario->id)>0){
			$session = Session::instance();
			Auth::instance()->force_login($usuario->username, TRUE);
			Kohana_Cookie::set('TOVLID', $usuario->id);
			$this->request->redirect('comercios/cupones');
		}
	}
	public function action_login_comprados(){
		$user_id=$this->request->param('param1');
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen($usuario->id)>0){
			$session = Session::instance();
			Auth::instance()->force_login($usuario->username, TRUE);
			Kohana_Cookie::set('TOVLID', $usuario->id);
			$this->request->redirect('comercios/cupones/comprados');
		}
	}
	public function action_login_canjeados(){
		$user_id=$this->request->param('param1');
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen($usuario->id)>0){
			$session = Session::instance();
			Auth::instance()->force_login($usuario->username, TRUE);
			Kohana_Cookie::set('TOVLID', $usuario->id);
			$this->request->redirect('comercios/cupones/canjeados');
		}
	}
	public function action_login_buscar(){
		$user_id=$this->request->param('param1');
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen($usuario->id)>0){
			$session = Session::instance();
			Auth::instance()->force_login($usuario->username, TRUE);
			Kohana_Cookie::set('TOVLID', $usuario->id);
			$this->request->redirect('comercios/cupones/buscar');
		}
	}
	public function action_login_buscar_consumidos(){
		$user_id=$this->request->param('param1');
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen($usuario->id)>0){
			$session = Session::instance();
			Auth::instance()->force_login($usuario->username, TRUE);
			Kohana_Cookie::set('TOVLID', $usuario->id);
			$this->request->redirect('comercios/cupones/buscar_consumidos');
		}
	}
	public function action_login_buscar_publicados(){
		$user_id=$this->request->param('param1');
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen($usuario->id)>0){
			$session = Session::instance();
			Auth::instance()->force_login($usuario->username, TRUE);
			Kohana_Cookie::set('TOVLID', $usuario->id);
			$this->request->redirect('comercios/cupones/buscar_publicados');
		}
	}
	public function action_login_buscar_no_publicados(){
		$user_id=$this->request->param('param1');
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen($usuario->id)>0){
			$session = Session::instance();
			Auth::instance()->force_login($usuario->username, TRUE);
			Kohana_Cookie::set('TOVLID', $usuario->id);
			$this->request->redirect('comercios/cupones/buscar_no_publicados');
		}
	}
	public function action_login_aprobacion_ofertas(){
		$user_id=$this->request->param('param1');
		$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
		if(strlen($usuario->id)>0){
			$session = Session::instance();
			Auth::instance()->force_login($usuario->username, TRUE);
			Kohana_Cookie::set('TOVLID', $usuario->id);
			$this->request->redirect('comercios/cupones/aprobacion_ofertas');
		}
	}
}