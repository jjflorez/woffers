<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Test extends Controller
{

    public function action_index()
    {
        echo "hola test " . $this->request->param('param1');
    }

    public function action_hola_uno()
    {
        echo "hola";
    }

    public function action_getuser()
    {
        echo "user 123";
    }

    public function action_hola()
    {
        //if(Auth::instance()->logged_in())
        echo "hola 2 " . Auth::instance()->get_user()->username;
        $productos = ORM::factory('detalleproducto')->where('carrito_id', '=', 56)->find_all();
        //echo Database::instance()->last_query;die();

        $productos_array = array();
        $total_comision = 0;

        foreach($productos as $p){
            echo "<br>" . $p->producto_id . " -- " . $p->id . " -- " . $p->carrito_id;
            $pro = ORM::factory('producto', $p->producto_id);
            echo "<br>" . $pro->oferta_titulo;
            echo "<br>" . $pro->get_oferta_precio_final();
            echo "<br>" . $pro->oferta_comision_ovl;
            echo "<br>" . $pro->tienda->comision_ovl;
            //var_dump($pro);
            if($pro->oferta_comision_ovl == -1 || $pro->oferta_comision_ovl == ''){
                $total_comision += ($pro->get_oferta_precio_final() * $pro->tienda->comision_ovl) / 100;
            }else{
                $total_comision += ($pro->get_oferta_precio_final() * $pro->oferta_comision_ovl) / 100;
            }

            $productos_array[] = $p->id;
        }

        $this->_addcomision($total_comision, $p->carrito_id);
        echo "<hr>";

        echo "total: " . $total_comision;
    }

    private function _addcomision($comision=0, $idcompra=0)
    {
        $username = Auth::instance()->get_user()->username;
        $url = "http://localhost/backoffice/api/bono/add/".$username."/".$comision."/".$idcompra;

        //$url = "http://dev.onevisionlife.es/backoffice/api/bono/add/".$username."/".$comision."/".$idcompra;
        $valor1 = "valor0001";
        $valor2 = "valor0002";
        $parametros_post = 'parametro1='.urlencode($valor1).'&parametro2='.urlencode($valor2);

        $sesion = curl_init($url);
        curl_setopt ($sesion, CURLOPT_POST, true);
        curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post);
        curl_setopt($sesion, CURLOPT_HEADER, false);
        curl_setopt($sesion, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec($sesion);
        curl_close($sesion);

        var_dump($respuesta);
    }
}