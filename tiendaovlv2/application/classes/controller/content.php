<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Content extends Controller_Template
{
    public $template = 'ci/view_template';

    public function action_index()
    {
        $_url = $this->request->param('param1');

        $_page = ORM::factory('page')->where('url', '=', $_url)->find();

        if($_page->loaded()){
            $this->template->content = View::factory('view_page')
                ->set('page', $_page);
        }
    }

    public function action_oportunidad_de_negocio()
    {
        $this->template->content = View::factory('home/view_oportunidad');
    }

    public function action_conferencias()
    {
        $this->template->content = View::factory('home/view_conferencias');
    }
    public function action_registro_comercios()
    {
        $this->template->content = View::factory('home/view_registro_comercios');
    }


}