<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Session extends Controller
{
    //public $template = 'ci/view_template';

    //public $url_backoffice_check = 'http://localhost/backoffice/api/session/check';
    public $url_backoffice_check = 'http://dev.onevisionlife.es/backoffice/api/session/check';
    //public $url_backoffice_find = 'http://localhost/backoffice/api/user/find/';
    public $url_backoffice_find = 'http://dev.onevisionlife.es/backoffice/api/user/find/';


    public function action_index()
    {
        //$this->template->content = View::factory('view_home');
    }

    public function action_session()
    {

    }

    public function action_generate()
    {
        $check = Kohana_Cookie::get('SESSCHECK');

        if($check != '1')
        {
            Kohana_Cookie::set('SESSCHECK', '1');
            $this->request->redirect($this->url_backoffice_check . '?redirect=' . URL::site('session/generate', 'http'));
            //echo 'check';
        }

        if($this->request->query('r'))
        {
            Kohana_Cookie::delete('SESSCHECK');
            $session = Session::instance();

            $r = base64_decode(urldecode($this->request->query('r')));
            //$i = base64_decode(urldecode($this->request->query('i')));
            $i = "";

            $id_usuario = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, 'OVLComunicationSystem2012', $r, MCRYPT_MODE_CFB, $i);

            if($id_usuario == 'no_session' || $id_usuario == '')
            {
                echo 'no sesión';
                //return;
                //$this->request->redirect('http://dev.onevisionlife.es');
                $this->request->redirect('/home/login/');
            }

            $data = json_decode(file_get_contents($this->url_backoffice_find . $id_usuario));

			//echo '<pre>' .$id_usuario;
			//print_r($data);

            $usuario = ORM::factory('User')->where('username', '=' ,$data->data[0]->LOGINUSUARIO)->find();

            if($usuario->loaded())
            {
                Auth::instance()->force_login($usuario, TRUE);
				Kohana_Cookie::set('TOVLID', $usuario->id);

                $this->request->redirect('cesta/');
            }else{
                //echo "<pre>"; print_r($data); die();
                $_pass = uniqid();
                $_user = ORM::factory('User');

                $_email = ORM::factory('User')->where('email', '=', $data->data[0]->EMAIL)->find();

                if($_email->loaded()){
                    $user_email = uniqid() . "_tovl" . $data->data[0]->EMAIL;
                }else{
                    $user_email = $data->data[0]->EMAIL;
                }

                $_user->values(array(
                    'username' => $data->data[0]->LOGINUSUARIO,
                    'password' => $_pass,
                    'password_confirm' => $_pass,
                    /*'email' => $data->data[0]->EMAIL,*/
                    'email' => $user_email,
                    'name' => $data->data[0]->NOMBRES,
                    'lastname' => $data->data[0]->APEPATERNO
                ));

                try{
                    $_user->save();
                    $_user->add('roles', ORM::factory('Role')->where('name', '=', 'login')->find());

                    Auth::instance()->force_login($_user, TRUE);
					Kohana_Cookie::set('TOVLID', $_user->id);

                    $this->request->redirect('cesta/');
                    /*
                    if($session->get('IDCARRITO')){
                        $this->request->redirect('cesta/');
                    }else{
                        $this->request->redirect('http://dev.onevisionlife.es/backoffice_user/compras');
                    }*/

                }catch (ORM_Validation_Exception $e){
                     echo $e->getMessage()."<pre>";
                    print_r($e->errors());
                }
            }
            //$this->request->redirect($this->request->)
        }else{
            if($check != '1')            {
                echo 'go home';
                //$this->request->redirect('http://dev.onevisionlife.es');
            }else{
                echo "no se puede verificar la session " . Kohana_Cookie::get('SESSCHECK');

                Kohana_Cookie::delete('SESSCHECK');
                $this->request->redirect('cesta/');
            }
        }
    }

    public function action_generatesession()
    {
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CFB), MCRYPT_RAND);
        $_iv = base64_encode($iv);

        $encriptado = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, 'clave', 'gersondanieladuviri pareds', MCRYPT_MODE_CFB, $iv);
        echo 'RESult: ' . base64_encode($encriptado) . '<br/>';
        //echo mcrypt_decrypt(MCRYPT_RIJNDAEL_256, 'clave', $encriptado, MCRYPT_MODE_CFB, $iv);
        echo mcrypt_decrypt(MCRYPT_RIJNDAEL_256, 'clave', $encriptado, MCRYPT_MODE_CFB, base64_decode($_iv));

        //echo mc('holaaa', 'clave');

        $m_ovluser = new Model_OvlUsuario();
        $m_user = new Model_User();

        $redirect = $_GET['redirect'];
        $username = $_GET['username'];


        if($username)
        {
            $m_user->where('username', '=', $username);
            $user = $m_user->find_all();
            if(count($user) > 0){
                //Inicia sesión
                Auth::instance()->force_login($user[0]);

                if($redirect)
                    $this->request->redirect($redirect);
                else
                    $this->request->redirect('/');
            }else{
                //Guarda usuario y genera sesión
                $m_ovluser->where('LOGINUSUARIO', '=', $username);
                $userovl = $m_ovluser->find_all();
                if(count($userovl))
                {
                    $newUser['password'] = Kohana_Auth::instance()->hash(uniqid());
                    $newUser['email'] = $userovl[0]->persona->EMAIL;
                    $newUser['username'] = $userovl[0]->LOGINUSUARIO;
                    $newUser['name'] = $userovl[0]->persona->NOMBRES;
                    $newUser['last_name'] = $userovl[0]->persona->APEPATERNO;

                    $v_params = Validation::factory($newUser);
                    $v_params->rule('username', 'not_empty');
                    $v_params->rule('email', 'not_empty');
                    $v_params->rule('email', 'email_domain');

                    if($v_params->check())
                    {
                        $m_user->values($v_params->data());
                        $m_user->save();

                        Auth::instance()->force_login($userovl[0]->LOGINUSUARIO);
                        if($redirect)
                            $this->request->redirect($redirect);
                        else
                            $this->request->redirect('/');
                    }else{
                        $this->request->redirect('/');
                    }
                }else{
                    $this->request->redirect('/');
                }
            }
        }else{
            $this->request->redirect('/');
        }

        //Auth::instance()->login()
    }

    public function action_test()
    {
        $iv = ""; // mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CFB), MCRYPT_RAND);

        $encriptado = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, 'clave', 'gersondanieladuviri pareds', MCRYPT_MODE_CFB, $iv);
        echo 'RESult: ' . base64_encode($encriptado) . '<br/>';

        echo mcrypt_decrypt(MCRYPT_RIJNDAEL_256, 'clave', $encriptado, MCRYPT_MODE_CFB, $iv);
    }

}
