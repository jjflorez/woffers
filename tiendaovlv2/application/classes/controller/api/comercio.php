<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Comercio extends Quickdev_Api
{
    public function action_index()
    {
    }
    public function action_select()
    {
        $response = new Quickdev_Response();
        $id = $this->request->param('param1');
        if ($id) {
            $comercio = new Model_Comercio($id);
            $response->data = $comercio->as_array();
            if(isset($comercio->logo_id))
            {
                $image=new Model_File($comercio->logo_id);
                if(isset($image->url_path))
                {
                    $response->data['url_path']=$image->url_path;
                }
            }
            $this->makeResponse($response);
        }
    }

    public function action_findsubcategoria(){
        $response = new Quickdev_Response();
        $id = $this->request->param('param1');
        if ($id) {
            $subcategoria = ORM::factory('categoria')->where('parent_id','=',$id)->find_all();
            $data=array();
            foreach($subcategoria as $s)
            {
                array_push($data,$s->as_array());
            }
            $response->data = $data;
            $this->makeResponse($response);
        }
    }

    public function action_findubicacion(){
        $response = new Quickdev_Response();
        $id = $this->request->param('param1');
        if ($id) {
            $subcategoria = ORM::factory('ubicacion')->where('parent_id','=',$id)->find_all();
            $data=array();
            foreach($subcategoria as $s)
            {
                array_push($data,$s->as_array());
            }
            $response->data = $data;
            $this->makeResponse($response);
        }
    }


    public function action_buscarciudad(){
        $response = new Quickdev_Response();
        $id = $this->request->param('param1');
        if ($id) {
            $codigos=new Model_Codigo($id);
            $cpostal=$codigos->find_all();

            $subcategoria = ORM::factory('ubicacion')->where('parent_id','=',$id)->find_all();
            $data=array();
            $c_postal=array();
            foreach($subcategoria as $s){
                array_push($data,$s->as_array());
            }

            foreach($cpostal as  $p){
                array_push($c_postal,$p->as_array());
            }
            $response->data['ubication'] = $data;
            $response->data['codigo_postal']=$c_postal;
            $this->makeResponse($response);
        }
    }
}