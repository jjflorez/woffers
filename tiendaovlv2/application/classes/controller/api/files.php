<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Files extends Quickdev_Api
{

	public function action_index()
	{

	}

	public function action_get()
	{
		$file = new Model_File();
		$response = new Quickdev_Response();

		//Set Params
		if($this->request->post('name'))
			$file->where('name', '=', $this->request->post('name'));
		if($this->request->post('id'))
			$file->where('id', '=', $this->request->post('id'));
		if($this->request->post('user_id')){
			if($this->request->post('user_id') == '-1'){
				if(Kohana_Auth::instance()->logged_in()){
					$file->where('user_id', '=', Kohana_Auth::instance()->get_user()->id);
				}else{
					$file->where('user_id', '=', -2);
				}
			}else{
				$file->where('user_id', '=', $this->request->post('user_id'));
			}
		}else{
			if(Kohana_Auth::instance()->logged_in()){
				$file->where('user_id', '=', Kohana_Auth::instance()->get_user()->id);
			}else{
				Kohana_Cookie::$salt = 'quickdev';
				$heads = Kohana_Cookie::get(Quickdev_Session::GUEST_HEADS);
				if($heads)
				{
					$heads = explode(',', $heads);
					$file->or_where_open();
					foreach($heads as $head)
						$file->or_where('id', '=', $head);
					$file->or_where_close();
				}else{
					$file->where('user_id', '=', -2);
				}
			}
		}



		if($this->request->post('search'))
		{
			$file->and_where_open();
			$file->or_where('name', 'LIKE', '%' . $this->request->post('search') . '%');
			$file->or_where('description', 'LIKE', '%' . $this->request->post('search') . '%');
			//$file->or_where('url_path', 'LIKE', '%' . $this->request->post('search') . '%');
			$file->and_where_close();
		}
		//End of params

		$file->order_by('id', 'desc');
		$res_files = $file->find_all();

		if(Kohana::$environment == Kohana::DEVELOPMENT)
			$response->status->debug_string = $file->last_query();

		foreach($res_files as $row)
		{
			$r_row = $row->as_array();
			$r_row['filetype'] = $row->filetype;
			array_push($response->data, $row->as_array());
		}

		$this->makeResponse($response);
	}

	public function action_save()
	{
		$m_file = new Model_File();
		$response = new Quickdev_Response();

        $file_check = true;
        if(isset($_FILES['file_upload'])){
		    $file = Validation::factory( $_FILES );
		    $file->rule( 'file_upload', array( 'Upload', 'valid' ) );
            $file_check = $file->check();
        }else{
			if(!isset($_POST['file_upload'])){
				$file_check = false;
			}
		}

		if($file_check)
		{
			//--------------- Upload file
			$current_path = 'user_files/' . date('Y/m') . '/';
			if(!file_exists($current_path)){
                mkdir($current_path, 0777, true);
            }

            if(isset($_FILES['file_upload'])){
				$ext = explode('.', $file['file_upload']['name']);
				$ext = array_reverse($ext);
			    $nombre_archivo = uniqid() . '_' . URL::title($file['file_upload']['name']) . '.' . $ext[0];
			    $res_file = Upload::save( $file['file_upload'], $nombre_archivo, $current_path);
            }else{
				$nombre_archivo = uniqid() . '.png';
				$fp = fopen($current_path . $nombre_archivo, 'wb' );
				fwrite( $fp, base64_decode($this->request->post('file_upload')) );
				fclose( $fp );
				$res_file = true;
			}

			if ( $res_file === false )
			{
				$response->status->setStatus('ERROR');
			}else{
				$_POST['url_path'] = $current_path . $nombre_archivo;
				$_POST['created'] = date('Y-m-d g:i:s');

                if(isset($_POST['user_id'])){
			    	if($_POST['user_id'] == '-1' || $_POST['user_id'] == '' || $_POST['user_id'] == '0')
                    {
                        if(Kohana_Auth::instance()->logged_in())
                            $_POST['user_id'] = Kohana_Auth::instance()->get_user()->id;
                        else
                            $_POST['user_id'] = -1;
                    }
                }
				$val_reg = new Validation($_POST);
				$val_reg->rule('name', 'not_empty');

				$response = $this->insertUpdate($val_reg, 'file', $response);
				if(!Kohana_Auth::instance()->logged_in())
				{
					Kohana_Cookie::$salt = 'quickdev';
					$heads = Kohana_Cookie::get(Quickdev_Session::GUEST_HEADS);

					if(!$heads)
					{
						if($heads == '')
						{
							$heads = $response->data['id'];
						}else{
							$heads = $heads . ',' . $response->data['id'];
						}
					}else{
						$heads = $heads . ',' . $response->data['id'];
					}

					//array_push($heads, $response->data['id']);

					Kohana_Cookie::set(Quickdev_Session::GUEST_HEADS, $heads);

					/*if(Session::instance()->get(Quickdev_Session::GUEST_HEADS))
						$heads = Session::instance()->get(Quickdev_Session::GUEST_HEADS);

					array_push($heads, $response->data['id']);
					Session::instance()->set(Quickdev_Session::GUEST_HEADS, $heads);*/
				}
			}
		}else{
			$response->status->setStatus('PARAMS');
			if(isset($file))
				$response->status->details = $file->errors(null, FALSE);
			else
				$response->status->details = 'file_upload NULL';
		}

		$this->makeResponse($response);
	}
} // End Roles
