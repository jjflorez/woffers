<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Mails extends Quickdev_Api
{

	public function action_index()
	{

	}

	public function action_send()
	{
		$response = new Quickdev_Response();

		if(isset($_POST['send']) && isset($_POST['to']) && $this->request->post('from'))
		{
			$from = $this->request->post('from');
			$message = $this->request->post('message');

			$template = View::factory('mail/tpl_sharevideo')
				->set('from', $from)
				->set('url', $_POST['video_url'])
				->set('preview', $_POST['video_preview'])
				->set('message', $message);

			if($_POST['send'] == 'on')
			{
				if(isset($_POST['date']))
				{
					$fecha = explode('/', $_POST['date']);
					if(count($fecha) > 3)	$_POST['date_send'] = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0] . ' 00:00:00';
				}

				if(isset($_POST['to']))
				{
					$mails = explode(',', $_POST['to']);
					$valid_mails = '';
					foreach($mails as $mail)
					{
						if(Valid::email($mail)) $valid_mails .= $mail . ',';
					}
				}

				$date = explode('/', $_POST['date']);

				$_POST['subject'] = 'Han compartido un video contigo';
				$_POST['body'] = $template;
				$_POST['to'] = $valid_mails;
				$_POST['status'] = 0;
				$_POST['date_send'] = $date[2] . '-' . $date[1] . '-' . $date[0] . ' 12:00:00';

				$val_reg = new Validation($_POST);
				$val_reg->rule('from', 'not_empty');
				$val_reg->rule('to', 'not_empty');
				$val_reg->rule('date_send', 'not_empty');

				$response = $this->insertUpdate($val_reg, 'mail', $response);
			}else{
				//Enviar mail!!!!
				$error = array();
				if($error)
				{
					$response->data = $error;
					$response->status->setStatus('ERROR');
				}


				if(isset($_POST['to']))
				{
					$mails = explode(',', $_POST['to']);
					foreach($mails as $mail)
					{
						$mail = trim($mail);
						if(Valid::email($mail)){
							$email = Email::factory('Han compartido un video contigo');
							$email->message($template, 'text/html');
							$email->to($mail);
							$email->from('mailing@qd.pe', $_POST['from']);
							$email->send();
						}
					}
				}
			}
		}else{
			$response->status->setStatus('PARAMS');
		}

		$this->makeResponse($response);
	}

	public function action_send_programed()
	{
		$response = new Quickdev_Response();
		$m_mails = new Model_Mail();

		$m_mails->where('status', '=', 0);
		$m_mails->where('date_send', '>', '2012-'.date('m').'-'.date('d').' 00:00:00');
		$m_mails->where('date_send', '<', '2012-'.date('m').'-'.date('d').' 23:59:59');

		$mails = $m_mails->find_all();


		$response->data = '';
		$response->data->count_mails = $mails->count();
		foreach($mails as $m)
		{
			$s_mail = explode(',', $m->to);

			foreach($s_mail as $m1)
			{
				if(Valid::email($m1)){
					$email = Email::factory('Han compartido un video contigo');
					$email->message($m->body, 'text/html');
					$email->from('mailing@qd.pe', $m->from_name);
					$email->to($m1);
					$email->send();
				}
			}

			$m->values(array('status'=>1));
			$m->update();
		}

		$this->makeResponse($response);
	}
} // End Roles
