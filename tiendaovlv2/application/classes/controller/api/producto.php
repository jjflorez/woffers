<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Producto extends Quickdev_Api
{
    public function action_index()
    {
    }

    public function action_paises(){
        $response = new Quickdev_Response();
        $id = $this->request->param('param1');
        if ($id) {
            $products=new Model_Producto();
            $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_pais','=',$id)->limit(12)->order_by('created','desc');

            if(isset($post['categoria_id'])&&$post['categoria_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_categoria_id','=',$post['categoria_id'])->limit(12)->order_by('created','desc');
            }

            if(isset($post['subcategoria_id'])&&$post['subcategoria_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_subcategoria_id','=',$post['subcategoria_id'])->limit(12)->order_by('created','desc');
            }
            $products=$products->find_all();
            if(isset($products)&&count($products)==0){
                $response->data['mensaje']='No hay ofertas para este pais,intenta con otra búsqueda.';
            }
            $ubications = ORM::factory('ubicacion')->where('parent_id','=',$id)->find_all();
            $data=array();
            $product=array();

            foreach($ubications as $s){
                array_push($data,$s->as_array());
            }
            foreach($products as $key=>$p){
                if(isset($p->oferta_imagen_id))
                {
                    $imagen=new Model_File($p->oferta_imagen_id);
                    $url=$imagen->url_path;
                }
                array_push($product,$p->as_array());
                $product[$key]['url_imagen']=$url;
            }
            $response->data['provincias'] = $data;
            $response->data['productos'] = $product;
            $this->makeResponse($response);
        }
    }

    public function action_provincias(){
        $response = new Quickdev_Response();
        $post=$this->request->post();
        $id = $this->request->param('param1');
        if ($id) {
            $products=new Model_Producto();
            if($id=='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_pais','=',$post['pais_id'])->limit(12)->order_by('created','desc');
            }
            else{
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_provincia','=',$id)->limit(12)->order_by('created','desc');
            }

            if(isset($post['categoria_id'])&&$post['categoria_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_categoria_id','=',$post['categoria_id'])->limit(12)->order_by('created','desc');
            }

            if(isset($post['subcategoria_id'])&&$post['subcategoria_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_subcategoria_id','=',$post['subcategoria_id'])->limit(12)->order_by('created','desc');
            }

            $products=$products->find_all();
            if(isset($products)&&count($products)==0){
                $response->data['mensaje']='No hay ofertas para esta provincia,intenta con otra búsqueda.';
            }
            $ubications = ORM::factory('ubicacion')->where('parent_id','=',$id)->find_all();
            $data=array();
            $product=array();
            foreach($ubications as $s){
                array_push($data,$s->as_array());
            }
            foreach($products as $key=>$p){
                if(isset($p->oferta_imagen_id))
                {
                    $imagen=new Model_File($p->oferta_imagen_id);
                    $url=$imagen->url_path;
                }
                array_push($product,$p->as_array());
                $product[$key]['url_imagen']=$url;
            }
            $response->data['provincias'] = $data;
            $response->data['productos'] = $product;
            $this->makeResponse($response);
        }
    }

    public function action_ciudades(){
        $response = new Quickdev_Response();
        $post=$this->request->post();
        $id = $this->request->param('param1');

        if ($id) {
            $products=new Model_Producto();
            if($id=='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_provincia','=',$post['provincia_id'])->limit(12)->order_by('created','desc');
            }
            else{
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_ciudad','=',$id)->limit(12)->order_by('created','desc');
            }

            if(isset($post['categoria_id'])&&$post['categoria_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_categoria_id','=',$post['categoria_id'])->limit(12)->order_by('created','desc');
            }

            if(isset($post['subcategoria_id'])&&$post['subcategoria_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_subcategoria_id','=',$post['subcategoria_id'])->limit(12)->order_by('created','desc');
            }

            $products=$products->find_all();
            if(isset($products)&&count($products)==0){
                $response->data['mensaje']='No hay ofertas para esa ciudades,intenta con otra búsqueda.';
            }
            $ubications = ORM::factory('ubicacion')->where('parent_id','=',$id)->find_all();
            $data=array();
            $product=array();
            foreach($ubications as $s){
                array_push($data,$s->as_array());
            }
            foreach($products as $key=>$p){


                if(isset($p->oferta_imagen_id))
                {
                    $imagen=new Model_File($p->oferta_imagen_id);
                    $url=$imagen->url_path;
                }
                array_push($product,$p->as_array());
                $product[$key]['url_imagen']=$url;
            }
            $response->data['provincias'] = $data;
            $response->data['productos'] = $product;

            $this->makeResponse($response);
        }
    }

    public function action_categorias(){
        $response = new Quickdev_Response();
        $post=$this->request->post();
        //return var_dump($post);
        $id = $this->request->param('param1');
        if ($id) {
            $products=new Model_Producto();

            if($id=='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->limit(12)->order_by('created','desc');
            }
            else{
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_categoria_id','=',$id)->limit(12)->order_by('created','desc');
            }

            $subcategoria = ORM::factory('categoria')->where('parent_id','=',$id)->find_all();
            $subcategorias=array();
            foreach($subcategoria as $s)
            {
                array_push($subcategorias,$s->as_array());
            }

            if(isset($post['pais_id'])&&$post['pais_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_pais','=',$post['pais_id']);
            }
            if(isset($post['provincia_id'])&&$post['provincia_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_provincia','=',$post['provincia_id']);
            }

            if(isset($post['ciudad_id'])&&$post['ciudad_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_ciudad','=',$post['ciudad_id']);
            }
            $products=$products->find_all();
            if(isset($products)&&count($products)==0){
                $response->data['mensaje']='No hay ofertas para esa categoria,intenta con otra búsqueda.';
            }

            //$products=ORM::factory('producto')->where('oferta_categoria_id','=',$id)->order_by('created','desc')->find_all();
            $ubications = ORM::factory('ubicacion')->where('parent_id','=',$id)->find_all();
            $data=array();
            $product=array();
            foreach($ubications as $s){
                array_push($data,$s->as_array());
            }
            foreach($products as $key=>$p){
                if(isset($p->oferta_imagen_id))
                {
                    $imagen=new Model_File($p->oferta_imagen_id);
                    $url=$imagen->url_path;
                }
                array_push($product,$p->as_array());
                $product[$key]['url_imagen']=$url;
            }
            $response->data['provincias'] = $data;
            $response->data['productos'] = $product;
            $response->data['subcategorias']=$subcategorias;
            $this->makeResponse($response);
        }
    }

    public function action_subcategorias(){
        $response = new Quickdev_Response();
        $post=$this->request->post();

        $id = $this->request->param('param1');
        if ($id) {
            $products=new Model_Producto();

            if($id=='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->limit(12)->order_by('created','desc');
            }
            else{
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_subcategoria_id','=',$id)->limit(12)->order_by('created','desc');
            }
            $subcategoria = ORM::factory('categoria')->where('parent_id','=',$id)->find_all();
            $subcategorias=array();
            foreach($subcategoria as $s)
            {
                array_push($subcategorias,$s->as_array());
            }

            if(isset($post['categoria_id'])&&$post['pais_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_categoria_id','=',$post['categoria_id']);
            }

            if(isset($post['pais_id'])&&$post['pais_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_pais','=',$post['pais_id']);
            }
            if(isset($post['provincia_id'])&&$post['provincia_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_provincia','=',$post['provincia_id']);
            }

            if(isset($post['ciudad_id'])&&$post['ciudad_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_ciudad','=',$post['ciudad_id']);
            }
            $products=$products->find_all();
            if(isset($distrito_ids)&&count($products)==0){
                $response->data['mensaje']='No hay ofertas para esa subcategoria,intenta con otra búsqueda.';
            }

            //$products=ORM::factory('producto')->where('oferta_categoria_id','=',$id)->order_by('created','desc')->find_all();
            $ubications = ORM::factory('ubicacion')->where('parent_id','=',$id)->find_all();
            $data=array();
            $product=array();
            foreach($ubications as $s){
                array_push($data,$s->as_array());
            }
            foreach($products as $key=>$p){
                if(isset($p->oferta_imagen_id))
                {
                    $imagen=new Model_File($p->oferta_imagen_id);
                    $url=$imagen->url_path;
                }
                array_push($product,$p->as_array());
                $product[$key]['url_imagen']=$url;
            }
            $response->data['provincias'] = $data;
            $response->data['productos'] = $product;
            $response->data['subcategorias']=$subcategorias;
            $this->makeResponse($response);
        }
    }

    public function action_filtrobusqueda(){
        $response = new Quickdev_Response();
        $post=$this->request->post();

        $id = $this->request->param('param1');
            $products=new Model_Producto();
            if(isset($post['pais_id'])&&$post['pais_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_pais','=',$post['pais_id'])->limit(12)->order_by('created','desc');;
            }
            if(isset($post['provincia_id'])&&$post['provincia_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_provincia','=',$post['provincia_id'])->limit(12)->order_by('created','desc');;
            }

            if(isset($post['ciudad_id'])&&$post['ciudad_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_ciudad','=',$post['ciudad_id'])->limit(12)->order_by('created','desc');
            }

            if(isset($post['categoria_id'])&&$post['ciudad_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_categoria_id','=',$post['categoria_id'])->limit(12)->order_by('created','desc');;
            }

            if(isset($post['subcategoria_id'])&&$post['ciudad_id']!='todos'){
                $products=$products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_subcategoria_id','=',$post['subcategoria_id'])->limit(12)->order_by('created','desc');;
            }


            if(isset($post['texto'])&&$post['texto']!=''){

                $codigos=ORM::factory('codigo')->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('codigo','like','%'.$post['texto'].'%')->find_all();
                $distritos=ORM::factory('ubicacion')->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('nombre','like','%'.$post['texto'].'%')->find_all();

                $distrito_ids=array();
                if(isset($distritos)){
                    foreach($distritos as $d){
                        array_push($distrito_ids,$d->id);
                    }
                }
                //id codigos
                $id=array();
                foreach($codigos as $c){
                    array_push($id,$c->ubication_id);
                }
                if(count($id)!=0) {
                    $products = $products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_ciudad', 'IN', $id)->limit(12)->order_by('created', 'desc');
                }
                if(count($distrito_ids)!=0) {
                    $products = $products->where('oferta_duracion', '>=', date('Y-m-d h:i:s'))->where('oferta_publicada','=','1')->where('oferta_ubicacion_distrito', 'IN', $distrito_ids)->limit(12)->order_by('created', 'desc');
                }
            }
            $products=$products->find_all();
            if(isset($distrito_ids)&&count($distrito_ids)==0&&count($id)==0){
                $response->data['mensaje']='No hay ofertas para esa búsqueda,intenta con otra búsqueda.';
                //$products=null;
            }
            $response->data['busqueda']='Búsqueda:'.$post['texto'];
            $count_product=0;
            $product=array();
            foreach($products as $key=>$p){
                if(isset($p->oferta_imagen_id))
                {
                    $imagen=new Model_File($p->oferta_imagen_id);
                    $url=$imagen->url_path;
                }
                array_push($product,$p->as_array());
                $product[$count_product]['url_imagen']=$url;
                $count_product++;
            }
            $response->data['productos'] = $product;
            $this->makeResponse($response);

    }

    public function action_publicar(){
        $response = new Quickdev_Response();
        $post=$this->request->post();
        $flag=false;

        if(isset($post['list_id'])){
            $list=explode(',',$post['list_id']);

            if(count($list)!=0){
                foreach($list as $val){

                    $oferta=new Model_Producto($val);
                    if($oferta->oferta_estado==0&&$flag==false)
                    {
                        $response->data['mensaje']='Las ofertas seleccionadas primero deben ser aprobadas para ser publicadas';
                    }
                    if($oferta->oferta_estado==1)
                    {
                        $flag=true;
                        $oferta->oferta_publicada=1;
                        $oferta->save();
                        $response->data['mensaje']='Las ofertas fueron publicadas con éxito';
                    }
                }
            }
        }
        $this->makeResponse($response);
    }

    public function action_despublicar(){
        $response = new Quickdev_Response();
        $post=$this->request->post();

        if(isset($post['list_id'])){
            $list=explode(',',$post['list_id']);

            if(count($list)!=0){
                foreach($list as $val){
                    $oferta=new Model_Producto($val);
                    $oferta->oferta_publicada=0;
                    $oferta->save();
                }
            }
        }
        $this->makeResponse($response);
    }

    public function action_aprobar(){
        $response = new Quickdev_Response();
        $post=$this->request->post();

        if(isset($post['list_id'])){
            $list=explode(',',$post['list_id']);

            if(count($list)!=0){
                foreach($list as $val){

                    $oferta=new Model_Producto($val);
                    $oferta->oferta_estado=1;
                    $oferta->save();
                }
            }
        }
        $this->makeResponse($response);
    }

    public function action_desaprobar(){
        $response = new Quickdev_Response();
        $post=$this->request->post();

        if(isset($post['list_id'])){
            $list=explode(',',$post['list_id']);

            if(count($list)!=0){
                foreach($list as $val){

                    $oferta=new Model_Producto($val);
                    $oferta->oferta_estado=0;
                    $oferta->save();
                }
            }
        }
        $this->makeResponse($response);
    }

    public function action_borrar(){
        $response = new Quickdev_Response();
        $post=$this->request->post();

        if(isset($post['list_id'])){
            $list=explode(',',$post['list_id']);

            if(count($list)!=0){
                foreach($list as $val){

                    $oferta=new Model_Producto($val);
                    $oferta->delete();
                }
            }
        }
        $this->makeResponse($response);
    }

    public  function action_estado(){
        $response = new Quickdev_Response();
        $post = $this->request->post();

        if(isset($post['estado']))
        {
            $products=new Model_Producto();
            $products=$products->where('oferta_estado','=',$post['estado'])->limit(12)->order_by('created','desc');
        }
        if(isset($products)&&count($products)==0){
            $response->data['mensaje']='No se encontraron ofertas';
        }
        $products=$products->find_all();
        $count_product=0;
        $prod=array();
        foreach($products as $k=>$p){
            //array_push($prod,$p->as_array());
            $prod[$k]->id=$p->id;

            $prod[$k]->oferta_titulo=$p->oferta_titulo;
            $prod[$k]->num_referencia=$p->num_referencia;
            $prod[$k]->oferta_publicada=$p->oferta_publicada;
            $prod[$k]->total_vendido=$p->total_vendido;
            //$prod[$k]->updated=$p->updated;

            if(isset($p->updated)){
                $newDateString = date_format(date_create_from_format('Y-m-d H:i:s',$p->updated), 'd-m-Y H:i:s');
                if($newDateString!=false)
                {
                    $prod[$k]->updated=$newDateString;
                }
            }

            if(isset($p->oferta_publicada)){
                if($p->oferta_publicada==1){
                    $prod[$k]->oferta_publicada=URL::site('public/img/check_circle.png');
                }

                if($p->oferta_publicada==0){
                    $prod[$k]->oferta_publicada=URL::site('public/img/cross_circle.png');
                }
            }

            if(isset($p->comercio_id)){
                $comercio=new Model_Comercio($p->comercio_id);
                $prod[$k]->comercio_id=$comercio->name;
            }

            if(isset($p->oferta_publicacion)){
                $newDateString = date_format(date_create_from_format('Y-m-d H:i:s', $p->oferta_publicacion), 'd-m-Y');
                if($newDateString!=false)
                {
                    $prod[$k]->oferta_publicacion=$newDateString;
                }
            }

            if(isset($p->oferta_duracion)){
                $newDateString = date_format(date_create_from_format('Y-m-d H:i:s', $p->oferta_duracion), 'd-m-Y');
                if($newDateString!=false)
                {
                    $prod[$k]->oferta_duracion=$newDateString;
                }
            }

            if(isset($p->oferta_estado)){
                if($p->oferta_estado=='0'){
                    $prod[$k]->oferta_estado='Pendiente de confirmar';
                }
                if($p->oferta_estado=='1'){
                    $prod[$k]->oferta_estado='Confirmarda';
                }
            }

            $count_product++;
        }



        $response->data['productos'] = $prod;
        $this->makeResponse($response);
    }
}