<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Scaffolding extends Quickdev_Api
{

	public function action_index()
	{
		$file = new Model_File();
		$response = new Quickdev_Response();

		$res_files = $file->find_all();

		foreach($res_files as $row)
		{
			$r_row = $row->as_array();
			$r_row['filetype'] = $row->filetype;
			array_push($response->data, $row->as_array());
		}

		$this->makeResponse($response);
	}

	public function action_tables()
	{
		$response = new Quickdev_Response();

		$data_base = Kohana_Database_MySQL::instance();


		$this->template->title = __('title.scaffolding');
		$this->template->t_content = View::factory('scaffolding/view_home')
			->set('tablas', $data_base->list_tables());
	}

	public function action_save($__type = 'imagen')
	{
		$m_file = new Model_File();
		$response = new Quickdev_Response();

		$file = Validation::factory( $_FILES );
		$file->rule( 'file_upload', array( 'Upload', 'not_empty' ) );
		$file->rule( 'file_upload', array( 'Upload', 'valid' ) );

		if($file->check())
		{
			//--------------- Upload file
			$current_path = 'user_files/' . date('Y/m') . '/';
			$nombre_archivo = uniqid() . '_' . $file['file_upload']['name'];
			if(!file_exists($current_path))		{			mkdir($current_path, 777, true);		}
			$res_file = Upload::save( $file['file_upload'], $nombre_archivo, $current_path);

			if ( $res_file === false )
			{
				$response->status->setStatus('ERROR');
			}else{
				$response->data = $file['file_upload'];

				//-------------- Guardar registro
				$m_file->filetype_id = $this->request->post('filetype_id');
				$m_file->user_id = $this->request->post('user_id');
				$m_file->name = $this->request->post('name');
				$m_file->description = $this->request->post('description');
				$m_file->url_path = $current_path . $nombre_archivo;
				$m_file->status = $this->request->post('status');

				$res_regfile = $m_file->save();
			}
		}

		//Verificar registro
		if($res_file)
		{
			$response->data = $res_file;
		}else{
			$response->status->setStatus('ERROR');
		}

		if($this->request->is_ajax()){
			header("Content-Type: application/json");
			echo json_encode($response);
		}else{
			Message::add('info', 'Registro guardado correctamente');
			$this->request->redirect($this->request->referrer());
		}

	}
} // End Roles
