<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Compras extends Quickdev_Api
{
    public function action_index()
    {
    }

	public function action_get()
	{
        $response = new Quickdev_Response();
        $username = $this->request->param('param1');

        $_user = ORM::factory('user')->where('username', '=', $username)->find();

        if($_user->loaded()){
            $user_id = $_user->id;
        }

		$filtro = $this->request->post('filtro');
		$tipo = $this->request->post('tipo');

        if(isset($user_id)){
            $m_carrito = new Model_Carrito();
			$m_detalleproducto = new Model_Detalleproducto();

            $carritos = $m_carrito->where('user_id', '=', $user_id)->where('status', '=', 1)->find_all();

			$m_detalleproducto->or_where_open();
			foreach($carritos as $_car){
				$m_detalleproducto->or_where('carrito_id', '=', $_car->id);
			}
			$m_detalleproducto->or_where_close();

			$detallespro = array();
			if($carritos->count() > 0)
			{
				$m_detalleproducto->order_by('id', 'DESC');
				switch($filtro)
				{
                    case 'disponible':
						$m_detalleproducto->where('is_gift', '!=', 1);
                            break;
					case 'utilizado':
						$m_detalleproducto->where('status', '=', '2');
						break;
					case 'regalado':
						$m_detalleproducto->where('is_gift', '=', 1);
						break;
				}

				$detallespro = $m_detalleproducto->find_all();
			}

			//echo $m_detalleproducto->last_query();



			foreach($detallespro as $_pro)
			{
				$row = $_pro->as_array();

				$row['carrito'] = $_pro->carrito->as_array();
				$row['carrito']['created'] = date('d/m/Y', strtotime($_pro->carrito->created));
				$row['producto'] = $_pro->producto->as_array();
				$row['producto']['oferta_imagen'] = $_pro->producto->oferta_imagen->as_array();
				$row['producto']['oferta_imagen']['url_path'] = URL::base('http') . $_pro->producto->oferta_imagen->url_path;

				//$row['producto']['comercio_imagen'] = $_pro->producto->comercio_imagen->as_array();
				//$row['producto']['comercio_imagen']['url_path'] = URL::base('http') . $_pro->producto->comercio_imagen->url_path;

				$comercio = ORM::factory('comercio')->where('id', '=', $_pro->producto->comercio_id )->find();
				$logo = ORM::factory('file')->where('id', '=', $comercio->logo_id )->find();

				$row['producto']['comercio_imagen'] = $logo->as_array();
				$row['producto']['comercio_imagen']['url_path'] = URL::base('http') . $logo->url_path;

				$row['producto']['tiempo_validez'] = date('d/m/Y', strtotime($_pro->producto->tiempo_validez));
				if(strtotime($_pro->producto->tiempo_validez) < time())
					$row['estado'] = 'Caducado';
				else
					$row['estado'] = 'Vigente';

				if($this->request->post('tipo'))
				{
					if($_pro->producto->oferta_tipo == $this->request->post('tipo'))
					{
						if($filtro == 'caducado')
						{
							if(strtotime($_pro->producto->tiempo_validez) < time())
								array_push($response->data, $row);
						}else{
							array_push($response->data, $row);
						}
					}
				}else{
					array_push($response->data, $row);
				}
			}
            //echo "<pre>"; print_r($response->data); die();
        }else{
            $response->status->setStatus('PARAMS');
        }

        $this->makeResponse($response);


    }

    //http://localhost/tiendaovlv2/api/compras/devolver/7
    public function action_devolver()
    {
        $response = new Quickdev_Response();

        $_carrito_id = $this->request->param('param1');
        $_detalleproducto = $this->request->post('detalleproducto'); // 51;
        $_estado = $this->request->post('estado'); // 2;

        if($_carrito_id == "" || $_detalleproducto == "" || $_estado == ""){
            $response->status->setStatus('PARAMS');
        }else{
            $detalle_pro = ORM::factory('detalleproducto', $_detalleproducto);

            if ($detalle_pro->loaded()){
                $detalle_pro->status = $_estado;

                $detalle_pro->save();

                $response->data = $detalle_pro->as_array();
            }
        }
        $this->makeResponse($response);
    }

    //http://localhost/tiendaovlv2/api/compras/oferta/51
    public function action_oferta()
    {
        $response = new Quickdev_Response();

        $_detalleproducto = $this->request->param('param1');
    	try{
    		$carrito_id = $this->request->param('param2');
    	}catch(Exception $ex){
    		$carrito_id = -1;
    	}

    	$user=ORM::factory('user')->where("id","=",$carrito->user_id)->find();

        if(!$_detalleproducto){
            $response->status->setStatus('PARAMS');
        }else{
        	$dproducto = ORM::factory('detalleproducto')->where('id', '=', $_detalleproducto )->find();
        	if($carrito_id!=-1){
    			$carrito=ORM::factory('carrito')->where("id","=",$carrito_id)->find();
        	}else{
        		$carrito=ORM::factory('carrito')->where("id","=",$dproducto->carrito_id)->find();
        	}
        	$producto = ORM::factory('producto')->where("id",'=', $dproducto->producto_id)->find();
        	//echo json_encode($producto);

            if($producto->loaded()){
				$row = $dproducto->as_array();
				$row['codigo_referencia']=$dproducto->codigo_referencia;
            	$row['codigo_cupon']=$dproducto->codigo_cupon;
            	$row['codigo_validacion']=$dproducto->codigo_validacion;
            	$row['added']=$dproducto->added;
            	$row['is_gift']=$dproducto->is_gift;
            	$row['regalo_nombre']=$dproducto->regalo_nombre;

				$row['carrito'] = $carrito->as_array();
            	unset($user->password);
				$row['carrito']['user'] = $user->as_array();

            	if(isset($carrito->pago)==true && $carrito->pago!=NULL){
            		$row['carrito']['pago']=date("d/m/Y",strtotime($carrito->pago));
            	}else{
            		$row['carrito']['pago']="";
            	}

				$row['producto'] = $producto->as_array();

            	$row['monto']=$dproducto->monto;

            	$row['producto']=$producto->as_array();
				$row['producto']['oferta_duracion'] = date("d/m/Y",strtotime($producto->oferta_duracion));
            	$row['producto']['oferta_publicacion'] = date("d/m/Y",strtotime($producto->oferta_publicacion));
            	$row['producto']['tiempo_validez'] = date("d/m/Y",strtotime($producto->tiempo_validez));

            	$imagen=ORM::factory('file')->where("id","=",$producto->oferta_imagen_id)->find();
				$row['producto']['oferta_imagen'] = $imagen->as_array();

            	$image_path=URL::base('http') . $imagen->url_path;
            	if($image_path!=URL::base("http")){
            		$row['producto']['oferta_imagen']['url_path'] = URL::base('http'). $imagen->url_path;
            	}else{
            		$row['producto']['oferta_imagen']['url_path']='';
            	}


            	$comercio = ORM::factory('comercio')->where('id', '=', $producto->comercio_id )->find();
            	$logo = ORM::factory('file')->where('id', '=', $comercio->logo_id )->find();

				$row['producto']['comercio_imagen'] = $logo->as_array();
            	$image_path=trim(URL::base('http') . $logo->url_path);
            	if($image_path!=URL::base("http")){
					$row['producto']['comercio_imagen']['url_path'] = URL::base('http') . $logo->url_path;
            	}else{
            		$row['producto']['comercio_imagen']['url_path'] = '';
            	}

            	if(strtotime($dproducto->producto->tiempo_validez) < time())
            		$row['producto']['estado'] = 'Caducado';
            	else
            		$row['producto']['estado'] = 'Vigente';


                $response->data = $row;
                //echo "<pre>";
                //print_r($_pro);
            	//echo json_encode($row);
            }
        }
        $this->makeResponse($response);
    }
}