<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Test extends Controller_Template
{
	public $template = 'ci/view_template';

	public function action_index()
	{
		$this->template->content = View::factory('ci/view_test_api');
	}
} // End Roles
