<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cupones extends Controller_Template
{
	public $template = 'ci/view_admin';

    public function before()
    {
        if(!Auth::instance()->logged_in('admin'))
            $this->request->redirect('user/login');

        return parent::before();
    }

    public function action_index(){
		$user_id = Auth::instance()->get_user()->id;
    	$usuario = ORM::factory('user')->where("id","=",$user_id)->find();
    	$carritos = ORM::factory('carrito')->where("user_id","=",$user_id)->find_All();
    	$data=array();
    	$count=1;
    	foreach($carritos->as_array() as $i=>$val){
    		$detalles= ORM::factory('detalleproducto')->where("carrito_id","=",$val->id)->find_All();
    		foreach($detalles->as_array() as $d=>$w){
    			$data[$count]=array();
    			$data[$count]['id']=$w->id;
    			foreach($val->as_array() as $k=>$d){
    				if($k!='id'){
    					$data[$count][$k]=$d;
    				}
    			}
    			$data[$count]['numero']=$count;
    			$producto = ORM::factory('producto')->where("id","=",$detalle->producto_id)->find();
    			foreach($producto->as_array() as $k=>$d){
    				if($k!='id'){
    					$data[$count][$k]=$d;
    				}
    			}
    			foreach($w->as_array() as $k=>$d){
    				$data[$count][$k]=$d;
    				//echo $k." : ".$d."<br>";
    			}
    			if($w->is_gift=='1'){
    				$data[$count]['nombre']=$w->regalo_nombre;
    			}else{
    				$data[$count]['nombre']=$usuario->name;
    			}
    			$count++;
    		}
    	}
    	//echo json_encode($data);
        $this->template->content = View::factory('admin/view_admin_cupones')
        								->set('carritos', $data);
    }
	public function action_validar(){
		$codigo_validacion=$_POST['codigo_validacion'];
		$m_d = new Model_Detalleproducto($_POST['id']);
		if($codigo_validacion==$m_d->codigo_validacion){
			$m_d->status=10;//validacion con estado 10
		}
		$m_d->save();
		$this->request->redirect('/admin/cupones/');
	}
	public function action_permanentes(){

        $this->response = View::factory('admin/view_admin_cupones');
    }
}

