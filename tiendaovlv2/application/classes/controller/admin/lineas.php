<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Lineas extends Controller_Template
{
	public $template = 'ci/view_admin';

	public function before()
	{
		if(!Auth::instance()->logged_in('admin'))
			$this->request->redirect('user/login');

		return parent::before();
	}

	public function action_index()
	{
		echo $this->request->param('param1');
		$this->template->content = View::factory('admin/view_admin_home');
	}

	public function action_productos()
	{
		$post=$this->request->post();
		$comercios = ORM::factory('comercio')->find_All();

		$m_productos = new Model_Producto();
		if(isset($_GET['estado'])&&$_GET['estado']!=''&&$_GET['estado']!=' '){
			$m_productos=$m_productos->where('oferta_estado','=',$_GET['estado']);
		}

		if(isset($_GET['publicada'])&&$_GET['publicada']!=''&&$_GET['publicada']!=' '){

			$m_productos=$m_productos->where('oferta_publicada','=',$_GET['publicada']);
		}
		$m_tiendas = new Model_Tienda();
		$m_linea = new Model_Linea();
		$code_linea='';
		$comercio_id='';
		try{
			$code_linea = $this->request->param('param1');
		}catch(Exception $ex){
			$code_linea='';
		}
		try{
			$comercio_id = $this->request->param('param2');
		}catch(Exception $ex){
			$comercio_id='';
		}
		if($code_linea!=''){
			if($code_linea!='all'){
				$linea = $m_linea->where('code', '=', $code_linea)->find();
				$tiendas = $m_tiendas->where('linea_id', '=', $linea->id)->find_all();
			}else{
				$tiendas = $m_tiendas->find_all();
			}
		}else{
			$tiendas = $m_tiendas->find_all();
		}

        //$pro_lineas = ORM::factory('producto')->where('status','=', 1);
        $pro_lineas = ORM::factory('producto');

        if(count($tiendas)>0){
        	if($comercio_id!=''){
        		if($comercio_id!='all'){
	        		$pro_lineas->and_where_open();
	        		$pro_lineas->and_where('comercio_id','=',$comercio_id);
	        		$pro_lineas->and_where_close();
        		}
        	}
            $pro_lineas->and_where_open();

        	foreach($tiendas as $t){
        		$pro_lineas->or_where('tienda_id','=', $t->id);
        	}
            $pro_lineas->and_where_close();
        	//echo "hola  mundo<br>";
        }

        //$total = $m_productos->count_all();
        $total = $pro_lineas->find_all()->count();

		/* Start listing --------------*/
		$pagination = new Pagination(array('total_items' => $total,'items_per_page' => 10,'view' => 'pagination/useradmin'));
		$sort = isset($_GET['sort']) ? $_GET['sort'] : 'id';
		$dir = isset($_GET['dir']) ? 'DESC' : 'ASC';
		$m_productos->limit($pagination->items_per_page);
		$m_productos->offset($pagination->offset);
		$m_productos->order_by($sort, $dir);
		/* End listing --------------*/

		//$m_productos->where('status','=', 1);
		if(count($tiendas)>0){
			//echo "hola  mundo<br>";
			if($comercio_id!=''){
				if($comercio_id!='all'){
					$m_productos->and_where_open();
					$m_productos->and_where('comercio_id','=',$comercio_id);
					$m_productos->and_where_close();
				}
			}
		}
		$results = $m_productos->find_all();


		$prod=$results->as_array();
		foreach($prod as $k=>$p)
		{
			$p=$p->as_array();

			if(isset($p['comercio_id'])){
				$comercio=new Model_Comercio($p['comercio_id']);
				$prod[$k]->comercio_id=$comercio->name;
			}

			if(isset($p['oferta_publicacion'])){
				$newDateString = date_format(date_create_from_format('Y-m-d H:i:s', $p['oferta_publicacion']), 'd-m-Y');
				if($newDateString!=false){
					$prod[$k]->oferta_publicacion=$newDateString;
				}
			}

			if(isset($p['oferta_duracion'])){
				$newDateString = date_format(date_create_from_format('Y-m-d H:i:s', $p['oferta_duracion']), 'd-m-Y');
				if($newDateString!=false){
					$prod[$k]->oferta_duracion=$newDateString;
				}
			}

			if(isset($p['updated'])){
				$newDateString = date_format(date_create_from_format('Y-m-d H:i:s',$p['updated']), 'd-m-Y H:i:s');
				if($newDateString!=false){
					$prod[$k]->updated=$newDateString;
				}
			}

			if(isset($p['oferta_estado'])){
				if($p['oferta_estado']=='0'){
					$prod[$k]->oferta_estado='Pendiente de confirmar';
				}
				if($p['oferta_estado']=='1'){
					$prod[$k]->oferta_estado='Confirmada';
				}
			}
		}

		$this->template->content = View::factory('admin/view_admin_productos')
			->set('productos', $prod)
			->set('paging', $pagination)
			->set('comercios', $comercios)
			->set('linea', $code_linea)
			->set('comercio', $comercio_id);
	}

	public function action_tiendas()
	{
		$m_tiendas = new Model_Tienda();
		$m_linea = new Model_Linea();

		$code_linea = $this->request->param('param1');
		$linea = $m_linea->where('code', '=', $code_linea)->find();

		/* Start listing --------------*/
		$pagination = new Pagination(array('total_items' => $m_tiendas->count_all(),'items_per_page' => 10,'view' => 'pagination/useradmin'));;
		$sort = isset($_GET['sort']) ? $_GET['sort'] : 'id'; // set default sorting direction here
		$dir = isset($_GET['dir']) ? 'DESC' : 'ASC';
		$results = $m_tiendas->limit($pagination->items_per_page)->offset($pagination->offset)->order_by($sort, $dir)
			->where('linea_id', '=', $linea->id)->find_all();
		/* End listing --------------*/

		$this->template->content = View::factory('admin/view_admin_tiendas')
			->set('tiendas', $results)
			->set('paging', $pagination)
			->set('linea', $this->request->param('param1'));
	}

	public function action_cupones()
	{

		$post=$this->request->post();
		$m_productos = new Model_Detalleproducto();

		if(isset($_GET['ofertas'])&&$_GET['ofertas']!=''&&$_GET['ofertas']!=' '){
			$m_productos=$m_productos->where('producto_id','=',$_GET['ofertas']);

		}

		if(isset($_GET['estado'])&&$_GET['estado']!=''&&$_GET['estado']!=' '){
			switch($_GET['estado']){
				case 1:
					$hoy=date("Y-m-d H:i:s");//la fecha de hoy
					$m_productos=$m_productos->where('added','>',$hoy)->where('fecha_consumo','=',null)->where('fecha_cancelacion','=',null);
					break;
				case 2:
					$hoy=date("Y-m-d H:i:s");//la fecha de hoy
					$m_productos=$m_productos->where('fecha_consumo','!=',null)->where('fecha_cancelacion','=',null);
					break;
				case 3:
					$hoy=date("Y-m-d H:i:s");//la fecha de hoy
					$m_productos=$m_productos->where('added','<',$hoy)->where('fecha_consumo','=',null)->where('fecha_cancelacion','=',null);
					break;
				case 4:
					$hoy=date("Y-m-d H:i:s");//la fecha de hoy
					$m_productos=$m_productos->where('fecha_consumo','=',null)->where('fecha_cancelacion','!=',null);
					break;
			}
		}

		$ofertas= new Model_Producto();
		$results = $m_productos->find_all();
		/* Start listing --------------*/
		$total = $m_productos->find_all()->count();
		$pagination = new Pagination(array('total_items' => $total,'items_per_page' => 10,'view' => 'pagination/useradmin'));
		$sort = isset($_GET['sort']) ? $_GET['sort'] : 'id';
		$dir = isset($_GET['dir']) ? 'DESC' : 'ASC';
		/*$m_productos->limit($pagination->items_per_page);
		$m_productos->offset($pagination->offset);
		$m_productos->order_by($sort, $dir);*/
		/* End listing --------------*/

		$prod=$results->as_array();
		$cupones=array();
		foreach($results as $k=>$p)
		{
			$p=$p->as_array();
			$producto=new Model_Producto($p['producto_id']);

			if(isset($producto->id)){
				$cupones[$k]['id']=$p['id'];
				$cupones[$k]['codigo_cupon']=$p['codigo_cupon'];
				if(isset($p['fecha_cancelacion'])){
					$newDateString = date_format(date_create_from_format('Y-m-d H:i:s', $p['fecha_cancelacion']), 'd/m/Y (H:i)');
					if($newDateString!=false){
						$cupones[$k]['fecha_cancelacion']=$newDateString;
					}
				}

				if(isset($p['added'])){
					$newDateString = date_format(date_create_from_format('Y-m-d H:i:s', $p['added']), 'd/m/Y (H:i)');
					if($newDateString!=false){
						$cupones[$k]['fecha_compra']=$newDateString;
					}
				}

				if(isset($p['fecha_consumo'])){
					$newDateString = date_format(date_create_from_format('Y-m-d H:i:s', $p['fecha_consumo']), 'd/m/Y (H:i)');
					if($newDateString!=false){
						$cupones[$k]['fecha_consumo']=$newDateString;
					}
				}

				$cupones[$k]['codigo_validacion']=$p['codigo_validacion'];
				$cupones[$k]['status']=$p['status'];

				if(isset($p['producto_id'])){
					$producto=new Model_Producto($p['producto_id']);
					$cupones[$k]['oferta']=$producto->oferta_titulo;

					if(isset($producto->tiempo_validez)){
						$newDateString = date_format(date_create_from_format('Y-m-d H:i:s',$producto->tiempo_validez), 'd/m/Y');
						if($newDateString!=false){
							$cupones[$k]['tiempo_validez']=$newDateString;
						}
						if(isset($p['added'])){
							$hoy=strtotime('now');//la fecha de hoy
							$fecha_validez = strtotime(date_format(date_create_from_format('Y-m-d H:i:s', $producto->tiempo_validez), 'd-m-Y'));
							if($fecha_validez >= $hoy &&($p['fecha_consumo']==null)&&($p['fecha_cancelacion']==null)){
								$cupones[$k]['estado']=1;
							}
							if($fecha_validez >= $hoy &&($p['fecha_consumo']!=null)&&($p['fecha_cancelacion']==null)){
								$cupones[$k]['estado']=2;
							}

							if($fecha_validez < $hoy &&($p['fecha_consumo']==null)&&($p['fecha_cancelacion']==null)){
								$cupones[$k]['estado']=3;
							}

							if(($p['fecha_cancelacion']!=null)){
								$cupones[$k]['estado']=4;
							}
						}
					}
				}

				if(isset($p['carrito_id'])){
					$carrito=new Model_Carrito($p['carrito_id']);
					if(isset($carrito)){
						$user=new Model_User($carrito->user_id);
						$cupones[$k]['name']=$user->name.' '.$user->last_name;
						$cupones[$k]['user']=$user->username;
					}
				}
			}
		}
		$ofertas=$ofertas->find_all();
		//return var_dump($ofertas);

		$this->template->content = View::factory('admin/view_admin_cupones')
			->set('cupones', $cupones)
			->set('get_oferta',$_GET['ofertas'])
			->set('ofertas',$ofertas)
			->set('paging', $pagination);
	}

} // End Home Admin
