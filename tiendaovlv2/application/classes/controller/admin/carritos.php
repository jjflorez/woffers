<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Carritos extends Controller_Template
{
	public $template = 'ci/view_template';

	public function before()
	{
		if(!Auth::instance()->logged_in('admin'))
			$this->request->redirect('login/');

		return parent::before();
	}

	public function action_compras()
	{
		$comercios = ORM::factory('comercio')->find_All()->as_array();

		$comercio_id='';
		try{
			$comercio_id = $this->request->param('param1');
		}catch(Exception $ex){
			$comercio_id='';
		}

		$status='all';
		$estado='all';

		try{
			$status = $this->request->param('param2');
			$estado=$status;
			if($status=='1'){
				$status='0';
			}
			if($status=='2'){
				$status='1';
			}
		}catch(Exception $ex){
			$status='all';
		}

		$page='1';
		try{
			$page = $this->request->param('param3');
		}catch(Exception $ex){
			$page='1';
		}
		//echo $page."<br>";

		$url = URL::query();
		$url=str_replace("?","",$url);
		$url=str_replace("=","",$url);
		//echo $url."<br>";

		//echo $_request['page']."<br>";

		if(isset($comercio_id)==true){
			$productos = ORM::factory('producto')->where('comercio_id', '=', $comercio_id )->where('comercio_id', '<>', 'NULL' )->find_all();
		}else{
			$productos = ORM::factory('producto')->where('comercio_id', '<>', 'NULL' )->find_all();
		}

		$detalles = ORM::factory('detalleproducto');
		if(count($productos)>0){
			$detalles->and_where_open();
			foreach($productos as $item){
				$detalles->or_where('producto_id','=',$item->id);
			}
			$detalles->and_where_close();
		}


		$data=$detalles->find_all();


		if($status!='all'){
			if($status==0){
				$carritos = ORM::factory('carrito')->where("pago",'IS',NULL);
			}else{
				$carritos = ORM::factory('carrito')->where("pago",'<>','');
			}
		}else{
			$carritos = ORM::factory('carrito');
		}

		//$carritos = ORM::factory('carrito');

		$carritos->and_where_open();
		foreach($data as $item){
			$carritos->or_where("id",'=',$item->carrito_id);
		}
		$carritos->and_where_close();

        $total = $carritos->find_all()->count();

		$pagination = new Pagination(array('total_items' => $total,'items_per_page' => 50,'view' => 'pagination/useradmin','current_page'=>$page));
		$sort = isset($_GET['sort']) ? $_GET['sort'] : 'id';
		$dir = isset($_GET['dir']) ? 'DESC' : 'ASC';
		$carritos->limit($pagination->items_per_page);
		$carritos->offset($pagination->offset);
		$carritos->order_by($sort, $dir);

		$results=$carritos->find_all()->as_array();
		$extra=array();
		$eval=true;
		foreach($results as $i=>$item){
			/*if($status==0){
				$eval=strlen($item->pago)==0;
			}elseif($status==1){
				$eval=strlen($item->pago)>0;
			}else{
				$eval=true;
			}*/
			if($eval){
				$direccion=ORM::factory('direccion')->where('id','=',$item->direccion_id)->find();
				$extra[$i]['direccion']=($direccion->direccion==NULL)?'Sin direccion':$direccion->direccion;
				$usuario=ORM::factory('user')->where('id','=',$item->user_id)->find();
				$extra[$i]['usuario']=($usuario->username==NULL)?"":$usuario->username;
			}else{
				unset($results[$i]);
			}

			//$extra[$i]['status']=($item->pago==1)?"Comprado":"No comprado";
		}

		$estados=array(
				array(
					"id"=>"1",
					"name"=>"no comprado"
				),array(
					"id"=>"2",
					"name"=>"comprado"
				)
			);

		$this->template->content = View::factory('admin/view_admin_carritos')
			->set('compras', $results)
			->set('extra', $extra)
			->set('paging', $pagination)
			->set('comercios', $comercios)
			->set('estados', $estados)
			->set('status', $estado)
			->set('page', $page)
			->set('comercio', $comercio_id);

	}

} // End Home Admin
