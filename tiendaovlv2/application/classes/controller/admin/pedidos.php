<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Pedidos extends Controller_Template
{
	public $template = 'ci/view_admin';

	public function before()
	{
		if(!Auth::instance()->logged_in('admin'))
			$this->request->redirect('login/');

		return parent::before();
	}

	public function action_index(){
		//$m_detalleproducto = new Model_Pedido();

		//$cupones = $m_detalleproducto->find_all()->as_array();

		$this->template->content = View::factory('admin/view_admin_pedidos');
	}
}

