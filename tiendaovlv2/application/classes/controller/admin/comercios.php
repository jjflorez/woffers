<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Comercios extends Controller_Template
{
	public $template = 'ci/view_admin';

	public function before()
	{
		if(!Auth::instance()->logged_in('admin'))
			$this->request->redirect('login/');

		return parent::before();
	}

	public function action_index()
	{
		//$m_comercio = new Model_Comercio();
		$comercios=array();

		$comercios = ORM::factory('comercio')->find_all();

		$logos=array();

		foreach($comercios as $i=>$var){
			$logo = ORM::factory('file')->where('id', '=', $var->logo_id )->find();
			$logos[$var->id]['url_path']=$logo->url_path;
		}

		$this->template->content = View::factory('admin/view_admin_comercios')
									->set('logos', $logos)
									->set('comercios', $comercios);
	}

	public function action_agregar()
	{
		$m_comercio = new Model_Comercio();

		$comercio = $m_comercio->where('id', '=', $this->request->param('param1'))->find();

		$usuarios = ORM::factory('user')->find_all();

		$this->template->content = View::factory('admin/view_registro_comercio')
			->set('usuarios', $usuarios)
			->set('comercio', $comercio);
	}

	public function action_editar()
	{
		$id = $this->request->param('param1');
		if($id){
			$comercio = new Model_Comercio($id);
			$logo = ORM::factory('file')->where('id', '=', $comercio->logo_id )->find();
			$usuarios = ORM::factory('user')->find_all();

			$this->template->content = View::factory('admin/view_editar_comercio')
										->set('usuarios', $usuarios)
			                            ->set('comercio', $comercio)
			                            ->set('logo', $logo);

		}else{
			$this->request->redirect($this->request->referrer());
		}
	}

	public function action_eliminar()
	{
		$id = $this->request->param('param1');

		if($id)
		{
			$producto = new Model_Comercio($id);

			if($producto->loaded())
			{
				$producto->delete();
				Message::add('info', 'Comercio eliminado correctamente');
				$this->request->redirect($this->request->referrer());
			}else{
				Message::add('error', 'No se pudo eliminar el comercio');
				$this->request->redirect($this->request->referrer());
			}
		}else{
			$this->request->redirect($this->request->referrer());
		}
	}

	public function action_savecomercio()
	{
		$validation = new Validation($_POST);

		$validation->rule('name', 'not_empty');
		$validation->rule('user_id', 'not_empty');
		$validation->rule('logo_id', 'not_empty');
		$validation->rule('status', 'not_empty');

		if($validation->check())
		{
			$m_comercio = new Model_Comercio($_POST['comercio_id']);
			$m_comercio->values($validation->data());
			$m_comercio->save();
			Message::add('info', 'Tienda guardada correctamente');
			$this->request->redirect('/admin/comercios/');
		}else{
			Message::add('error', 'No se pudo guardar');
		}
		$this->request->redirect($this->request->referrer());
	}




} // End Home Admin
