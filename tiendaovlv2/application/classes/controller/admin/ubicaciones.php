<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Ubicaciones extends Controller_Template
{
    public $template = 'ci/view_admin';

    public function action_index()
    {
        //$m_comercio = new Model_Comercio();
        $categorias = array();

        $paises= ORM::factory('ubicacion')->where('parent_id', '=', '-1')->find_all();
                $this->template->content = View::factory('admin/view_admin_ubicaciones')
                 ->set('paises', $paises);
    }

    public function action_provincias()
    {
        $ubicacion_id = $this->request->param('param1');
        if($ubicacion_id) {
            $provincias = ORM::factory('ubicacion')->where('parent_id','=',$ubicacion_id)->find_all();
            $this->template->content = View::factory('admin/view_admin_provincias')
                ->set('provincias', $provincias)
                ->set('ubicacion_id',$ubicacion_id);
        }
    }

    public function action_ciudades()
    {
        $ubicacion_id = $this->request->param('param1');
        if($ubicacion_id) {
            $ubicacion= new Model_Ubicacion($ubicacion_id);
            $provincias = ORM::factory('ubicacion')->where('parent_id','=',$ubicacion_id)->find_all();

            $this->template->content = View::factory('admin/view_admin_ciudades')
                ->set('provincias', $provincias)
                ->set('ubicacion_id',$ubicacion_id)
                ->set('provincia_id',$ubicacion->parent_id);
        }
    }

    public function action_distritos()
    {
        $ubicacion_id = $this->request->param('param1');
        if($ubicacion_id) {
            $ubicacion= new Model_Ubicacion($ubicacion_id);
            $provincia = new Model_Ubicacion($ubicacion->parent_id);
            $provincias = ORM::factory('ubicacion')->where('parent_id','=',$ubicacion_id)->find_all();

            $this->template->content = View::factory('admin/view_admin_distrito')
                ->set('ubicacion_id',$ubicacion_id)
                ->set('provincias', $provincias)
                ->set('provincia_id',$provincia->parent_id)
                ->set('ciudad_id',$ubicacion->parent_id);
        }
    }

    public function action_barrios()
    {
        $ubicacion_id = $this->request->param('param1');
        if($ubicacion_id) {
            $ubicacion= new Model_Ubicacion($ubicacion_id);
            $ciudad = new Model_Ubicacion($ubicacion->parent_id);
            $provincia = new Model_Ubicacion($ciudad->parent_id);
            $provincias = ORM::factory('ubicacion')->where('parent_id','=',$ubicacion_id)->find_all();

            $this->template->content = View::factory('admin/view_admin_barrios')
                ->set('provincias', $provincias)
                ->set('provincia_id',$provincia->parent_id)
                ->set('ciudad_id',$ciudad->parent_id)
                ->set('distrito_id',$ubicacion->parent_id)
                ->set('ubicacion_id',$ubicacion);
        }
    }

    public function action_codigos()
    {
        $ubicacion_id = $this->request->param('param1');
        if($ubicacion_id) {
            $codigos = ORM::factory('codigo')->where('ubication_id','=',$ubicacion_id)->find_all();
            $ubicacion= new Model_Ubicacion($ubicacion_id);
            $provincia = new Model_Ubicacion($ubicacion->parent_id);
            $provincias = ORM::factory('ubicacion')->where('parent_id','=',$ubicacion_id)->find_all();

            $this->template->content = View::factory('admin/view_admin_codigos')
                ->set('codigos',$codigos)
                ->set('ubicacion_id',$ubicacion_id)
                ->set('provincias', $provincias)
                ->set('provincia_id',$provincia->parent_id)
                ->set('ciudad_id',$ubicacion->parent_id);
        }
    }

    public function action_agregarpais()
    {
        $this->template->content = View::factory('admin/view_registro_paises');
    }

    public function action_agregarprovincias()
    {
        $ubicacion_id = $this->request->param('param1');

        $this->template->content = View::factory('admin/view_registro_provincia')

            ->set('ubicacion_id',$ubicacion_id);
    }

    public function action_agregarciudades()
    {
        $ubicacion_id = $this->request->param('param1');
        $ubicacion= new Model_Ubicacion($ubicacion_id);
        $this->template->content = View::factory('admin/view_registro_ciudades')
            ->set('provincia_id',$ubicacion->parent_id)
            ->set('ubicacion_id',$ubicacion_id);
    }

    public function action_agregardistrito()
    {
        $ubicacion_id = $this->request->param('param1');
        $ubicacion= new Model_Ubicacion($ubicacion_id);
        $provincia= new Model_Ubicacion($ubicacion->parent_id);
        $this->template->content = View::factory('admin/view_registro_distrito')
            ->set('provincia_id',$provincia->parent_id)
            ->set('ciudad_id',$ubicacion->parent_id)
            ->set('distrito_id',$ubicacion_id);
    }

    public function action_agregarbarrio()
    {
        $ubicacion_id = $this->request->param('param1');
        $ubicacion= new Model_Ubicacion($ubicacion_id);
        $distrito= new Model_Ubicacion($ubicacion->parent_id);
        $ciudad= new Model_Ubicacion($distrito->parent_id);
        $provincia= new Model_Ubicacion($ciudad->parent_id);
        $this->template->content = View::factory('admin/view_registro_barrios')
            ->set('provincia_id',$provincia->id)
            ->set('ciudad_id',$ciudad->id)
            ->set('distrito_id',$ubicacion->parent_id)
            ->set('barrio_id',$ubicacion_id)
            ->set('ubicacion_id',$ubicacion_id);
    }

    public function action_editarpais()
    {
        $pais_id = $this->request->param('param1');
        if($pais_id) {
            $ubicacion=new Model_Ubicacion($pais_id);
            $this->template->content = View::factory('admin/view_editar_paises')
                ->set('ubicacion',$ubicacion);
        }
    }

    public function action_agregarcodigo()
    {
        $ubicacion_id = $this->request->param('param1');
        $ubicacion= new Model_Ubicacion($ubicacion_id);
        $provincia= new Model_Ubicacion($ubicacion->parent_id);
        $this->template->content = View::factory('admin/view_registro_codigos')
            ->set('provincia_id',$provincia->parent_id)
            ->set('ciudad_id',$ubicacion->parent_id)
            ->set('distrito_id',$ubicacion_id);
    }

    public function action_editarprovincia()
    {
        $pais_id = $this->request->param('param1');
        if($pais_id) {
            $ubicacion=new Model_Ubicacion($pais_id);
            $this->template->content = View::factory('admin/view_editar_provincias')
                ->set('ubicacion',$ubicacion);
        }
    }

    public function action_editarciudad()
    {
        $pais_id = $this->request->param('param1');
        if($pais_id) {
            $ubicacion=new Model_Ubicacion($pais_id);
            if(isset($ubicacion)){
                $provincia=new Model_Ubicacion($ubicacion->parent_id);
            }
            $this->template->content = View::factory('admin/view_editar_ciudades')
                ->set('provincia_id',$provincia->parent_id)
                ->set('ubicacion',$ubicacion);

        }
    }

    public function action_editardistrito()
    {
        $pais_id = $this->request->param('param1');
        if($pais_id) {
            $ubicacion=new Model_Ubicacion($pais_id);
            if(isset($ubicacion)){
                $ciudad=new Model_Ubicacion($ubicacion->parent_id);
            }
            if(isset($ubicacion)){
                $provincia=new Model_Ubicacion($ciudad->parent_id);
            }
            $this->template->content = View::factory('admin/view_editar_distrito')
                ->set('provincia_id',$provincia->parent_id)
                ->set('ciudad_id',$ciudad->parent_id)
                ->set('ubicacion',$ubicacion);
        }
    }

    public function action_editarbarrio()
    {
        $pais_id = $this->request->param('param1');
        if($pais_id) {
            $ubicacion=new Model_Ubicacion($pais_id);
            if(isset($ubicacion)){
                $distrito=new Model_Ubicacion($ubicacion->parent_id);
            }
            if(isset($distrito)){
                $ciudad=new Model_Ubicacion($distrito->parent_id);
            }
            if(isset($ciudad)){
                $provincia=new Model_Ubicacion($ciudad->parent_id);
            }
            $this->template->content = View::factory('admin/view_editar_barrio')
                ->set('provincia_id',$provincia->parent_id)
                ->set('ciudad_id',$ciudad->parent_id)
                ->set('provincia_id',$provincia->parent_id)
                ->set('ubicacion',$ubicacion);
        }
    }

    public function action_editarcodigo()
    {
        $id= $this->request->param('param1');
        if($id) {
            $codigo=new Model_Codigo($id);
            if(isset($codigo)){
                $ciudad=new Model_Ubicacion($codigo->ubication_id);
            }
            if(isset($codigo)){
                $provincia=new Model_Ubicacion($ciudad->parent_id);
            }
            $this->template->content = View::factory('admin/view_editar_codigo')
                ->set('provincia_id',$provincia->parent_id)
                ->set('ciudad_id',$ciudad->parent_id)
                ->set('codigo',$codigo);
        }
    }

    public function action_savepais()
    {
        $validation = new Validation($_POST);
        $validation->rule('name', 'not_empty');

        if($validation->check())
        {
            $ubicacion_id=$validation->data()['ubicacion_id'];
            if(isset($ubicacion_id))
            {
                $ubicacion = new Model_Ubicacion($ubicacion_id);
                $ubicacion->nombre=$validation->data()['name'];
                $ubicacion->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/ubicaciones/');
            }
            else{
                $ubicacion = new Model_Ubicacion();
                $ubicacion->nombre=$validation->data()['name'];
                $ubicacion->parent_id='-1';
                $ubicacion->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/ubicaciones/');
            }
        }else{
            Message::add('error', 'No se pudo guardar');
        }
        $this->request->redirect($this->request->referrer());
    }

    public function action_saveprovincia()
    {
        $pais_id = $this->request->param('param1');
        $validation = new Validation($_POST);
        $validation->rule('name', 'not_empty');

        if($validation->check())
        {
            $ubicacion_id=$validation->data()['ubicacion_id'];
            if(isset($ubicacion_id))
            {
                $ubicacion = new Model_Ubicacion($ubicacion_id);
                $ubicacion->nombre=$validation->data()['name'];
                $ubicacion->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/ubicaciones/provincias/'.$ubicacion->parent_id);
            }
            else{

                $ubicacion = new Model_Ubicacion();
                $ubicacion->nombre=$validation->data()['name'];
                $ubicacion->parent_id=$pais_id;
                $ubicacion->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/ubicaciones/provincias/'.$pais_id);
            }
        }else{
            Message::add('error', 'No se pudo guardar');
        }
        $this->request->redirect($this->request->referrer());
    }

    public function action_saveciudad()
    {
        $provincia_id = $this->request->param('param1');
        $validation = new Validation($_POST);
        $validation->rule('name', 'not_empty');

        if($validation->check())
        {
            $ubicacion_id=$validation->data()['ubicacion_id'];
            if(isset($ubicacion_id))
            {
                $ubicacion = new Model_Ubicacion($ubicacion_id);
                $ubicacion->nombre=$validation->data()['name'];
                $ubicacion->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/ubicaciones/ciudades/'.$ubicacion->parent_id);
            }
            else{
                $ubicacion = new Model_Ubicacion();
                $ubicacion->nombre=$validation->data()['name'];
                $ubicacion->parent_id=$provincia_id;
                $ubicacion->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/ubicaciones/ciudades/'.$provincia_id);
            }
        }else{
            Message::add('error', 'No se pudo guardar');
        }
        $this->request->redirect($this->request->referrer());
    }

    public function action_savedistrito()
    {
        $id = $this->request->param('param1');
        $post=$this->request->post();

        if(isset($post['default'])&&$post['default']=='on')
        {
            $validation = new Validation($_POST);
            $post['name']='No hay distrito';
        }
        else{
            $validation = new Validation($_POST);
            $validation->rule('name', 'not_empty');
        }

        if($validation->check())
        {
            $ubicacion_id=$validation->data()['ubicacion_id'];
            if(isset($ubicacion_id))
            {
                $ubicacion = new Model_Ubicacion($ubicacion_id);
                if(isset($post['default'])&&$post['default']=='on'){
                    $ubicacion->default='1';
                }
                $ubicacion->nombre= $post['name'];
                $ubicacion->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/ubicaciones/distritos/'.$ubicacion->parent_id);
            }
            else{
                $ubicacion = new Model_Ubicacion();
                if(isset($post['default'])&&$post['default']=='on')
                {
                    $ubicacion->default='1';
                }
                $ubicacion->nombre= $post['name'];
                $ubicacion->parent_id=$id;
                $ubicacion->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/ubicaciones/distritos/'.$id);
            }
        }else{
            Message::add('error', 'No se pudo guardar');
        }
        $this->request->redirect($this->request->referrer());
    }

    public function action_savebarrio()
    {
        $id = $this->request->param('param1');
        $validation = new Validation($_POST);
        $validation->rule('name', 'not_empty');

        if($validation->check())
        {
            $ubicacion_id=$validation->data()['ubicacion_id'];
            if(isset($ubicacion_id))
            {
                $ubicacion = new Model_Ubicacion($ubicacion_id);
                $ubicacion->nombre=$validation->data()['name'];
                $ubicacion->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/ubicaciones/barrios/'.$ubicacion->parent_id);
            }
            else{
                $ubicacion = new Model_Ubicacion();
                $ubicacion->nombre=$validation->data()['name'];
                $ubicacion->parent_id=$id;
                $ubicacion->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/ubicaciones/barrios/'.$id);
            }
        }else{
            Message::add('error', 'No se pudo guardar');
        }
        $this->request->redirect($this->request->referrer());
    }

    public  function action_savecodigo()
    {
        $id = $this->request->param('param1');
        $validation = new Validation($_POST);
        $validation->rule('codigo', 'not_empty');
        if($validation->check()) {
            $codigo_id=$validation->data()['codigo_id'];
            if(isset($codigo_id))
            {
                $codigo = new Model_Codigo($codigo_id);
                $codigo->codigo=$validation->data()['codigo'];
                $codigo->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/ubicaciones/codigos/'.$codigo->ubication_id);
            }
            else{
                $codigo = new Model_Codigo();
                $codigo->codigo=$validation->data()['codigo'];
                $codigo->ubication_id=$id;
                $codigo->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/ubicaciones/codigos/'.$id);
            }
        }else{
            Message::add('error', 'No se pudo guardar');
        }
        $this->request->redirect($this->request->referrer());
    }

    public function action_eliminar()
    {
        $id = $this->request->param('param1');
        if($id)
        {
            $ubicacion = new Model_Ubicacion($id);
            if($ubicacion->loaded())
            {
                $ubicacion->delete();
                Message::add('info', 'Registro eliminada correctamente');
                $this->request->redirect($this->request->referrer());
            }else{
                Message::add('error', 'No se pudo eliminar el registro');
                $this->request->redirect($this->request->referrer());
            }
        }else{
            $this->request->redirect($this->request->referrer());
        }
    }

    public function action_eliminarcodigo()
    {
        $id = $this->request->param('param1');
        if($id)
        {
            $ubicacion = new Model_Codigo($id);
            if($ubicacion->loaded())
            {
                $ubicacion->delete();
                Message::add('info', 'Registro eliminada correctamente');
                $this->request->redirect($this->request->referrer());
            }else{
                Message::add('error', 'No se pudo eliminar el registro');
                $this->request->redirect($this->request->referrer());
            }
        }else{
            $this->request->redirect($this->request->referrer());
        }
    }


}