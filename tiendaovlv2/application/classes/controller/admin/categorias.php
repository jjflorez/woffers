<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Categorias extends Controller_Template
{
    public $template = 'ci/view_admin';

    public function action_index()
    {
        //$m_comercio = new Model_Comercio();
        $categorias=array();

        $categorias = ORM::factory('categoria')->where('parent_id','=',null)->find_all();


        $logos=array();

        foreach($categorias as $i=>$var){
            //$logo = ORM::factory('file')->where('id', '=', $var->logo_id )->find();
            //$logos[$var->id]['url_path']=$logo->url_path;
        }

        $this->template->content = View::factory('admin/view_admin_categorias')
            ->set('logos', $logos)
            ->set('categorias', $categorias);

    }

    public function action_prueba()
    {
        return var_dump('pruebaaa');
    }

    public function action_agregar()
    {
        $m_categoria = new Model_Categoria();
        //$categoria = $m_categoria->where('id', '=', $this->request->param('param1'))->find();

       // $usuarios = ORM::factory('user')->find_all();
        $this->template->content = View::factory('admin/view_registro_categoria');
            //->set('usuarios', $usuarios)
            //->set('categoria', $categoria);
    }

    public function action_agregarsubcategoria()
    {
        $id = $this->request->param('param1');
        if(isset($id))
        {
            $id = $this->request->param('param1');
            $categoria = new Model_Categoria($id);
            $this->template->content = View::factory('admin/view_registro_subcategoria')
            ->set('categoria', $categoria);
        }
    }

    public function action_savecategoria()
    {
        $validation = new Validation($_POST);
        $validation->rule('name', 'not_empty');

        if($validation->check())
        {
            $m_comercio = new Model_Categoria($_POST['categoria_id']);
            $m_comercio->nombre=$validation->data()['name'];
            $m_comercio->descripcion=$validation->data()['description'];
            $m_comercio->save();
            Message::add('info', 'Datos guardados correctamente');
            $this->request->redirect('/admin/categorias/');
        }else{
            Message::add('error', 'No se pudo guardar');
        }
        $this->request->redirect($this->request->referrer());
    }


    public function action_editar()
    {
        $id = $this->request->param('param1');
        if($id){
            $comercio = new Model_Categoria($id);
            //$logo = ORM::factory('file')->where('id', '=', $comercio->logo_id )->find();
            $this->template->content = View::factory('admin/view_editar_categoria')
                ->set('categoria', $comercio);
                //->set('logo', $logo);

        }else{
            $this->request->redirect($this->request->referrer());
        }
    }

    public function action_eliminar()
    {
        $id = $this->request->param('param1');

        if($id)
        {
            $producto = new Model_Categoria($id);
            if($producto->loaded())
            {
                $producto->delete();
                Message::add('info', 'Categoria eliminada correctamente');
                $this->request->redirect($this->request->referrer());
            }else{
                Message::add('error', 'No se pudo eliminar la categoria');
                $this->request->redirect($this->request->referrer());
            }
        }else{
            $this->request->redirect($this->request->referrer());
        }
    }
}