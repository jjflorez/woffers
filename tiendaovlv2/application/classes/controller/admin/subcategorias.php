<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Subcategorias extends Controller_Template
{
    public $template = 'ci/view_admin';

    public function action_index()
    {
        //$m_comercio = new Model_Comercio();
        $categorias=array();

        $categorias = ORM::factory('categoria')->where('parent_id','!=',null)->find_all();

        $logos=array();

        foreach($categorias as $i=>$var){
            //$logo = ORM::factory('file')->where('id', '=', $var->logo_id )->find();
            //$logos[$var->id]['url_path']=$logo->url_path;
        }

        $this->template->content = View::factory('admin/view_admin_subcategorias')
            ->set('logos', $logos)
            ->set('categorias', $categorias);
    }

    public function action_editar()
    {
        $id = $this->request->param('param1');
        if($id){
            $comercio = new Model_Categoria($id);
            //$logo = ORM::factory('file')->where('id', '=', $comercio->logo_id )->find();
            $this->template->content = View::factory('admin/view_editar_subcategoria')
                ->set('categoria', $comercio);
            //->set('logo', $logo);

        }else{
            $this->request->redirect($this->request->referrer());
        }
    }

    public function action_categoria()
    {
        $id = $this->request->param('param1');
        if(isset($id))
        {
            $categorias = ORM::factory('categoria')->where('parent_id','=',$id)->find_all();

            $logos=array();
            $this->template->content = View::factory('admin/view_admin_subcategorias')
                ->set('logos', $logos)
                ->set('categorias', $categorias)
                ->set('id_categoria', $id);
        }
    }

    public function action_save()
    {
        $id = $this->request->param('param1');
        $validation = new Validation($_POST);
        $validation->rule('name', 'not_empty');

        if(isset($id))
        {
            if($validation->check())
            {
                $m_categoria = new Model_Categoria($id);
                $m_categoria->nombre=$validation->data()['name'];
                $m_categoria->descripcion=$validation->data()['description'];
                $m_categoria->save();
                Message::add('info', 'Datos guardados correctamente');
                $this->request->redirect('/admin/subcategorias/categoria/'.$m_categoria->parent_id);
            }else{
                Message::add('error', 'No se pudo guardar');
            }
            $this->request->redirect($this->request->referrer());
        }
        else{
            if(isset($validation->data()['categoria_id']))
            {
                if($validation->check())
                {
                    $m_categoria = new Model_Categoria();
                    $m_categoria->parent_id=$validation->data()['categoria_id'];
                    $m_categoria->nombre=$validation->data()['name'];
                    $m_categoria->descripcion=$validation->data()['description'];
                    $m_categoria->save();
                    Message::add('info', 'Datos guardados correctamente');
                    $this->request->redirect('/admin/subcategorias/categoria/'.$validation->data()['categoria_id']);
                }else{
                    Message::add('error', 'No se pudo guardar');
                }
                $this->request->redirect($this->request->referrer());
            }
        }
    }
}