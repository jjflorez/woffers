
qd.URLs = new Object();
qd.URLs.API_ROLES = 'api/roles/get';
qd.URLs.API_ROLES_RULES = 'api/roles/rules';

qd.URLs.API_USERS_GET = 'api/users/get';
qd.URLs.API_USERS_SAVE = 'api/users/save';
qd.URLs.API_USERS_LIST_VIDEOS = 'api/users/list_videos';
qd.URLs.API_USERS_CREATE_VIDEO = 'api/users/create_video';
qd.URLs.API_USERS_LOGUED = 'api/users/logued';

qd.URLs.API_FILES_GET = 'api/files/get';
qd.URLs.API_FILES_SAVE = 'api/files/save';

qd.URLs.HOME_PAGE = '/';
qd.URLs.API_SEND_MAIL = 'api/mails/send';